item_changehero_herotable = {
				"npc_dota_hero_axe",
				"npc_dota_hero_drow_ranger",
				"npc_dota_hero_ogre_magi",
				"npc_dota_hero_dark_seer",
				"npc_dota_hero_earthshaker",
				"npc_dota_hero_juggernaut",
				"npc_dota_hero_omniknight",
				"npc_dota_hero_bounty_hunter",
				"npc_dota_hero_pudge",
				"npc_dota_hero_warlock",
				"npc_dota_hero_phantom_lancer",
				"npc_dota_hero_undying",
				"npc_dota_hero_phoenix"

}

function GameMode:OnPlayerChat(keys)

	if not IsServer() then return end
  local teamonly = keys.teamonly
  local userID = keys.userid
  local playerID = self.vUserIds[userID]:GetPlayerID()

  local text = keys.text
  
  if string.find(text,"-puppets on",0,8) then  
    if GameRules:State_Get() == DOTA_GAMERULES_STATE_PRE_GAME and playerID == 0 then	
		self.AllowedHeroes=4
			self:AutoFillPuppets()
		--Say(PlayerResource:GetPlayer(playerID),"#bots_enabled_message_generic",false)
		Say(PlayerResource:GetPlayer(playerID),"Multiple Heroes Enabled",false)
		self:ExtendTurnSpeed(3)
	end  
  elseif string.find(text,"-puppets set",0,8) then  
    if GameRules:State_Get() <= DOTA_GAMERULES_STATE_PRE_GAME and playerID == 0 then	
		local numvalue = string.gsub(text,"-puppets set ","")
		if numvalue and tonumber(numvalue) then
			self.AllowedHeroes=math.min(4,tonumber(numvalue))
			--Say(PlayerResource:GetPlayer(playerID),string.format("#bots_enabled_message",self.AllowedHeroes),false)
			Say(PlayerResource:GetPlayer(playerID),string.format("Multiple Hero Set To %d",self.AllowedHeroes),false)
			
			if self.TurnSpeed == 1 then
				if (tonumber(numvalue)==2) then
					self:ExtendTurnSpeed(2)
				elseif (tonumber(numvalue)==3) or (tonumber(numvalue)==4) then			
					self:ExtendTurnSpeed(3)
				end
			end
		end
	end  
  elseif string.find(text,"-speed",0,6) then 
	
	if GameRules:State_Get() <= DOTA_GAMERULES_STATE_PRE_GAME and playerID == 0 then	--is host
		local numvalue = string.gsub(text,"-speed ","")
		if numvalue and tonumber(numvalue) then
			self:ExtendTurnSpeed(tonumber(numvalue))
			--Say(PlayerResource:GetPlayer(playerID),string.format("#speed_set_message",self.TurnSpeed),false)
			Say(PlayerResource:GetPlayer(playerID),string.format("Turn speed extended to x%d",self.TurnSpeed),false)
		end
	end 
  elseif string.find(text,"-turbo",0,6) then 
	
	if GameRules:State_Get() <= DOTA_GAMERULES_STATE_PRE_GAME and playerID == 0 then	--is host
			
			local multiplier = 1/2
			if self.GameSpeed > 1 then
				self.GameSpeed = 1
				multiplier = 1/self.GameSpeed
				Say(PlayerResource:GetPlayer(playerID),"#TURBO off",false)
			else
				self.GameSpeed = 2
				--Say(PlayerResource:GetPlayer(playerID),"#turbo_off_message",false)
				Say(PlayerResource:GetPlayer(playerID),"#TURBO on",false)
			end
			
			--PLAYER_TIME_SIMULTANIOUS = PLAYER_TIME_SIMULTANIOUS * multiplier
			PLAYER_TIME_SHORT = PLAYER_TIME_SHORT * multiplier
			PLAYER_TIME_SUPERSHORT = PLAYER_TIME_SUPERSHORT * multiplier
			PLAYER_TIME_MODIFIER = PLAYER_TIME_MODIFIER * multiplier	

			MOVE_TIME  = MOVE_TIME * multiplier
			MOVE_TIME_SHORT  = MOVE_TIME_SHORT * multiplier
			HERO_BASE_MOVEMENTSPEED  = HERO_BASE_MOVEMENTSPEED / multiplier
			
	end 

  elseif string.find(text,"-reset",0,8) then 
		
				GameRules:ResetToCustomGameSetup();

  elseif string.find(text,"-puppet",0,8) and not string.find(text,"-puppets",0,8) then 
		
		if  GameRules:State_Get() >= DOTA_GAMERULES_STATE_PRE_GAME and GameRules:State_Get() < DOTA_GAMERULES_STATE_GAME_IN_PROGRESS then
		
			local playerheroes = self:GetPlayerHeroes(playerID)
			local teamheroes = self:GetTeamHeroes(PlayerResource:GetTeam(playerID))
			if (#teamheroes) >= self.AllowedHeroes  then return end	--or #playerheroes>=math.ceil(math.sqrt(self.AllowedHeroes))
		
			local heroname = string.gsub(string.gsub(text,"-puppets ",""),"-puppet ","")
			
			local newhero = ""
			local playerTable = PlayerResource:GetPlayer(playerID)
			
			if string.find(heroname,"npc_dota_hero_axe") or string.find(heroname,"axe") or string.find(heroname,"bodyspray") then
				newhero = "npc_dota_hero_axe"
				
			elseif string.find(heroname,"npc_dota_hero_drow_ranger") or string.find(heroname,"drow") or string.find(heroname,"drowranger") then
				newhero = "npc_dota_hero_drow_ranger"
				
			elseif string.find(heroname,"npc_dota_hero_ogre_magi") or string.find(heroname,"ogre") or string.find(heroname,"ogremagi") then
				newhero = "npc_dota_hero_ogre_magi"
				
			elseif string.find(heroname,"npc_dota_hero_dark_seer") or string.find(heroname,"seer") or string.find(heroname,"darkseer") then
				newhero = "npc_dota_hero_dark_seer"
				
			elseif string.find(heroname,"npc_dota_hero_earthshaker") or string.find(heroname,"shaker") or string.find(heroname,"earthshaker") then
				newhero = "npc_dota_hero_earthshaker"
				
			elseif string.find(heroname,"npc_dota_hero_juggernaut") or string.find(heroname,"jugger") or string.find(heroname,"juggernaut") then
				newhero = "npc_dota_hero_juggernaut"
				
			elseif string.find(heroname,"npc_dota_hero_omniknight") or string.find(heroname,"omni") or string.find(heroname,"omniknight") then
				newhero = "npc_dota_hero_omniknight"
				
			elseif string.find(heroname,"npc_dota_hero_bounty_hunter") or string.find(heroname,"gondar") or string.find(heroname,"bountyhunter") then
				newhero = "npc_dota_hero_bounty_hunter"
							
			elseif string.find(heroname,"npc_dota_hero_pudge") or string.find(heroname,"pudge") or string.find(heroname,"butcher") then
				newhero = "npc_dota_hero_pudge"
				
			elseif string.find(heroname,"npc_dota_hero_warlock") or string.find(heroname,"warlock") or string.find(heroname,"cultist") then
				newhero = "npc_dota_hero_warlock"
				
			elseif string.find(heroname,"npc_dota_hero_phantom_lancer") or string.find(heroname,"phantomlancer") or string.find(heroname,"lancer") then
				newhero = "npc_dota_hero_phantom_lancer"
				
			elseif string.find(heroname,"npc_dota_hero_undying") or string.find(heroname,"dirge") or string.find(heroname,"undying") then
				newhero = "npc_dota_hero_undying"
				
			elseif string.find(heroname,"npc_dota_hero_phoenix") or string.find(heroname,"phoenix") or string.find(heroname,"bird") then
				newhero = "npc_dota_hero_phoenix"
				
			end
			
			if newhero~="" then
				if GameMode:HeroExists(newhero) then
					--Say(PlayerResource:GetPlayer(playerID),string.format("#puppet_spawn_message",heroname,playerID),false)
					Say(PlayerResource:GetPlayer(playerID),string.format("Hero "..heroname.." has already spawned"),true)	
				else
					Say(PlayerResource:GetPlayer(playerID),string.format("Puppet hero spawned: "..heroname),false)				
					MakePuppetHero(newhero,playerTable,nil)
				end
			end
		end
	end
end

function GameMode:ExtendTurnSpeed(numvalue)
	
			local oldSpeed = self.TurnSpeed
			self.TurnSpeed=math.max(.75,numvalue)
			TURN_TIME=TURN_TIME/oldSpeed*self.TurnSpeed
end

function GameMode:AutoFillPuppets()
	self:AutoFillPuppetsForTeam(DOTA_TEAM_GOODGUYS)
	self:AutoFillPuppetsForTeam(DOTA_TEAM_BADGUYS)
end

function GameMode:AutoFillPuppetsForTeam(teamnumber)
	
	local teamheroes = self:GetTeamHeroes(teamnumber)
	
	
	print("found "..#teamheroes.." for team "..teamnumber)
	
	if #teamheroes>self.AllowedHeroes then
		print("earse heroes")
	else	
		print("automake heroes")
		local players = GameMode:GetPlayersForTeam(teamnumber)
		print ("found "..#players.." players")
	
		if #players==1 then 
		
			print("making 4 heroes")
			for i=2,self.AllowedHeroes do
				MakePuppetHero(nil, players[1],nil)
			end
		
		elseif #players==2 and self.AllowedHeroes==4 then 
			print("making 1 hero each")
			for index, player in pairs(players) do
				MakePuppetHero(nil, player,nil)
			end	
		end
	end
end

--Add extra heroes
	--cycle through all players
	--add hero
	--only if team has 1 or 2 players
--remove extra heroes
	--while teamheroes greater than total heroes
	--remove one hero from each player

function MakePuppetHero(name, player,point)

			if name == nil then
			
				local hero_index = 1
				while GameMode:HeroExists(item_changehero_herotable[hero_index]) do
					hero_index=hero_index+1
					if hero_index>#item_changehero_herotable then
						return -- this should never happen
					end
				end
				name = item_changehero_herotable[hero_index]
			
			end
	
			local hero =  CreateUnitByName(name, Vector(0,0,0) , true, player:GetAssignedHero(), player, player:GetTeam()) 
			--hero:AddNewModifier(self:GetCaster(),self,"modifier_pudgeling_rot_caster",{})
			--hero:AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_absolute",{speed = (self:GetSpecialValueFor("cast_range"))/PLAYER_TIME_MODIFIER*3/2})
			--hero:FindAbilityByName("dummy_unit"):SetLevel(1)
			hero:SetControllableByPlayer(player:GetPlayerID(), true);
			hero:SetOwner(player);	
			
			hero:AddItem(CreateItem("item_changehero", hero, hero))			
			
			if point==nil then
				FindSpawnPointForHero(hero);
			else
				FindClearSpaceForUnit(hero,point,true)
			end
				
	return hero
end

function FindSpawnPointForHero(hero)
	local spawnname = "info_player_start_badguys"
	if hero:GetTeam() == DOTA_TEAM_GOODGUYS then
		spawnname = "info_player_start_goodguys"
	end
	
	for _,spawn in pairs(Entities:FindAllByClassname(spawnname)) do	
		if #FindUnitsInRadius( hero:GetTeam(), spawn:GetAbsOrigin(), nil, hero:GetHullRadius()/2, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO, 0, 0, false ) == 0 then
				FindClearSpaceForUnit(hero,spawn:GetAbsOrigin(),true)
			return
		end
	end
end