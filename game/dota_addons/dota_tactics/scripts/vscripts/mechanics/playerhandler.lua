

function GameMode:GetPlayerHeroes(playerID)

	local hero_table = {}
	for _,hero in pairs(FindUnitsInRadius( PlayerResource:GetTeam(playerID), Vector(0,0,0), nil, FIND_UNITS_EVERYWHERE, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_DEAD, 0, false )) do	
		if hero ~= nil and hero:GetPlayerOwnerID() == playerID and hero:IsRealHero() then
			table.insert(hero_table,hero)
		end	
	end
	return hero_table
end

function GameMode:GetTeamHeroes(team)

	local hero_table = {}
	for _,hero in pairs(FindUnitsInRadius( team, Vector(0,0,0), nil, FIND_UNITS_EVERYWHERE, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO, 0, 0, false )) do	
		if hero ~= nil and hero:IsRealHero() then
			table.insert(hero_table,hero)
		end	
	end
	return hero_table
end

function GameMode:HeroExists(heroname)
	
	if heroname==nil then return end
	for _,hero in pairs(FindUnitsInRadius( DOTA_TEAM_GOODGUYS, Vector(0,0,0), nil, FIND_UNITS_EVERYWHERE, DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_HERO, 0, 0, false )) do	
		if hero ~= nil and hero:IsRealHero() and not hero:IsIllusion() and hero:GetUnitName() == heroname then
			return true
		end	
	end
	
	return false
end

function GameMode:GetPlayersForTeam(team)
    local tplayers = {};
    for nPlayerID = 0, DOTA_MAX_TEAM_PLAYERS - 1 do
        if PlayerResource:IsValidPlayerID(nPlayerID) and  PlayerResource:GetTeam(nPlayerID) == team then
			
			local player = PlayerResource:GetPlayer(nPlayerID);
			if player ~= nil then
				table.insert(tplayers, player);
			end
        end
    end
    return tplayers;
end
