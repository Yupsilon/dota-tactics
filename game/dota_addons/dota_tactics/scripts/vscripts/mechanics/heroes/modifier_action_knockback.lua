modifier_action_knockback = class({})

--------------------------------------------------------------------------------

function modifier_action_knockback:OnCreated(kv)

	self.kb_start = self:GetParent():GetAbsOrigin()
	self.kb_direction = Vector(kv.direction_x,kv.direction_y,0)
	self.kb_speed = kv.speed or 600
	self.kb_throw=kv.direction_z or 1000
	self.distance_traveled = 0
	self.kb_height=0

	if IsServer() then	
	
		self.kb_speed = math.max(self.kb_speed,self.kb_direction:Length2D()/MOVE_TIME_SHORT*3)
		self:GetParent():FaceTowards(self.kb_start-self.kb_direction)	
		local nFXIndex = ParticleManager:CreateParticle( "particles/items_fx/force_staff.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		self:AddParticle( nFXIndex, false, false, -1, false, false )		
	
		self:StartIntervalThink( 1/30 )
		self:OnIntervalThink()
	end
end

--------------------------------------------------------------------------------

function modifier_action_knockback:OnIntervalThink()
	if IsServer() then

		local vectorDelta=self.kb_direction
		vectorDelta.z=0

		if self.distance_traveled < vectorDelta:Length2D() then
			self:GetParent():SetAbsOrigin(self:GetParent():GetAbsOrigin() + vectorDelta:Normalized() * self.kb_speed / 10)
			self.distance_traveled = self.distance_traveled + (vectorDelta:Normalized() * self.kb_speed / 10):Length2D()
			self.kb_height=math.sin(self.distance_traveled/vectorDelta:Length2D()*math.pi)*self.kb_throw
		else
			self.kb_height=0
			--self:CheckBrush()
			FindClearSpaceForUnit(self:GetParent(), Tileify(self:GetParent():GetAbsOrigin()), false)
			self:Destroy()
		end
	end
end
--------------------------------------------------------------------------------

function modifier_action_knockback:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_action_knockback:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_OVERRIDE_ANIMATION,
		MODIFIER_PROPERTY_VISUAL_Z_DELTA
	}
end

--------------------------------------------------------------------------------

function modifier_action_knockback:CheckBrush()
	if not IsPointOverBrush(self:GetParent():GetAbsOrigin()) then
		self:GetParent():RemoveModifierByName("modifier_brush_invisibility")
	end
end

--------------------------------------------------------------------------------

function modifier_action_knockback:GetVisualZDelta( params )
	return self.kb_height
end

--------------------------------------------------------------------------------

function modifier_action_knockback:GetOverrideAnimation( params )
	return ACT_DOTA_FLAIL
end

--------------------------------------------------------------------------------
	