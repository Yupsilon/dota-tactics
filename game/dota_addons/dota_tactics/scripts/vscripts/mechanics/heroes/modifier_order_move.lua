LinkLuaModifier( "generic_modifier_movespeed_absolute","abilities/generic/generic_modifier_movespeed_absolute.lua", LUA_MODIFIER_MOTION_NONE )
modifier_order_move = class({})

--------------------------------------------------------------------------------

function modifier_order_move:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_order_move:GetTexture()
	return "kunkka_x_marks_the_spot"
end

--------------------------------------------------------------------------------

function modifier_order_move:OnCreated( kv )
	
	if IsServer() then 

		self.orderStartPosition = self:GetParent():GetAbsOrigin();	
		self.orderMovePositions = { Tileify(Vector(kv.pointX,kv.pointY,0)) };
		
		for _,modifierorder in pairs(self:GetParent():FindAllModifiers()) do				
			if modifierorder:GetName()=="modifier_order_cast" and modifierorder:GetAbility() ~= nil and modifierorder:IsComplete() and modifierorder:GetAbility().ChangesCasterPosition then			
					self.orderStartPosition = modifierorder:GetCastPointIndex(modifierorder:GetAbility():ChangesCasterPosition())
				break;				
			end			
		end

		self.dummyUnit = CreateUnitByName("npc_walker_dummy_unit", self.orderStartPosition , true, nil, nil, DOTA_TEAM_NOTEAM)	
		self.dummyUnit:AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_absolute",{speed = DUMMY_DISPLAY_MOVEMENT_SPEED})	
		self.dummyUnit:FindAbilityByName("dummy_unit"):SetLevel(1)
		FindClearSpaceForUnit(self.dummyUnit, self.orderStartPosition, false)
		
		local nFXIndex = nil
		if self:GetParent():GetTeamNumber() == DOTA_TEAM_BADGUYS then
			nFXIndex = ParticleManager:CreateParticleForTeam( "particles/econ/items/kunkka/divine_anchor/hero_kunkka_dafx_skills/kunkka_spell_x_spot_line_fxset.vpcf", PATTACH_ABSORIGIN_FOLLOW, self.dummyUnit,self:GetParent():GetTeamNumber() )
		else
			nFXIndex = ParticleManager:CreateParticleForTeam( "particles/econ/items/kunkka/divine_anchor/hero_kunkka_dafx_skills/kunkka_spell_x_spot_line_red_fxset.vpcf", PATTACH_ABSORIGIN_FOLLOW, self.dummyUnit, self:GetParent():GetTeamNumber())
		end
		self:AddParticle( nFXIndex, false, false, -1, false, false )
		
		self.dummyPos = self.dummyUnit:GetAbsOrigin();
		self.walk_distance=0
		
		self.dummyUnit:SetHullRadius(self:GetParent():GetHullRadius()/5)
		--self.dummyUnit:MoveToPosition(self.orderMovePosition);
			
			local pointID = 1
			local castFrames = -1
			local moveDistance = self:GetRunDistance(  )
						
					Timers:CreateTimer(3/30, function()
										
										if self:IsNull() then return nil end
										
						if castFrames==-1 then
							self:GetCaster():Interrupt()
							castFrames=0
							self.dummyUnit:MoveToPosition(self:GetPoint(pointID));
						else
							if self.dummyUnit~=nil and not self.dummyUnit:IsNull() then
							
								local deltaVector = self.dummyUnit:GetAbsOrigin()-self.dummyPos
								
								if deltaVector:Length2D()>0 then
									self.walk_distance = self.walk_distance+deltaVector:Length2D()
									
									if self.walk_distance >= moveDistance then
										castFrames = 1
									end
								else
									if self:GetPoint(pointID+1) ~= nil then
										pointID = pointID+1
										self.dummyUnit:MoveToPosition(self:GetPoint(pointID));
									end
								end							
								self.dummyPos = self.dummyUnit:GetAbsOrigin()
							else 
								return nil
							end
							
							if castFrames==1 then
								self.dummyUnit:Stop()
								return nil
							end
						end	
						return 1/30
					
					end);										
	end
end

--------------------------------------------------------------------------------

function modifier_order_move:QueuePoint( point )

	table.insert(self.orderMovePositions,Tileify(point))
end

--------------------------------------------------------------------------------

function modifier_order_move:GetPoint( pointID )

	return self.orderMovePositions[pointID]
end

--------------------------------------------------------------------------------

function modifier_order_move:SetFollowTarget( target )

	self.followTarget = target
	self.orderMovePositions = { target:GetAbsOrigin() }
	self.orderMovePositions[0] = target:GetAbsOrigin()
end

--------------------------------------------------------------------------------

function modifier_order_move:GetFollowTarget(  )

	return self.followTarget
end

--------------------------------------------------------------------------------

function modifier_order_move:GetRunDistance(  )

	local parent = self:GetParent()
	if parent:IsRooted() then
		return 0
	end
	
	local ftime = MOVE_TIME_SHORT
	local fSpeed = self:GetParent():GetIdealSpeed()
	
	if self:GetParent():IsHero() then
		fSpeed = HERO_BASE_MOVEMENTSPEED * self:GetParent():GetAgility() * (self:GetParent():GetIdealSpeed() / self:GetParent():GetBaseMoveSpeed())
	end
  
			for _,modifierorder in pairs(parent:FindAllModifiers()) do				
				if modifierorder:GetName()=="modifier_order_cast" and modifierorder:GetAbility() ~= nil  then
				
					if modifierorder:GetAbility().isFreeAction~=nil and modifierorder:GetAbility():isFreeAction()==false and ftime==MOVE_TIME_SHORT then
						ftime=ftime/2
					end		
					if modifierorder:GetAbility().disablesMovement~=nil and modifierorder:GetAbility():disablesMovement()==true then
						return 0
					end
					if modifierorder:GetAbility().GetHasteBonus~=nil then
						fSpeed=fSpeed*modifierorder:GetAbility():GetHasteBonus()
					end
					
				end			
			end						
		

	return (fSpeed*ftime)
end

--------------------------------------------------------------------------------

function modifier_order_move:OnDestroy( kv )
	if self.dummyUnit~=nil and not self.dummyUnit:IsNull() then
		self.dummyUnit:Destroy()
	end		
end

--------------------------------------------------------------------------------

function modifier_order_move:Resolve()
	if GameMode:GetCurrentGamePhase() == GAMEPHASE_WALK then
		self:WalkMovementAction()
	end		
end
	
--------------------------------------------------------------------------------

function modifier_order_move:WalkMovementAction()
	
	if self.orderMovePositions~=nil then
		
		local parent = self:GetParent()
		if parent:IsRooted() or not parent:IsAlive() then
			return MODIFIERACTION_NOPAUSE_DESTROY
		end
		
		--parent:MoveToPosition(self.orderMovePositions);
		
		local walk_time = MOVE_TIME_SHORT;
		
		--if parent.takesnoactions==GameMode:GetCurrentTurn() then
		--	walk_time=MOVE_TIME_SHORT/2;
		--end
		walk_time = walk_time+0.2
		local move_modifier = nil
		
		if self:GetFollowTarget()~= nil then			
		
			if self:GetCaster():CanEntityBeSeenByMyTeam(self:GetFollowTarget()) and self:GetFollowTarget():IsAlive() then
				move_modifier = self:GetCaster():AddNewModifier(self:GetFollowTarget(),nil,"modifier_follow",{duration=walk_time, x = self.orderMovePositions[0].x, y = self.orderMovePositions[0].y})				
			else
				move_modifier = self:GetParent():AddNewModifier(self:GetParent(),nil,"modifier_movement",{duration=walk_time})
				if move_modifier ~= nil then			
					move_modifier:InsertPoint(self.orderMovePositions[0])
				end
			end
		else
			move_modifier = self:GetParent():AddNewModifier(self:GetParent(),nil,"modifier_movement",{duration=walk_time})
			if move_modifier ~= nil then			
				move_modifier.movePoints = self.orderMovePositions			
			end
		end
		
		if parent.takesnoactions==GameMode:GetCurrentTurn() then
			--walk_time=MOVE_TIME_SHORT/2;
			move_modifier.moveSpeed = move_modifier.moveSpeed / 2
		else
			local nFXIndex = ParticleManager:CreateParticle( "particles/items2_fx/phase_boots.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
			move_modifier:AddParticle( nFXIndex, false, false, -1, false, false )	
		end
	end	
	return MODIFIERACTION_PAUSE_DESTROY;
end

--------------------------------------------------------------------------------