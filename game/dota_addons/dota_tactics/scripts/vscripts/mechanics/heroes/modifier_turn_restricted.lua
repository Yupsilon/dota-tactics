modifier_turn_restricted = class({})

--------------------------------------------------------------------------------

function modifier_turn_restricted:GetTexture()
	return "phantom_assassin_coup_de_grace"
end

--------------------------------------------------------------------------------

function modifier_turn_restricted:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_turn_restricted:OnCreated( kv )

	if IsServer() then
		self.killer = nil
		self.minushealth = 0
		self.topdamage=0
		
		self:GetParent():SetBaseManaRegen( - 99)
		--self:GetParent():AddNewModifier(self:GetParent(),self:GetAbility(),"modifier_npc",{})
	end
end
	
--------------------------------------------------------------------------------

function modifier_turn_restricted:GetPriority()
	return 0
end

--------------------------------------------------------------------------------

function modifier_turn_restricted:IsPermanent()
	return true
end

--------------------------------------------------------------------------------

function modifier_turn_restricted:RemoveOnDeath()
	return false
end

--------------------------------------------------------------------------------

function modifier_turn_restricted:DeclareFunctions()
	return {
		MODIFIER_EVENT_ON_TAKEDAMAGE ,
		MODIFIER_PROPERTY_TOTAL_CONSTANT_BLOCK,
		--MODIFIER_PROPERTY_EXTRA_HEALTH_BONUS,
		--MODIFIER_PROPERTY_COOLDOWN_PERCENTAGE_ONGOING 
	}	
end

--------------------------------------------------------------------------------

function modifier_turn_restricted:GetModifierTotal_ConstantBlock(params)
	if IsServer() then
	
		local target = self:GetParent()				

		if params.damage > 0 then	

		
			if self.aboutToDie==true then
				return 0
			else
				if target:GetHealth()>1 then
				
					if target:GetHealth()>params.damage then
						return 0
					end
				
					self.minushealth = self.minushealth + params.damage-target:GetHealth()+1					
					return params.damage-target:GetHealth()+1
				else
					self.minushealth = self.minushealth + params.damage
					return params.damage
				end
			end
		end
	end
end

--------------------------------------------------------------------------------

function modifier_turn_restricted:OnTakeDamage ( params )
		if IsServer() then
				local hAttacker = params.attacker
				local hVictim = params.unit

			if hVictim == self:GetParent() then
						
				if hAttacker~=nil and params.damage > 5 then
					hVictim:AddNewModifier( hAttacker, nil, "modifier_hurt", {duration=.15} )	
				end
				if hAttacker ~= self:GetParent() then
					if params.damage_type ~= DAMAGE_TYPE_PURE then						
						hAttacker:AddNewModifier( hVictim, nil, "modifier_temp_reveal", {duration=PLAYER_TIME_MODIFIER*2} )	
					end							
					hVictim:AddNewModifier( hAttacker, nil, "modifier_temp_reveal", {duration=PLAYER_TIME_MODIFIER*2} )		
						
					if self.killer == nil or params.damage>self.topdamage then
						self.killer=hAttacker
						self.topdamage=params.damage
					end
							
				end
			end
		end
end
	
--------------------------------------------------------------------------------

function modifier_turn_restricted:WalkMovementAction()
	
	--if (self:GetParent():IsAlive()) then
		--self:GetParent():GiveMana(5);		
		if self:GetCaster():HasModifier("modifier_indicator_ready") then
			self:GetCaster():FindModifierByName("modifier_indicator_ready"):SetStackCount(1)
		end
	--end
	
	local scar_mod = self:GetParent():FindModifierByName("generic_modifier_scarred")
	local scar_damage = 0
	
	if scar_mod~=nil then
		scar_damage=scar_mod:GetStackCount()
	end
	
	self.aboutToDie=true	
	if self.minushealth>0 and not self:GetParent():HasModifier("generic_modifier_cannot_die") then
		self:GetParent():AddNewModifier( hVictim, nil, "modifier_respawn_endofturn", {} )	
			
		if scar_mod~=nil then
			scar_mod:SetStackCount(0)
		end
	
		ApplyDamage({
			victim = self:GetParent(),
			attacker = self.killer,
			damage = self.minushealth,
			damage_type = DAMAGE_TYPE_PURE,
		})
	end
	self.aboutToDie=false
			
	self.killer = nil
	self.minushealth = 0
	self.topdamage=0
	
		if self:GetParent():IsAlive() then
		
			if scar_mod~=nil then
				scar_mod:SetStackCount(scar_damage)
			end		
			self:GetParent():RemoveModifierByName("modifier_respawn_endofturn")
			return MODIFIERACTION_NOPAUSE_SURVIVE;
		else			
			self:GetParent():RemoveModifierByName("modifier_brush_invisibility")				
		end
	return MODIFIERACTION_PAUSE_SURVIVE;
end

--------------------------------------------------------------------------------

function modifier_turn_restricted:GetModifierExtraHealthBonus( params )
	
	return -1
end
	
--------------------------------------------------------------------------------

function modifier_turn_restricted:EndOfTurnAction()

		self:GetParent():SetBaseHealthRegen(0)
		self:GetParent():SetBaseManaRegen(0)
		
		self:GetParent():SetBaseHealthRegen(0-self:GetParent():GetHealthRegen())
		self:GetParent():SetBaseManaRegen(0 - self:GetParent():GetManaRegen())
	
	return MODIFIERACTION_NOPAUSE_SURVIVE;
end
	
--------------------------------------------------------------------------------