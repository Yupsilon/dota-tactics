attribute_turn_restricted_hero = class({})
LinkLuaModifier( "modifier_turn_restricted","mechanics/heroes/modifier_turn_restricted.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_hurt","mechanics/heroes/modifier_hurt.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_npc","mechanics/heroes/modifier_npc.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_temp_reveal","mechanics/heroes/modifier_temp_reveal.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_respawn_endofturn","mechanics/heroes/modifier_respawn_endofturn.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_absolute","abilities/generic/generic_modifier_movespeed_absolute.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function attribute_turn_restricted_hero:GetIntrinsicModifierName()
	return "modifier_turn_restricted"
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
