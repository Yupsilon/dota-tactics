LinkLuaModifier( "modifier_order_move","mechanics/heroes/modifier_order_move.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_order_cast","mechanics/heroes/modifier_order_cast.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_movement","mechanics/heroes/modifier_movement.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_follow","mechanics/heroes/modifier_follow.lua", LUA_MODIFIER_MOTION_NONE )

function CDOTA_BaseNPC:IsDotATacticsCreature()
	return self:HasModifier("modifier_turn_restricted") or self:HasModifier("modifier_turn_restricted_creature")
end

function GameMode:FilterExecuteOrder( filterTable )

    local units = filterTable["units"]
    local order_type = filterTable["order_type"]
    local issuer = filterTable["issuer_player_id_const"]

    local abilityIndex = filterTable["entindex_ability"]
    local targetIndex = filterTable["entindex_target"]
    local x = tonumber(filterTable["position_x"])
    local y = tonumber(filterTable["position_y"])
    local z = tonumber(filterTable["position_z"])
    local queue = tonumber(filterTable["queue"])==1
    local point = Vector(x,y,z)

    local issuerUnit
    if units["0"] then
      issuerUnit = EntIndexToHScript(units["0"])
      if issuerUnit then issuerUnit.lastOrder = order_type end
    end

    if issuerUnit and issuerUnit.skip then
      issuerUnit.skip = false
      return true
    end

    --[[for n,unit_index in pairs(units) do
      local unit = EntIndexToHScript(unit_index)
      local ownerID = unit:GetPlayerOwnerID()

      if PlayerResource:GetConnectionState(ownerID) == 3 or
        PlayerResource:GetConnectionState(ownerID) == 4
        then
        return false
      end
    end]]
	
	--------------------------------------------
	--		QUEUE ORDERS FOR ACTIONS
	--------------------------------------------
	if issuerUnit == nil then
		DebugPrint("IssuerUnit is nil.")
		return true
	end
	if ((not issuerUnit:IsDotATacticsCreature())) then
		DebugPrint("Invalid turn restricted unit.")
		return true
	end		
	--if issuerUnit:HasModifier("modifier_action_forbidden") then
	--	return false
	--end		
	
	--Orders allowed any time, regardless of phase
	 if order_type == DOTA_UNIT_ORDER_TRAIN_ABILITY or order_type == DOTA_UNIT_ORDER_MOVE_ITEM then
		return true
	 elseif order_type == DOTA_UNIT_ORDER_CAST_TOGGLE_AUTO then
		local ability = EntIndexToHScript(abilityIndex)
		--if ability.SetAutoCastState then
		
			local assigned_modifier = nil
			for _,modifierorder in pairs(issuerUnit:FindAllModifiers()) do				
				if modifierorder:GetName() == "modifier_order_cast" and modifierorder:GetAbility() ~=nil and modifierorder:GetAbility() == ability then 
					assigned_modifier = modifierorder
				end
			end
			
			if ability:GetAutoCastState() and assigned_modifier~=nil then
				assigned_modifier:Destroy()
			elseif ability:GetAutoCastState() and assigned_modifier==nil then
				ability:ToggleAutoCast()
			end
		--end
	 end

	point = point or EntIndexToHScript(targetIndex):GetAbsOrigin()
	
		--if issuerUnit:HasModifier("modifier_turn_restricted") then
				 

			 if order_type == DOTA_UNIT_ORDER_STOP or order_type == DOTA_UNIT_ORDER_HOLD_POSITION then	
	
					DebugPrint("Ordering HALT")
				if GameMode:GetCurrentGamePhase() ~= GAMEPHASE_THINK then
					return false;	--only issue orders during specific phase
				end				
					for _,modifierorder in pairs(issuerUnit:FindAllModifiers()) do				
						if modifierorder:GetName() == "modifier_order_cast" or modifierorder:GetName() == "modifier_order_move" then						
							modifierorder:Destroy();
						end			
					end
					
					if issuerUnit:HasModifier("modifier_indicator_ready") then
						issuerUnit:FindModifierByName("modifier_indicator_ready"):SetStackCount(1)
						self:SetAllHeroesReady(false)
					end
				return true;			 
	 
			elseif order_type == DOTA_UNIT_ORDER_MOVE_TO_POSITION or order_type == DOTA_UNIT_ORDER_MOVE_TO_TARGET or order_type == DOTA_UNIT_ORDER_ATTACK_MOVE or order_type == DOTA_UNIT_ORDER_ATTACK_TARGET then 
	
					DebugPrint("Ordering CAST")
				if GameMode:GetCurrentGamePhase() ~= GAMEPHASE_THINK then
					return false;	--only issue orders during specific phase
				end
				if issuerUnit:IsAlive() and not issuerUnit:HasModifier("generic_modifier_snare") then
							
						for _,modifierorder in pairs(issuerUnit:FindAllModifiers()) do				
							if modifierorder:GetName() == "modifier_order_cast" and modifierorder:GetAbility() ~=nil and
							modifierorder:GetAbility().disablesMovement~=nil and modifierorder:GetAbility():disablesMovement()==true then 
							
								return false;
							end
						end
					
					local targetpoint = point 					
					if targetIndex>0 then
						targetpoint=EntIndexToHScript(targetIndex):GetAbsOrigin()
					end
					
					if issuerUnit:HasModifier("modifier_indicator_ready") then
						issuerUnit:FindModifierByName("modifier_indicator_ready"):SetStackCount(1)
						self:SetAllHeroesReady(false)	
					elseif not issuerUnit:IsRealHero() then
						--UNREADY HERO
						--self:SetAllHeroesReady(false)	
					end
					
					if queue and issuerUnit:HasModifier("modifier_order_move") then
						for _,modifierorder in pairs(issuerUnit:FindAllModifiers()) do				
							if modifierorder:GetName() == "modifier_order_move" then 
							
								modifierorder:QueuePoint(targetpoint);
							end
						end
					else
						issuerUnit:RemoveModifierByName("modifier_order_move");
						local move_modifier = issuerUnit:AddNewModifier(issuerUnit,nil,"modifier_order_move",{pointX=targetpoint.x,pointY=targetpoint.y})
						
						if targetIndex > 0 then
							move_modifier:SetFollowTarget(EntIndexToHScript(targetIndex))
						end
					end
					
					DebugPrint("Turn restricted unit issued an order")
					return false
				else
					if issuerUnit:HasModifier("modifier_respawn_endofturn") then
						DebugPrint("Trying to respawn this unit!")
						local mymodifier = issuerUnit:FindModifierByName("modifier_respawn_endofturn")
									
						if targetIndex>0 then
							point=EntIndexToHScript(targetIndex):GetAbsOrigin()
						end
						
						if mymodifier.turn_of_death==GameMode:GetCurrentTurn() then
							mymodifier:ChangeRespawnLocation({center = point})
						else
							mymodifier:ChangeRunLocation({center = point})
						end
					end
				end		
			
		
		  elseif order_type == DOTA_UNIT_ORDER_CAST_POSITION or order_type == DOTA_UNIT_ORDER_CAST_TARGET or order_type == DOTA_UNIT_ORDER_CAST_NO_TARGET then
		  
			local ability = EntIndexToHScript(abilityIndex)
			if ability~=nil then
			
				if ability.HandleTactical~=nil and GameMode:GetCurrentGamePhase() <= GAMEPHASE_THINK  then
					if issuerUnit:HasModifier("modifier_indicator_ready") then
						issuerUnit:FindModifierByName("modifier_indicator_ready"):SetStackCount(1)
						self:SetAllHeroesReady(false)	
					end
					ability:HandleTactical()	
					return false;
				end			
				
				if GameMode:GetCurrentGamePhase() <= GAMEPHASE_THINK and GameMode:GetCurrentTurn() == 1 and ability:IsItem() and IsItemSwappable(ability)>=0 then
					SwapItem(ability)
					return false;
				elseif GameMode:GetCurrentGamePhase() ~= GAMEPHASE_THINK then
					return false;	--only issue orders during specific phase
				end
				if ability:GetManaCost(ability:GetLevel())>issuerUnit:GetMana() then
					return false;
				end
			
				DebugPrint("Casting ability "..ability:GetAbilityName().." from caster")
				
				local targetpoint = point		
				if ability.isFreeAction~=nil then				
					if ability:isFreeAction()==false then			
						for _,modifierorder in pairs(issuerUnit:FindAllModifiers()) do				
							if modifierorder:GetName() == "modifier_order_cast" and modifierorder:GetAbility() ~=nil and
							modifierorder:GetAbility().isFreeAction~=nil and modifierorder:GetAbility():isFreeAction()==false then
							
								if modifierorder:GetAbility()==ability then
									if modifierorder:IsIncomplete() then
										modifierorder:InsertPoint(targetpoint);
										return false;
									else
										modifierorder:Destroy();											
									end
								else
									modifierorder:Destroy();											
								end
							elseif modifierorder:GetName() == "modifier_order_move" and modifierorder:GetAbility() ~=nil and (
							(modifierorder:GetAbility().disablesMovement and modifierorder:GetAbility():disablesMovement() == true ) or
							(modifierorder:GetAbility().ChangesCasterPosition )) then 	
								issuerUnit:RemoveModifierByName("modifier_order_move");						
							end
						end
					elseif ability:isFreeAction()==true then
						for _,modifierorder in pairs(issuerUnit:FindAllModifiers()) do				
							if modifierorder:GetName() == "modifier_order_cast" and modifierorder:GetAbility() == ability then
								modifierorder:Destroy();	
								return false;
							end
						end
					end			
				end
				
				local castTable = {ent_index=targetIndex}
				
				if targetpoint~=nil then
					castTable.pointX=targetpoint.x
					castTable.pointY=targetpoint.y
				end
				
				if ability.disablesMovement~=nil and ability:disablesMovement()==true then 
					issuerUnit:RemoveModifierByName("modifier_order_move");
				end
			
					if issuerUnit:HasModifier("modifier_indicator_ready") then
						issuerUnit:FindModifierByName("modifier_indicator_ready"):SetStackCount(1)
						self:SetAllHeroesReady(false)	
					end
				issuerUnit:AddNewModifier(issuerUnit,ability,"modifier_order_cast",castTable)
				
				if ability.GetCastPoints and (ability:GetCastPoints() or 0 )>1 then
				
					ability._vectorTargetKeys = {
						initialPosition = nil,                      -- initial position of vector input
						terminalPosition = nil,                     -- terminal position of vector input
						pointOfCast = "initial",
					}
				
                    local orderData = {
                        initialPosition = targetpoint,
                        currentPosition = targetpoint,
                        seqNum = filterTable["sequence_number_const"],
                        abilId = abilityIndex,
                        time = Time(),
                        orderType = order_type,
                        unitId = issuerUnit:GetEntityIndex(),
                        shiftPressed = false,
                        resolved = false,
                    }
                    self.inProgressOrders[issuer] = orderData 
                    CustomGameEventManager:Send_ServerToPlayer(PlayerResource:GetPlayer(issuer), "vector_target_order_start", orderData)
				end
				
				return false;
			
			end
			
		-- end
	 end
	
    return false
end

