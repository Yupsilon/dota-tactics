modifier_npc = class({})

--------------------------------------------------------------------------------

function modifier_npc:GetTexture()
	return "phantom_assassin_coup_de_grace"
end

--------------------------------------------------------------------------------

function modifier_npc:IsHidden()
	return false
end
	
--------------------------------------------------------------------------------

function modifier_npc:GetPriority()
	return 0
end

--------------------------------------------------------------------------------

function modifier_npc:IsPermanent()
	return true
end

--------------------------------------------------------------------------------

function modifier_npc:RemoveOnDeath()
	return false
end

--------------------------------------------------------------------------------

function modifier_npc:DeclareFunctions()
	return {
		MODIFIER_PROPERTY_SUPER_ILLUSION_WITH_ULTIMATE,
		MODIFIER_PROPERTY_TEMPEST_DOUBLE 
	}	
end

--------------------------------------------------------------------------------

function modifier_npc:GetModifierSuperIllusionWithUltimate(params)
	return 1
end

function modifier_npc:GetModifierTempestDouble(params)
	return 1
end
	
--------------------------------------------------------------------------------