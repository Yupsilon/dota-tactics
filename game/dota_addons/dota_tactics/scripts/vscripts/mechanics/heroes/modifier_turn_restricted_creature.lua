modifier_turn_restricted_creature = class({})

--------------------------------------------------------------------------------

function modifier_turn_restricted_creature:GetTexture()
	return "phantom_assassin_coup_de_grace"
end

--------------------------------------------------------------------------------

function modifier_turn_restricted_creature:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_turn_restricted_creature:OnCreated( kv )

	self.killer = nil
	self.minushealth = 0
	self.topdamage=0
	
end

--------------------------------------------------------------------------------

function modifier_turn_restricted_creature:IsPermanent()
	return true
end

--------------------------------------------------------------------------------

function modifier_turn_restricted_creature:RemoveOnDeath()
	return false
end

--------------------------------------------------------------------------------

function modifier_turn_restricted_creature:DeclareFunctions()
	return {
		MODIFIER_EVENT_ON_TAKEDAMAGE ,
		MODIFIER_PROPERTY_TOTAL_CONSTANT_BLOCK,
		--MODIFIER_PROPERTY_COOLDOWN_PERCENTAGE_ONGOING 
	}	
end

--------------------------------------------------------------------------------

function modifier_turn_restricted_creature:GetModifierTotal_ConstantBlock(params)
	if IsServer() then
	
		local target = self:GetParent()				

		if params.damage > 0 then			
			if self.aboutToDie==true then
				return 0
			else
				if target:GetHealth()>1 then
				
					if target:GetHealth()>params.damage then
						return 0
					end
				
					self.minushealth = self.minushealth + params.damage-target:GetHealth()+1					
					return params.damage-target:GetHealth()+1
				else
					self.minushealth = self.minushealth + params.damage
					return params.damage
				end
			end
		end
	end
end

--------------------------------------------------------------------------------

function modifier_turn_restricted_creature:OnTakeDamage ( params )
		if IsServer() then
				local hAttacker = params.attacker
				local hVictim = params.unit

			if hVictim == self:GetParent() then
						
				if hAttacker~=nil and params.damage > 5 then
					hVictim:AddNewModifier( hAttacker, nil, "modifier_hurt", {duration=.15} )	
				end
				if hAttacker ~= self:GetParent() then
					
					if params.damage_type ~= DAMAGE_TYPE_PURE then
						hAttacker:AddNewModifier( hVictim, nil, "modifier_temp_reveal", {duration=PLAYER_TIME_MODIFIER*2} )		
						hVictim:AddNewModifier( hAttacker, nil, "modifier_temp_reveal", {duration=PLAYER_TIME_MODIFIER*2} )		
					end	
							
					if self.killer == nil or params.damage>self.topdamage then
						self.killer=hAttacker
						self.topdamage=params.damage
					end
							
				end
			end
		end
end
	
--------------------------------------------------------------------------------

function modifier_turn_restricted_creature:WalkMovementAction()
	
	--if (self:GetParent():IsAlive()) then
		--self:GetParent():GiveMana(5);		
		if self:GetCaster():HasModifier("modifier_indicator_ready") then
			self:GetCaster():FindModifierByName("modifier_indicator_ready"):SetStackCount(1)
		end
	--end
	
	self.aboutToDie=true	
	if self.minushealth>0 and not self:GetParent():HasModifier("generic_modifier_cannot_die") then
		--self:GetParent():AddNewModifier( hVictim, nil, "modifier_respawn_endofturn", {} )	
	
		ApplyDamage({
			victim = self:GetParent(),
			attacker = self.killer,
			damage = self.minushealth,
			damage_type = DAMAGE_TYPE_PURE,
		})
	end
	self.aboutToDie=false
			
	self.killer = nil
	self.minushealth = 0
	self.topdamage=0
	
		if self:GetParent():IsAlive() then
		
			--self:GetParent():RemoveModifierByName("modifier_respawn_endofturn")
			return MODIFIERACTION_NOPAUSE_SURVIVE;
		else
			self:GetParent():RemoveModifierByName("modifier_brush_invisibility")
			self:GetParent():RemoveModifierByName("generic_modifier_scarred")
		end
	return MODIFIERACTION_PAUSE_SURVIVE;
end
	
--------------------------------------------------------------------------------