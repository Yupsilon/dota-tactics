modifier_order_knockback = class({})

--------------------------------------------------------------------------------

function modifier_order_knockback:OnCreated(kv)
	self.vectorDirection = Vector(kv.direction_x,kv.direction_y,0)
	self.solved=false
	
	if IsServer() then
		
		if self:GetParent():HasModifier("generic_modifier_combobreaker") then	
			self:GetParent():AddNewModifier(self:GetCaster(),self:GetAbility(),"generic_modifier_movespeed_slow",{turns=1})
			self.solved=true
		end
	end
end
--------------------------------------------------------------------------------

function modifier_order_knockback:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_order_knockback:Resolve()
	if GameMode:GetCurrentGamePhase() == GAMEPHASE_WALK then
		self:WalkMovementAction()
	end		
end
	
--------------------------------------------------------------------------------

function modifier_order_knockback:WalkMovementAction()
	
	if self:GetParent():HasModifier("generic_modifier_unstoppable") or self.solved == true or (not self:GetParent():IsAlive()) then return MODIFIERACTION_NOPAUSE_DESTROY end
	
	if not self:GetParent():HasModifier("generic_modifier_combobreaker") then			
		self:GetParent():AddNewModifier(self:GetParent(),self:GetAbility(),"generic_modifier_combobreaker",{turns=2})
	end
	
				
		local parent = self:GetParent()
		parent:Interrupt()
		
		local startPosition = self:GetParent():GetAbsOrigin();
		local vectorDelta = Vector(0,0,0)
	
		for _,modifierorder in pairs(self:GetParent():FindAllModifiers()) do				
			if modifierorder:GetName() == "modifier_order_knockback"  then
							
				vectorDelta.x=vectorDelta.x+modifierorder.vectorDirection.x
				vectorDelta.y=vectorDelta.y+modifierorder.vectorDirection.y
				modifierorder.solved=true
			end			
		end	
		

		parent:RemoveModifierByName("modifier_movement")
		parent:RemoveModifierByName("modifier_follow")
		parent:RemoveModifierByName("modifier_order_move")
		parent:RemoveModifierByName("modifier_order_forcefollow")
		
				local projectile = {
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPosition+Vector (0,0,50),
				  fDistance = vectorDelta:Length2D(),
				  Source = self:GetCaster(),
				  vVelocity = vectorDelta:Normalized(), 
				  UnitBehavior = PROJECTILES_NOTHING,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_DESTROY,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_DESTROY,
				  GroundBehavior = PROJECTILES_DESTROY,
				  fGroundOffset = 0,
				  bZCheck = false,
				  bGroundLock = false,
				  draw = false,
				  bUseFindUnitsInRadius = true,
				  bHitScan=true,
				   OnFinish = function(instance, pos) 
					
						local direction = pos-startPosition
						parent:AddNewModifier(parent,nil,"modifier_action_knockback",{
							direction_x = direction.x,
							direction_y = direction.y		
						})
				  end,
				}
				
						
				Projectiles:CreateProjectile(projectile)
		
		
	return MODIFIERACTION_PAUSE_DESTROY;
end
	