modifier_temp_reveal = class({})

--------------------------------------------------------------------------------

function modifier_temp_reveal:IsDebuff()
	return true
end

--------------------------------------------------------------------------------

function modifier_temp_reveal:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_temp_reveal:GetPriority()
	
	return 2
end

--------------------------------------------------------------------------------

function modifier_temp_reveal:CheckState()
	local state = {
	[MODIFIER_STATE_INVISIBLE] = false,
	}

	return state
end

--------------------------------------------------------------------------------

function modifier_temp_reveal:WalkMovementAction()
	
	self:SetDuration(PLAYER_TIME_MODIFIER/2,false)
	return MODIFIERACTION_NOPAUSE_SURVIVE
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
