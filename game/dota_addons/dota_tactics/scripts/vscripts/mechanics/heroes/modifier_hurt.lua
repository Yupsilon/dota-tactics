modifier_hurt = class({})

--------------------------------------------------------------------------------

function modifier_hurt:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_hurt:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_OVERRIDE_ANIMATION,
	}

	return funcs
end

--------------------------------------------------------------------------------

function modifier_hurt:GetOverrideAnimation( params )
	return ACT_DOTA_DISABLED
end