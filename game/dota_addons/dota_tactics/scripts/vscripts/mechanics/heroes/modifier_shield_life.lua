modifier_shield_life = class({})


--------------------------------------------------------------------------------

function modifier_shield_life:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function modifier_shield_life:Revise()
			local remaining = GetCombinedShieldLife(self:GetParent())
			if remaining>0 then
				self:SetStackCount(remaining)
			else
				self:Destroy()
			end
end

--------------------------------------------------------------------------------

function modifier_shield_life:GetTexture()
	return "medusa_mana_shield"
end