modifier_movement = class({})

--------------------------------------------------------------------------------

function modifier_movement:OnCreated(kv)
	self.caster=self:GetParent()
	
	if IsServer() then
		self.moveSpeed = self:GetParent():GetIdealSpeed()
		
		if self:GetParent():IsHero() then
			self.moveSpeed = HERO_BASE_MOVEMENTSPEED * self:GetParent():GetAgility() * self:GetParent():GetIdealSpeed() / self:GetParent():GetBaseMoveSpeed()
		end
		
		self:GetParent():SetHullRadius(self:GetParent():GetHullRadius()/4)
		FindClearSpaceForUnit(self:GetParent(), GetGroundPosition(self:GetParent():GetAbsOrigin(),self:GetParent()), true)
		
		self.pointID = 0
		self.lastPoint = self:GetParent():GetAbsOrigin()
		self.movePoints = {}
		
		self:StartIntervalThink(1/30)
		self:OnIntervalThink()
	end
end

--------------------------------------------------------------------------------

function modifier_movement:InsertPoint(point)
	self.movePoints = self.movePoints  or {}
	table.insert(self.movePoints,point)
end

--------------------------------------------------------------------------------

function modifier_movement:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_movement:CheckBrush()
	if not IsPointOverBrush(self:GetParent():GetAbsOrigin()) then
		self:GetParent():RemoveModifierByName("modifier_brush_invisibility")
	end
end

--------------------------------------------------------------------------------

function modifier_movement:OnIntervalThink()
	
	if self.movePoints == nil or #self.movePoints == 0 then
		self:GetParent():Interrupt()	
	end
	
	local alphaVector = self:GetParent():GetAbsOrigin()-(self.movePoints[self.pointID] or self:GetParent():GetAbsOrigin())
	local deltaVector = self:GetParent():GetAbsOrigin()-self.lastPoint
	
	if deltaVector:Length2D()<DOTA_TILE_TO_UNITS/5 and alphaVector:Length2D()<DOTA_TILE_TO_UNITS/5 then
		if self.movePoints[self.pointID+1] ~= nil then
			self.pointID = self.pointID+1
			self:GetParent():MoveToPosition(self.movePoints[self.pointID]);
			--self:CheckBrush()
		end
	end							
	self.lastPoint = self:GetParent():GetAbsOrigin()	
end

--------------------------------------------------------------------------------

function modifier_movement:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE
	}	

	return funcs
end

--------------------------------------------------------------------------------

function modifier_movement:CheckState()
	local state = {
	[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}

	return state
end

--------------------------------------------------------------------------------

function modifier_movement:GetModifierMoveSpeed_Absolute()	
	return self.moveSpeed
end

--------------------------------------------------------------------------------

function modifier_movement:OnDestroy( kv )
	if IsServer() then
		self.caster:Stop()		
		self:GetParent():SetHullRadius(self:GetParent():GetHullRadius()*4)
		FindClearSpaceForUnit(self.caster, Tileify(self.caster:GetAbsOrigin()), false)
		--self:CheckBrush()
	end
end
	