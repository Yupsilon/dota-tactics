modifier_order_cast = class({})

--------------------------------------------------------------------------------

function modifier_order_cast:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_order_cast:OnCreated( kv )

	if not IsServer() then return end

	self.orderCastPositions = {};
	
	if kv.ent_index~=nil and kv.ent_index>0 then
		local target = EntIndexToHScript(kv.ent_index)	
		if target~=nil then self:InsertTargetUnit(target) end	
	elseif kv.pointX~=nil and kv.pointY~=nil then self:InsertPoint(Vector(kv.pointX,kv.pointY,0)) end
	
	if self:IsComplete() then
		if not self:GetAbility():GetAutoCastState() then
			self:GetAbility():ToggleAutoCast()
		end	
		
		if self:GetAbility():IsItem() then
			self:GetAbility():SetCurrentCharges(0)
		end
	end	
end
	
--------------------------------------------------------------------------------

function modifier_order_cast:GetPriority()
	
						
						if self:GetAbility().GetPriority ~= nil then
							return self:GetAbility():GetPriority()
						end
						return 3;
end
	
--------------------------------------------------------------------------------

function modifier_order_cast:GetPlayerColor()
	
	return GameMode.PlayerColors[self:GetParent():GetPlayerOwnerID()] or Vector(255,255,255);
end
	
--------------------------------------------------------------------------------

function modifier_order_cast:IsComplete()
	return not self:IsIncomplete()
end
	
--------------------------------------------------------------------------------

function modifier_order_cast:IsIncomplete()
	if self:GetAbility():getAbilityType()>=0 then 
		return self:GetCastPoints()<self:GetAbility():GetCastPoints();
	end
	return false;
end
	
--------------------------------------------------------------------------------

function modifier_order_cast:AutoComplete()
	while self:IsIncomplete() do

		local vectorA = self:GetCastPointIndex(self:GetCastPoints()-2)
		local vectorB = self:GetCastPointIndex(self:GetCastPoints()-1)
				
		self:InsertPoint(vectorB+((vectorB-vectorA):Normalized())*100)
	end
end
	
--------------------------------------------------------------------------------

function modifier_order_cast:GetCastPoints()

	local orderI = 0;
		
	while self.orderCastPositions[orderI]~=nil do
		orderI=orderI+1;
	end

	return orderI
end
	
--------------------------------------------------------------------------------

function modifier_order_cast:GetCastPointIndex(index)

	if index<0 then
		return self:GetParent():GetAbsOrigin()
	end

	return self.orderCastPositions[index]
end
	
--------------------------------------------------------------------------------

function modifier_order_cast:GetRangeTotal()

	local range = 0

	for index = 0,	self:GetCastPoints()-1 do
	
		range=range+(self:GetCastPointIndex(index)-self:GetCastPointIndex(index-1)):Length();
	end

	return range
end
	
--------------------------------------------------------------------------------

function modifier_order_cast:InsertTargetUnit(unit)
	
	local delta = self:GetCaster():GetAbsOrigin()-unit:GetAbsOrigin()		
	local integ = self:GetCastPoints()	
	local valid = true
		
	if self:GetAbility().GetMinRange~=nil and delta:Length2D()<self:GetAbility():GetMinRange(integ) then
		valid = false
	elseif self:GetAbility().GetMaxRange~=nil and delta:Length2D()>self:GetAbility():GetMaxRange(integ) then
		valid = false
	end
	
	if not valid then 
		self:Destroy()
	else
		self.orderCastTarget = unit;
		self:InsertPoint(Vector(unit:GetAbsOrigin().x,unit:GetAbsOrigin().y,0))
	end
end
	
--------------------------------------------------------------------------------

function modifier_order_cast:InsertPoint(vector)

	local color = self:GetPlayerColor()	
	local integ = self:GetCastPoints()
	
	if self:IsSnapToGrid() then vector = Tileify(vector) end
	
	if self:GetAbility().GetCastPoints == nil or self:GetAbility():GetCastPoints()<=integ then return end
	
	if self:GetAbility().RequireFirstTargetUnit and self:GetAbility():RequireFirstTargetUnit() == true and self.orderCastTarget == nil then return end
	
	if self.orderCastPositions[integ]==nil then
		
		if self:GetAbility():getAbilityType()==ABILITY_TYPE_POINT or self:GetAbility():getAbilityType()==ABILITY_TYPE_POINT_MULTIPLE then 
			
			local center = self:GetCastPointIndex(integ-1)
			
			if self:GetAbility():getAbilityType()==ABILITY_TYPE_POINT_MULTIPLE then
				center = self:GetCastPointIndex(-1)
			end
		
			local delta = (vector-center)
			
			if self:GetAbility().TotalMaxRange~=nil and self:GetRangeTotal()+delta:Length2D()>self:GetAbility():TotalMaxRange(integ) then
			
				local deltarange = self:GetRangeTotal()+delta:Length2D()-self:GetAbility():TotalMaxRange(integ)
				
				if deltarange<=0 then
					self.orderCastPositions[integ]=self:GetCastPointIndex(integ-1)
					return	--dont draw
				end
				
				self.orderCastPositions[integ]=center+delta:Normalized()*(delta:Length2D()-deltarange)
				
			elseif self:GetAbility().GetMinRange~=nil and delta:Length2D()<self:GetAbility():GetMinRange(integ) then
				self.orderCastPositions[integ]=center+delta:Normalized()*self:GetAbility():GetMinRange(integ);
			elseif self:GetAbility().GetMaxRange~=nil and delta:Length2D()>self:GetAbility():GetMaxRange(integ) then
				self.orderCastPositions[integ]=center+delta:Normalized()*self:GetAbility():GetMaxRange(integ);
			else
				self.orderCastPositions[integ]=vector		
			end
					
			self:DrawArrow(			
				center,
				self:GetCastPointIndex(integ),
				delta,
				self:GetAbility():GetConeArc(integ*2,delta:Length2D()),
				self:GetAbility():GetConeArc(integ*2+1,delta:Length2D())
				
			)
			
			--[[local nFXIndexBody = ParticleManager:CreateParticleForTeam( "particles/interface/msg_arrow_helper.vpcf", PATTACH_WORLDORIGIN, nil, self:GetParent():GetTeam() );
			ParticleManager:SetParticleControl( nFXIndexBody, 1, center+delta:Normalized()*100 );
			ParticleManager:SetParticleControl( nFXIndexBody, 2, self:GetCastPointIndex(integ)-delta:Normalized()*100 );
			ParticleManager:SetParticleControl( nFXIndexBody, 3, Vector (self:GetAbility():GetConeArc(integ*2,delta:Length2D()),self:GetAbility():GetConeArc(integ*2+1,delta:Length2D()),0) );
			ParticleManager:SetParticleControl( nFXIndexBody, 4, color );
			self:AddParticle( nFXIndexBody, false, false, -1, false, false )
			
			local nFXIndexHead = ParticleManager:CreateParticleForTeam( "particles/ui_mouseactions/range_finder_cone_end.vpcf", PATTACH_WORLDORIGIN, nil, self:GetParent():GetTeam() );
			ParticleManager:SetParticleControl( nFXIndexHead, 1, center+delta:Normalized()*100 );
			ParticleManager:SetParticleControl( nFXIndexHead, 2, self:GetCastPointIndex(integ)-delta:Normalized()*100 );
			ParticleManager:SetParticleControl( nFXIndexHead, 4, color );
			self:AddParticle( nFXIndexHead, false, false, -1, false, false )]]
			
		elseif self:GetAbility():getAbilityType()==ABILITY_TYPE_CONE or self:GetAbility():getAbilityType()==ABILITY_TYPE_DOUBLE_CONE then 
					
			local center = self:GetCastPointIndex(-1)	
			
			local delta = (vector-center)
			local dlength = delta:Length2D()									
			
			--[[if self:GetAbility().TotalMaxRange~=nil and self:GetRangeTotal()+dlength>self:GetAbility():TotalMaxRange(integ) then
			
				local deltarange = self:GetRangeTotal()+dlength-self:GetAbility():TotalMaxRange(integ)
				
				if deltarange<=0 then
					self.orderCastPositions[integ]=self:GetCastPointIndex(integ-1)
					return	--dont draw
				end
				
				self.orderCastPositions[integ]=center+delta:Normalized()*(dlength-deltarange)
				
			else]] if self:GetAbility().GetMinRange~=nil and dlength<self:GetAbility():GetMinRange(integ) then
				self.orderCastPositions[integ]=center+delta:Normalized()*self:GetAbility():GetMinRange(integ);
			elseif self:GetAbility().GetMaxRange~=nil and dlength>self:GetAbility():GetMaxRange(integ) then
				self.orderCastPositions[integ]=center+delta:Normalized()*self:GetAbility():GetMaxRange(integ);
			else
				self.orderCastPositions[integ]=vector		
			end
			
			local direction = (self:GetCastPointIndex(integ)-center)
			direction.z=0
			
			local startangle = math.atan2(direction.x,direction.y)
			local cone_radius = direction:Length2D()
			
			local beam_count = self:GetAbility():GetConeArc(integ*2+1,dlength)
			local beam_angle = self:GetAbility():GetConeArc(integ*2,dlength) / beam_count*math.pi/360
						
			self:DrawCone(center,startangle,cone_radius,beam_angle,beam_count)
			
			if self:GetAbility():getAbilityType()==ABILITY_TYPE_DOUBLE_CONE then 
			
				local beam_count = self:GetAbility():GetConeArc((integ+1)*2+1,dlength)
				local beam_angle = self:GetAbility():GetConeArc((integ+1)*2,dlength) / beam_count*math.pi/360
			
				if self:GetAbility().GetMinRange~=nil and cone_radius<self:GetAbility():GetMinRange(integ+1) then
					cone_radius=self:GetAbility():GetMinRange(integ+1);
				elseif self:GetAbility().GetMaxRange~=nil and cone_radius>self:GetAbility():GetMaxRange(integ+1) then
					cone_radius=self:GetAbility():GetMaxRange(integ+1);						
				end
			
				self:DrawCone(center,startangle,cone_radius,beam_angle,beam_count)
			
			end			
		elseif self:GetAbility():getAbilityType()==ABILITY_TYPE_POINT_CONE_HYBRID then 		
			local center = self:GetCastPointIndex(-1)	
			
			local delta = (vector-center)
			local dlength = delta:Length2D()									
			
			if self:GetAbility().GetMinRange~=nil and dlength<self:GetAbility():GetMinRange(integ) then
				self.orderCastPositions[integ]=center+delta:Normalized()*self:GetAbility():GetMinRange(integ);
			elseif self:GetAbility().GetMaxRange~=nil and dlength>self:GetAbility():GetMaxRange(integ) then
				self.orderCastPositions[integ]=center+delta:Normalized()*self:GetAbility():GetMaxRange(integ);
			else
				self.orderCastPositions[integ]=vector		
			end
			
			local direction = (self:GetCastPointIndex(integ)-center)
			direction.z=0
			
			local startangle = math.atan2(direction.x,direction.y)
			local cone_radius = direction:Length2D()
						
			local beam_count = self:GetAbility():GetConeArc(integ*2+1,dlength)
			local beam_angle = self:GetAbility():GetConeArc(integ*2,dlength) / beam_count*math.pi/180
			
			local beam_projectile = self:GetAbility():GetSpecialValueFor("projectile_radius")
													
			for i=0,beam_count-1 do
										
					local fireAngle = startangle-beam_angle*(beam_count-1)/2+beam_angle*i
					local fireDirection = Vector(math.sin(fireAngle),math.cos(fireAngle),0)
					local distance = cone_radius
					
					if self:GetAbility().GetForcedMinRange~=nil and distance<self:GetAbility():GetForcedMinRange(integ) then 
						distance=self:GetAbility():GetForcedMinRange(integ)
					end
					
					self:DrawArrow(			
						center,
						center+fireDirection*distance ,
						delta,
						beam_projectile,
						beam_projectile
						
					)				
			end				
			
		end
	end
	self:SetStackCount(self:GetCastPoints())
	
	if self:IsComplete() then
		if not self:GetAbility():GetAutoCastState() then
			self:GetAbility():ToggleAutoCast()
		end	
	end
end

-----------------------------------------------------------------------------

function modifier_order_cast:DrawCone(center,startangle,cone_radius,beam_angle,beam_count)
				
			local beam_width = math.pi*cone_radius*beam_angle/(2*math.pi)			
			for i=0,beam_count-1 do
				
				local fireAngle = startangle-beam_angle*(beam_count-1)/2+beam_angle*i
				local fireDirection = Vector(math.sin(fireAngle),math.cos(fireAngle),0)
				local distance = cone_radius
				
				if self:GetAbility().GetForcedMinRange~=nil and distance<self:GetAbility():GetForcedMinRange(integ) then 
					distance=self:GetAbility():GetForcedMinRange(integ)
				elseif self:GetAbility().GetForcedMaxRange~=nil and distance>self:GetAbility():GetForcedMaxRange(integ) then 
					distance=self:GetAbility():GetForcedMaxRange(integ)
				end
				
				local nFXIndexBody = ParticleManager:CreateParticleForTeam( "particles/interface/msg_arrow_helper.vpcf", PATTACH_WORLDORIGIN, nil, self:GetParent():GetTeam() )
				ParticleManager:SetParticleControl( nFXIndexBody, 1, center );
				ParticleManager:SetParticleControl( nFXIndexBody, 2, center+fireDirection*distance );
				ParticleManager:SetParticleControl( nFXIndexBody, 3, Vector (beam_width,0,0) );
				ParticleManager:SetParticleControl( nFXIndexBody, 4, self:GetPlayerColor() );
				self:AddParticle( nFXIndexBody, false, false, -1, false, false )
			end	
		
end

-----------------------------------------------------------------------------

function modifier_order_cast:DrawArrow(pStart,pEnd,vDelta,wStart,wEnd)
			
			if self:IsHitScan() then
			
				local hitScanDirection = pEnd-pStart
			
				Projectiles:CreateProjectile({
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = pStart+Vector(0,0,100),
				  fDistance = hitScanDirection:Length2D(),
				  fStartRadius = wStart,
				  fEndRadius = wEnd,
				  fCollisionRadius = 30,
				  Source = self:GetParent(),
				  vVelocity = Vector(hitScanDirection.x,hitScanDirection.y,0), 
				  UnitBehavior = PROJECTILES_NOTHING,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_NOTHING,
				  GroundBehavior = PROJECTILES_DESTROY,
				  fGroundOffset = 0,
				  bZCheck = false,
				  bGroundLock = false,
				  draw = false,
				  --bUseFindUnitsInRadius = true,
									  bHitScan = true,

				  --UNUSED
				  --UnitTest = function(instance, unit) return unit:HasAbility("walker_dummy_unit") == false and unit:HasAbility("dummy_unit") == false and unit:GetTeamNumber() ~= keys.caster:GetTeamNumber() end,
				  --OnUnitHit = function(instance, unit) 
					
					--	self:OnProjectileHit(unit,instance:GetPosition())
					
				  --end,
				   --OnWallHit = function(instance, pos) 
				   OnFinish = function(instance, pos) 
					
						pEnd=instance:GetPosition()
					
				  end,
				})
		end
			
			local arrowhead = 100
			
			local nFXIndexBody = ParticleManager:CreateParticleForTeam( "particles/interface/msg_arrow_helper.vpcf", PATTACH_WORLDORIGIN, nil, self:GetParent():GetTeam() );
			ParticleManager:SetParticleControl( nFXIndexBody, 2, pStart+vDelta:Normalized()*arrowhead );
			ParticleManager:SetParticleControl( nFXIndexBody, 1, pEnd-vDelta:Normalized()*arrowhead );
			ParticleManager:SetParticleControl( nFXIndexBody, 3, Vector (wStart,wEnd,0) );
			ParticleManager:SetParticleControl( nFXIndexBody, 4, self:GetPlayerColor()	 );
			self:AddParticle( nFXIndexBody, false, false, -1, false, false )
			
			local nFXIndexHead = ParticleManager:CreateParticleForTeam( "particles/interface/msg_arrow_helper.vpcf", PATTACH_WORLDORIGIN, nil, self:GetParent():GetTeam() );
			ParticleManager:SetParticleControl( nFXIndexHead, 1, pEnd);
			ParticleManager:SetParticleControl( nFXIndexHead, 2, pEnd-vDelta:Normalized()*arrowhead );
			ParticleManager:SetParticleControl( nFXIndexHead, 3, Vector (wEnd+50,0,0) );
			ParticleManager:SetParticleControl( nFXIndexHead, 4, self:GetPlayerColor()	 );
			self:AddParticle( nFXIndexHead, false, false, -1, false, false )	
		
end

-----------------------------------------------------------------------------

function modifier_order_cast:IsHitScan()
	return self:GetAbility().RequiresHitScanCheck and self:GetAbility():RequiresHitScanCheck()==true
end

-----------------------------------------------------------------------------

function modifier_order_cast:IsSnapToGrid()
	return self:GetAbility().RequiresGridSnap and self:GetAbility():RequiresGridSnap()==true
end

-----------------------------------------------------------------------------

function modifier_order_cast:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function modifier_order_cast:Resolve()
	if GameMode:GetCurrentGamePhase() == GAMEPHASE_PREP then
		return self:PrepAction()
	elseif GameMode:GetCurrentGamePhase() == GAMEPHASE_DASH then
		return self:DashAction()
	elseif GameMode:GetCurrentGamePhase() == GAMEPHASE_ACTION then
		return self:SolveAction()
	elseif GameMode:GetCurrentGamePhase() == GAMEPHASE_WALK then
		return self:WalkAction()
	end		
	return MODIFIERACTION_NOPAUSE_SURVIVE
end
	
--------------------------------------------------------------------------------

function modifier_order_cast:PrepAction()
	
	if self:GetAbility()~=nil then
		
		if self:GetAbility().PrepAction~=nil then
			DebugPrint(self:GetName().." solve prep "..self:GetAbility():GetAbilityName())
			return self:GetAbility():PrepAction(self:GetCastInfo());
		end
		DebugPrint(self:GetAbility():GetAbilityName().." has no prep!")
		
	end	
	return MODIFIERACTION_NOPAUSE_SURVIVE;
end
	
--------------------------------------------------------------------------------

function modifier_order_cast:DashAction()
	
	if self:GetAbility()~=nil then
		
		if self:GetAbility().DashAction~=nil then
			DebugPrint(self:GetName().." solve dash "..self:GetAbility():GetAbilityName())
			return self:GetAbility():DashAction(self:GetCastInfo());
		end
		DebugPrint(self:GetAbility():GetAbilityName().." has no dash!")
	end	
	return MODIFIERACTION_NOPAUSE_SURVIVE;
end
	
--------------------------------------------------------------------------------

function modifier_order_cast:SolveAction()
	
	if self:GetAbility()~=nil then
		
		if self:GetAbility().SolveAction~=nil then
			DebugPrint(self:GetName().." solve action "..self:GetAbility():GetAbilityName())
			return self:GetAbility():SolveAction(self:GetCastInfo());
		end
		DebugPrint(self:GetAbility():GetAbilityName().." has no action!")
	end	
	return MODIFIERACTION_NOPAUSE_SURVIVE;
end
	
--------------------------------------------------------------------------------

function modifier_order_cast:WalkAction()
	
	if self:GetAbility()~=nil then
		
		if self:GetAbility().WalkMovementAction~=nil then
			DebugPrint(self:GetName().." solve action "..self:GetAbility():GetAbilityName())
			return self:GetAbility():WalkMovementAction(self:GetCastInfo());
		end
		DebugPrint(self:GetAbility():GetAbilityName().." has no action!")
	end	
	return MODIFIERACTION_NOPAUSE_SURVIVE;
end
	
--------------------------------------------------------------------------------

function modifier_order_cast:GetCastInfo()
		
	local castTable = {
		caster = self:GetParent(),
		ability = self:GetAbility(),
		target_point_A = self.orderCastPositions[0],
		target_point_B = self.orderCastPositions[1],
		target_point_C = self.orderCastPositions[2]
	}
	if self.orderCastTarget~=nil then
		castTable.target_unit = self.orderCastTarget:GetEntityIndex()
	end
	return castTable
end
	
--------------------------------------------------------------------------------

function modifier_order_cast:OnDestroy()
	if not IsServer() then return end
	if self:GetAbility():GetAutoCastState() then
		self:GetAbility():ToggleAutoCast()	
	end
		
	if self:GetAbility():IsItem() then
		self:GetAbility():SetCurrentCharges(1)
	end
end
	
--------------------------------------------------------------------------------