modifier_respawn_endofturn = class({})

--------------------------------------------------------------------------------

function modifier_respawn_endofturn:GetTexture()
	return "skeleton_king_reincarnation"
end

--------------------------------------------------------------------------------

function modifier_respawn_endofturn:GetAttributes()
	return MODIFIER_ATTRIBUTE_PERMANENT + MODIFIER_ATTRIBUTE_IGNORE_INVULNERABLE
end

--------------------------------------------------------------------------------

function modifier_respawn_endofturn:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_respawn_endofturn:RemoveOnDeath()
	return false
end
	
--------------------------------------------------------------------------------

function modifier_respawn_endofturn:GetPlayerColor()
	
	return GameMode.PlayerColors[self:GetParent():GetPlayerOwnerID()] or Vector(255,255,255);
end

--------------------------------------------------------------------------------

function modifier_respawn_endofturn:OnCreated( kv )

	self.respawnLocation=Vector(0,0,0)
	if IsServer() then 
		self.turn_of_death=GameMode:GetCurrentTurn()+1
		self.heromana=self:GetParent():GetMana() or 0
		self:ChangeRespawnLocation({center = self:GetParent():GetAbsOrigin(), force = true})
		
		local parent = self:GetParent()
		Timers:CreateTimer(0.15,function()
		
			if (not parent:IsAlive()) then
			
				
				self.tombstone = CreateUnitByName("npc_tombstone_dummy", parent:GetAbsOrigin() , false, parent, parent:GetPlayerOwner(), parent:GetTeamNumber())		
				self.tombstone:SetControllableByPlayer(parent:GetPlayerID(), true);
				self.tombstone:SetOwner(parent:GetPlayerOwner());				
				self.tombstone:FindAbilityByName("attribute_ward"):SetLevel(1)
				self.tombstone:SetSelectionOverride (parent)
			--else
			--print("modifier invalid")
			end		
		end)
	end
end

--------------------------------------------------------------------------------

function modifier_respawn_endofturn:IsValidRespawnLocation(point)

	if point==nil then return false end

	if GridNav:IsBlocked(point) or not GridNav:IsTraversable(point)  then
		return false
	end 

	--[[local minDistance = BASE_BUILD_DISTANCE	
	for _,structure in pairs(Entities:FindAllByClassname("npc_dota_creature")) do
	
      if IsValidEntity(structure) and structure:IsAlive() and structure:IsNull() == false and 
		structure:HasAbility("warlords_attribute_required_for_victory")  then

		
			local myDistance = VectorDistance(point, structure:GetAbsOrigin())
			if  myDistance<minDistance then
				local player = self:GetCaster():GetPlayerOwner();
				return UF_FAIL_CUSTOM
			end
		end
	end]]

	return true
end

--------------------------------------------------------------------------------

function modifier_respawn_endofturn:ChangeRespawnLocation( kv )

	if IsServer() then
	
		if not force then kv.center = Tileify(kv.center) end
	
		if not self:IsValidRespawnLocation(kv.center) and not kv.force and GameMode:GetCurrentGamePhase()~=GAMEPHASE_THINK then
			return
		end

		self.respawnLocation = kv.center
	
		if self.spawnparticle==nil then				
			self.spawnparticle = ParticleManager:CreateParticleForTeam( "particles/econ/events/league_teleport_2014/teleport_start_league_silver.vpcf", PATTACH_WORLDORIGIN, self:GetParent(),self:GetParent():GetTeamNumber() )			
			ParticleManager:SetParticleControl( self.spawnparticle, 2, self:GetPlayerColor() );
			self:AddParticle( self.spawnparticle, false, false, -1, false, true )
		end
		ParticleManager:SetParticleControl( self.spawnparticle, 0, self.respawnLocation  );			
	end
end
	
--------------------------------------------------------------------------------

function modifier_respawn_endofturn:ChangeRunLocation( kv )

	if GameMode:GetCurrentGamePhase()~=GAMEPHASE_THINK then
		return
	end

	self.orderMovePosition = Tileify(kv.center) 
	
	if IsServer() and GameMode:GetCurrentGamePhase()==GAMEPHASE_THINK  then
			
		self.dummyUnit = CreateUnitByName("npc_walker_dummy_unit", self.respawnLocation , true, nil, nil, DOTA_TEAM_NOTEAM)	
		self.dummyUnit:AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_absolute",{speed = DUMMY_DISPLAY_MOVEMENT_SPEED})	
		self.dummyUnit:FindAbilityByName("dummy_unit"):SetLevel(1)
		FindClearSpaceForUnit(self.dummyUnit, self.respawnLocation, false)
		
		if self.wparticle ~= nil then
			ParticleManager:DestroyParticle(self.wparticle ,true)
		end
		
		if self:GetParent():GetTeamNumber() == DOTA_TEAM_BADGUYS then
			self.wparticle  = ParticleManager:CreateParticleForTeam( "particles/econ/items/kunkka/divine_anchor/hero_kunkka_dafx_skills/kunkka_spell_x_spot_line_fxset.vpcf", PATTACH_ABSORIGIN_FOLLOW, self.dummyUnit,self:GetParent():GetTeamNumber() )
		else
			self.wparticle  = ParticleManager:CreateParticleForTeam( "particles/econ/items/kunkka/divine_anchor/hero_kunkka_dafx_skills/kunkka_spell_x_spot_line_red_fxset.vpcf", PATTACH_ABSORIGIN_FOLLOW, self.dummyUnit, self:GetParent():GetTeamNumber())
		end
		self:AddParticle( self.wparticle, false, false, -1, false, false )
		
		self.dummyPos = self.dummyUnit:GetAbsOrigin();
		self.walk_distance=0
		
		self.dummyUnit:SetHullRadius(self:GetParent():GetHullRadius()/4)
		self.dummyUnit:MoveToPosition(self.orderMovePosition);
			
			
			local castFrames = -1
			local moveDistance = self:GetRunDistance(  )
			
					Timers:CreateTimer(1/30, function()
										
										if self:IsNull() then return nil end
										
						if castFrames==-1 then
							castFrames=0
							self.dummyUnit:MoveToPosition(self.orderMovePosition);
						else
							local destroy = false
							if self.dummyUnit~=nil and not self.dummyUnit:IsNull() then
								local deltaVector = self.dummyUnit:GetAbsOrigin()-self.dummyPos
								
								if deltaVector:Length2D()>0 then
									self.walk_distance = self.walk_distance+deltaVector:Length2D()
									
									if self.walk_distance >= moveDistance then
										destroy = true
									end
								end							
								self.dummyPos = self.dummyUnit:GetAbsOrigin()
							else 
								return nil
							end
							
							if destroy then
								self.dummyUnit:Stop()
								return nil
							end
						end	
							return 1/30
					
					end);					
	end
end

--------------------------------------------------------------------------------

function modifier_respawn_endofturn:GetRunDistance(  )

	local parent = self:GetParent()
	if parent:IsRooted() then
		return 0
	end
	
	local ftime = MOVE_TIME_SHORT
	local fSpeed = HERO_BASE_MOVEMENTSPEED * self:GetParent():GetAgility() * (self:GetParent():GetIdealSpeed() / self:GetParent():GetBaseMoveSpeed())
  
	--[[		for _,modifierorder in pairs(parent:FindAllModifiers()) do				
				if modifierorder:GetName()=="modifier_order_cast" and modifierorder:GetAbility() ~= nil  then
				
					if modifierorder:GetAbility().isFreeAction~=nil and modifierorder:GetAbility():isFreeAction()==false and ftime==MOVE_TIME_SHORT then
						ftime=ftime/2
					end		
					if modifierorder:GetAbility().disablesMovement~=nil and modifierorder:GetAbility():disablesMovement()==true then
						return 0
					end
					if modifierorder:GetAbility().GetHasteBonus~=nil then
						fSpeed=fSpeed*modifierorder:GetAbility():GetHasteBonus()
					end
					
				end			
			end		]]				
		

	return (fSpeed*ftime)
end
	
--------------------------------------------------------------------------------

function modifier_respawn_endofturn:WalkMovementAction()
	
	if self:GetParent():IsAlive() then
		return MODIFIERACTION_NOPAUSE_SURVIVE;	
	end
	
	if GameMode:GetCurrentTurn() == self.turn_of_death then
		
		self.spawnparticle = ParticleManager:CreateParticle( "particles/econ/events/league_teleport_2014/teleport_end_league_silver.vpcf", PATTACH_WORLDORIGIN, self:GetParent() )		
		ParticleManager:SetParticleControl( self.spawnparticle, 0, self.respawnLocation )
		ParticleManager:SetParticleControl( self.spawnparticle, 1, self.respawnLocation )
		ParticleManager:SetParticleControl( self.spawnparticle, 2, self:GetPlayerColor());			
		self:AddParticle( self.spawnparticle, false, false, -1, false, true )
		
		if self.tombstone ~= nil and (not self.tombstone:IsNull()) and self.tombstone:IsAlive() then		
			self.tombstone:SetAbsOrigin(self.respawnLocation)
		end
		
	elseif GameMode:GetCurrentTurn() > self.turn_of_death then

		if self.spawnparticle~=nil then
			ParticleManager:DestroyParticle(self.spawnparticle,false)	
		end
		
		if self.tombstone ~= nil and (not self.tombstone:IsNull()) and self.tombstone:IsAlive() then
		
			self.tombstone:Destroy()
		end

			local nFXIndex = ParticleManager:CreateParticle( "particles/econ/items/tinker/boots_of_travel/teleport_end_bots_ground_flash.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )

				ParticleManager:SetParticleControl( nFXIndex, 0, self.respawnLocation )
				ParticleManager:SetParticleControl( nFXIndex, 2, self:GetPlayerColor() );
				ParticleManager:ReleaseParticleIndex( nFXIndex )
	
			self:GetParent():RespawnHero(false, false)	
			FindClearSpaceForUnit(self:GetParent(), self.respawnLocation, false)
				
				if self.orderMovePosition~=nil then
						
					local walkmodifier = self:GetParent():AddNewModifier(self:GetParent(),nil,"modifier_movement",{duration=MOVE_TIME_SHORT})
					local hero = self:GetParent()
					hero:RemoveModifierByName("generic_modifier_scarred")
					Timers:CreateTimer(1/30, function()
						hero:RemoveModifierByName("generic_modifier_scarred")
						hero:SetMana(self.heromana)
						hero:SetHealth(hero:GetMaxHealth())
						walkmodifier:InsertPoint(self.orderMovePosition);
						
										
						Timers:CreateTimer(MOVE_TIME_SHORT, function()
										hero:Stop();	
										return nil;
										end);
										
						 for i=1, 6 do
							local abil = hero:GetAbilityByIndex(i)
							if abil ~= nil then
							  
								hero:AddNewModifier(hero,abil,"modifier_ability_cooldown",{})
							  
							end
						  end
						end);
				
				end	
		return MODIFIERACTION_PAUSE_DESTROY;
	end
	return MODIFIERACTION_NOPAUSE_SURVIVE;		
end
	
--------------------------------------------------------------------------------