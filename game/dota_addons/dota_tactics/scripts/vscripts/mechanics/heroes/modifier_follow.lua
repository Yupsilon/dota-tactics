modifier_follow = class({})

--------------------------------------------------------------------------------

function modifier_follow:OnCreated(kv)	
	if IsServer() then
		self.moveSpeed = self:GetParent():GetIdealSpeed()
		
		if self:GetParent():IsHero() then
			self.moveSpeed = HERO_BASE_MOVEMENTSPEED * self:GetParent():GetAgility() * self:GetParent():GetIdealSpeed() / self:GetParent():GetBaseMoveSpeed()
		end
		
		self:GetParent():SetHullRadius(self:GetParent():GetHullRadius()/4)
		FindClearSpaceForUnit(self:GetParent(), GetGroundPosition(self:GetParent():GetAbsOrigin(),self:GetParent()), true)
		
		self.alphapoint = self:GetParent():GetAbsOrigin()
		self.omegapoint = Vector (kv.x,kv.y,0)
		self.state = -1
		
		self:StartIntervalThink(1/3)
		self:OnIntervalThink()
	end
end

-----------------------------------------------------------------------------

function modifier_follow:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end


--------------------------------------------------------------------------------

function modifier_follow:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_follow:CheckBrush()
	if not IsPointOverBrush(self:GetParent():GetAbsOrigin()) then
		self:GetParent():RemoveModifierByName("modifier_brush_invisibility")
	end
end

--------------------------------------------------------------------------------

function modifier_follow:OnIntervalThink()
		
	if self:GetParent():CanEntityBeSeenByMyTeam(self:GetCaster()) then
		
		self.omegapoint = self:GetCaster():GetAbsOrigin()
		
		if self.state ~= 1 then
			self:GetParent():MoveToNPC(self:GetCaster())
			self.state = 1
		end
	
	else
		
		if self.state ~= 0 then
			self:GetParent():MoveToPosition(self.omegapoint);
			self.state = 0
		end
	
	end
	
	--self:CheckBrush()
	self.alphapoint = self:GetParent():GetAbsOrigin()
end

--------------------------------------------------------------------------------

function modifier_follow:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE
	}	

	return funcs
end

--------------------------------------------------------------------------------

function modifier_follow:CheckState()
	local state = {
	[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}

	return state
end

--------------------------------------------------------------------------------

function modifier_follow:GetModifierMoveSpeed_Absolute()	
	return self.moveSpeed
end

--------------------------------------------------------------------------------

function modifier_follow:OnDestroy( kv )
	if IsServer() then	
		--self:CheckBrush()
		self:GetParent():Stop()		
		self:GetParent():SetHullRadius(self:GetParent():GetHullRadius()*4)
		FindClearSpaceForUnit(self:GetParent(), Tileify(self:GetParent():GetAbsOrigin()), false)
	end
end
	
--------------------------------------------------------------------------------