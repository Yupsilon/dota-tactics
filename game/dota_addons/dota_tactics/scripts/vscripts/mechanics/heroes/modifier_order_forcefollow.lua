modifier_order_forcefollow = class({})

--------------------------------------------------------------------------------

function modifier_order_forcefollow:OnCreated(kv)
	
	if IsServer() then
	
		self.solved=false		
		if kv.unit then self:SetFollowTarget( kv.unit) end
		if self:IsCrowdControl() and self:GetParent():HasModifier("generic_modifier_combobreaker") then	
			--self:GetParent():AddNewModifier(self:GetCaster(),self:GetAbility(),"generic_modifier_movespeed_slow",{turns=1})
			self.solved=true
		end
	end
end
--------------------------------------------------------------------------------

function modifier_order_forcefollow:IsHidden()
	return true
end
--------------------------------------------------------------------------------

function modifier_order_forcefollow:IsCrowdControl()
	return self:GetParent()~=self:GetCaster()
end

--------------------------------------------------------------------------------

function modifier_order_forcefollow:Resolve()
	if GameMode:GetCurrentGamePhase() == GAMEPHASE_WALK then
		self:WalkMovementAction()
	end		
end

--------------------------------------------------------------------------------

function modifier_order_forcefollow:GetFollowTarget(  )

	return self.followTarget
end

--------------------------------------------------------------------------------

function modifier_order_forcefollow:SetFollowTarget( target )

	self.followTarget = target
	self.lastfollowpoint =  target:GetAbsOrigin() 
end
	
--------------------------------------------------------------------------------

function modifier_order_forcefollow:WalkMovementAction()
	
	if self:IsCrowdControl() then
		if (self:GetParent():HasModifier("generic_modifier_unstoppable") or self.solved == true or (not self:GetParent():IsAlive())) then return MODIFIERACTION_NOPAUSE_DESTROY end
		
		self:GetParent():RemoveModifierByName("generic_modifier_stealth")
		if not self:GetParent():HasModifier("generic_modifier_combobreaker") then			
			self:GetParent():AddNewModifier(self:GetParent(),self:GetAbility(),"generic_modifier_combobreaker",{turns=2})
		end
	end
		
		self:GetParent():RemoveModifierByName("modifier_movement")
		self:GetParent():RemoveModifierByName("modifier_follow")
		self:GetParent():RemoveModifierByName("modifier_order_move")				
		self:GetParent():Interrupt()
		self.solved=true
		
		local walk_time = MOVE_TIME_SHORT;		
		if self:GetFollowTarget()~= nil then			
		
			if self:GetParent():CanEntityBeSeenByMyTeam(self:GetFollowTarget()) and self:GetFollowTarget():IsAlive() then
				local move_modifier = self:GetParent():AddNewModifier(self:GetFollowTarget(),nil,"modifier_follow",{duration=walk_time, x = self.lastfollowpoint.x, y = self.lastfollowpoint.y})
				move_modifier.moveSpeed = move_modifier.moveSpeed / 2
			end
		end
		
	return MODIFIERACTION_PAUSE_DESTROY;
end
	