function GameMode:InitializeTurnBasedTactics()
	
	DebugPrint("DOTA_TACTICS: initialized");
	
	self.currentGamePhase = GAMEPHASE_PREGAME;
	self:SetAllHeroesReady(false)	
	self:InitKeyValues()
	self:InitRuneSpawns()
	
	self.AllowedHeroes=1
	self.GameSpeed=1
	self.TurnSpeed=1
	 
	self.inProgressOrders = { }
	
		Timers:CreateTimer(.25, function()
							if self:GetAllHeroesReady() == true or GameRules:State_Get() >= DOTA_GAMERULES_STATE_GAME_IN_PROGRESS then
								self:GoToNextGamePhase()	
								return nil;
							end
							return .25
							end);
							
end

function GameMode:GetCurrentGamePhase()	
	return self.currentGamePhase or 0;
end

function GameMode:GetCurrentTurn()	
	return self.currentTurn or 1;
end

function GameMode:SetAllHeroesReady(value)	
	self.allHeroesReady = value;
end

function GameMode:GetAllHeroesReady()	
	return self.allHeroesReady
end

function GetModifierDestroyed(modifierID)
	return modifierID == MODIFIERACTION_PAUSE_DESTROY or modifierID == MODIFIERACTION_SHORTPAUSE_DESTROY or modifierID == MODIFIERACTION_NOPAUSE_DESTROY
end

function GetModifierPause(modifierID)
	if modifierID == MODIFIERACTION_PAUSE_DESTROY or modifierID == MODIFIERACTION_PAUSE_SURVIVE then
		return 2	--long pause
	elseif modifierID == MODIFIERACTION_SHORTPAUSE_SURVIVE or modifierID == MODIFIERACTION_SHORTPAUSE_DESTROY then
		return 1	--short pause
	end
	return 0
end

function GameMode:GoToNextGamePhase()	
	
	if not IsServer() then
		return
	end
	
	if self:GetCurrentGamePhase() == GAMEPHASE_PREGAME then
		--Tutorial:ForceGameStart();
		GameRules:ForceGameStart()
	end
	
	DebugPrint("DOTA_TACTICS: changing game phase from "..self.currentGamePhase);
	self.currentGamePhase = self:GetCurrentGamePhase() + 1
	local CurrentTurn = self:GetCurrentTurn()
		
	if self.currentGamePhase > GAMEPHASE_WALK then
		
		for _,actor in pairs(HeroList:GetAllHeroes()) do	
		  if IsValidEntity(actor) and actor:IsNull() == false then			  
			for _,modifierorder in pairs(actor:FindAllModifiers()) do				
				if modifierorder.EndOfTurnAction~=nil then
						local integ = modifierorder:EndOfTurnAction()
						if SINGLEPLAYER_DEBUG then print("EoT action "..modifierorder:GetName()) end
						if GetModifierDestroyed(integ)  then
							modifierorder:Destroy();											
						end
					end
				end			
			end						
		end
		
		--VICTORY WINNING CONDITIONS
		if self:GetCurrentTurn() >= GAME_GAMEOVER_TURNS or GetTeamHeroKills(DOTA_TEAM_GOODGUYS)>=GAME_GAMEOVER_KILLS or GetTeamHeroKills(DOTA_TEAM_BADGUYS)>=GAME_GAMEOVER_KILLS then
		
			if GetTeamHeroKills(DOTA_TEAM_BADGUYS)>GetTeamHeroKills(DOTA_TEAM_GOODGUYS) then
				GameRules:SetGameWinner( DOTA_TEAM_BADGUYS )
			elseif GetTeamHeroKills(DOTA_TEAM_BADGUYS)<GetTeamHeroKills(DOTA_TEAM_GOODGUYS) then
				GameRules:SetGameWinner( DOTA_TEAM_GOODGUYS )
			end			
			--Timers:CreateTimer(10,function()
			--	GameRules:ResetToCustomGameSetup();
			-- end)
		end
		
		self:ReviseRuneSpawners()
		self.currentGamePhase = GAMEPHASE_THINK;
		self.currentTurn = self:GetCurrentTurn()+1;
		
		--Notifications:ClearBottomFromAll()
		--Notifications:BottomToAll({text="#phasename_turn", duration=99999})
		--Notifications:BottomToAll({text=tostring(self:GetCurrentTurn()), continue=true, duration=99999})
		
	end
	
	Notifications:ClearTopFromAll()
	
	if self.currentGamePhase == GAMEPHASE_THINK then 
	
		if self:GetCurrentTurn()==1 then		
			for _,actor in pairs(HeroList:GetAllHeroes()) do	
				if actor:GetAbilityPoints()>=ABILITY_POINTS then
					for i=0, 5 do
						local abil = actor:GetAbilityByIndex(i)
						if abil ~= nil then		  
							abil:SetLevel(3)		  
						end
					end
				else
					for i=0, 5 do
						local abil = actor:GetAbilityByIndex(i)
						if abil ~= nil and abil.onStartGame then
							abil:onStartGame()
						end
					end
				end
				actor:SetAbilityPoints(0)
				
				for i=DOTA_ITEM_SLOT_1, TP_SCROLL_SLOT do
					local abil = actor:GetItemInSlot(i)
					if abil ~= nil then
						if (abil:GetAbilityName() == "item_respec" or abil:GetAbilityName() == "item_changehero") then		
							if abil.onStartGame then
								abil:onStartGame()
							end
							abil:Destroy()
						else--if abil:GetBehavior() > 0 then
							
							actor:AddNewModifier(actor,abil,"modifier_ability_cooldown",{})
							abil.LastCastTurn = 2;
						end
					end
				end
			end
		end
	
		self:SetAllHeroesReady(false)	
		
		for _,actor in pairs(HeroList:GetAllHeroes()) do	
			if IsValidEntity(actor) and actor:IsNull() == false then			  
				if actor:HasModifier("modifier_indicator_ready") then
					actor:FindModifierByName("modifier_indicator_ready"):SetStackCount(1)
				end
			end
		end
		
		local top_duration = 3
		Notifications:TopToAll({text="#phasename_think", duration=top_duration})
		local timePassed = TURN_TIME
		Timers:CreateTimer(top_duration, function()
							timePassed=timePassed-1
							if self:GetAllHeroesReady() == true or timePassed<=0 then
								self:GoToNextGamePhase()	
								return nil;
							end
							
							--Notifications:ClearTopFromAll()
							--Notifications:TopToAll({text="#phasename_think", duration=1})
							Notifications:TopToAll({text=tostring(timePassed), continue=true, duration=.9})
							
							
							return 1
							end);
							
		Notifications:ClearBottomFromAll()
		
		if self:GetCurrentTurn()<=GAME_GAMEOVER_TURNS then
		
			local vcolor = "#FFFFFF"
		
			if self:GetCurrentTurn()>GAME_GAMEOVER_TURNS-3 then
				vcolor = "#FB2943"
			end
		
			Notifications:BottomToAll({text="#phasename_turn", duration=99999})
			Notifications:BottomToAll({text=tostring(self:GetCurrentTurn()), continue=true, duration=99999,style={color=vcolor}})
		else
			Notifications:BottomToAll({text="#phasename_suddendeath", duration=99999,style={color="#FB2943"}})		
		end
						
							
	elseif self.currentGamePhase == GAMEPHASE_PREP then 	
	
		Notifications:TopToAll({text="#phasename_prep", style={color="#5BB23C"},duration=PLAYER_TIME_SHORT*10})
		
		local ordertable = {}
		local orderindex = 0
		
		for _,actor in pairs(HeroList:GetAllHeroes()) do
			  if IsValidEntity(actor) and actor:IsNull() == false then	
						
						if actor:IsHero() and not actor:HasModifier("modifier_order_cast") and not actor:HasModifier("modifier_order_move") then
						
							local idleRegen = actor:GetStrength()*REGEN_IDLE_CONSTANT
							
							--[[Alert_Damage({
									victim = actor,
									attacker = actor,
									damage = 0,
									damage_type = DAMAGE_TYPE_PURE,
									ability  = nil,
									scar_damage_absolute = idleRegen,
									cover_reduction = 1
								})]]
							Alert_Heal({caster=actor,target=actor,value=idleRegen})							
						end
					for iOrder = 0,5 do		

						local modifiers = actor:FindAllModifiers()
					
						for _,modifierorder in pairs(modifiers) do	

							local ipriority = 3
							
							if modifierorder.GetPriority ~= nil then
								ipriority = modifierorder:GetPriority()
							end
							
							if (modifierorder.handled_prep==nil or modifierorder.handled_prep<self:GetCurrentTurn()) and ipriority==iOrder then
				  			
								if modifierorder.PrepAction~=nil then
								
									ordertable[orderindex] = modifierorder
									orderindex=orderindex+1
									
									if modifierorder:GetAbility()~=nil and modifierorder:GetName() == "modifier_order_cast" and modifierorder:GetAbility().isFreeAction~=nil and modifierorder:GetAbility():isFreeAction()==false then
										actor.takesnoactions=self:GetCurrentTurn();
										if modifierorder:GetAbility().removesInvisible==nil or modifierorder:GetAbility():removesInvisible()==false then
											actor:RemoveModifierByName("generic_modifier_invisible")
										end
									end
									if modifierorder.IsIncomplete~=nil and modifierorder:IsIncomplete() then
										modifierorder:AutoComplete()
									end
								end									
							end
						end
					end						
				end
			end
					
		Timers:CreateTimer(TURN_TIME_TRANSITION, function()
		
			for _,modifierorder in pairs(ordertable) do		
							
				if (modifierorder.handled_prep==nil or modifierorder.handled_prep<self:GetCurrentTurn()) then
							
					local integ = modifierorder:PrepAction()
					if SINGLEPLAYER_DEBUG then print("Prep action "..modifierorder:GetName().." at turn "..self:GetCurrentTurn()) end
					modifierorder.handled_prep=self:GetCurrentTurn()	
					
					if GetModifierDestroyed(integ) then
						modifierorder:Destroy();	
					end	 
					if GetModifierPause(integ)==2 then
						return PLAYER_TIME_SHORT;
					elseif GetModifierPause(integ)==1 then
						return PLAYER_TIME_SUPERSHORT;
					end	
				end	
			end
			
			self:GoToNextGamePhase()
			return nil
		end);
		
	elseif self.currentGamePhase == GAMEPHASE_DASH then 	
	
		Notifications:TopToAll({text="#phasename_dash",style={color="#F4DB3B"}, duration=PLAYER_TIME_SIMULTANIOUS})
		
		local turntime= TURN_TIME_TRANSITION
							
		for _,actor in pairs(HeroList:GetAllHeroes()) do	
		  if IsValidEntity(actor) and actor:IsNull() == false then		  
			for _,modifierorder in pairs(actor:FindAllModifiers()) do				
				if modifierorder.DashAction~=nil then 
				
					local integ = modifierorder:DashAction()
						if SINGLEPLAYER_DEBUG then print("Dash action "..modifierorder:GetName()) end
					if modifierorder:GetAbility()~=nil and modifierorder:GetName() == "modifier_order_cast" and modifierorder:GetAbility().isFreeAction~=nil and modifierorder:GetAbility():isFreeAction()==false then
						actor.takesnoactions=self:GetCurrentTurn();
						
						if modifierorder:GetAbility().removesInvisible==nil or modifierorder:GetAbility():removesInvisible()==false then
							actor:RemoveModifierByName("generic_modifier_invisible")
						end
					end
					if GetModifierDestroyed(integ) then
						modifierorder:Destroy();											
					end
					if GetModifierPause(integ)>0 then
						turntime=PLAYER_TIME_SIMULTANIOUS
					end
					end
				end			
			end						
		end
		
		Timers:CreateTimer(turntime, function()
							self:GoToNextGamePhase()	
							return nil;
							end);
		
	elseif self.currentGamePhase == GAMEPHASE_ACTION then 	
		
		Notifications:TopToAll({text="#phasename_action", style={color="#FB2943"},duration=PLAYER_TIME_SHORT*10})
		
		local ordertable = {}
		local orderindex = 0
		
		for iOrder = 0,5 do
				for _,actor in pairs(HeroList:GetAllHeroes()) do	
				  if IsValidEntity(actor) and actor:IsNull() == false then		

						local modifiers = actor:FindAllModifiers()
					
						for _,modifierorder in pairs(modifiers) do	

							local ipriority = 3
							
							if modifierorder.GetPriority ~= nil then
								ipriority = modifierorder:GetPriority()
							end
							
							if (modifierorder.handled_action==nil or modifierorder.handled_action<self:GetCurrentTurn()) and ipriority==iOrder then
				  			
								if modifierorder ~= nil and modifierorder.SolveAction~=nil then
									ordertable[orderindex] = modifierorder
									orderindex=orderindex+1
									
								end									
							end
						end
					end						
				end
			end
					
		Timers:CreateTimer(TURN_TIME_TRANSITION, function()
		
			for _,modifierorder in pairs(ordertable) do		
							
				if (modifierorder ~= nil and modifierorder.handled_action==nil or modifierorder.handled_action<self:GetCurrentTurn()) then
							
					local integ = modifierorder:SolveAction()
					if SINGLEPLAYER_DEBUG then print("Solve action "..modifierorder:GetName()) end
					modifierorder.handled_action=self:GetCurrentTurn()
									
									if modifierorder:GetAbility()~=nil and modifierorder:GetName() == "modifier_order_cast" and modifierorder:GetAbility().isFreeAction~=nil and modifierorder:GetAbility():isFreeAction()==false then
										modifierorder:GetParent().takesnoactions=self:GetCurrentTurn();
										if modifierorder:GetAbility().removesInvisible==nil or modifierorder:GetAbility():removesInvisible()==false then
											modifierorder:GetParent():RemoveModifierByName("generic_modifier_invisible")
										end
									end
					
					if GetModifierDestroyed(integ) then
						modifierorder:Destroy();	
					end	 
					if GetModifierPause(integ)==2 then
						return PLAYER_TIME_SHORT;
					elseif GetModifierPause(integ)==1 then
						return PLAYER_TIME_SUPERSHORT;
					end	
				end	
			end
			
			self:GoToNextGamePhase()
			return nil
		end);
		
	elseif self.currentGamePhase == GAMEPHASE_WALK then 

		local turntime= TURN_TIME_TRANSITION
		Notifications:TopToAll({text="#phasename_walk", duration=MOVE_TIME})	
							
		for _,actor in pairs(HeroList:GetAllHeroes()) do	
			if IsValidEntity(actor) and actor:IsNull() == false then		  
				for _,modifierorder in pairs(actor:FindAllModifiers()) do				
					if modifierorder ~= nil and modifierorder.WalkMovementAction~=nil then			
						local integ = modifierorder:WalkMovementAction()
						if SINGLEPLAYER_DEBUG then print("Walk action "..modifierorder:GetName()) end
						
						if GetModifierDestroyed(integ) then
							modifierorder:Destroy();
						end		
						if GetModifierPause(integ)>0 then
							turntime=MOVE_TIME		
						end
					end
				end
			end
		end		
		Timers:CreateTimer(turntime, function()
							self:GoToNextGamePhase()	
							return nil;
							end);					
	end
		--self.currentGamePhase=nextGamePhase
end