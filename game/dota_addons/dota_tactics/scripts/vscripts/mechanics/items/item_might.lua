item_might = class({})
LinkLuaModifier( "generic_modifier_might","abilities/generic/generic_modifier_might.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_destroy_item","mechanics/items/modifier_destroy_item.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function item_might:PrepAction(keys)

	self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_might",{turns=self:GetSpecialValueFor("might_duration"),value=self:GetSpecialValueFor("might_power")})	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_destroy_item",{})	
	self:SetActivated(false)
			
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function item_might:IsMuted()
	return self:GetCurrentCharges()==0;
end

--------------------------------------------------------------------------------

function item_might:getAbilityType()
	return -1;
end

--------------------------------------------------------------------------------

function item_might:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function item_might:GetPriority()
	return 0;
end

--------------------------------------------------------------------------------

function item_might:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function item_might:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function item_might:GetCooldown(level)	
	return 0
end

--------------------------------------------------------------------------------