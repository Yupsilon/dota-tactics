item_blinky = class({})
LinkLuaModifier( "modifier_destroy_item","mechanics/items/modifier_destroy_item.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_dashed_this_turn","abilities/generic/generic_modifier_dashed_this_turn.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function item_blinky:DashAction(keys)

	if keys.target_point_A~=nil then
					
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_dashed_this_turn",{turns = 1})
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_destroy_item",{})	
			self:SetActivated(false)
			
			local startPoint = self:GetCaster():GetAbsOrigin();
			local endPoint = keys.target_point_A		
			
			local blinkStart = ParticleManager:CreateParticle("particles/units/heroes/hero_antimage/antimage_blink_start.vpcf", PATTACH_WORLDORIGIN, caster)
			ParticleManager:SetParticleControl(blinkStart, 0, startPoint)		
			ParticleManager:ReleaseParticleIndex(blinkStart)	
							
			--Timers:CreateTimer(0.1,function()				
				FindClearSpaceForUnit(self:GetCaster(), endPoint, false)				
						
				EmitSoundOn( "DOTA_Item.BlinkDagger.Activate", self:GetCaster() )
				StartAnimation(self:GetCaster(), {duration=2, activity=ACT_DOTA_TELEPORT_END})	
			
				local blinkEnd = ParticleManager:CreateParticle("particles/units/heroes/hero_antimage/antimage_blink_end.vpcf", PATTACH_WORLDORIGIN, caster)
				ParticleManager:SetParticleControl(blinkEnd, 0, self:GetCaster():GetAbsOrigin())		
				ParticleManager:ReleaseParticleIndex(blinkEnd)	
			--end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function item_blinky:RequiresHitScanCheck()
	return true;
end

--------------------------------------------------------------------------------

function item_blinky:RequiresGridSnap()
	return false;
end

--------------------------------------------------------------------------------

function item_blinky:IsMuted()
	return self:GetCurrentCharges()==0
end

--------------------------------------------------------------------------------

function item_blinky:GetBehavior()
	if GameRules:State_Get() <= 7 then
		return DOTA_ABILITY_BEHAVIOR_NO_TARGET
	end
	return DOTA_ABILITY_BEHAVIOR_POINT
end

--------------------------------------------------------------------------------

function item_blinky:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function item_blinky:disablesMovement()
	return true;
end

--------------------------------------------------------------------------------

function item_blinky:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function item_blinky:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function item_blinky:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function item_blinky:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function item_blinky:GetConeArc(coneID,coneRange)
	return self:GetCaster():GetHullRadius()
end

--------------------------------------------------------------------------------

function item_blinky:GetMinRange(coneID)	
	return 100
end

--------------------------------------------------------------------------------

function item_blinky:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("blinky_range")
end

--------------------------------------------------------------------------------

function item_blinky:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------