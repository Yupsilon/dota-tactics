item_cooldown_lesser = class({})
LinkLuaModifier( "modifier_destroy_item","mechanics/items/modifier_destroy_item.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function item_cooldown_lesser:SolveAction(keys)

	local refresh_abil = nil
	local next_turn = -1

	for abi = 0, 6, 1	do	
		local locabi = self:GetCaster():GetAbilityByIndex(abi)
		if locabi~=nil and locabi.GetCooldownTimeRemaining and locabi:GetCooldownTimeRemaining() > next_turn then
			refresh_abil = locabi
			next_turn = locabi:GetCooldownTimeRemaining()
		end
	end
	
	if refresh_abil~=nil then
		
		Alert_CooldownRefresh(self:GetCaster(),refresh_abil,-self:GetSpecialValueFor("refres_duration"))			
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_destroy_item",{})	
		self:SetActivated(false)

		local blinkStart = ParticleManager:CreateParticle("particles/items2_fx/refresher.vpcf", PATTACH_WORLDORIGIN, caster)
		ParticleManager:SetParticleControl(blinkStart, 0, self:GetCaster():GetAbsOrigin())		
		ParticleManager:ReleaseParticleIndex(blinkStart)	
			
		EmitSoundOn( "DOTA_Item.Refresher.Activate", self:GetCaster() )
	end
			
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function item_cooldown_lesser:IsMuted()
	return self:GetCurrentCharges()==0
end

--------------------------------------------------------------------------------

function item_cooldown_lesser:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function item_cooldown_lesser:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function item_cooldown_lesser:GetPriority()
	return 5;
end

--------------------------------------------------------------------------------

function item_cooldown_lesser:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function item_cooldown_lesser:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function item_cooldown_lesser:GetCooldown(level)	
	return 0
end

--------------------------------------------------------------------------------