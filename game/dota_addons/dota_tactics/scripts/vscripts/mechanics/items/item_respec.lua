LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
item_respec = class({})

--------------------------------------------------------------------------------

function item_respec:HandleTactical()	
	
	for i=0, 5 do
    local abil = self:GetCaster():GetAbilityByIndex(i)
    if abil ~= nil then
      if abil:GetLevel() > 1 then
        abil:SetLevel(1)
      end
    end
  end
  self:GetCaster():SetAbilityPoints(ABILITY_POINTS)
end

--------------------------------------------------------------------------------

function item_respec:getAbilityType()		
	return -1
end

--------------------------------------------------------------------------------

function item_respec:onStartGame()		
	for i=DOTA_ITEM_SLOT_1, TP_SCROLL_SLOT do
	
		local abil = self:GetCaster():GetItemInSlot(i)
		if abil ~= nil then
		
							
							self:GetCaster():AddNewModifier(self:GetCaster(),abil,"modifier_ability_cooldown",{})
							abil.LastCastTurn = 2;
					end
				end
end

--------------------------------------------------------------------------------
