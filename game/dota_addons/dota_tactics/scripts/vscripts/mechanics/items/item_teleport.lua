item_teleport = class({})
LinkLuaModifier( "modifier_destroy_item","mechanics/items/modifier_destroy_item.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_dashed_this_turn","abilities/generic/generic_modifier_dashed_this_turn.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function item_teleport:DashAction(keys)

	local target_unit = EntIndexToHScript(keys.target_unit)
	if target_unit~=nil and keys.target_point_B~=nil then
					
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_dashed_this_turn",{turns = 1})
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_destroy_item",{})	
			self:SetActivated(false)
			
			local startPoint = self:GetCaster():GetAbsOrigin()	
			
			--Timers:CreateTimer(0.1,function()				
			
				local blinkStart = ParticleManager:CreateParticle("particles/econ/events/ti5/blink_dagger_start_ti5.vpcf", PATTACH_WORLDORIGIN, caster)
				ParticleManager:SetParticleControl(blinkStart, 0, startPoint)		
				ParticleManager:ReleaseParticleIndex(blinkStart)	
				
				FindClearSpaceForUnit(self:GetCaster(), keys.target_point_B, true)				
				keys.caster:FaceTowards(keys.target_point_A)	
						
				EmitSoundOn( "DOTA_Item.BlinkDagger.Activate", self:GetCaster() )
				StartAnimation(self:GetCaster(), {duration=2, activity=ACT_DOTA_TELEPORT_END})	
			
				local blinkEnd = ParticleManager:CreateParticle("particles/econ/events/ti5/blink_dagger_end_ti5.vpcf", PATTACH_WORLDORIGIN, caster)
				ParticleManager:SetParticleControl(blinkEnd, 0, self:GetCaster():GetAbsOrigin())		
				ParticleManager:ReleaseParticleIndex(blinkEnd)	
				self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_shield",{turns=self:GetSpecialValueFor("regen_duration"),value=self:GetSpecialValueFor("regen_health")})
			--end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function item_teleport:GetBehavior()
	if GameRules:State_Get() <= 7 then
		return DOTA_ABILITY_BEHAVIOR_NO_TARGET
	end
	return DOTA_ABILITY_BEHAVIOR_OPTIONAL_POINT + DOTA_ABILITY_BEHAVIOR_UNIT_TARGET
end

--------------------------------------------------------------------------------

function item_teleport:IsMuted()
	return self:GetCurrentCharges()==0
end

--------------------------------------------------------------------------------

function item_teleport:RequiresHitScanCheck()
	return false;
end

--------------------------------------------------------------------------------

function item_teleport:RequiresGridSnap()
	return true;
end

--------------------------------------------------------------------------------

function item_teleport:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function item_teleport:GetCastPoints()
	return 2;
end

--------------------------------------------------------------------------------

function item_teleport:RequireFirstTargetUnit()
	return true;
end

--------------------------------------------------------------------------------

function item_teleport:disablesMovement()
	return true;
end

--------------------------------------------------------------------------------

function item_teleport:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function item_teleport:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function item_teleport:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function item_teleport:GetConeArc(coneID,coneRange)
	return self:GetCaster():GetHullRadius()
end

--------------------------------------------------------------------------------

function item_teleport:GetMinRange(coneID)	
	if coneID==1 then
		return self:GetSpecialValueFor("jump_range")
	end
	
	return 100
end

--------------------------------------------------------------------------------

function item_teleport:GetMaxRange(coneID)

	if coneID==1 then
	return self:GetSpecialValueFor("jump_range")
	end
	return self:GetSpecialValueFor("dash_range")
end

--------------------------------------------------------------------------------

function item_teleport:GetCastRange()	
	return self:GetSpecialValueFor("dash_range")
end

--------------------------------------------------------------------------------

function item_teleport:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function item_teleport:ChangesCasterPosition()
	return 1;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------