item_ward = class({})
LinkLuaModifier( "generic_modifier_summonedunit","abilities/generic/generic_modifier_summonedunit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ward_reveal","mechanics/items/modifier_ward_reveal.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_temp_reveal","mechanics/heroes/modifier_temp_reveal.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_destroy_item","mechanics/items/modifier_destroy_item.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function item_ward:SolveAction(keys)

	if keys.target_point_A~=nil then
					
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_destroy_item",{})	
			self:SetActivated(false)
			
			
	
			local ward_unit = ( CreateUnitByName("npc_ward_vision_invulnerable", keys.target_point_A , true, self:GetCaster(), self:GetCaster(), self:GetCaster():GetTeamNumber() ))
			ward_unit:SetControllableByPlayer(self:GetCaster():GetPlayerOwnerID(), true)
			ward_unit:SetOwner(self:GetCaster())
			
			local main_modifier = self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_temp_reveal",{ range = self:GetSpecialValueFor("ward_sight")})		
									
			local aiModifier = self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_summonedunit",{turns = self:GetSpecialValueFor("ward_life"),destroyonend=1})
			aiModifier:AssignUnit(main_modifier.AssignedUnit)
						
			ward_unit:FindAbilityByName("dummy_unit"):SetLevel(1)
	end
			
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function item_ward:GetBehavior()
	if GameRules:State_Get() < 7 then
		return DOTA_ABILITY_BEHAVIOR_NO_TARGET
	end
	return DOTA_ABILITY_BEHAVIOR_POINT
end

--------------------------------------------------------------------------------

function item_ward:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function item_ward:IsMuted()
	return self:GetCurrentCharges()==0
end

--------------------------------------------------------------------------------

function item_ward:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function item_ward:GetPriority()
	return 0;
end

--------------------------------------------------------------------------------

function item_ward:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function item_ward:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function item_ward:GetCooldown(level)	
	return 0
end

--------------------------------------------------------------------------------