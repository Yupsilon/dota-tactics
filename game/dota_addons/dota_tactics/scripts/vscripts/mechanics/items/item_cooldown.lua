item_cooldown = class({})
LinkLuaModifier( "modifier_destroy_item","mechanics/items/modifier_destroy_item.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_energized","abilities/generic/generic_modifier_energized.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function item_cooldown:SolveAction(keys)

	for abi = 0, 6, 1	do		
		Alert_CooldownRefresh(self:GetCaster(),abi,-self:GetSpecialValueFor("refres_duration"))			
	end
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_destroy_item",{})	
	self:SetActivated(false)

			local blinkStart = ParticleManager:CreateParticle("particles/items2_fx/refresher.vpcf", PATTACH_WORLDORIGIN, caster)
			ParticleManager:SetParticleControl(blinkStart, 0, self:GetCaster():GetAbsOrigin())		
			ParticleManager:ReleaseParticleIndex(blinkStart)		
	
				
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_energized",{
		energy=self:GetSpecialValueFor("energy_fatigue"),
		turns=self:GetSpecialValueFor("energy_fatigue_duration")
	})
			
				EmitSoundOn( "DOTA_Item.Refresher.Activate", self:GetCaster() )
			
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function item_cooldown:IsMuted()
	return self:GetCurrentCharges()==0
end

--------------------------------------------------------------------------------

function item_cooldown:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function item_cooldown:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function item_cooldown:GetPriority()
	return 5;
end

--------------------------------------------------------------------------------

function item_cooldown:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function item_cooldown:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function item_cooldown:GetCooldown(level)	
	return 0
end

--------------------------------------------------------------------------------