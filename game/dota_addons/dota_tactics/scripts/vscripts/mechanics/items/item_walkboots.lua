item_walkboots = class({})

--------------------------------------------------------------------------------

function item_walkboots:GetCastRange(  )

	local parent = self:GetCaster()
	if parent:IsRooted() then
		return 0
	end
	
	local HERO_BASE_MOVEMENTSPEED = 200/3 * self:GetCaster():GetAgility() / 10
	
	local fSpeed = HERO_BASE_MOVEMENTSPEED * (self:GetCaster():GetIdealSpeed() / self:GetCaster():GetBaseMoveSpeed())
	local ftime = 3
  
			--[[for _,modifierorder in pairs(parent:FindAllModifiers()) do				
				if modifierorder:GetName()=="modifier_order_cast" and modifierorder:GetAbility() ~= nil  then
				
					if modifierorder:GetAbility().isFreeAction~=nil and modifierorder:GetAbility():isFreeAction()==false and ftime==MOVE_TIME_SHORT then
						ftime=ftime/2
					end		
					if modifierorder:GetAbility().disablesMovement~=nil and modifierorder:GetAbility():disablesMovement()==true then
						return 0
					end
					if modifierorder:GetAbility().GetHasteBonus~=nil and modifierorder:GetAbility():disablesMovement()==true then
						fSpeed=fSpeed*modifierorder:GetAbility():GetHasteBonus()
					end
					
				end			
			end	]]					
		

	return (fSpeed*ftime)/2
end

--------------------------------------------------------------------------------
