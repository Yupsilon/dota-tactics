item_energy = class({})
LinkLuaModifier( "generic_modifier_energized","abilities/generic/generic_modifier_energized.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_destroy_item","mechanics/items/modifier_destroy_item.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function item_energy:PrepAction(keys)

	self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_energized",{energy=50,turns=self:GetSpecialValueFor("energy_duration")})	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_destroy_item",{})	
	self:SetActivated(false)
	
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function item_energy:IsMuted()
	return self:GetCurrentCharges()==0;
end

--------------------------------------------------------------------------------

function item_energy:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function item_energy:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function item_energy:GetPriority()
	return 0;
end

--------------------------------------------------------------------------------

function item_energy:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function item_energy:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function item_energy:GetCooldown(level)	
	return 0
end

--------------------------------------------------------------------------------