item_cleanse = class({})
LinkLuaModifier( "generic_modifier_scar_immunity","abilities/generic/generic_modifier_scar_immunity.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_destroy_item","mechanics/items/modifier_destroy_item.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function item_cleanse:PrepAction(keys)

			local blinkStart = ParticleManager:CreateParticle("particles/items5_fx/magic_lamp.vpcf", PATTACH_WORLDORIGIN, caster)
			ParticleManager:SetParticleControl(blinkStart, 0, self:GetCaster():GetAbsOrigin())		
			ParticleManager:ReleaseParticleIndex(blinkStart)	
			
				EmitSoundOn( "DOTA_Item.Bloodstone.Cast", ward_unit )

	Alert_HealScars({unit = self:GetCaster(), value = self:GetSpecialValueFor("scar_healing")})
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_scar_immunity",{turns=self:GetSpecialValueFor("scar_duration")})	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_destroy_item",{})	
	self:SetActivated(false)
			
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function item_cleanse:IsMuted()
	return self:GetCurrentCharges()==0;
end

--------------------------------------------------------------------------------

function item_cleanse:getAbilityType()
	return -1;
end

--------------------------------------------------------------------------------

function item_cleanse:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function item_cleanse:GetPriority()
	return 0;
end

--------------------------------------------------------------------------------

function item_cleanse:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function item_cleanse:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function item_cleanse:GetCooldown(level)	
	return 0
end

--------------------------------------------------------------------------------