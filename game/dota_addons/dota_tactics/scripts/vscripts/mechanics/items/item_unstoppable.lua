item_unstoppable = class({})
LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_destroy_item","mechanics/items/modifier_destroy_item.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function item_unstoppable:SolveAction(keys)

	self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_unstoppable",{turns=self:GetSpecialValueFor("resistance_duration")})	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_destroy_item",{})	
	self:SetActivated(false)
			
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function item_unstoppable:IsMuted()
	return self:GetCurrentCharges()==0
end

--------------------------------------------------------------------------------

function item_unstoppable:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function item_unstoppable:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function item_unstoppable:GetPriority()
	return 0;
end

--------------------------------------------------------------------------------

function item_unstoppable:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function item_unstoppable:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function item_unstoppable:GetCooldown(level)	
	return 0
end

--------------------------------------------------------------------------------