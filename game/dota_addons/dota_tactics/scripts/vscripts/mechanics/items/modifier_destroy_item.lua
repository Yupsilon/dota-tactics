modifier_destroy_item = class({})

-----------------------------------------------------------------------------

function modifier_destroy_item:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function modifier_destroy_item:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_destroy_item:OnCreated( kv )

	self.myItem = self:GetAbility()
end
	
--------------------------------------------------------------------------------

function modifier_destroy_item:EndOfTurnAction()
	self.myItem:Destroy()
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------