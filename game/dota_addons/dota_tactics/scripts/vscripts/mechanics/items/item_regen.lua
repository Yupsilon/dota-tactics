item_regen = class({})
LinkLuaModifier( "generic_modifier_regenerate","abilities/generic/generic_modifier_regenerate.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_weaken","abilities/generic/generic_modifier_weaken.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_destroy_item","mechanics/items/modifier_destroy_item.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function item_regen:PrepAction(keys)

	local target_unit = EntIndexToHScript(keys.target_unit)
	if target_unit~=nil then		
	
		if self:GetCaster()~=target_unit then
			self:GetCaster():FaceTowards(target_unit:GetAbsOrigin())
			StartAnimation(self:GetCaster(), {duration=PLAYER_TIME_MODIFIER, activity=ACT_DOTA_TELEPORT, rate = 1/PLAYER_TIME_MODIFIER})
		end
		
		target_unit:AddNewModifier(self:GetCaster(),self,"generic_modifier_regenerate",{turns=self:GetSpecialValueFor("regen_duration"),health=self:GetSpecialValueFor("regen_health")})	
		--self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_weaken",{turns=self:GetSpecialValueFor("regen_duration"),value=self:GetSpecialValueFor("weaken_strength")})	
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_destroy_item",{})	
		self:SetActivated(false)		
	end
		
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function item_regen:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function item_regen:IsMuted()
	return self:GetCurrentCharges()==0;
end

--------------------------------------------------------------------------------

function item_regen:GetBehavior()
	if GameRules:State_Get() <= 7 then
		return DOTA_ABILITY_BEHAVIOR_NO_TARGET
	end
	return DOTA_ABILITY_BEHAVIOR_UNIT_TARGET
end

--------------------------------------------------------------------------------

function item_regen:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function item_regen:GetCastPoints()

	return 1;
end

--------------------------------------------------------------------------------

function item_regen:GetPriority()
	return 5;
end

--------------------------------------------------------------------------------

function item_regen:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function item_regen:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function item_regen:GetCooldown(level)	
	return 0
end

--------------------------------------------------------------------------------

function item_regen:GetConeArc(coneID,coneRange)
	return 25
end

--------------------------------------------------------------------------------

function item_regen:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function item_regen:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("reach_range")
end

--------------------------------------------------------------------------------

function item_regen:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------