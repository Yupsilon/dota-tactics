item_miniward = class({})
LinkLuaModifier( "generic_modifier_summonedunit","abilities/generic/generic_modifier_summonedunit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_truesight","abilities/generic/generic_modifier_truesight.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_temp_reveal","mechanics/heroes/modifier_temp_reveal.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_destroy_item","mechanics/items/modifier_destroy_item.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function item_miniward:SolveAction(keys)

	if keys.target_point_A~=nil then
					
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_destroy_item",{})	
			self:SetActivated(false)
	
			local ward_unit = ( CreateUnitByName("npc_ward_vision_invulnerable", keys.target_point_A , true, self:GetCaster(), self:GetCaster(), self:GetCaster():GetTeamNumber() ))
			ward_unit:SetControllableByPlayer(self:GetCaster():GetPlayerOwnerID(), true)
			ward_unit:SetOwner(self:GetCaster())
			
			local main_modifier = ward_unit:AddNewModifier(self:GetCaster(),self,"generic_modifier_truesight",{ range = self:GetSpecialValueFor("ward_sight")})		
									
			local aiModifier = self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_summonedunit",{turns = self:GetSpecialValueFor("ward_life"),destroyonend=1})
			aiModifier:AssignUnit(ward_unit)
			FindClearSpaceForUnit(ward_unit, keys.target_point_A, true)	
			
				EmitSoundOn( "DOTA_Item.ObserverWard.Activate", ward_unit )
						
			ward_unit:FindAbilityByName("dummy_unit"):SetLevel(1)
	end
			
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function item_miniward:GetBehavior()
	if GameRules:State_Get() <= 7 then
		return DOTA_ABILITY_BEHAVIOR_NO_TARGET
	end
	return DOTA_ABILITY_BEHAVIOR_POINT + DOTA_ABILITY_BEHAVIOR_AOE
end

--------------------------------------------------------------------------------

function item_miniward:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function item_miniward:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function item_miniward:IsMuted()
	return self:GetCurrentCharges()==0
end

--------------------------------------------------------------------------------

function item_miniward:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function item_miniward:GetPriority()
	return 0;
end

--------------------------------------------------------------------------------

function item_miniward:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function item_miniward:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function item_miniward:GetCooldown(level)	
	return 0
end

--------------------------------------------------------------------------------

function item_miniward:GetConeArc(coneID,coneRange)
	return 25
end

--------------------------------------------------------------------------------

function item_miniward:GetMinRange(coneID)	
		
	return 0
end

--------------------------------------------------------------------------------

function item_miniward:GetMaxRange(coneID)

	return self:GetSpecialValueFor("cast_range")
end

--------------------------------------------------------------------------------

function item_miniward:GetCastRange()	
	return self:GetSpecialValueFor("cast_range")
end

--------------------------------------------------------------------------------

function item_miniward:GetAOERadius()	
	return self:GetSpecialValueFor("ward_sight")
end

--------------------------------------------------------------------------------