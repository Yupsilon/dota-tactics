item_ready_up = class({})
LinkLuaModifier( "modifier_indicator_ready","mechanics/items/modifier_indicator_ready.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function item_ready_up:GetIntrinsicModifierName(  )
	return "modifier_indicator_ready"
end

--------------------------------------------------------------------------------

function item_ready_up:IsCasterReady()
	return self:GetCaster():GetModifierStackCount("modifier_indicator_ready",self:GetCaster())==2
end

--------------------------------------------------------------------------------

function item_ready_up:HandleTactical()	
	
	--if self:IsCasterReady() then
	--	self:GetCaster():FindModifierByName("modifier_indicator_ready"):SetStackCount(1)
	--	GameMode:SetAllHeroesReady(false)	
	--else
		for index,hero in pairs (GameMode:GetPlayerHeroes(self:GetCaster():GetPlayerOwnerID())) do
			if not hero:HasModifier("modifier_indicator_ready") then
				hero:AddNewModifier(hero,self,"modifier_indicator_ready",{})	
			end
			hero:FindModifierByName("modifier_indicator_ready"):SetStackCount(2)
		end
		self:CheckPlayersReady(0)
	--end	
end

--------------------------------------------------------------------------------

function item_ready_up:GetAbilityTextureName()		
	if self:IsCasterReady() then return "custom/item_ready" end
	return "custom/item_unready"
end

--------------------------------------------------------------------------------

function item_ready_up:getAbilityType()		
	return -1
end

--------------------------------------------------------------------------------

function item_ready_up:CheckPlayersReady(forced)

	for _,actor in pairs(HeroList:GetAllHeroes()) do	
				if IsValidEntity(actor) and actor:IsNull() == false and not actor:IsIllusion() then		
					if not (actor:HasModifier("modifier_indicator_ready") and actor:FindModifierByName("modifier_indicator_ready"):GetStackCount()==2) then
						return false
					end
				end
			end
	
		if forced==1 then
				GameMode:SetAllHeroesReady(true)	
		else
		local tick_time = self:GetSpecialValueFor("ready_up_time")
		self.ticktimer = Timers:CreateTimer(tick_time,function()	
			self:CheckPlayersReady(1) 
		end)
	
	end
			
	return true
end

--------------------------------------------------------------------------------
