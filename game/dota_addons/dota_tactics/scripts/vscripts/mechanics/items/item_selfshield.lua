item_selfshield = class({})
LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_destroy_item","mechanics/items/modifier_destroy_item.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function item_selfshield:PrepAction(keys)

	self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_shield",{turns=self:GetSpecialValueFor("regen_duration"),value=self:GetSpecialValueFor("regen_health")})	
	--self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_slow",{turns=self:GetSpecialValueFor("regen_duration"),value=self:GetSpecialValueFor("slow_strength")})	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_destroy_item",{})	
	self:SetActivated(false)
		
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function item_selfshield:IsMuted()
	return self:GetCurrentCharges()==0;
end

--------------------------------------------------------------------------------

function item_selfshield:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function item_selfshield:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function item_selfshield:GetPriority()
	return 5;
end

--------------------------------------------------------------------------------

function item_selfshield:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function item_selfshield:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function item_selfshield:GetCooldown(level)	
	return 0
end

--------------------------------------------------------------------------------