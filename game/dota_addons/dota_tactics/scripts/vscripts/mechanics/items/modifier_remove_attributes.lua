modifier_remove_attributes = class({})

--------------------------------------------------------------------------------

function modifier_remove_attributes:IsHidden()
	return true
end
	
--------------------------------------------------------------------------------

function modifier_remove_attributes:EndOfTurnAction()
	if self:GetParent():IsHero() then
	
		if self:GetParent():GetAbilityPoints()>=ABILITY_POINTS then
			for i=0, 5 do
			local abil = self:GetParent():GetAbilityByIndex(i)
			if abil ~= nil then		  
				abil:SetLevel(3)		  
			end
		  end
		end
		self:GetParent():SetAbilityPoints(0)
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------