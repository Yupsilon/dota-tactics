item_changehero = class({})

--------------------------------------------------------------------------------

function item_changehero:HandleTactical()	
	
	print("changehero")
	local original_hero = self:GetCaster()
	local original_pos = original_hero:GetAbsOrigin()
	local player = original_hero:GetPlayerOwner()
	
	local original_name = original_hero:GetUnitName()
	
	local hero_index=0
	for i = 1,#item_changehero_herotable do
		if item_changehero_herotable[i] == original_name then
			hero_index = i;
		end
	end
	
	hero_index=hero_index+1
	if hero_index>#item_changehero_herotable then
		hero_index=1
	end
		
	while GameMode:HeroExists(item_changehero_herotable[hero_index]) and original_name~=item_changehero_herotable[hero_index] do
		hero_index=hero_index+1
		if hero_index>#item_changehero_herotable then
			hero_index=1
		end
	end
	
	if not (original_name==item_changehero_herotable[hero_index]) then
		original_hero:Destroy()					
		MakePuppetHero(item_changehero_herotable[hero_index],player,original_pos)
	end
end

--------------------------------------------------------------------------------

function item_changehero:getAbilityType()		
	return -1
end

--------------------------------------------------------------------------------
