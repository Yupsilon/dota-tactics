modifier_indicator_ready = class({})

--------------------------------------------------------------------------------

function modifier_indicator_ready:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_indicator_ready:GetAttributes()
	return MODIFIER_ATTRIBUTE_PERMANENT + MODIFIER_ATTRIBUTE_IGNORE_INVULNERABLE
end

--------------------------------------------------------------------------------

function modifier_indicator_ready:IsPermanent()
	return true
end

--------------------------------------------------------------------------------

function modifier_indicator_ready:RemoveOnDeath()
	return false
end

--------------------------------------------------------------------------------

function modifier_indicator_ready:GetTexture()
	return "invoker_invoke"
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------