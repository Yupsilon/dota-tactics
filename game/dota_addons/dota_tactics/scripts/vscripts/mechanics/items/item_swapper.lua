
item_table_offense={
	"item_might",
	"item_regen",
	"item_energy",
	"item_cleanse",
	"item_selfshield"
}
item_table_defense={
	"item_unstoppable",
	"item_cooldown",
	"item_miniward",
	"item_cooldown_lesser",
}
item_table_mobility={
	"item_blinky",
	"item_invisible",
	"item_teleport"
}

function IsItemSwappable(item)

	for index,itemname in pairs(item_table_offense) do
		if itemname == item:GetAbilityName() then
			return 0
		end
	end
	for index,itemname in pairs(item_table_defense) do
		if itemname == item:GetAbilityName() then
			return 1
		end
	end
	for index,itemname in pairs(item_table_mobility) do
		if itemname == item:GetAbilityName() then
			return 2
		end
	end

	return -1
end


function SwapItem(item)

	local item_name = item:GetAbilityName() 
	local table_name = IsItemSwappable(item)	
	local item_table = item_table_offense
	
	if table_name<0 then 
		print("invalid "..table_name)
		return	
	elseif table_name == 1 then
		item_table = item_table_defense	
	elseif table_name == 2 then
		item_table = item_table_mobility		
	end
	
	local item_index=0
	for i = 1,#item_table do
		if item_table[i] == item_name then
			item_index = i;
		end
	end
	
	item_index = item_index + 1;
	if item_index > #item_table then
		item_index = 1
	end
	
	local new_item_name = item_table[item_index]
	
	if SINGLEPLAYER_DEBUG then
		print("Swapping "..item_name.." for "..new_item_name)
	end
	
	if new_item_name~=nil and not ( new_item_name == item_name) then		
		local owner_hero = item:GetCaster()
	
		item:Destroy()
		local new_item = owner_hero:AddItemByName(new_item_name)
		--new_item:SetCurrentCharges(1)
	elseif SINGLEPLAYER_DEBUG then
		print("Fail swapping "..item_name.." for "..new_item_name)
	end
	
end
