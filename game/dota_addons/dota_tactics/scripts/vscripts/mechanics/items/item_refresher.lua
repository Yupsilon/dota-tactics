item_refresher = class({})
LinkLuaModifier( "modifier_destroy_item","mechanics/items/modifier_destroy_item.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function item_refresher:SolveAction(keys)

	for abi = 0, 6, 1	do		
		Alert_CooldownRefresh(self:GetCaster(),abi,self:GetSpecialValueFor("bonus_refresh"))			
	end
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_destroy_item",{})	
	self:SetActivated(false)
			
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function item_refresher:IsMuted()
	return self:GetCurrentCharges()==0
end

--------------------------------------------------------------------------------

function item_refresher:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function item_refresher:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function item_refresher:GetPriority()
	return 0;
end

--------------------------------------------------------------------------------

function item_refresher:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function item_refresher:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function item_refresher:GetCooldown(level)	
	return 0
end

--------------------------------------------------------------------------------