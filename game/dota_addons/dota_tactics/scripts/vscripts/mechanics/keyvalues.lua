function GameMode:InitKeyValues()

	self.default_keys = LoadKeyValues( "scripts/maps/default.kv" )
	self.scenario_keys = LoadKeyValues( "scripts/maps/" .. GetMapName() .. ".kv" )

end

function GameMode:LoadRuneSpawnerKV(entity_spawner)

	local name = entity_spawner:GetName()
	if name == "" or name == nil then
		name = "unused_rune_spawner"
	end

	if self.scenario_keys~=nil and type( self.scenario_keys[name] ) == "table" then
		
		entity_spawner.runeName = self.scenario_keys[name].RuneName
		entity_spawner.nextSpawnTurn = tonumber(self.scenario_keys[name].StartTurn)
		entity_spawner.refreshTime = tonumber(self.scenario_keys[name].RefreshTime)
		
	elseif type( self.default_keys[name] ) == "table" then
		
		entity_spawner.runeName = self.default_keys[name].RuneName
		entity_spawner.nextSpawnTurn = tonumber(self.default_keys[name].StartTurn)
		entity_spawner.refreshTime = tonumber(self.default_keys[name].RefreshTime)
		
	else
		entity_spawner.runeName = "npc_rune_dummy_regen"
		entity_spawner.nextSpawnTurn = 99
		entity_spawner.refreshTime = 99
	end
end