LinkLuaModifier( "modifier_brush_invisibility","mechanics/map/modifier_brush_invisibility.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_brush_invisibility_vision","mechanics/map/modifier_brush_invisibility_vision.lua", LUA_MODIFIER_MOTION_NONE )

function OnStartTouch(trigger)
	
	if IsServer() and
		IsValidEntity(trigger.activator) and trigger.activator:IsNull() == false and
		trigger.activator:IsAlive() and trigger.activator:IsHero() then
		--Timers:CreateTimer(1/30, function ()
			local brush_modifier = trigger.activator:AddNewModifier( trigger.activator, nil, "modifier_brush_invisibility", {} )
			brush_modifier.brush_entity = trigger.caller
		--end)
	end
end
 
function OnEndTouch(trigger)

	if IsServer() and
		IsValidEntity(trigger.activator) and trigger.activator:IsNull() == false and
		trigger.activator:IsAlive() and trigger.activator:IsHero() then
		--Timers:CreateTimer(1/30, function ()
			trigger.activator:RemoveModifierByName("modifier_brush_invisibility")
		--end)
			
			
	end
end

function IsPointOverBrush( point )
	
	local borders = DOTA_TILE_TO_UNITS
	
	local scan = Entities:FindAllByClassname("trigger_dota")
	for _,trigger in pairs(scan) do 
		if trigger:GetName()=="area_brush" then
				
			local Mins = trigger:GetBoundingMins()+trigger:GetAbsOrigin()-Vector(borders,borders,0)
			local Maxs = trigger:GetBoundingMaxs()+trigger:GetAbsOrigin()+Vector(borders,borders,0)
			  if point.x>=Mins.x and point.x<=Maxs.x and point.y>=Mins.y and point.y<=Maxs.y then
				return true
			  end	
		end
	end

	return false
end