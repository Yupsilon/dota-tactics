modifier_brush_invisibility_vision = class({})

--------------------------------------------------------------------------------

function modifier_brush_invisibility_vision:IsDebuff()
	return true
end

--------------------------------------------------------------------------------

function modifier_brush_invisibility_vision:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_brush_invisibility_vision:OnCreated( kv )
	-- NOTHING
end

--------------------------------------------------------------------------------

function modifier_brush_invisibility_vision:GetPriority()
	
	return 2
end

--------------------------------------------------------------------------------

function modifier_brush_invisibility_vision:CheckState()
	local state = {
	[MODIFIER_STATE_INVISIBLE] = false,
	}

	return state
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
