modifier_rune_passive = class({})

--------------------------------------------------------------------------------

function modifier_rune_passive:OnCreated( kv )

	if IsServer() then
		self:StartIntervalThink( .05 )
		self:OnIntervalThink()
		
			StartAnimation(self:GetParent(), { duration = 9999, activity=ACT_DOTA_IDLE})	
		
		
					if self:GetParent().Name == "npc_rune_dummy_regen" then
						local nFXIndex = ParticleManager:CreateParticle( "particles/generic_gameplay/rune_regeneration.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
						self:AddParticle( nFXIndex, false, false, -1, false, false )	
					elseif self:GetParent().Name == "npc_rune_dummy_haste" then
						local nFXIndex = ParticleManager:CreateParticle( "particles/generic_gameplay/rune_haste.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
						self:AddParticle( nFXIndex, false, false, -1, false, false )	
					elseif self:GetParent().Name == "npc_rune_dummy_might" then
						local nFXIndex = ParticleManager:CreateParticle( "particles/generic_gameplay/rune_doubledamage.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
						self:AddParticle( nFXIndex, false, false, -1, false, false )	
					elseif self:GetParent().Name == "npc_rune_dummy_energy" then
						local nFXIndex = ParticleManager:CreateParticle( "particles/generic_gameplay/rune_arcane.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
						self:AddParticle( nFXIndex, false, false, -1, false, false )	
					elseif self:GetParent().Name == "npc_rune_dummy_regen_constant" then 
						local nFXIndex = ParticleManager:CreateParticle( "particles/econ/courier/courier_shibe/courier_shibe_ambient_salve_glow.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
						self:AddParticle( nFXIndex, false, false, -1, false, false )	
					end
		
	end
end

--------------------------------------------------------------------------------

function modifier_rune_passive:OnIntervalThink()
	if IsServer() then	
		local caster = self:GetParent()
		local valid_heroes = FindUnitsInRadius( DOTA_TEAM_NEUTRALS, caster:GetAbsOrigin(), caster,self:GetAbility():GetSpecialValueFor("rune_collision_aoe") , DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_NONE, FIND_ANY_ORDER, false )
		if #valid_heroes > 0 then
			for _,hero in pairs(valid_heroes) do
				if caster:IsAlive() and hero ~= nil and hero:IsAlive() then
			
					local keyvalues = {
						target = hero
					}
					keyvalues.modifierName = "generic_modifier_might"
						keyvalues.turns = self:GetAbility():GetSpecialValueFor("duration")
						keyvalues.value = self:GetAbility():GetSpecialValueFor("might")	
					
					if caster.Name == "npc_rune_dummy_regen" then
						keyvalues.modifierName = "generic_modifier_regenerate"
						keyvalues.turns = self:GetAbility():GetSpecialValueFor("life_turns")	
						keyvalues.health = self:GetAbility():GetSpecialValueFor("life_per_turn")	
					elseif caster.Name == "npc_rune_dummy_haste" then
						keyvalues.modifierName = "generic_modifier_movespeed_haste"
						keyvalues.speed = self:GetAbility():GetSpecialValueFor("haste")	
						keyvalues.turns = self:GetAbility():GetSpecialValueFor("duration")						
					elseif caster.Name == "npc_rune_dummy_energy" then
						keyvalues.modifierName = "generic_modifier_energized"
						keyvalues.turns = self:GetAbility():GetSpecialValueFor("duration")
						keyvalues.value = self:GetAbility():GetSpecialValueFor("energy")	
					elseif caster.Name == "npc_rune_dummy_regen_constant" then
						keyvalues.modifierName = "generic_modifier_regenerate"
						keyvalues.turns = 1
						keyvalues.health = self:GetAbility():GetSpecialValueFor("life_per_turn")	
					end
					
					self:GiveRune(keyvalues)
				end
			end
		end
	end
end

--------------------------------------------------------------------------------

function modifier_rune_passive:GiveRune(kv)
	
	if kv.target==nil then
		return
	end
	
	kv.target:AddNewModifier(self:GetParent(),nil,kv.modifierName,{
		turns = kv.turns,
						health = kv.health,
						speed = kv.speed,
						value = kv.value
	})
		
	if self:GetParent().spawner ~= nil then
		self:GetParent().spawner.nextSpawnTurn = GameMode:GetCurrentTurn() + self:GetParent().spawner.refreshTime
		self:GetParent().spawner.runeEntity = nil
	end
	self:GetParent():SetModelScale(0)
	self:GetParent():ForceKill(false)
end
	
--------------------------------------------------------------------------------