modifier_brush_invisibility = class({})
--------------------------------------------------------------------------------

function modifier_brush_invisibility:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function modifier_brush_invisibility:IsAura()
	--if self:GetCaster() == self:GetParent() then
	--	return true
	--end
	
	return true
end

--------------------------------------------------------------------------------

function modifier_brush_invisibility:GetTexture()
	return "treant_natures_guise"
end

--------------------------------------------------------------------------------

function modifier_brush_invisibility:GetModifierAura()
	return "modifier_brush_invisibility_vision"
end
--------------------------------------------------------------------------------

function modifier_brush_invisibility:OnIntervalThink()

	if self.brush_entity == nil or IsPointOverBrush(self:GetParent():GetAbsOrigin()) then
		self:GetParent():RemoveModifierByName("modifier_brush_invisibility")
	end


	--for index,unit in pairs(FindUnitsInRadius(self:GetParent():GetTeamNumber(),self:GetParent():GetAbsOrigin(),nil,self:GetAuraRadius(),self:GetAuraSearchTeam(),self:GetAuraSearchType(),self:GetAuraSearchFlags(),0,false)) do
	--	unit:AddNewModifier(self:GetParent(),nil, "modifier_brush_invisibility_vision",{duration=self.think})
	--end
end

--------------------------------------------------------------------------------

function modifier_brush_invisibility:GetStatusEffectName()
	return "particles/status_fx/status_effect_blur.vpcf"
end

--------------------------------------------------------------------------------

function modifier_brush_invisibility:GetAuraSearchTeam()
	return DOTA_UNIT_TARGET_TEAM_ENEMY
end

--------------------------------------------------------------------------------

function modifier_brush_invisibility:GetAuraSearchType()
	return DOTA_UNIT_TARGET_ALL
end

--------------------------------------------------------------------------------

function modifier_brush_invisibility:GetAuraSearchFlags()
	return DOTA_UNIT_TARGET_FLAG_INVULNERABLE+DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES
end

--------------------------------------------------------------------------------

function modifier_brush_invisibility:GetAuraRadius()
	return self.reveal_radius
end

--------------------------------------------------------------------------------

function modifier_brush_invisibility:OnCreated( kv )
	self.reveal_radius = COVER_REVEAL_RADIUS

	if IsServer() then
	
			local nFXIndex = ParticleManager:CreateParticle( "particles/generic_gameplay/rune_invisibility.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
			self:AddParticle( nFXIndex, false, false, -1, false, false )
			
			--self.think = 1/4
			--self:OnIntervalThink()
			--self:StartIntervalThink(self.think)
	end
end

--------------------------------------------------------------------------------

function modifier_brush_invisibility:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_INVISIBILITY_LEVEL,
	}

	return funcs
end

--------------------------------------------------------------------------------

function modifier_brush_invisibility:GetPriority()
	
	return 1
end

--------------------------------------------------------------------------------

function modifier_brush_invisibility:CheckState()
	local state = {
	[MODIFIER_STATE_INVISIBLE] = true,
	}

	return state
end

--------------------------------------------------------------------------------

function modifier_brush_invisibility:GetModifierInvisibilityLevel( params )
	return 50
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
