function GameMode:InitRuneSpawns()	
	
	self.runes_valid =  {}
	local intID=0
	
	for _,runespawn in pairs(Entities:FindAllByClassname("npc_dota_spawner")) do
	
		if runespawn ~= nil and runespawn:IsNull() == false then
			self.runes_valid[intID] = runespawn;	
			self:LoadRuneSpawnerKV(runespawn)
			intID=intID+1
			
			--runespawn:AddAbility("dummy_unit"):SetLevel(1)
		end
	end
	
	DebugPrint("Found "..intID.." valid rune spawner entities")
	
	self:ReviseRuneSpawners()
end

function GameMode:MakeCoverEfx()	
	
	local scan = Entities:FindAllByClassname("trigger_dota")
	DebugPrint("found "..#scan.. " valid CoverCollisionTrigger entities!")
	for _,trigger in pairs(scan) do 
		if trigger:GetName()=="CoverCollisionTrigger" or trigger:GetName()=="area_cover" then
				
			local Mins = trigger:GetBoundingMins()+trigger:GetAbsOrigin()
			local Maxs = trigger:GetBoundingMaxs()+trigger:GetAbsOrigin()
			  if point.x>=Mins.x and point.x<=Maxs.x and point.y>=Mins.y and point.y<=Maxs.y then
				return true
			  end	
		end
	end
	DebugPrint("Is in cover? "..tostring(final))

	return false
end

function GameMode:ReviseRuneSpawners()	
	
	if self:GetCurrentTurn()>20 then
		return
	end
	
	DebugPrint("Revising Rune Spawners!")	
	local intID=0
	
	while self.runes_valid[intID] ~= nil do
	
		local runespawn = self.runes_valid[intID]
		if runespawn.runeEntity == nil or runespawn.runeEntity:IsNull() or not runespawn.runeEntity:IsAlive() then
		
			DebugPrint("Next spawning rune "..tostring(runespawn.runeName).." at "..tostring(runespawn.nextSpawnTurn))
			if self:GetCurrentTurn()>=runespawn.nextSpawnTurn then
			
				DebugPrint("Spawning rune "..runespawn.runeName.." at "..intID)
				runespawn.runeEntity  = CreateUnitByName(runespawn.runeName, runespawn:GetAbsOrigin() , true, nil, nil, DOTA_TEAM_NOTEAM)
				runespawn.runeEntity.Name = runespawn.runeName
				
				runespawn.runeEntity:FindAbilityByName("dummy_unit"):SetLevel(1)
				runespawn.runeEntity:FindAbilityByName("attribute_rune_giver"):SetLevel(1)
				runespawn.runeEntity.spawner = runespawn
				
				StartAnimation(runespawn.runeEntity, {duration=9999999, rate=1, activity=ACT_DOTA_IDLE})
				
			end		
		end
		
		intID=intID+1
	end
	
end

