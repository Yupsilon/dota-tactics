attribute_rune_giver = class({})
LinkLuaModifier( "modifier_rune_passive","mechanics/map/modifier_rune_passive.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_regenerate","abilities/generic/generic_modifier_regenerate.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_might","abilities/generic/generic_modifier_might.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_energized50","abilities/generic/generic_modifier_energized50.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function attribute_rune_giver:GetIntrinsicModifierName()
	return "modifier_rune_passive"
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
