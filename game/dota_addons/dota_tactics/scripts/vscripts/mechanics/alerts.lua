function GetOutgoingDamagePercentage(unit)

	local basevalue = 100
	
	
	for _,modifierorder in pairs(unit:FindAllModifiers()) do				
		if modifierorder.GetModifierTotalDamageOutgoing_Percentage~=nil and modifierorder:GetModifierTotalDamageOutgoing_Percentage()~=0 then
						
			basevalue=basevalue+ modifierorder:GetModifierTotalDamageOutgoing_Percentage()
		end			
	end	
	
	return math.max(0,basevalue)
end

function GetIncomingEnergizedPercentage(unit)

	local basevalue = 100
	
		for _,modifierorder in pairs(unit:FindAllModifiers()) do				
			if modifierorder:GetName() == "generic_modifier_energized"  then
							
				basevalue=basevalue*((modifierorder.multiplier or 0)+100)/100
			end			
		end	
	
	
	return math.max(0,basevalue)
end

function GetCombinedShieldLife(unit)

	local basevalue = 0
	
		for _,modifierorder in pairs(unit:FindAllModifiers()) do				
			if modifierorder.GetShieldLife  then
							
				basevalue=basevalue+modifierorder:GetShieldLife()
			end			
		end	
	
	
	return math.max(0,basevalue)
end

function Alert_Heal(info_table)

	--if info_table.target:GetHealth() + info_table.value > info_table.target:GetMaxHealth() and info_table.target:HasModifier("generic_modifier_scarred") then
	--if info_table.target:GetHealth() == info_table.target:GetMaxHealth() and info_table.target:HasModifier("generic_modifier_scarred") then
		--Alert_HealScars({unit = info_table.target, value = math.min(info_table.target:GetHealth()+info_table.value-info_table.target:GetMaxHealth(),info_table.value/4)})
	--end

	if SINGLEPLAYER_DEBUG then print(info_table.caster:GetUnitName().." damages "..info_table.target:GetUnitName().." for "..info_table.value.." damage") end
	info_table.target:Heal(info_table.value,info_table.caster)	
	--SendOverheadEventMessage( nil, OVERHEAD_ALERT_HEAL, info_table.target,info_table.value, info_table.target:GetPlayerOwner() )			
	Alert_SendOverheadEventMessage({
				unit = info_table.target,
				value = info_table.value,
				message = ALERT_HEAL
			})
end

LinkLuaModifier( "generic_modifier_scarred","abilities/generic/generic_modifier_scarred.lua", LUA_MODIFIER_MOTION_NONE )
function Alert_Damage(damagetable)
	
	local victim = damagetable.victim
	local attacker = damagetable.attacker
	local damageMult = (GetOutgoingDamagePercentage(attacker)/100) or 1
	damagetable.cover_reduction=damagetable.cover_reduction or 0
	local scar_damage =  (damagetable.scar_damage_direct or 0)
	
	if damagetable.cover_reduction < 1 and (attacker:GetAbsOrigin()-victim:GetAbsOrigin()):Length2D()<2*DOTA_TILE_TO_UNITS then
		damagetable.cover_reduction = 1
	end
	
	if SINGLEPLAYER_DEBUG then print(attacker:GetUnitName().." damages "..victim:GetUnitName().." for "..damagetable.damage.." damage") end
	
	if 	damagetable.damage_type ~= DAMAGE_TYPE_PURE then
	
		if damagetable.cover_reduction < 1 then		
			damagetable.damage=damagetable.damage * damagetable.cover_reduction
			scar_damage=scar_damage * damagetable.cover_reduction
			--SendOverheadEventMessage( nil, OVERHEAD_ALERT_DAMAGE, victim,damagetable.damage*damageMult, attacker:GetPlayerOwner() )
			Alert_SendOverheadEventMessage({
				unit = victim,
				value = damagetable.damage*damageMult,
				message = ALERT_COVER_DAMAGE
			})
		else		
			--SendOverheadEventMessage( nil, OVERHEAD_ALERT_CRITICAL, victim,damagetable.damage*damageMult, attacker:GetPlayerOwner() )	
			Alert_SendOverheadEventMessage({
				unit = victim,
				value = damagetable.damage * damageMult,
				message = ALERT_DIRECT_DAMAGE
			})
		end		
	else
		--SendOverheadEventMessage( nil, OVERHEAD_ALERT_BONUS_POISON_DAMAGE, victim,damagetable.damage*damageMult, attacker:GetPlayerOwner() )			
		Alert_SendOverheadEventMessage({
				unit = victim,
				value = damagetable.damage * damageMult,
				message = ALERT_INDIRECT_DAMAGE
			})
	end
	
	if damagetable.scar_damage_indirect or 0>0 then 
		scar_damage = scar_damage + damagetable.scar_damage_indirect
	end
		
	local shield_life_current = 0
	for _,modifierorder in pairs(victim:FindAllModifiers()) do				
		if modifierorder:GetName() == "generic_modifier_shield"  then
							
			shield_life_current=shield_life_current+modifierorder.shield_remaining
						
		end	
	end
	
	ApplyDamage( damagetable )
	
	local permanent_scar_damage = 0	
	
	for _,modifierorder in pairs(attacker:FindAllModifiers()) do				
		if modifierorder.GetPermanentDamageConversion~=nil and modifierorder:GetPermanentDamageConversion()~=0 then
						
			permanent_scar_damage=permanent_scar_damage+ modifierorder:GetPermanentDamageConversion()
		end			
	end	
	
	if (damagetable.scar_damage_percent or 0)>0 then 
		permanent_scar_damage = permanent_scar_damage + damagetable.scar_damage_percent
	end
	
	--if shield_life_current<=0 then 
		--scar_damage = scar_damage + (victim:GetMaxHealth()-victim:GetHealth()) * (permanent_scar_damage or 0)/100
		scar_damage = scar_damage + damagetable.damage * (permanent_scar_damage or 0)/100		
	--end
	
	scar_damage=math.max(0,scar_damage*damageMult-shield_life_current)+(damagetable.scar_damage_absolute or 0)
	
	if scar_damage>0 and not victim:HasModifier("generic_modifier_scar_immunity") and victim:IsHero() then
	
		local scar_modifier = victim:FindModifierByName("generic_modifier_scarred")
		if scar_modifier==nil then			
			scar_modifier = victim:AddNewModifier(victim,nil,"generic_modifier_scarred",{})
		end
		
		Alert_SendOverheadEventMessage({
			unit = victim,
			value = math.min(scar_damage,victim:GetMaxHealth()-victim:GetHealth()),
			message = ALERT_PERMANENT_DAMAGE
		})
		
		local victim_max_life = victim:GetBaseMaxHealth()
		local PD_stacks = math.min(
			victim_max_life-victim:GetHealth(),
			victim_max_life-1,
			scar_modifier:GetStackCount()+scar_damage
		)
		
		if PD_stacks>0 then			
			scar_modifier:SetStackCount(PD_stacks)
			scar_modifier:OnIntervalThink()
		else 
			scar_modifier:Destroy()
		end
		if SINGLEPLAYER_DEBUG then print(attacker:GetUnitName().." damages "..victim:GetUnitName().." for "..scar_damage.." perma damage") end
	end
end

function Alert_HealScars(info_table)
	if info_table.unit:HasModifier("generic_modifier_scarred") then
			Alert_SendOverheadEventMessage({
				unit = info_table.unit,
				value = info_table.value,
				message = ALERT_HEALSCARS
			})
			
			local scar_mod = info_table.unit:FindModifierByName("generic_modifier_scarred")
			
			if scar_mod:GetStackCount()==0 or scar_mod:GetStackCount() < info_table.value then
				scar_mod:Destroy()
			else
				local newPermaDam = scar_mod:GetStackCount()-info_table.value
			
				if newPermaDam>0 then
					scar_mod:SetStackCount(newPermaDam)
					scar_mod:OnIntervalThink()
				else
					scar_mod:Destroy()					
						
				end
			end
		end		
end

function Alert_Mana(info_table)

	if info_table.manavalue<0 then 
		--SendOverheadEventMessage( nil, OVERHEAD_ALERT_MANA_LOSS, info_table.unit,0-info_table.manavalue, info_table.unit:GetPlayerOwner() ) 
		Alert_SendOverheadEventMessage({
			unit = info_table.unit,
			value = info_table.manavalue,
			message = ALERT_MANA_LOSS
		})
		info_table.unit:SpendMana(math.abs(info_table.manavalue),nil);
	return end

	local energized = 1
	
	if (info_table.energy or false) == true then 
		energized = GetIncomingEnergizedPercentage(info_table.unit)/100
	end
	
		--SendOverheadEventMessage( nil, OVERHEAD_ALERT_MANA_ADD, info_table.unit,info_table.manavalue*energized, info_table.unit:GetPlayerOwner() )
		Alert_SendOverheadEventMessage({
			unit = info_table.unit,
			value = info_table.manavalue,
			message = ALERT_MANA_GAIN
		})
			
			local particle = ParticleManager:CreateParticle("particles/items_fx/arcane_boots_recipient.vpcf", PATTACH_ABSORIGIN_FOLLOW, caster)
			ParticleManager:ReleaseParticleIndex(particle)
	
	info_table.unit:GiveMana(info_table.manavalue*energized);
end

function FindUnitsInCone(params)
	
	local foundunits = {}
	local searchunits = FindUnitsInRadius( params.caster:GetTeamNumber(), params.startPoint, params.caster, params.search_range, params.searchteam, params.searchtype, params.searchflags, FIND_CLOSEST, false )
	
	if #searchunits > 0 then
		for index,unit in pairs(searchunits) do
			if unit ~= nil then
				local direction = (unit:GetAbsOrigin()-params.startPoint)
				direction.z=0				
				local deltaangle = math.atan2(direction.x,direction.y)
								
				local hullAngle = unit:GetHullRadius() / direction:Length2D() / 2
				
				if math.abs(AngleDiff(params.startAngle*180/math.pi,deltaangle*180/math.pi))<=(params.search_angle+hullAngle)/2 then
					table.insert(foundunits,unit)	
				end		
			end
		end
	end
	return foundunits
end

function Alert_CooldownRefresh(caster, ability, value)

	local hAbility = nil

	if type(ability) == "number" then
		hAbility = caster:GetAbilityByIndex(ability)
	elseif type(ability) == "string" then
		hAbility = caster:FindAbilityByName(ability)
	elseif type(ability) == "table" then
		hAbility = ability
	end

	if hAbility~=nil and hAbility.GetLastCastTurn then
		hAbility.LastCastTurn=hAbility:GetLastCastTurn()+value	
		hAbility:StartCooldown(hAbility:GetCooldownTimeRemaining())	
	end

end

ALERT_DIRECT_DAMAGE = 0
ALERT_COVER_DAMAGE = 1
ALERT_INDIRECT_DAMAGE = 2
ALERT_SHIELD_DAMAGE = 6
ALERT_MANA_GAIN = 3
ALERT_MANA_LOSS = 4
ALERT_SHIELD_ADD = 5
ALERT_HEAL = 7
ALERT_HEALSCARS = 10
ALERT_PERMANENT_DAMAGE = 8
ALERT_DASH = 9

function Alert_SendOverheadEventMessage(info_table)

	local owner = info_table.unit	

	if (owner.lastAlertTime or 0) > Time() then
		Timers:CreateTimer(	owner.lastAlertTime - Time(),
		function()	Alert_SendOverheadEventMessage(info_table) end	)
		return
	end
	
	owner.lastAlertTime = Time()+0.33

	
	local attachPoint = owner:GetAbsOrigin()--GetAttachmentOrigin(owner:ScriptLookupAttachment("head"))		
	
	local real_damage = info_table.value or 0
	local display_damage = math.ceil(math.abs(real_damage))
	local vColor = Vector (255,255,255)
	
	--info_table.message
	--info_table.value

	--Prefix
	-- 1 -
	-- 2 :(
	-- 4 shades
	-- 5 miss
	-- 6 evade
	-- 7 deny
	-- 8 up

	--Suffix
	-- 1 .0
	-- 2 medal
	-- 3 tear
	-- 4 lightning
	-- 5 skull
	-- 6 eye
	-- 7 shield
	-- 8 .5
	-- 9 _
	-- 90 _!
	local prefix = 0
	local suffix = 0
	
	if info_table.message == ALERT_DIRECT_DAMAGE then
		--prefix = 1
		vColor = Vector (255,14,14)
		suffix = 6
		if owner:GetHealth()<=1 then suffix = 65 end
	elseif info_table.message == ALERT_COVER_DAMAGE then
		vColor = Vector (188,7,7)
		--prefix = 1
		if owner:GetHealth()<=1 then suffix = 5 end
	elseif info_table.message == ALERT_INDIRECT_DAMAGE then
		vColor = Vector (255,131,0)
		prefix = 4
		if owner:GetHealth()<=1 then suffix = 5 end
	elseif info_table.message == ALERT_MANA_GAIN then
		vColor = Vector (19,255,252)
		suffix = 4
	elseif info_table.message == ALERT_MANA_LOSS then
		vColor = Vector (19,155,152)
		prefix = 1
		suffix = 4
	elseif info_table.message == ALERT_SHIELD_ADD then
		vColor = Vector (46,186,255)
		prefix = 8
		suffix = 7	
		local shield_modifier = owner:FindModifierByName("modifier_shield_life")
		if shield_modifier == nil then
			shield_modifier = owner:AddNewModifier(owner,nil,"modifier_shield_life",{})
		end
		if shield_modifier ~= nil then shield_modifier:Revise() end
	elseif info_table.message == ALERT_SHIELD_DAMAGE then
		vColor = Vector (46,136,205)
		prefix = 1
		suffix = 7
		local shield_modifier = owner:FindModifierByName("modifier_shield_life")
		if shield_modifier ~= nil then shield_modifier:Revise() end
	elseif info_table.message == ALERT_HEAL then
		vColor = Vector (19,255,104)
		prefix = 1
		prefix = 8	
	elseif info_table.message == ALERT_HEALSCARS then
		vColor = Vector (19,255,104)
		prefix = 1
		suffix = 3
	elseif info_table.message == ALERT_PERMANENT_DAMAGE then
		vColor = Vector (238,42,27)
		suffix = 3
	elseif info_table.message == ALERT_DASH then
		vColor = Vector (244, 219, 59)
		prefix = 6
		display_damage=nil
		return --UNUSED
	end
	
	if display_damage==0 then return end
	
	local wordLength = 0
	if not (display_damage == nil) then wordLength = string.len(tostring(display_damage)) end
	if (prefix>0) then wordLength=wordLength+1 end
	if (suffix>0) then if (suffix>10) then wordLength=wordLength+2 else wordLength=wordLength+1 end	 end
	
	Timers:CreateTimer(1/30,function()
		
		local rvision = owner:GetTeamNumber() == DOTA_TEAM_GOODGUYS
		local dvision = owner:GetTeamNumber() == DOTA_TEAM_BADGUYS
		
		if not rvision then
			local runit = CreateUnitByName("npc_dummy_unit", Vector(0,0,0) , false, nil, nil, DOTA_TEAM_GOODGUYS)
			rvision = runit:CanEntityBeSeenByMyTeam(owner)
			runit:Destroy()
		else 
			local dunit = CreateUnitByName("npc_dummy_unit", Vector(0,0,0) , false, nil, nil, DOTA_TEAM_GOODGUYS)
			dvision = dunit:CanEntityBeSeenByMyTeam(owner)
			dunit:Destroy()
		end
		
		local alert_particle = "particles/interface/msg_damage_helper.vpcf"
		
		if rvision and dvision then
			alert_particle= ParticleManager:CreateParticle( alert_particle, PATTACH_CUSTOMORIGIN, owner )		--PATTACH_OVERHEAD_FOLLOW	
		elseif rvision then 
			alert_particle = ParticleManager:CreateParticleForTeam( alert_particle, PATTACH_CUSTOMORIGIN, owner, DOTA_TEAM_GOODGUYS )			
		elseif rvision then 
			alert_particle = ParticleManager:CreateParticleForTeam( alert_particle, PATTACH_CUSTOMORIGIN, owner, DOTA_TEAM_BADGUYS )
		else return end		

		ParticleManager:SetParticleControl( alert_particle, 0, attachPoint +Vector (0,0,250*GLOBAL_HERO_SCALE))
		ParticleManager:SetParticleControl( alert_particle, 1, Vector ( prefix, display_damage, suffix))	
		ParticleManager:SetParticleControl( alert_particle, 2, Vector ( 3, wordLength, 0))	
		ParticleManager:SetParticleControl( alert_particle, 3, vColor)	
		ParticleManager:ReleaseParticleIndex( alert_particle )	
	end)
end