-- This is the primary barebones gamemode script and should be used to assist in initializing your game mode
BAREBONES_VERSION = "1.00"

-- Set this to true if you want to see a complete debug output of all events/processes done by barebones
-- You can also change the cvar 'barebones_spew' at any time to 1 or 0 for output/no output
BAREBONES_DEBUG_SPEW = false 
SINGLEPLAYER_DEBUG = false

if GameMode == nil then
    DebugPrint( '[BAREBONES] creating barebones game mode' )
    _G.GameMode = class({})
end

-- This library allow for easily delayed/timed actions
require('libraries/timers')
-- This library can be used for advancted physics/motion/collision of units.  See PhysicsReadme.txt for more information.
require('libraries/physics')
-- This library can be used for advanced 3D projectile systems.
require('libraries/projectiles')
-- This library can be used for sending panorama notifications to the UIs of players/teams/everyone
require('libraries/notifications')
-- This library can be used for starting customized animations on units from lua
require('libraries/animations')
-- This library can be used for performing "Frankenstein" attachments on units
--require('libraries/attachments')
-- This library can be used to synchronize client-server data via player/client-specific nettables
require('libraries/playertables')
-- This library can be used to create container inventories or container shops
require('libraries/containers')
-- This library provides a searchable, automatically updating lua API in the tools-mode via "modmaker_api" console command
--require('libraries/modmaker')
-- This library provides an automatic graph construction of path_corner entities within the map
require('libraries/pathgraph')
-- This library (by Noya) provides player selection inspection and management from server lua
require('libraries/selection')

-- These internal libraries set up barebones's events and processes.  Feel free to inspect them/change them if you need to.
require('internal/gamemode')
require('internal/events')

-- settings.lua is where you can specify many different properties for your game mode and is one of the core barebones files.
require('settings')
-- events.lua is where you can specify the actions to be taken when any event occurs and is one of the core barebones files.
require('events')


--DOTA TACTICS
require('mechanics/variables')
require('mechanics/keyvalues')
require('mechanics/alerts')
require('mechanics/playerhandler')
require('mechanics/singleplayer_bots')
require('mechanics/map/covers')
require('mechanics/map/rune_spawner')
require('mechanics/turn_based_tactics')
require('mechanics/items/item_swapper')
require('mechanics/heroes/orders')
require('tutorial_expansion')

-- This is a detailed example of many of the containers.lua possibilities, but only activates if you use the provided "playground" map
if GetMapName() == "playground" then
  require("examples/playground")
end

--require("examples/worldpanelsExample")

--[[
  This function should be used to set up Async precache calls at the beginning of the gameplay.

  In this function, place all of your PrecacheItemByNameAsync and PrecacheUnitByNameAsync.  These calls will be made
  after all players have loaded in, but before they have selected their heroes. PrecacheItemByNameAsync can also
  be used to precache dynamically-added datadriven abilities instead of items.  PrecacheUnitByNameAsync will 
  precache the precache{} block statement of the unit and all precache{} block statements for every Ability# 
  defined on the unit.

  This function should only be called once.  If you want to/need to precache more items/abilities/units at a later
  time, you can call the functions individually (for example if you want to precache units in a new wave of
  holdout).

  This function should generally only be used if the Precache() function in addon_game_mode.lua is not working.
]]
function GameMode:PostLoadPrecache()
  DebugPrint("[BAREBONES] Performing Post-Load precache")    
  --PrecacheItemByNameAsync("item_example_item", function(...) end)
  --PrecacheItemByNameAsync("example_ability", function(...) end)

  --PrecacheUnitByNameAsync("npc_dota_hero_viper", function(...) end)
  --PrecacheUnitByNameAsync("npc_dota_hero_enigma", function(...) end)
end

--[[
  This function is called once and only once as soon as the first player (almost certain to be the server in local lobbies) loads in.
  It can be used to initialize state that isn't initializeable in InitGameMode() but needs to be done before everyone loads in.
]]
function GameMode:OnFirstPlayerLoaded()
  DebugPrint("[BAREBONES] First Player has loaded")
end

--[[
  This function is called once and only once after all players have loaded into the game, right as the hero selection time begins.
  It can be used to initialize non-hero player state or adjust the hero selection (i.e. force random etc)
]]
function GameMode:OnAllPlayersLoaded()
  DebugPrint("[BAREBONES] All Players have loaded into the game")
end

LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
--LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_destroy_item","mechanics/items/modifier_destroy_item.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_remove_attributes","mechanics/items/modifier_remove_attributes.lua", LUA_MODIFIER_MOTION_NONE )
				
function GameMode:OnHeroInGame(hero)
  DebugPrint("[BAREBONES] Hero spawned in game for first time -- " .. hero:GetUnitName())

	if hero.firstSpawned == true then
		return
	end
	
	
	--hero:AddNewModifier(hero,nil,"generic_modifier_movespeed_haste",{speed=20,turns = 1,nodraw=1})
		
	for i=0, 6 do
		local abil = hero:GetAbilityByIndex(i)
		if abil ~= nil then
			  			  
			abil.LastCastTurn = 2;
			hero:AddNewModifier(hero,abil,"modifier_ability_cooldown",{})
			  
		end
	end
  
  hero.firstSpawned = true  
    
  hero:SetAttackCapability(0)
  local item = CreateItem("item_ready_up", hero, hero)
  hero:AddItem(item)
  
  hero:SwapItems(DOTA_ITEM_SLOT_1,TP_SCROLL_SLOT)

	hero:AddItem(CreateItem("item_walkboots", hero, hero))  
  hero:SwapItems(DOTA_ITEM_SLOT_1,WALKBOOTS_SLOT)

  local itemA = nil
	if hero:GetPrimaryAttribute() == 0 then --str	
	  itemA = CreateItem("item_selfshield", hero, hero)
	  hero:AddItem(itemA)
	elseif hero:GetPrimaryAttribute() == 1 then --agi
	  itemA = CreateItem("item_might", hero, hero)
	  hero:AddItem(itemA)	
	elseif hero:GetPrimaryAttribute() == 2 then --int	
	  itemA = CreateItem("item_regen", hero, hero)
	  hero:AddItem(itemA)
	end
	
	  local itemB = CreateItem("item_blinky", hero, hero)
	  hero:AddItem(itemB)

	  local itemC = CreateItem("item_unstoppable", hero, hero)
	  hero:AddItem(itemC)

				--[[itemA.LastCastTurn = 2;
				hero:AddNewModifier(hero,itemA,"modifier_ability_cooldown",{})
				itemB.LastCastTurn = 2;
				hero:AddNewModifier(hero,itemB,"modifier_ability_cooldown",{})
				itemC.LastCastTurn = 2;
				hero:AddNewModifier(hero,itemC,"modifier_ability_cooldown",{})]]
				

	local itemD = CreateItem("item_respec", hero, hero)
	hero:AddItem(itemD)
	--hero:AddNewModifier(hero,itemD,"modifier_destroy_item",{})	
	--hero:AddNewModifier(hero,itemD,"modifier_remove_attributes",{})	
end

--[[
  This function is called once and only once when the game completely begins (about 0:00 on the clock).  At this point,
  gold will begin to go up in ticks if configured, creeps will spawn, towers will become damageable etc.  This function
  is useful for starting any game logic timers/thinkers, beginning the first round, etc.
]]
function GameMode:OnGameInProgress()
  DebugPrint("[BAREBONES] The game has officially begun")

 
end



-- This function initializes the game mode and is called before anyone loads into the game
-- It can be used to pre-initialize any values/tables that will be needed later
function GameMode:InitGameMode()
  GameMode = self
  DebugPrint('[BAREBONES] Starting to load Barebones gamemode...')

  -- Commands can be registered for debugging purposes or as functions that can be called by the custom Scaleform UI
  Convars:RegisterCommand( "command_example", Dynamic_Wrap(GameMode, 'ExampleConsoleCommand'), "A console command example", FCVAR_CHEAT )
  GameRules:GetGameModeEntity():SetExecuteOrderFilter( Dynamic_Wrap( GameMode, "FilterExecuteOrder" ), self )
  
  DebugPrint('[BAREBONES] Done loading Barebones gamemode!\n\n')
  GameRules.AbilitiesKV = LoadKeyValues("scripts/npc/npc_abilities_custom.kv")
  
  local GameModeEntity=GameRules:GetGameModeEntity()
  
	GameModeEntity:SetCustomAttributeDerivedStatValue(0,0)	--STRENGTH_DAMAGE = 0
	GameModeEntity:SetCustomAttributeDerivedStatValue(1,20)	--STRENGTH_HP = 1
	GameModeEntity:SetCustomAttributeDerivedStatValue(2,0)	--STRENGTH_HP_REGEN = 2
	GameModeEntity:SetCustomAttributeDerivedStatValue(3,0)	--STRENGTH_STATUS_RESISTANCE_PERCENT = 3
	GameModeEntity:SetCustomAttributeDerivedStatValue(4,0)	--STRENGTH_MAGIC_RESISTANCE_PERCENT = 4
	
	GameModeEntity:SetCustomAttributeDerivedStatValue(5,0)	--AGILITY_DAMAGE = 5
	GameModeEntity:SetCustomAttributeDerivedStatValue(6,0)	--AGILITY_ARMOR = 6
	GameModeEntity:SetCustomAttributeDerivedStatValue(7,0)	--AGILITY_ATTACK_SPEED = 7
	GameModeEntity:SetCustomAttributeDerivedStatValue(8,0)	--AGILITY_MOVE_SPEED_PERCENT = 8
	
	GameModeEntity:SetCustomAttributeDerivedStatValue(9,0)	--INTELLIGENCE_DAMAGE = 9
	GameModeEntity:SetCustomAttributeDerivedStatValue(10,0)	--INTELLIGENCE_MANA = 10
	GameModeEntity:SetCustomAttributeDerivedStatValue(11,0)	--INTELLIGENCE_MANA_REGEN = 11
	GameModeEntity:SetCustomAttributeDerivedStatValue(12,0)	--INTELLIGENCE_SPELL_AMP_PERCENT = 12
	GameModeEntity:SetCustomAttributeDerivedStatValue(13,0)	--INTELLIGENCE_MAGIC_RESISTANCE_PERCENT = 13
	
	GameModeEntity:SetCustomAttributeDerivedStatValue(14,0)	
	GameModeEntity:SetCustomAttributeDerivedStatValue(15,0)	
	GameModeEntity:SetCustomAttributeDerivedStatValue(16,0)	
	GameModeEntity:SetCustomAttributeDerivedStatValue(17,0)	
	GameModeEntity:SetCustomAttributeDerivedStatValue(18,0)	
	GameModeEntity:SetCustomAttributeDerivedStatValue(19,0)	
	GameModeEntity:SetCustomAttributeDerivedStatValue(20,0)	
  
  GameMode:InitializeTurnBasedTactics()
end

-- This is an example console command
function GameMode:ExampleConsoleCommand()
  print( '******* Example Console Command ***************' )
  local cmdPlayer = Convars:GetCommandClient()
  if cmdPlayer then
    local playerID = cmdPlayer:GetPlayerID()
    if playerID ~= nil and playerID ~= -1 then
      -- Do something here for the player who called this command
      PlayerResource:ReplaceHeroWith(playerID, "npc_dota_hero_viper", 1000, 1000)
    end
  end

  print( '*********************************************' )
end