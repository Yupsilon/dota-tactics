modifier_ability_cooldown = class({})

-----------------------------------------------------------------------------

function modifier_ability_cooldown:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function modifier_ability_cooldown:IsPermanent()
	return true
end

--------------------------------------------------------------------------------

function modifier_ability_cooldown:IsHidden()
	return not BAREBONES_DEBUG_SPEW -- show cooldown modifier in debug mode
end

--------------------------------------------------------------------------------

function modifier_ability_cooldown:RemoveOnDeath()
	return false
end

--------------------------------------------------------------------------------

function modifier_ability_cooldown:OnCreated( kv )

	
	if IsServer() then
		self:StartIntervalThink( 1/2 )
		self:EndOfTurnAction()
	end
end

--------------------------------------------------------------------------------

function modifier_ability_cooldown:DeclareFunctions()
	return { MODIFIER_PROPERTY_COOLDOWN_PERCENTAGE_ONGOING }	
end

--------------------------------------------------------------------------------

function modifier_ability_cooldown:GetModifierPercentageCooldownOngoing(params)
	return 100
end

--------------------------------------------------------------------------------

function modifier_ability_cooldown:OnIntervalThink()
	if IsServer() then
		self:EndOfTurnAction()
	end
end

--------------------------------------------------------------------------------

function modifier_ability_cooldown:EndOfTurnAction()
		if self:GetAbility()~=nil then
			local cooldown = self:GetAbility():GetCooldownTimeRemaining()
			if cooldown<=0 then
				self:GetAbility():EndCooldown()
				self:Destroy()
			else			
				self:GetAbility():StartCooldown(self:GetAbility():GetCooldownTimeRemaining())
			end
		else
			self:Destroy()
		end	
end
	
--------------------------------------------------------------------------------