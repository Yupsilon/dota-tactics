ability_lancer_onemanarmy = class({})
LinkLuaModifier( "modifier_lancer_onemanarmy","abilities/phantom_lancer/modifier_lancer_onemanarmy.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_dashed_this_turn","abilities/generic/generic_modifier_dashed_this_turn.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_energized","abilities/generic/generic_modifier_energized.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_lancer_onemanarmy:DashAction(keys)

	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
	self:UseResources(false,false,true)	
	Alert_Mana({unit=self:GetCaster(),manavalue=0-self:GetManaCost(self:GetLevel());} )
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_dashed_this_turn",{turns = 1})
		
	EmitSoundOn( "Hero_PhantomLancer.Doppelwalk", self:GetCaster() )	
	
	if self:GetSpecialValueFor("unstoppable_duration")>0 then
		Timers:CreateTimer(PLAYER_TIME_MODIFIER,function()
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_unstoppable",{turns = self:GetSpecialValueFor("unstoppable_duration")})		
		end)
	end
	if self:GetLevel() == 2 then
			Alert_CooldownRefresh(self:GetCaster(),1,-10)		
	elseif self:GetLevel() == 4 then
	
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_energized",{
			energy=self:GetSpecialValueFor("bonus_energy_boost"),
			turns=self:GetSpecialValueFor("bonus_energy_duration"),
			nodraw=1
		})
	end
			
	--first remove all illusions				
	for index,modifier in pairs(self:GetCaster():FindAllModifiersByName("generic_modifier_summonedunit")) do		
				
		if modifier.AssignedUnit~= nil and modifier.AssignedUnit:IsAlive() then
			modifier:Destroy()
		end
	end
	
	for index,modifier in pairs(self:GetCaster():FindAllModifiersByName("modifier_order_cast")) do	
		if modifier:GetAbility():GetAbilityName() == "lancer_juxtapouse" then
			modifier:Destroy();
		end
	end
			
	--do caster
	if keys.target_point_A~=nil then
			self:dashPhantomLancer(self:GetCaster(),keys.target_point_A);			
	end
	--1st clone
	if keys.target_point_B~=nil then
			local illusion = self:GetCaster():GetAbilityByIndex(0):CreateIllusion(self:GetCaster():GetAbsOrigin(),2)
			self:dashPhantomLancer(illusion,keys.target_point_B);			
	end
	--2nd clone
	if keys.target_point_C~=nil then
			local illusion = self:GetCaster():GetAbilityByIndex(0):CreateIllusion(self:GetCaster():GetAbsOrigin(),1)
			self:dashPhantomLancer(illusion,keys.target_point_C);			
	end
	return ABILITYACTION_COMPLETE;
end

function ability_lancer_onemanarmy:dashPhantomLancer(unit, point)

			local dash_range = self:GetSpecialValueFor("dash_range")
			local projectile_radius = self:GetSpecialValueFor("butt_size")
			local startPoint = self:GetCaster():GetAbsOrigin() + Vector(0,0,50);		
			local direction = (point-startPoint)
			direction.z=0
			
			
				unit:FaceTowards(point)
		Timers:CreateTimer(PLAYER_TIME_MODIFIER/5, function()
				local hitunit=false
				local projectile = {
				  --EffectName = projectile_model,	
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint,
				  fDistance = math.min(direction:Length2D(),dash_range),
				  fStartRadius = projectile_radius,
				  fEndRadius = projectile_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  --fExpireTime = 8.0,
				  vVelocity = direction:Normalized() * dash_range , 
				  UnitBehavior = PROJECTILES_NOTHING,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  --bCutTrees = true,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_DESTROY,
				  GroundBehavior = PROJECTILES_DESTROY,
				  fGroundOffset = 0,
				  bZCheck = false,
				  bGroundLock = false,
				  draw = false,
				  bHitScan=true,
				   OnFinish = function(instance, pos) 
					
						if not unit:HasModifier("modifier_lancer_onemanarmy") then
							unit:AddNewModifier(self:GetCaster(),self,"modifier_lancer_onemanarmy",{dash_destination_x=instance:GetPosition().x,dash_destination_y=instance:GetPosition().y})
						end
				  end,
				}
				
						
				Projectiles:CreateProjectile(projectile)
			end);

end

--------------------------------------------------------------------------------

function ability_lancer_onemanarmy:getAbilityType()
	return ABILITY_TYPE_POINT_MULTIPLE;
end

--------------------------------------------------------------------------------

function ability_lancer_onemanarmy:RequiresGridSnap()
	return true;
end

--------------------------------------------------------------------------------

function ability_lancer_onemanarmy:RequiresHitScanCheck()
	return true;
end

--------------------------------------------------------------------------------

function ability_lancer_onemanarmy:disablesMovement()
	return true;
end

--------------------------------------------------------------------------------

function ability_lancer_onemanarmy:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_lancer_onemanarmy:GetCastPoints()
	return 3;
end

--------------------------------------------------------------------------------

function ability_lancer_onemanarmy:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_lancer_onemanarmy:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_lancer_onemanarmy:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("butt_size")
end

--------------------------------------------------------------------------------

function ability_lancer_onemanarmy:GetMinRange(coneID)	

	return 0
end

--------------------------------------------------------------------------------

function ability_lancer_onemanarmy:GetMaxRange(coneID)	
	if self:GetLevel() == 3 then
		return self:GetSpecialValueFor("dash_range")+self:GetSpecialValueFor("bonus_range")	
	end
	return self:GetSpecialValueFor("dash_range")
end

--------------------------------------------------------------------------------

function ability_lancer_onemanarmy:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_lancer_onemanarmy:GetAOERadius()	
	return self:GetSpecialValueFor("butt_size")
end

--------------------------------------------------------------------------------