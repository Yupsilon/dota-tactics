ability_lancer_phantom_cone = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_reveal","abilities/generic/generic_modifier_reveal.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_lancer_phantom_cone:SolveAction(keys)

	if keys.target_point_A~=nil then
				
			local sound = "Hero_PhantomLancer.SpiritLance.Impact"
			
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;			
			self:UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})	
				
			self:GetCaster():FaceTowards(keys.target_point_A)			
			StartAnimation(self:GetCaster(), {duration=PLAYER_TIME_MODIFIER, rate=1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_CAST_ABILITY_1})	
			
			local allModifiers = self:GetCaster():FindAllModifiersByName("generic_modifier_summonedunit")	
			
			for index,modifier in pairs(allModifiers) do		
				
				local illusion = modifier.AssignedUnit
				if illusion~= nil and illusion:IsAlive() then
				
					illusion:FaceTowards(keys.target_point_A)
					StartAnimation(illusion, {duration=PLAYER_TIME_MODIFIER, rate=1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_CAST_ABILITY_1})
				end
			end
			
			Timers:CreateTimer(PLAYER_TIME_MODIFIER*2/5, function()
				self:CastInstance(keys.caster,keys.target_point_A)				
				EmitSoundOn( sound, self:GetCaster() )		
					
				for index,modifier in pairs(allModifiers) do		
					
					local illusion = modifier.AssignedUnit
					if illusion~= nil and illusion:IsAlive() then							
						self:CastInstance(illusion,keys.target_point_A)				
						EmitSoundOn( sound, modifier.AssignedUnit )
					end
				end
			end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_cone:CastInstance(caster,vEnd)

			local cleave_range = self:GetSpecialValueFor("cone_range")
			local cleave_angle = self:GetConeArc(0,0)/2
				
			local vStart = caster:GetAbsOrigin()
			local direction = (vEnd-vStart):Normalized()
			direction.z=0
			
				local cone_table = {				
					caster = self:GetCaster(),
					startPoint = vStart,
					startAngle = math.atan2(direction.x,direction.y),
					search_range = cleave_range,
					search_angle = cleave_angle,
					searchteam = DOTA_UNIT_TARGET_TEAM_ENEMY,
					searchtype = DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
					searchflags = 0
				}
			
				local projectile_count = 15
				local projectile_angle = cone_table.search_angle/projectile_count*math.pi/180
				local animtime = PLAYER_TIME_MODIFIER/2
	
				for i=0,projectile_count-1 do		
					local fireAngle = cone_table.startAngle-projectile_angle*(projectile_count-1)/2+projectile_angle*i
					local newdirection = Vector(math.sin(fireAngle),math.cos(fireAngle),0)									
				   
						local nChainParticleFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_phantom_lancer/phantomlancer_spiritlance_projectile.vpcf", PATTACH_CUSTOMORIGIN, caster )		
						--ParticleManager:SetParticleAlwaysSimulate( nChainParticleFXIndex )
						ParticleManager:SetParticleControlEnt( nChainParticleFXIndex, 0, caster, PATTACH_POINT_FOLLOW, "attach_spear_end",  cone_table.startPoint, false )						
						ParticleManager:SetParticleControl( nChainParticleFXIndex, 1, cone_table.startPoint + newdirection * cone_table.search_range+Vector (0,0,100))
						ParticleManager:SetParticleControl( nChainParticleFXIndex, 2, Vector( cone_table.search_range/animtime, 9999, 9999 ) )
						--ParticleManager:SetParticleControl( nChainParticleFXIndex, 4, Vector( 1, 0, 0 ) )
						--ParticleManager:SetParticleControl( nChainParticleFXIndex, 5, Vector( 0, 0, 0 ) )
						
						Timers:CreateTimer(animtime,function()
							ParticleManager:DestroyParticle(nChainParticleFXIndex,true)
						end)
				end
			
				local enemies = FindUnitsInCone(cone_table)
				
				if #enemies > 0 then
					for _,enemy in pairs(enemies) do
						if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
					
								local enemy_direction = (enemy:GetAbsOrigin()-vStart)
								local projectile = {
								EffectAttach = PATTACH_WORLDORIGIN,			  			  
								vSpawnOrigin = vStart+Vector(0,0,30),
								fDistance = cleave_range*2,
								fStartRadius = 50,
								fEndRadius = 50,
								fCollisionRadius = 30,
								  Source = self:GetCaster(),
								  vVelocity = enemy_direction:Normalized() * cleave_range, 
								  UnitBehavior = PROJECTILES_NOTHING,
								  bMultipleHits = true,
								  bIgnoreSource = true,
								  TreeBehavior = PROJECTILES_NOTHING,
								  bTreeFullCollision = false,
								  WallBehavior = PROJECTILES_DESTROY,
								  GroundBehavior = PROJECTILES_DESTROY,
								  fGroundOffset = 0,
								  bZCheck = false,
								  bGroundLock = false,
								  draw = false,
								  bUseFindUnitsInRadius = true,
								  bHitScan=true,
								  UnitTest = function(instance, unit) return unit==enemy end,
								  OnUnitHit = function(instance, unit) 
															
														
										local damage_table = {
											victim = enemy,
											attacker = self:GetCaster(),
											damage = self:GetSpecialValueFor("cone_damage_main"),
											damage_type = DAMAGE_TYPE_MAGICAL,
											cover_reduction = 1
										}	
										
										
										if enemy:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())~=nil then
											
											damage_table.damage = self:GetSpecialValueFor("cone_damage_side")
											
											if self:GetLevel() == 2 then
												enemy:AddNewModifier(self:GetCaster(),self,"generic_modifier_reveal",{turns=self:GetSpecialValueFor("bonus_reveal_duration")})
											elseif self:GetLevel() == 4 then
												damage_table.damage = damage_table.damage + self:GetSpecialValueFor("bonus_damage_side")
											end
										else
										
											enemy:AddNewModifier(self:GetCaster(),self,"modifier_hit",{turns=1})
											Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("cone_energy"), energy=true});
										end										
																
										if IsTargetOverCover(instance,damage_table.attacker) then
											damage_table.cover_reduction=1/2
										end

										Alert_Damage( damage_table )
								  end,
								}						
								Projectiles:CreateProjectile(projectile)
							
							end
					
						end
				end
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_cone:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_cone:getAbilityType()
	return ABILITY_TYPE_CONE;
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_cone:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_cone:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_cone:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_cone:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_cone:GetConeArc(coneID,coneRange)

	if coneID == 1 then
		return 8
	end

	local cone = self:GetSpecialValueFor("cone_angle")
	
	if self:GetLevel() == 3 then
		cone = cone + self:GetSpecialValueFor("bonus_cone_angle")
	end

	return cone*2
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_cone:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_cone:GetMaxRange(coneID)	
	return 99999
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_cone:GetForcedMinRange(coneID)	
	return self:GetSpecialValueFor("cone_range")
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_cone:GetForcedMaxRange(coneID)	
	return self:GetSpecialValueFor("cone_range")
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_cone:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------