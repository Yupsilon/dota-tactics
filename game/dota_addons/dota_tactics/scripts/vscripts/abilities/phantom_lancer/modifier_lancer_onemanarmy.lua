modifier_lancer_onemanarmy = class({})

--------------------------------------------------------------------------------

function modifier_lancer_onemanarmy:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_lancer_onemanarmy:OnCreated( kv )

	self.dash_start = self:GetParent():GetAbsOrigin()
	self.dash_end = Vector(kv.dash_destination_x,kv.dash_destination_y,0)
	self.dash_speed = (self:GetAbility():GetCastRange()/30) 
	self.distance_traveled = 0
	self.kb_height = 0

	if IsServer() then
		StartAnimation(self:GetParent(), {duration=PLAYER_TIME_MODIFIER/2, rate=2/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_RUN, translate = "haste"}) 
		
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_phantom_lancer/phantomlancer_doppelwalk.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		self:AddParticle( nFXIndex, false, false, -1, false, false )	
		self.dash_speed = self.dash_speed  / PLAYER_TIME_SIMULTANIOUS*3/2
		
		--EmitSoundOn( "Hero_Earthshaker.Attack.Impact", self:GetCaster() )
		
		self:StartIntervalThink( 1/30 )
		self:OnIntervalThink()
	end
end

--------------------------------------------------------------------------------

function modifier_lancer_onemanarmy:OnIntervalThink()
	if IsServer() then

		self:Activate ( )
		local vectorDelta=self.dash_end-self.dash_start
		vectorDelta.z=0
		
			
			--local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_phantom_lancer/phantomlancer_illusion_destroy.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )				
			--ParticleManager:SetParticleControl( nFXIndex, 0, self:GetParent():GetAbsOrigin() )			
			--ParticleManager:ReleaseParticleIndex( nFXIndex )	
			
		if self.distance_traveled < vectorDelta:Length2D() then
			self:GetParent():SetAbsOrigin(self:GetParent():GetAbsOrigin() + vectorDelta:Normalized() * self.dash_speed)
			self.distance_traveled = self.distance_traveled + (vectorDelta:Normalized() * self.dash_speed):Length2D()
			self.kb_height=math.sin(self.distance_traveled/vectorDelta:Length2D()*math.pi)*1000
			
		else
			self.kb_height=0
			local animationTime= 1/2;
			FindClearSpaceForUnit(self:GetParent(), self.dash_end, true)
			
			--local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_earthshaker/earthshaker_totem_leap_impact.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )				
			--ParticleManager:SetParticleControl( nFXIndex, 0, self.dash_end )			
			--ParticleManager:ReleaseParticleIndex( nFXIndex )	
			
			StartAnimation(self:GetParent(), { duration = animationTime,activity=ACT_DOTA_TELEPORT_END})	
			self:Destroy()
		end
	end
end

--------------------------------------------------------------------------------

function modifier_lancer_onemanarmy:Activate ( )

		if not GameMode:GetCurrentGamePhase() == GAMEPHASE_DASH then return end

		local helix_aoe = self:GetAbility():GetSpecialValueFor("butt_size")
		local caster = self:GetParent()
		local ability = self:GetAbility()
		
		local start = caster:GetAbsOrigin()

					local enemies = FindUnitsInRadius( self:GetParent():GetTeamNumber(), start, self:GetParent(),helix_aoe , DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
					if #enemies > 0 then
						for _,enemy in pairs(enemies) do						
							if enemy ~= nil and ( not enemy:IsInvulnerable() ) and (enemy:HasModifier("generic_modifier_dashed_this_turn")==false)  then
						
									if enemy:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())==nil then
									
										local helix_damage = self:GetAbility():GetSpecialValueFor("dash_damage_main")
										--if enemy:FindModifierByNameAndCaster("modifier_hit",self:GetCaster()) == nil then
										--	helix_damage = self:GetAbility():GetSpecialValueFor("dash_damage_side")
										--end									
										enemy:AddNewModifier(self:GetCaster(),ability,"modifier_hit",{turns=1})										
										
										local direction = enemy:GetAbsOrigin()-start
										direction.z=0
										
										local projectile = {		  			  
										vSpawnOrigin = start+Vector(0,0,30),
										fDistance = helix_aoe*2,
										fStartRadius = 50,
										fEndRadius = 50,
										fCollisionRadius = 30,
										Source = self:GetParent(),
										vVelocity = direction:Normalized() * helix_aoe, 
										UnitBehavior = PROJECTILES_NOTHING,
										bMultipleHits = true,
										bIgnoreSource = true,
										TreeBehavior = PROJECTILES_NOTHING,
										bTreeFullCollision = false,
										WallBehavior = PROJECTILES_DESTROY,
										GroundBehavior = PROJECTILES_DESTROY,
										fGroundOffset = 0,
										bZCheck = false,
										bGroundLock = false,
										bUseFindUnitsInRadius = true,
										bHitScan=true,

										UnitTest = function(instance, unit) return unit==enemy end,
										OnUnitHit = function(instance, unit) 
												
											local damage_table = {
												victim = unit,
												attacker = caster,
												damage = helix_damage,
												damage_type = DAMAGE_TYPE_MAGICAL,
												ability  = ability,
												cover_reduction = 1,
											}
									
											if IsTargetOverCover(instance,damage_table.attacker) then
												damage_table.cover_reduction=1/2
											end
													
											EmitSoundOn( "Hero_PhantomLancer.SpiritLance.Impact", unit )		

											Alert_Damage( damage_table )
										
										  end,	
										}
										Projectiles:CreateProjectile(projectile)
														
								end
							end
						end
					
					end	
end

--------------------------------------------------------------------------------

function modifier_lancer_onemanarmy:GetStatusEffectName()
	return "particles/status_fx/status_effect_phantom_lancer_illstrong.vpcf"
end

--------------------------------------------------------------------------------

function modifier_lancer_onemanarmy:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_OVERRIDE_ANIMATION
	}

	return funcs
end

--------------------------------------------------------------------------------

function modifier_lancer_onemanarmy:GetVisualZDelta( params )
	return self.kb_height
end

--------------------------------------------------------------------------------

function modifier_lancer_onemanarmy:GetOverrideAnimation( params )
	return ACT_DOTA_RUN
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------