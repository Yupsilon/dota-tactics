modifier_lancer_illusionstatus = class({})

--------------------------------------------------------------------------------

function modifier_lancer_illusionstatus:IsHidden()
	return true
end
--------------------------------------------------------------------------------

function modifier_lancer_illusionstatus:RemoveOnDeath()
	return true
end

--------------------------------------------------------------------------------

function modifier_lancer_illusionstatus:OnDestroy(keys)

	if IsServer() then
	
			local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_phantom_lancer/phantomlancer_illusion_destroy.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
			ParticleManager:ReleaseParticleIndex( nFXIndex)
		
	end

end

--------------------------------------------------------------------------------

function modifier_lancer_illusionstatus:CheckState()
	local state = {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
		[MODIFIER_STATE_UNSELECTABLE] = true,
		[MODIFIER_STATE_INVULNERABLE] = true,
		[MODIFIER_STATE_NO_HEALTH_BAR] = true
	}

	return state
end

--------------------------------------------------------------------------------

function modifier_lancer_illusionstatus:GetStatusEffectName()
	return "particles/status_fx/status_effect_phantom_lancer_illusion.vpcf"
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------