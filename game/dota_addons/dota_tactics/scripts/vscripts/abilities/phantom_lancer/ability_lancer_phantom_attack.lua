ability_lancer_phantom_attack = class({})
LinkLuaModifier( "modifier_energy_regeneration","abilities/modifier_energy_regeneration.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_summonedunit","abilities/generic/generic_modifier_summonedunit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_lancer_illusionstatus","abilities/phantom_lancer/modifier_lancer_illusionstatus.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_lancer_phantom_attack","abilities/phantom_lancer/modifier_lancer_phantom_attack.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:GetIntrinsicModifierName()
	return "modifier_energy_regeneration"
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:IsDisabledBySilence()
	return false
end


--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:GetDamageConversion()
	return 0
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:GetRegenerationValue()
	return self:GetSpecialValueFor("base_energy_regen")
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:SolveAction(keys)

	if keys.target_point_A~=nil then
				
			local projectile_range = self:GetSpecialValueFor("projectile_range")
			local projectile_radius = self:GetSpecialValueFor("projectile_radius") 
	
			self.LastCastTurn = math.max(GameMode:GetCurrentTurn()+math.ceil(self:GetCooldown(self:GetLevel())),self.LastCastTurn or -1);		
			
			
			local animationTime = PLAYER_TIME_MODIFIER;
			
			keys.caster:FaceTowards(keys.target_point_A)
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_SPAWN, rate = 1/PLAYER_TIME_MODIFIER, translate="loadout"})	
						   
			local vectorZ = Vector(0,0,10)
			
			local startPoint = keys.caster:GetAbsOrigin() + Vector(0,0,50)*keys.caster:GetModelScale();	
			startPoint=startPoint+(keys.target_point_A-startPoint):Normalized()*projectile_radius/2	
			
			--Timers:CreateTimer(animationTime*25/30, function() 
			
				
			self:CreateIllusion(startPoint,1)
			EmitSoundOn( "Hero_PhantomLancer.SpiritLance.Throw", self:GetCaster() )
				
			--cycle through all illusions
				
			for index,modifier in pairs(self:GetCaster():FindAllModifiersByName("generic_modifier_summonedunit")) do		
				
				local illusion = modifier.AssignedUnit
				if illusion~= nil and illusion:IsAlive() then
				
					local startPoint = illusion:GetAbsOrigin()
					local direction = (keys.target_point_A-startPoint):Normalized()
					direction.z=0	
					
					local hit = 0
					local projectile = {
					  EffectAttach = PATTACH_WORLDORIGIN,			  			  
					  vSpawnOrigin = startPoint+direction*projectile_radius,
					  fDistance = projectile_range-projectile_radius,
					  fStartRadius = projectile_radius,
					  fEndRadius = projectile_radius,
					  fCollisionRadius = 30,
					  Source = self:GetCaster(),
					  vVelocity = direction * projectile_radius/2, 
					  UnitBehavior = PROJECTILES_DESTROY,
					  bMultipleHits = false,
					  bIgnoreSource = true,
					  TreeBehavior = PROJECTILES_NOTHING,
					  bTreeFullCollision = false,
					  WallBehavior = PROJECTILES_DESTROY,
					  GroundBehavior = PROJECTILES_DESTROY,
					  fGroundOffset = 0,
					  bZCheck = false,
					  bGroundLock = false,
					  draw = false,
					  bUseFindUnitsInRadius = true,
					  bHitScan=true,

					  UnitTest = function(instance, unit) return unit:IsInvulnerable() == false and unit:GetTeamNumber() ~= keys.caster:GetTeamNumber() end,
					  OnUnitHit = function(instance, unit) 						
							self:OnProjectileHit(unit,instance:GetPosition())
							hit = 1						
					  end,
					  OnFinish = function(instance, pos) 
							illusion:FaceTowards(pos)
							illusion:AddNewModifier(self:GetCaster(),self,"modifier_lancer_phantom_attack",{dash_destination_x = pos.x, dash_destination_y = pos.y, hit=hit})	
												
							--Timers:CreateTimer(PLAYER_TIME_MODIFIER, function()
							--			modifier:Destroy()
							--end)				  
					  end,
					}						
					Projectiles:CreateProjectile(projectile)
				end
			end
		--end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:OnProjectileHit(hTarget, vLocation)
					
		local energy = self:GetSpecialValueFor("projectile_energy")
		local damage_table = {
							victim = hTarget,
							attacker = self:GetCaster(),
							damage = self:GetSpecialValueFor("projectile_damage_main") or 1,
							damage_type = DAMAGE_TYPE_PHYSICAL,
							ability=self,
							cover_reduction = 1
						}						
						
		if hTarget:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())~=nil then						
		
				damage_table.damage=self:GetSpecialValueFor("projectile_damage_side")
			if self:GetLevel() == 2 then
				damage_table.damage=damage_table.damage+self:GetSpecialValueFor("bonus_damage_side")
			elseif self:GetLevel() == 4 then
				damage_table.damage=damage_table.damage-self:GetSpecialValueFor("bonus_damage_side_penalty")
			end
		else
			if self:GetLevel() == 4 then
				damage_table.damage=damage_table.damage+self:GetSpecialValueFor("bonus_damage_main")
			end
			hTarget:AddNewModifier(self:GetCaster(),self,"modifier_hit",{turns=1})
		end
		
		if self:GetLevel() == 3 then
			energy=energy+self:GetSpecialValueFor("bonus_energy")
		end
					
		if IsTargetPointOverCover(vLocation,damage_table.attacker) then
			damage_table.cover_reduction=1/2
		end
												
		Timers:CreateTimer(PLAYER_TIME_MODIFIER/2, function()
					Alert_Damage(damage_table)
					Alert_Mana({unit=self:GetCaster(), manavalue=energy, energy=true}  )
		end)
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:CreateIllusion(vLocation,lifespan)
					
	local caster = self:GetCaster()
    local ability = self
    local casterTeam = caster:GetTeam()
    local casterPlayerID = caster:GetPlayerOwnerID()

    local illusion = CreateUnitByName(caster:GetUnitName(), vLocation, true, caster, caster, casterTeam)
    illusion:SetControllableByPlayer(casterPlayerID, true)
    illusion:SetOwner(caster)

    illusion:SetMaxHealth(caster:GetMaxHealth())
    illusion:SetHealth(caster:GetHealth())

    illusion:SetMaxMana(caster:GetMaxMana())
    illusion:SetMana(caster:GetMana())

    illusion:SetPhysicalArmorBaseValue(caster:GetPhysicalArmorBaseValue())
    illusion:SetBaseMoveSpeed(caster:GetBaseMoveSpeed())

    illusion:SetBaseAgility(caster:GetBaseAgility())
    illusion:SetBaseIntellect(caster:GetBaseIntellect())
    illusion:SetBaseStrength(caster:GetBaseStrength())

    illusion:SetBaseDamageMin(caster:GetBaseDamageMin())
    illusion:SetBaseDamageMax(caster:GetBaseDamageMax())

    illusion:SetDayTimeVisionRange(0)
    illusion:SetNightTimeVisionRange(0)
	
   -- illusion:AddNewModifier(caster, ability, "modifier_illusion", {})
    illusion:AddNewModifier(caster, ability, "modifier_lancer_illusionstatus", {})
	
	local summonmodifier = self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_summonedunit",{turns = lifespan})
	summonmodifier:AssignUnit(illusion)
	illusion:SetModelScale(self:GetCaster():GetModelScale())
	FindClearSpaceForUnit(illusion, vLocation, true)
		EmitSoundOn( "Hero_PhantomLancer.Doppelganger.Appear", illusion )

    --illusion:AddAbility("conjure_image_lua"):UpgradeAbility(true)
	
    illusion:MakeIllusion()
	InitHeroAbilities(illusion)
	
	illusion:RemoveModifierByName("modifier_destroy_item")
	for i=0,TP_SCROLL_SLOT do
		if illusion:GetItemInSlot(i)~= nil then
			illusion:GetItemInSlot(i):Destroy()
		end
	end
	
	return illusion
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:getAbilityType()
	return 1;
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:RequiresHitScanCheck()
	return true;
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:GetMaxRange(coneID)	
	return 99999
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:GetForcedMinRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:GetForcedMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_lancer_phantom_attack:GetCastRange()	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------