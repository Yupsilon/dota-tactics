ability_lancer_swap_dash = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_dashed_this_turn","abilities/phantom_lancer/generic_modifier_dashed_this_turn.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_might","abilities/generic/generic_modifier_might.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_lancer_swap_dash:DashAction(keys)

	local closest_point = keys.target_point_A or self:GetCaster():GetAbsOrigin()

	if closest_point~=nil then
			
			local startPoint = self:GetCaster():GetAbsOrigin()
				
			self:GetCaster():FaceTowards(keys.target_point_A)			
			StartAnimation(self:GetCaster(), {duration=PLAYER_TIME_SIMULTANIOUS, rate=1/PLAYER_TIME_SIMULTANIOUS, activity=ACT_DOTA_SPAWN})	
						
			local closest_illusion = nil
			local illusion_distance = -1
			
			for index,modifier in pairs(self:GetCaster():FindAllModifiersByName("generic_modifier_summonedunit")	) do		
				
				local illusion = modifier.AssignedUnit
				if illusion~= nil and illusion:IsAlive() then
					local distance = (illusion:GetAbsOrigin()-startPoint):Length2D()				
					if closest_illusion == nil then
						closest_illusion = illusion
						illusion_distance = distance
					else 
						if (illusion_distance<distance) then
							closest_illusion = illusion
							illusion_distance = distance
						end
					end					
				end
			end
			
			if closest_illusion~=nil  then
				self:CastInstance(closest_illusion)
			end
		end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_lancer_swap_dash:CastInstance(uTargetIllusion)

	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;			
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_dashed_this_turn",{turns = 1})
	
	Alert_Mana({unit=self:GetCaster(), manavalue=self:GetSpecialValueFor("dash_energy") , energy=true} );
	
	local startPoint = self:GetCaster():GetAbsOrigin()
	local endPoint = uTargetIllusion:GetAbsOrigin()
	
	local startFX = ParticleManager:CreateParticle( "particles/units/heroes/hero_phantom_lancer/phantom_lancer_doppleganger_illlmove.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )				
	ParticleManager:SetParticleControl( startFX, 0, startPoint )			
	ParticleManager:SetParticleControl( startFX, 1, endPoint )	
	ParticleManager:ReleaseParticleIndex( startFX )	
	
	--local endFX = ParticleManager:CreateParticle( "particles/units/heroes/hero_phantom_lancer/phantom_lancer_doppleganger_illlmove.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )				
	--ParticleManager:SetParticleControl( endFX, 1, startPoint )			
	--ParticleManager:SetParticleControl( endFX, 0, endPoint )	
	--ParticleManager:ReleaseParticleIndex( endFX )	

	Timers:CreateTimer(PLAYER_TIME_SIMULTANIOUS/2,function()
		
		EmitSoundOn( "Hero_PhantomLancer.Doppelganger.PhantomEdge", self:GetCaster() )		
		
		FindClearSpaceForUnit(self:GetCaster(), endPoint, true)	
		FindClearSpaceForUnit(uTargetIllusion, startPoint, true)	
		
		StartAnimation(self:GetCaster(), {duration=PLAYER_TIME_MODIFIER, rate=1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_TELEPORT_END})			
		StartAnimation(self:GetCaster(), {duration=PLAYER_TIME_MODIFIER, rate=1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_TELEPORT_END})
	end)
	
	if self:GetLevel() == 2 then
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_haste",{speed=self:GetSpecialValueFor("bonus_haste"),turns = self:GetSpecialValueFor("bonus_modifier_duration")})
	elseif self:GetLevel() == 3 then
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_might",{value=self:GetSpecialValueFor("bonus_might"),turns = self:GetSpecialValueFor("bonus_modifier_duration")})	
	elseif self:GetLevel() == 4 then
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_shield",{value=self:GetSpecialValueFor("bonus_shield"),turns = self:GetSpecialValueFor("bonus_modifier_duration")})		
	end
end

--------------------------------------------------------------------------------

function ability_lancer_swap_dash:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_lancer_swap_dash:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_lancer_swap_dash:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function ability_lancer_swap_dash:disablesMovement()
	return false;
end

--------------------------------------------------------------------------------

function ability_lancer_swap_dash:ChangesCasterPosition()
	return 0;
end

--------------------------------------------------------------------------------

function ability_lancer_swap_dash:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_lancer_swap_dash:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_lancer_swap_dash:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_lancer_swap_dash:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("pl_butt")*2
end

--------------------------------------------------------------------------------

function ability_lancer_swap_dash:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_lancer_swap_dash:GetMaxRange(coneID)	
	return 9999
end

--------------------------------------------------------------------------------

function ability_lancer_swap_dash:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------