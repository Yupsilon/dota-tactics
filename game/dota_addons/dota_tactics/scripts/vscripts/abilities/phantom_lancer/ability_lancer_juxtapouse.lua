ability_lancer_juxtapouse = class({})
LinkLuaModifier( "modifier_lancer_leaveillusion","abilities/phantom_lancer/modifier_lancer_leaveillusion.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_summonedunit","abilities/generic/generic_modifier_summonedunit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_lancer_illusionstatus","abilities/phantom_lancer/modifier_lancer_illusionstatus.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_absolute","abilities/generic/generic_modifier_movespeed_absolute.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_stealth","abilities/generic/generic_modifier_stealth.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_lancer_juxtapouse:GetIntrinsicModifierName()
	return "modifier_lancer_leaveillusion"
end

--------------------------------------------------------------------------------

function ability_lancer_juxtapouse:IsDisabledBySilence()
	return false
end

--------------------------------------------------------------------------------

function ability_lancer_juxtapouse:PrepAction(keys)

	if self:GetLevel() == 2 then
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_shield",{life=self:GetSpecialValueFor("bonus_shield") , turns = self:GetSpecialValueFor("bonus_shield_duration")})
		return MODIFIERACTION_SHORTPAUSE_SURVIVE
	end
	return MODIFIERACTION_NOPAUSE_SURVIVE
end

--------------------------------------------------------------------------------

function ability_lancer_juxtapouse:SolveAction(keys)
	
	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
	
	if self:GetLevel() == 4 then
		self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_stealth", {turns = self:GetSpecialValueFor("bonus_invisi_duration")} )
		return MODIFIERACTION_SHORTPAUSE_SURVIVE
	end
	return MODIFIERACTION_NOPAUSE_SURVIVE
end

--------------------------------------------------------------------------------

function ability_lancer_juxtapouse:GetPriority()
	return 5;
end

--------------------------------------------------------------------------------

function ability_lancer_juxtapouse:getAbilityType()
	return 1;
end

--------------------------------------------------------------------------------

function ability_lancer_juxtapouse:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function ability_lancer_juxtapouse:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_lancer_juxtapouse:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_lancer_juxtapouse:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_lancer_juxtapouse:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_lancer_juxtapouse:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_lancer_juxtapouse:GetMaxRange(coneID)	
	if self:GetLevel() == 3 then return self:GetSpecialValueFor("projectile_range") + self:GetSpecialValueFor("bonus_range") end
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_lancer_juxtapouse:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------