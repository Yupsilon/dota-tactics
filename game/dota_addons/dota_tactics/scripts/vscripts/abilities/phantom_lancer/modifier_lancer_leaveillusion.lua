modifier_lancer_leaveillusion = class({})

--------------------------------------------------------------------------------

function modifier_lancer_leaveillusion:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_lancer_leaveillusion:WalkMovementAction()
	
	if self:GetParent():IsIllusion() or not self:GetAbility():IsActivated() then
		return MODIFIERACTION_NOPAUSE_DESTROY
	end
	
	local illusion = nil
		
	for index,modifier in pairs(self:GetCaster():FindAllModifiersByName("modifier_order_cast")) do
		if modifier:GetAbility() == self:GetAbility() then
		
			local walkpoint = modifier.orderCastPositions[0]
			local distance = self:GetAbility():GetMaxRange(0)
			local walktime = PLAYER_TIME_MODIFIER/2
			
			illusion = self:GetCaster():GetAbilityByIndex(0):CreateIllusion(self:GetCaster():GetAbsOrigin(),2)
			
			illusion:AddNewModifier(self:GetCaster(), self:GetAbility(), "generic_modifier_movespeed_absolute", {speed = distance/walktime})
			illusion:SetHullRadius(10)
			
			local framesWalk = walktime*30
			
			Timers:CreateTimer(1/30, function()
						
						if (framesWalk>0) then
							illusion:MoveToPosition(walkpoint);
							framesWalk = framesWalk-1
							return 1/30
						else						
							illusion:Stop()							
							return nil
						end
					end);	
			modifier:Destroy()
		end
	end
	
	if illusion == nil then
	
		if not self:GetCaster():IsRooted() then
			if self:GetCaster():HasModifier("modifier_order_move") or self:GetCaster():HasModifier("modifier_order_knockback") then
				illusion = self:GetCaster():GetAbilityByIndex(0):CreateIllusion(self:GetCaster():GetAbsOrigin(),2)
			elseif self:GetCaster():HasModifier("generic_modifier_summonedunit") then
			
				local summons = self:GetCaster():FindAllModifiersByName("generic_modifier_summonedunit")
			
				for index,modifier in pairs(summons) do
					if index >= #summons and modifier.AssignedUnit:IsIllusion() then				
						modifier:SetStackCount(modifier:GetStackCount()+1);					
						break
					end
				end
			end	
		end
	end
	
	return MODIFIERACTION_NOPAUSE_SURVIVE;
end

--------------------------------------------------------------------------------
