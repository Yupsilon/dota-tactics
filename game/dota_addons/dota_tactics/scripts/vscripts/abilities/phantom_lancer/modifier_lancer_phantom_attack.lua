modifier_lancer_phantom_attack = class({})

--------------------------------------------------------------------------------

function modifier_lancer_phantom_attack:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_lancer_phantom_attack:OnCreated( kv )

	self.dash_start = self:GetParent():GetAbsOrigin()
	self.dash_end = Vector(kv.dash_destination_x,kv.dash_destination_y,0)
	self.dash_speed = self:GetAbility():GetSpecialValueFor("projectile_range") * 1/30
	self.attack = kv.hit or 0
	self.distance_traveled = 0

	if IsServer() then
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_phantom_lancer/phantomlancer_doppelwalk.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		self:AddParticle( nFXIndex, false, false, -1, false, false )		
		self.dash_speed=(self.dash_speed)/PLAYER_TIME_MODIFIER*2
		StartAnimation(self:GetParent(), {duration=PLAYER_TIME_MODIFIER/2, rate=2/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_RUN, translate = "haste"}) 
	
		self:StartIntervalThink( 1/30 )
		self:OnIntervalThink()
	end
end

--------------------------------------------------------------------------------

function modifier_lancer_phantom_attack:OnIntervalThink()
	if IsServer() then

		local vectorDelta=self.dash_end-self.dash_start
		vectorDelta.z=0

		if self.distance_traveled <= vectorDelta:Length2D() then
			self:GetParent():SetAbsOrigin(self:GetParent():GetAbsOrigin() + vectorDelta:Normalized() * self.dash_speed)
			self.distance_traveled = self.distance_traveled + (vectorDelta:Normalized() * self.dash_speed):Length2D()
		else
			FindClearSpaceForUnit(self:GetParent(), self.dash_end, true)
			
			local caster = self:GetParent()
			if self.attack==1 then 
				StartAnimation(caster, {duration=PLAYER_TIME_MODIFIER/2, rate=2/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_ATTACK}) 		
				
				local soundunit = self:GetParent() 
				EmitSoundOn( "Hero_PhantomLancer.PreAttack",soundunit )	
				Timers.CreateTimer(PLAYER_TIME_MODIFIER/4, function()
					EmitSoundOn( "Hero_PhantomLancer.Attack", soundunit)	
				end)
			end
			self:Destroy()
		end
	end
end

--------------------------------------------------------------------------------

function modifier_lancer_phantom_attack:CheckState()
	return {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true		
	}
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------