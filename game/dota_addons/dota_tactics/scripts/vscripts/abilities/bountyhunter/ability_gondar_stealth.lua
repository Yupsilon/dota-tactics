ability_gondar_stealth = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_gondar_bleed","abilities/bountyhunter/modifier_gondar_bleed.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "modifier_gondar_bleeding_attack","abilities/bountyhunter/modifier_gondar_bleeding_attack.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_invisible","abilities/generic/generic_modifier_invisible.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_scarred","abilities/generic/generic_modifier_scarred.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_gondar_stealth:PrepAction(keys)

			EmitSoundOn( "Hero_BountyHunter.WindWalk", self:GetCaster() )
	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;			
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			
	local animationTime = PLAYER_TIME_MODIFIER*1.25;			
	StartAnimation(self:GetCaster(), {duration=animationTime, rate=1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_CAST_ABILITY_1})
	
	
	Timers:CreateTimer(animationTime*3/4, function() 
	
		local minvisi = self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_invisible", {turns = self:GetSpecialValueFor("modifier_duration")} )
		local mhaste = self:GetCaster():AddNewModifier( self:GetCaster(), self, "modifier_gondar_bleeding_attack", { turns = self:GetSpecialValueFor("modifier_duration")} )
				
		local hide_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_bounty_hunter/bounty_hunter_windwalk.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())				
		ParticleManager:SetParticleControl(hide_particle, 0, self:GetCaster():GetAbsOrigin())
		ParticleManager:ReleaseParticleIndex(hide_particle)
							
		if self:GetLevel() == 3 then
			self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_movespeed_haste", {value = self:GetSpecialValueFor("bonus_haste"), turns = self:GetSpecialValueFor("bonus_haste_duration")} )
		elseif self:GetLevel() == 2 then
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_shield",{life=self:GetSpecialValueFor("bonus_shield") , turns = self:GetSpecialValueFor("bonus_shield_duration")})	
		end
		
		Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("self_energy"), energy=true} )
	end)
	return MODIFIERACTION_SHORTPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function ability_gondar_stealth:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_gondar_stealth:GetPriority()
	return 1;
end

--------------------------------------------------------------------------------

function ability_gondar_stealth:GetMaxRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_gondar_stealth:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_gondar_stealth:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_gondar_stealth:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_gondar_stealth:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end


--------------------------------------------------------------------------------