modifier_gondar_bleed = class({})

--------------------------------------------------------------------------------

function modifier_gondar_bleed:IsDebuff()
	return true
end

-----------------------------------------------------------------------------

function modifier_gondar_bleed:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function modifier_gondar_bleed:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_gondar_bleed:OnCreated( kv )
	
	local turnsDuration = kv.turns or 1
	self:SetStackCount(turnsDuration)
	
	if IsServer() then			
		self.particle_shield_fx = ParticleManager:CreateParticle("particles/units/heroes/hero_bounty_hunter/bounty_hunter_track_shield.vpcf", PATTACH_OVERHEAD_FOLLOW,  self:GetParent())
		ParticleManager:SetParticleControl(self.particle_shield_fx, 0, self:GetParent():GetAbsOrigin())
		self:AddParticle(self.particle_shield_fx, false, false, -1, false, true)
	
		self.particle_trail_fx = ParticleManager:CreateParticle("particles/units/heroes/hero_bounty_hunter/bounty_hunter_track_trail.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
		ParticleManager:SetParticleControl(self.particle_trail_fx, 0, self:GetParent():GetAbsOrigin())
		ParticleManager:SetParticleControlEnt(self.particle_trail_fx, 1, self:GetParent(), PATTACH_ABSORIGIN_FOLLOW, nil, self:GetParent():GetAbsOrigin(), true)
		ParticleManager:SetParticleControl(self.particle_trail_fx, 8, Vector(1,0,0))
		self:AddParticle(self.particle_trail_fx, false, false, -1, false, false)	
	end
end

--------------------------------------------------------------------------------

function modifier_gondar_bleed:WalkMovementAction()
	
	
		--[[local damage_table = {
			victim = self:GetParent(),
			attacker = self:GetCaster(),
			damage = self:GetAbility():GetSpecialValueFor("bleeding_damage"),
			damage_type = DAMAGE_TYPE_PURE,
			ability = self:GetAbility()
		}
	
		Alert_Damage( damage_table )]]
	
		
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
