ability_gondar_knifecone = class({})
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_energy_regeneration","abilities/modifier_energy_regeneration.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_scarred","abilities/generic/generic_modifier_scarred.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_gondar_knifecone:GetIntrinsicModifierName()
	return "modifier_energy_regeneration"
end

--------------------------------------------------------------------------------

function ability_gondar_knifecone:IsDisabledBySilence()
	return false
end


--------------------------------------------------------------------------------

function ability_gondar_knifecone:GetRegenerationValue()
	return self:GetSpecialValueFor("base_energy_regen")
end

--------------------------------------------------------------------------------

function ability_gondar_knifecone:GetDamageConversion()
	return 0
end

--------------------------------------------------------------------------------

function ability_gondar_knifecone:SolveAction(keys)

	self.LastCastTurn = math.max(GameMode:GetCurrentTurn()+math.ceil(self:GetCooldown(self:GetLevel())),self.LastCastTurn or -1);	
	self.firstHit = false
	if keys.target_point_A~=nil then self:CastInstance(keys.target_point_A) end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_gondar_knifecone:CastInstance(point)

			local cleave_range = self:GetSpecialValueFor("cleave_range")
			local cleave_angle = self:GetSpecialValueFor("cleave_angle")
				
			local startPoint = self:GetCaster():GetAbsOrigin()
			local direction = (point-startPoint):Normalized()
			direction.z=0
					
			local animationTime = PLAYER_TIME_MODIFIER;
						   
				self:GetCaster():FaceTowards(point)			
			StartAnimation(self:GetCaster(), {duration=animationTime, rate=1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_ATTACK_EVENT, translate="twinblade_jinada"})	
								
				EmitSoundOn( "Hero_BountyHunter.PreAttack", self:GetCaster() )
			Timers:CreateTimer(animationTime/2, function() 
			
			EmitSoundOn( "Hero_BountyHunter.Jinada", self:GetCaster() )
			
				--[[local nFXIndex = ParticleManager:CreateParticle( "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave_gods_strength_crit.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )

				
				ParticleManager:SetParticleControl( nFXIndex, 0, startPoint )
				ParticleManager:SetParticleControlForward( nFXIndex, 0, (point-startPoint):Normalized());
				ParticleManager:SetParticleControl( nFXIndex, 2, startPoint )
				ParticleManager:SetParticleControl( nFXIndex, 3, startPoint )
				ParticleManager:SetParticleControl( nFXIndex, 4, startPoint )
				ParticleManager:SetParticleControl( nFXIndex, 5, startPoint )
				ParticleManager:ReleaseParticleIndex( nFXIndex )]]

				local enemies = FindUnitsInCone({				
					caster = self:GetCaster(),
					startPoint = startPoint,
					startAngle = math.atan2(direction.x,direction.y),
					search_range = cleave_range,
					search_angle = cleave_angle,
					searchteam = DOTA_UNIT_TARGET_TEAM_ENEMY,
					searchtype = DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
					searchflags = 0
				})
				
				if #enemies > 0 then
					for _,enemy in pairs(enemies) do
						if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
						
								local enemy_direction = (enemy:GetAbsOrigin()-startPoint)
								local projectile = {
								EffectAttach = PATTACH_WORLDORIGIN,			  			  
								vSpawnOrigin = startPoint+Vector(0,0,30),
								fDistance = cleave_range*2,
								fStartRadius = 50,
								fEndRadius = 50,
								fCollisionRadius = 30,
								  Source = self:GetCaster(),
								  vVelocity = enemy_direction:Normalized() * cleave_range, 
								  UnitBehavior = PROJECTILES_NOTHING,
								  bMultipleHits = true,
								  bIgnoreSource = true,
								  TreeBehavior = PROJECTILES_NOTHING,
								  bTreeFullCollision = false,
								  WallBehavior = PROJECTILES_DESTROY,
								  GroundBehavior = PROJECTILES_DESTROY,
								  fGroundOffset = 0,
								  bZCheck = false,
								  bGroundLock = false,
								  draw = false,
								  bUseFindUnitsInRadius = true,
								  bHitScan=true,
								  --OnFinish = function(instance, pos) ParticleManager:ReleaseParticleIndex( nFXIndex ) end,
								  UnitTest = function(instance, unit) return unit==enemy end,
								  OnUnitHit = function(instance, unit) 
															
														
										local damage_table = {
											victim = enemy,
											attacker = self:GetCaster(),
											damage = self:GetSpecialValueFor("cleave_damage"),
											damage_type = DAMAGE_TYPE_MAGICAL,
											cover_reduction = 1
										}	
										local energyGain = self:GetSpecialValueFor("cleave_energy")
										if self.firstHit == false then
											self.firstHit = true
											damage_table.damage_type = DAMAGE_TYPE_PHYSICAL
										end
										
										local hit_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_beastmaster/beastmaster_wildaxes_hit.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())				
										ParticleManager:SetParticleControl(hit_particle, 0, unit:GetAbsOrigin())
										ParticleManager:ReleaseParticleIndex(hit_particle)
										
										--[[local blood_duration = 0
										
										local track_modifier = enemy:FindModifierByNameAndCaster("modifier_gondar_tracked",self:GetCaster())
										if track_modifier~=nil then
											blood_duration = 1
											
											if track_modifier:GetAbility():GetLevel() == 4 then
												Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=track_modifier:GetAbility():GetSpecialValueFor("bonus_lifesteal")})
											end
										end
										if self:GetCaster():HasModifier("modifier_gondar_bleeding_attack") then
											self:GetCaster():FindModifierByName("modifier_gondar_bleeding_attack"):SetStackCount(1)
											blood_duration=blood_duration+1
										end]]
										
										local refference_modifier = enemy:FindModifierByNameAndCaster("modifier_gondar_bleed",self:GetCaster())
										if refference_modifier~=nil then
											Alert_Damage({
												victim = enemy,
												attacker = self:GetCaster(),
												damage = refference_modifier:GetAbility():GetSpecialValueFor("bonus_bleeding_damage"),
												damage_type = DAMAGE_TYPE_PURE,
												ability  = refference_modifier:GetAbility(),
												})
										end
									
										--[[if blood_duration>0 then
											
											for i=1,blood_duration do	
												enemy:AddNewModifier( self:GetCaster(), self:GetCaster():GetAbilityByIndex(2), "modifier_gondar_bleed", { turns = 2} )	
											end
										end]]

											if self:GetLevel()==2 then --and enemy:HasModifier("generic_modifier_shield")
												damage_table.damage = damage_table.damage+self:GetSpecialValueFor("bonus_damage_aoe")												
											--elseif self:GetLevel()==4 then
											--	local selfheal = self:GetSpecialValueFor("bonus_selfheal")
											--	Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=selfheal})
											elseif self:GetLevel()==3 then
												energyGain = energyGain + self:GetSpecialValueFor("bonus_energy")
											elseif self:GetLevel()==4 and #enemies == 1 then
												damage_table.damage = damage_table.damage+self:GetSpecialValueFor("bonus_damage_single")
											--elseif self:GetLevel()==4 then
												--damage_table.scar_damage_percent = self:GetSpecialValueFor("scar_damage")
											end
																				
																
										if IsTargetOverCover(instance,damage_table.attacker) then
											damage_table.cover_reduction=1/2
										end

										Alert_Damage( damage_table )
										Alert_Mana({unit=self:GetCaster(),manavalue=energyGain, energy=true});
								  end,
								}						
								Projectiles:CreateProjectile(projectile)
												
						end
					end
				end
			end)
end

--------------------------------------------------------------------------------

function ability_gondar_knifecone:getAbilityType()
	return ABILITY_TYPE_CONE;
end

--------------------------------------------------------------------------------

function ability_gondar_knifecone:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_gondar_knifecone:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_gondar_knifecone:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_gondar_knifecone:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_gondar_knifecone:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_gondar_knifecone:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("cleave_angle")*2
end

--------------------------------------------------------------------------------

function ability_gondar_knifecone:GetMinRange(coneID)	
	return self:GetSpecialValueFor("cleave_range")
end

--------------------------------------------------------------------------------

function ability_gondar_knifecone:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("cleave_range")
end

--------------------------------------------------------------------------------

function ability_gondar_knifecone:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------