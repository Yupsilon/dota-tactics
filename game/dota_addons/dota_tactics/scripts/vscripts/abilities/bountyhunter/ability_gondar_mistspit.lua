 ability_gondar_mistspit = class({})
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_reveal","abilities/generic/generic_modifier_reveal.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_invisible","abilities/generic/generic_modifier_invisible.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "modifier_gondar_tracked","abilities/bountyhunter/modifier_gondar_tracked.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_gondar_mistspit:SolveAction(keys)

	if keys.target_point_A~=nil then
				
			local cone_full_range = self:GetMaxRange(0)
			local projectile_radius = self:GetSpecialValueFor("projectile_radius")
			local projectile_angle = (self:GetSpecialValueFor("projectile_angle")*math.pi/180)/2
			local projectile_count = self:GetConeArc(1,0)
	
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,false)	
			Alert_Mana({unit=self:GetCaster(),manavalue=0-self:GetManaCost(self:GetLevel());} )
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			
			local startPoint = keys.caster:GetAbsOrigin() + Vector(0,0,50)*keys.caster:GetModelScale();		
			local direction = (keys.target_point_A-startPoint):Normalized()
			direction.z=0
			startPoint=startPoint+direction*projectile_radius/2
			
			local animationTime = PLAYER_TIME_MODIFIER;
			local decalSpeed = math.max(900,cone_full_range/PLAYER_TIME_MODIFIER)*2
			
			if self:GetLevel() == 3 then
				self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_invisible",{turns = self:GetSpecialValueFor("bonus_invisible_duration")})	
			end
			
				keys.caster:FaceTowards(keys.target_point_A)
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_1, rate = PLAYER_TIME_MODIFIER})
			
			Alert_CooldownRefresh(self:GetCaster(),"ability_gondar_cut_dash",-99)
			Timers:CreateTimer(animationTime/3,function()
	
			EmitSoundOn( "Hero_BountyHunter.WindWalk", self:GetCaster() )
	
				local startangle = math.atan2(direction.x,direction.y)
	
				for i=0,projectile_count-1 do
					
					local fireAngle = startangle+projectile_angle*((projectile_count-1)/2-i)
					local newdirection = Vector(math.sin(fireAngle),math.cos(fireAngle),0)
				
					local nChainParticleFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_dark_willow/dark_willow_ley_cast.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )			ParticleManager:SetParticleControl( nChainParticleFXIndex, 0, startPoint + Vector (0,0,50) )
					ParticleManager:SetParticleControl( nChainParticleFXIndex, 1, startPoint + Vector (0,0,50)  + newdirection*cone_full_range )
					ParticleManager:SetParticleControl( nChainParticleFXIndex, 2, Vector( decalSpeed, 0, 0 ) )
			
					local projectile = {
					  --EffectName = projectile_model,			  			  
					  vSpawnOrigin = startPoint,
					  fDistance = cone_full_range,
					  fStartRadius = projectile_radius,
					  fEndRadius = projectile_radius,
					  fCollisionRadius = 30,
					  Source = self:GetCaster(),
					  vVelocity = newdirection * decalSpeed, 
					  UnitBehavior = PROJECTILES_NOTHING,
					  bMultipleHits = false,
					  bIgnoreSource = true,
					  TreeBehavior = PROJECTILES_NOTHING,
					  bTreeFullCollision = false,
					  WallBehavior = PROJECTILES_DESTROY,
					  GroundBehavior = PROJECTILES_DESTROY,
					  fGroundOffset = 0,
					  bZCheck = false,
					  bGroundLock = false,
					  draw = false,
					  bUseFindUnitsInRadius = true,
					  bHitScan=false,

					  UnitTest = function(instance, unit) return unit:IsInvulnerable() == false and unit:GetTeamNumber() ~= keys.caster:GetTeamNumber() and unit:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())==nil end,
					  OnUnitHit = function(instance, unit) 
						
							self:OnProjectileHit(unit,instance:GetPosition())
							--EmitSoundOn( "Hero_DrowRanger.ProjectileImpact", unit )
						
					  end,
					  OnFinish = function(instance, pos)		  
						ParticleManager:DestroyParticle( nChainParticleFXIndex, false )
						ParticleManager:ReleaseParticleIndex( nChainParticleFXIndex )
								
					  end
					}
							
						
							
					Projectiles:CreateProjectile(projectile)
				end
			end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_gondar_mistspit:GetPriority()
	return 5;
end

--------------------------------------------------------------------------------

function ability_gondar_mistspit:OnProjectileHit(hTarget, vLocation)
					
					if hTarget:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())~=nil then
						return
					else
						hTarget:AddNewModifier(self:GetCaster(),self,"modifier_hit",{turns=1})
					end
					
					local damage_table = {
							victim = hTarget,
							attacker = self:GetCaster(),
							damage = self:GetSpecialValueFor("cleave_damage_full") ,
							damage_type = DAMAGE_TYPE_MAGICAL,
							ability=self,
							cover_reduction = 1
						}
							
					
					if IsTargetPointOverCover(vLocation,damage_table.attacker) then
						damage_table.cover_reduction=1/2					
					end	
										local refference_modifier = hTarget:FindModifierByNameAndCaster("modifier_gondar_bleed",self:GetCaster())
										if refference_modifier~=nil then
												Alert_Damage({
													victim = hTarget,
													attacker = self:GetCaster(),
													damage = refference_modifier:GetAbility():GetSpecialValueFor("bonus_bleeding_damage"),
													damage_type = DAMAGE_TYPE_PURE,
													ability  = refference_modifier:GetAbility(),
												})
											if refference_modifier:GetAbility():GetLevel() == 4 then
												Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=refference_modifier:GetAbility():GetSpecialValueFor("bonus_lifesteal")})
											end
											
											--if self:GetLevel()==3 then
											--	self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_haste",{speed=self:GetSpecialValueFor("bonus_haste"),turns = 1})
											--end
										end
					
					if self:GetLevel() == 2 then
						hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_reveal", {turns = self:GetSpecialValueFor("bonus_reveal_duration") , nodraw=1} )					
					elseif self:GetLevel() == 4 and hTarget:HasModifier("generic_modifier_shield") then
						damage_table.damage = damage_table.damage + self:GetSpecialValueFor("bonus_damage")
					end
				
					Alert_Damage(damage_table)												
					hTarget:AddNewModifier(self:GetCaster(),nil,"modifier_gondar_tracked",{ turns = self:GetSpecialValueFor("modifier_duration") })		
end

--------------------------------------------------------------------------------

function ability_gondar_mistspit:getAbilityType()
	return ABILITY_TYPE_CONE;
end

--------------------------------------------------------------------------------

function ability_gondar_mistspit:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_gondar_mistspit:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_gondar_mistspit:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_gondar_mistspit:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_gondar_mistspit:GetConeArc(coneID,coneRange)
	local cone_angle = self:GetSpecialValueFor("cone_angle")	
	if coneID == 1 then return cone_angle / self:GetSpecialValueFor("projectile_angle") end	
	return cone_angle
end

--------------------------------------------------------------------------------

function ability_gondar_mistspit:GetMinRange(coneID)	
	return self:GetSpecialValueFor("cone_range")
end

--------------------------------------------------------------------------------

function ability_gondar_mistspit:GetMaxRange(coneID)	
	if self:GetLevel()== 2 then return self:GetSpecialValueFor("bonus_cone_range") end
	return self:GetSpecialValueFor("cone_range")
end

--------------------------------------------------------------------------------

function ability_gondar_mistspit:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------