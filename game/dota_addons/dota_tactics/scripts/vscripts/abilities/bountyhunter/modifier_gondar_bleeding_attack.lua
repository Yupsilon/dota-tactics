modifier_gondar_bleeding_attack = class({})

--------------------------------------------------------------------------------

function modifier_gondar_bleeding_attack:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_gondar_bleeding_attack:OnCreated( kv )

	self:SetStackCount(kv.turns or 3)
end

--------------------------------------------------------------------------------

function modifier_gondar_bleeding_attack:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------