ability_gondar_cut_dash = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_gondar_bleed","abilities/bountyhunter/modifier_gondar_bleed.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_stealth","abilities/generic/generic_modifier_stealth.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_dashed_this_turn","abilities/generic/generic_modifier_dashed_this_turn.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_scarred","abilities/generic/generic_modifier_scarred.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "modifier_order_forcefollow","abilities/generic/modifier_order_forcefollow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_follow","mechanics/heroes/modifier_follow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_combobreaker","abilities/generic/generic_modifier_combobreaker.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_gondar_cut_dash:DashAction(keys)

	local target_unit = EntIndexToHScript(keys.target_unit)
	if target_unit~=nil and keys.target_point_B~=nil then
					
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_dashed_this_turn",{turns = 1})
				
			--if self:GetLevel()==3 then
			--	self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_stealth",{ turns = self:GetSpecialValueFor("bonus_invisi_duration")})
			--end	
			
			local animationTime = PLAYER_TIME_SIMULTANIOUS*2;
			StartAnimation(self:GetCaster(), {duration=animationTime, rate=1/PLAYER_TIME_SIMULTANIOUS, activity=ACT_DOTA_ATTACK_EVENT})	
			Alert_Mana({unit=self:GetCaster(), manavalue=self:GetSpecialValueFor("dash_energy") , energy=true} );
	
			EmitSoundOn( "Hero_BountyHunter.WindWalk", self:GetCaster() )
			EmitSoundOn( "Hero_BountyHunter.PreAttack", self:GetCaster() )
			
			Timers:CreateTimer(animationTime/10,function()
				EmitSoundOn( "Hero_BountyHunter.Attack", self:GetCaster() )

				local hide_particle = ParticleManager:CreateParticle("particles/econ/events/ti5/blink_dagger_start_ti5.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())				
				ParticleManager:SetParticleControl(hide_particle, 0, self:GetCaster():GetAbsOrigin())
				ParticleManager:ReleaseParticleIndex(hide_particle)
				
				FindClearSpaceForUnit(self:GetCaster(), keys.target_point_B, true)				
				keys.caster:FaceTowards(keys.target_point_A)

				local hide_particle = ParticleManager:CreateParticle("particles/econ/events/ti5/blink_dagger_end_ti5.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())				
				ParticleManager:SetParticleControl(hide_particle, 0, keys.target_point_B)
				ParticleManager:ReleaseParticleIndex(hide_particle)
				
				Timers:CreateTimer(animationTime/3,function()
					if target_unit ~= nil and ( not target_unit:IsInvulnerable() ) and (target_unit:HasModifier("generic_modifier_dashed_this_turn")==false)  then
					
						local damage_table = {
												victim = target_unit,
												attacker = self:GetCaster(),
												damage = self:GetSpecialValueFor("target_damage"),
												damage_type = DAMAGE_TYPE_PHYSICAL,
												ability  = ability,
												cover_reduction = 1,
											}
											
										--local blood_duration = 0
										
										local track_modifier = target_unit:FindModifierByNameAndCaster("modifier_gondar_tracked",self:GetCaster())
										if track_modifier~=nil then
											--blood_duration = 1
											
											Alert_CooldownRefresh(self:GetCaster(),self,-99)
											local hit_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_dark_willow/dark_willow_leyconduit_marker_ground_steam.vpcf", PATTACH_ABSORIGIN_FOLLOW, target_unit)			
											ParticleManager:SetParticleControl(hit_particle, 0, target_unit:GetAbsOrigin())
											ParticleManager:ReleaseParticleIndex(hit_particle)
											track_modifier:Destroy()
										end
											
										local hit_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_bounty_hunter/bounty_hunter_jinda_slow.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())			
										ParticleManager:SetParticleControl(hit_particle, 0, target_unit:GetAbsOrigin())
										ParticleManager:ReleaseParticleIndex(hit_particle)
													
										
											
										--[[if self:GetCaster():HasModifier("modifier_gondar_bleeding_attack") then
											self:GetCaster():FindModifierByName("modifier_gondar_bleeding_attack"):SetStackCount(1)
											blood_duration=blood_duration+1
										end]]
										
										local refference_modifier = target_unit:FindModifierByNameAndCaster("modifier_gondar_bleed",self:GetCaster())
										if refference_modifier~=nil then
												Alert_Damage({
													victim = target_unit,
													attacker = self:GetCaster(),
													damage = refference_modifier:GetAbility():GetSpecialValueFor("bonus_bleeding_damage"),
													damage_type = DAMAGE_TYPE_PURE,
													ability  = refference_modifier:GetAbility(),
												})
											if refference_modifier:GetAbility():GetLevel() == 4 then
												Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=refference_modifier:GetAbility():GetSpecialValueFor("bonus_lifesteal")})
											end
											
											--if self:GetLevel()==3 then
											--	self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_haste",{speed=self:GetSpecialValueFor("bonus_haste"),turns = 1})
											--end
										end
									
										--[[if blood_duration>0 then											
											for i=1,blood_duration do	
												target_unit:AddNewModifier( self:GetCaster(), self:GetCaster():GetAbilityByIndex(2), "modifier_gondar_bleed", { turns = 2} )	
											end
										end]]
										
											if self:GetLevel()==2 then
												damage_table.scar_damage_percent = self:GetSpecialValueFor("bonus_damage_scar")
											elseif self:GetLevel() == 4 then
												damage_table.damage = damage_table.damage + self:GetSpecialValueFor("bonus_damage")
												
												local ffollow = self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_order_forcefollow",{  })								
												ffollow:SetFollowTarget( target_unit)
											end
											
									
											Alert_Damage( damage_table )
					end
				end)						
			end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_gondar_cut_dash:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_gondar_cut_dash:RequireFirstTargetUnit()
	return true;
end

--------------------------------------------------------------------------------

function ability_gondar_cut_dash:disablesMovement()
	return self:GetLevel()==4;
end

--------------------------------------------------------------------------------

function ability_gondar_cut_dash:ChangesCasterPosition()
	return 1;
end

--------------------------------------------------------------------------------

function ability_gondar_cut_dash:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_gondar_cut_dash:GetCastPoints()
	return 2;
end

--------------------------------------------------------------------------------

function ability_gondar_cut_dash:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_gondar_cut_dash:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_gondar_cut_dash:GetConeArc(coneID,coneRange)
	return self:GetCaster():GetHullRadius()
end

--------------------------------------------------------------------------------

function ability_gondar_cut_dash:GetMinRange(coneID)	
	if coneID==1 then
		return self:GetSpecialValueFor("jump_range")
	end
	
	return 100
end

--------------------------------------------------------------------------------

function ability_gondar_cut_dash:GetMaxRange(coneID)

	if coneID==1 then
	return self:GetSpecialValueFor("jump_range")
	end

	if self:GetLevel()==3 then
		return self:GetSpecialValueFor("dash_range")+self:GetSpecialValueFor("bonus_dash_range")
	end
	return self:GetSpecialValueFor("dash_range")
end

--------------------------------------------------------------------------------

function ability_gondar_cut_dash:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_gondar_cut_dash:GetAOERadius()
	return self:GetCaster():GetHullRadius()/2
end

--[[------------------------------------------------------------------------------

function ability_gondar_cut_dash:GetHasteBonus()	
	if self:GetLevel()==3 then			
		return (100+(self:GetSpecialValueFor("bonus_haste")))/100
	end
	return 1
end

--------------------------------------------------------------------------------]]