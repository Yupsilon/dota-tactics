modifier_gondar_tracked = class({})

--------------------------------------------------------------------------------

function modifier_gondar_tracked:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_gondar_tracked:OnCreated( kv )

	self:SetStackCount(kv.turns or 2)
	
	if IsServer() then			
	
		self.particle_shield_fx = ParticleManager:CreateParticle("particles/units/heroes/hero_dark_willow/dark_willow_leyconduit_debuff_energy.vpcf", PATTACH_ABSORIGIN_FOLLOW,  self:GetParent())
		ParticleManager:SetParticleControl(self.particle_shield_fx, 0, self:GetParent():GetAbsOrigin())
		self:AddParticle(self.particle_shield_fx, false, false, -1, false, true)
			
	end
end

--------------------------------------------------------------------------------

function modifier_gondar_tracked:EndOfTurnAction()
	
		
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------