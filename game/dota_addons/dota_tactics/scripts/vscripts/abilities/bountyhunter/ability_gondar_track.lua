ability_gondar_track = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_reveal","abilities/generic/generic_modifier_reveal.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_gondar_bleed","abilities/bountyhunter/modifier_gondar_bleed.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_stealth","abilities/generic/generic_modifier_stealth.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_gondar_track:PrepAction(keys)

	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
	self:UseResources(false,false,true)	
	--Alert_Mana({unit=self:GetCaster(),manavalue=0-self:GetManaCost(self:GetLevel())} )
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
							   
	if keys.target_point_A~=nil then				
		self:CastInstance(keys.target_point_A)			
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_gondar_track:CastInstance(point)

			local animationTime = PLAYER_TIME_MODIFIER;
	self:GetCaster():FaceTowards(point)		
	StartAnimation(self:GetCaster(), {duration=animationTime,rate=1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_CAST_ABILITY_4})
				
			local projectile_range = self:GetSpecialValueFor("projectile_range")
			local projectile_radius = self:GetSpecialValueFor("projectile_radius") 
			
			local startPoint = self:GetCaster():GetAbsOrigin() + Vector(0,0,50)*self:GetCaster():GetModelScale();	
			local direction = (point-startPoint):Normalized()
			direction.z=0		
			startPoint=startPoint+direction*projectile_radius/2
			
			local animationTime = 1;
			local decalSpeed = 2400	
						   
	if self:GetLevel()==2 then
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_haste",{speed=self:GetSpecialValueFor("bonus_haste"),turns = self:GetSpecialValueFor("bonus_haste_duration"),applythisturn=1})
	end
			if self:GetLevel()==3 then
				self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_stealth",{ turns = self:GetSpecialValueFor("bonus_invisi_duration")})
			end	
			
			Timers:CreateTimer(animationTime*3/4, function() 

			
			
	
				local projectile = {
				  --EffectName = projectile_model,	
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint,--{unit=hero, attach="attach_attack1", offset=Vector(0,0,0)},
				  fDistance = projectile_range,
				  fStartRadius = projectile_radius,
				  fEndRadius = projectile_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  --fExpireTime = 8.0,
				  vVelocity = direction * decalSpeed, 
				  UnitBehavior = PROJECTILES_DESTROY,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  --bCutTrees = true,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_DESTROY,
				  GroundBehavior = PROJECTILES_DESTROY,
				  fGroundOffset = 0,
				  --nChangeMax = 1,
				  --bRecreateOnChange = true,
				  bZCheck = false,
				  bGroundLock = false,
				  draw = false,
				  bUseFindUnitsInRadius = true,
				  bHitScan = true,

				  UnitTest = function(instance, unit) return unit~= self:GetCaster() and unit:IsInvulnerable() == false  and unit:IsInvisible() == false and unit:GetTeamNumber() ~= self:GetCaster():GetTeamNumber() end,
				  OnUnitHit = function(instance, unit) 
					
						self:OnProjectileHit(unit,instance:GetPosition())						
						
						if not self:GetCaster():IsInvisible() then 
							local particle_projectile_fx = ParticleManager:CreateParticle("particles/units/heroes/hero_bounty_hunter/bounty_hunter_track_cast.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())
							ParticleManager:SetParticleControlEnt(particle_projectile_fx, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetCaster():GetAbsOrigin(), true)
							ParticleManager:SetParticleControlEnt(particle_projectile_fx, 1, unit, PATTACH_POINT_FOLLOW, "attach_hitloc", unit:GetAbsOrigin(), true)
							ParticleManager:ReleaseParticleIndex(particle_projectile_fx)
						end
				  end,
				 --[[OnIntervalThink = function(instance)
						ParticleManager:SetParticleControl( self.nChainParticleFXIndex, 1, instance:GetPosition() )
				  end,
				  OnFinish = function(instance, pos)		  
						
				  end]]
				  --OnTreeHit = function(self, tree) ... end,
				  --OnWallHit = function(self, gnvPos) ... end,
				  --OnGroundHit = function(self, groundPos) ... end,
				  --OnFinish = function(self, pos) ... end,
				}						
				Projectiles:CreateProjectile(projectile)
			end)
end


--------------------------------------------------------------------------------

function ability_gondar_track:OnProjectileHit(hTarget, vLocation)
																				
	EmitSoundOn( "Hero_BountyHunter.Target", self:GetCaster() )
					
	local mod_duration = self:GetSpecialValueFor("modifier_duration")
				
	hTarget:AddNewModifier(self:GetCaster(),self,"modifier_gondar_bleed",{turns=mod_duration})
									
	--if self:GetLevel()~=3 then
		--mod_duration = mod_duration + self:GetSpecialValueFor("bonus_modifier_duration")
	--elseif self:GetLevel()==4 then
	--if self:GetLevel()==2 then
	--	hTarget:AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_slow",{move_slow = self:GetSpecialValueFor("bonus_slow_strength"), turns=self:GetSpecialValueFor("bonus_slow_duration")})
	--end
							
	local reveal_modifier = hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_reveal", {turns = 1 , nodraw=1} )
						
	--local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_ogre_magi/ogre_magi_bloodlust_buff.vpcf", PATTACH_ABSORIGIN_FOLLOW, hTarget )
	--reveal_modifier:AddParticle( nFXIndex, false, false, -1, false, false )	
end

--------------------------------------------------------------------------------

function ability_gondar_track:getAbilityType()
	return ABILITY_TYPE_POINT_MULTIPLE;
end

--------------------------------------------------------------------------------

function ability_gondar_track:removesInvisible()
	return true;
end

--------------------------------------------------------------------------------

function ability_gondar_track:GetPriority()
	return 2;
end

--------------------------------------------------------------------------------

function ability_gondar_track:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function ability_gondar_track:RequiresHitScanCheck()
	return true;
end

--------------------------------------------------------------------------------

function ability_gondar_track:GetCastPoints()


	return 1;
end

--------------------------------------------------------------------------------

function ability_gondar_track:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_gondar_track:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_gondar_track:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_gondar_track:GetMinRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_gondar_track:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_gondar_track:GetCastRange()	
	return self:GetMaxRange(0)
end

--[[------------------------------------------------------------------------------

function ability_gondar_track:GetHasteBonus()	
	if self:GetLevel()==2 then			
		return (100+(self:GetSpecialValueFor("bonus_haste")))/100
	end
	return 1
end

--------------------------------------------------------------------------------]]