 ability_gondar_shurikens = class({})
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_gondar_bleed","abilities/bountyhunter/modifier_gondar_bleed.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_gondar_bleed","abilities/bountyhunter/modifier_gondar_bleed.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_scarred","abilities/generic/generic_modifier_scarred.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_gondar_shurikens:IsDisabledBySilence()
	return true
end

--------------------------------------------------------------------------------

function ability_gondar_shurikens:SolveAction(keys)

	if keys.target_point_A~=nil then
				
			local projectile_range = self:GetSpecialValueFor("projectile_range")
			local projectile_radius = self:GetSpecialValueFor("projectile_radius")*2
			
			self.projectile_launch = 0
			self.projectile_hit = 0
	
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			
			local startPoint = keys.caster:GetAbsOrigin() + Vector(0,0,50)*keys.caster:GetModelScale();		
			local direction = (keys.target_point_A-startPoint):Normalized()
			direction.z=0
			startPoint=startPoint+direction*projectile_radius/2
			
			local cone_arc = self:GetConeArc(0,(keys.target_point_A-keys.caster:GetAbsOrigin()):Length2D())
			local projectile_count = self:GetConeArc(1,0)
			local projectile_angle = cone_arc/projectile_count*math.pi/180
			
			local animationTime = PLAYER_TIME_MODIFIER;
			local decalSpeed = math.max(900,projectile_range/PLAYER_TIME_MODIFIER)*3
						
			keys.caster:FaceTowards(keys.target_point_A)
			StartAnimation(self:GetCaster(), {duration=animationTime, rate=1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_CAST_ABILITY_1})
			
			Timers:CreateTimer(animationTime*1/4,function()
	
			EmitSoundOn( "Hero_BountyHunter.Shuriken", self:GetCaster() )
			
	
			local startangle = math.atan2(direction.x,direction.y)
	
				for i=0,projectile_count-1 do											
				   
						local nChainParticleFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_bounty_hunter/bounty_hunter_suriken_toss.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster() )		
						ParticleManager:SetParticleAlwaysSimulate( nChainParticleFXIndex )
						ParticleManager:SetParticleControlEnt( nChainParticleFXIndex, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_attack1",  startPoint, true )
						ParticleManager:SetParticleControl( nChainParticleFXIndex, 1, startPoint )
						ParticleManager:SetParticleControl( nChainParticleFXIndex, 2, Vector( decalSpeed, 0, 0 ) )
						ParticleManager:SetParticleControl( nChainParticleFXIndex, 4, Vector( 1, 0, 0 ) )
						ParticleManager:SetParticleControl( nChainParticleFXIndex, 5, Vector( 0, 0, 0 ) )
						ParticleManager:SetParticleControlEnt( nChainParticleFXIndex, 7, self:GetCaster(), PATTACH_CUSTOMORIGIN, nil, self:GetCaster():GetOrigin(), true )	
	
					local fireAngle = startangle-projectile_angle*(projectile_count-1)/2+projectile_angle*i
					local newdirection = Vector(math.sin(fireAngle),math.cos(fireAngle),0)
			
					local projectile = {			  			  
					  vSpawnOrigin = startPoint,
					  fDistance = projectile_range-projectile_radius,
					  fStartRadius = projectile_radius,
					  fEndRadius = projectile_radius,
					  fCollisionRadius = 30,
					  Source = self:GetCaster(),
					  vVelocity = newdirection * decalSpeed, 
					  UnitBehavior = PROJECTILES_DESTROY,
					  bMultipleHits = false,
					  bIgnoreSource = true,
					  TreeBehavior = PROJECTILES_NOTHING,
					  bTreeFullCollision = false,
					  WallBehavior = PROJECTILES_DESTROY,
					  GroundBehavior = PROJECTILES_DESTROY,
					  fGroundOffset = 0,
					  bZCheck = false,
					  bGroundLock = false,
					  draw = false,
					  bUseFindUnitsInRadius = true,

					  UnitTest = function(instance, unit) return (not unit:IsInvulnerable()) and not (unit:GetTeamNumber()== self:GetCaster():GetTeamNumber()) end,
					  OnUnitHit = function(instance, unit) 
						
							self:OnProjectileHit(unit,instance)
							EmitSoundOn( "Hero_BountyHunter.Shuriken.Impact", unit )
						
					  end,
					  OnIntervalThink = function(instance)
							ParticleManager:SetParticleControl( nChainParticleFXIndex, 1, instance:GetPosition() )
					  end,
					  OnFinish = function(instance, pos)		  
								ParticleManager:DestroyParticle( nChainParticleFXIndex, false )
								ParticleManager:ReleaseParticleIndex( nChainParticleFXIndex )
								
								self.projectile_launch = self.projectile_launch+1
								if self.projectile_launch == 3 and self.projectile_hit == 0 and self:GetLevel() == 2 then
								
									Alert_CooldownRefresh(self:GetCaster(),self,-self:GetSpecialValueFor("bonus_refreshing"))	
								end
					  end
					}
							
						
							
					Projectiles:CreateProjectile(projectile)
				end
			end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_gondar_shurikens:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_gondar_shurikens:OnProjectileHit(hTarget, instance)
					
					local projectile_energy = self:GetSpecialValueFor("energy_first")
					
					
					if hTarget:GetTeamNumber() ~= self:GetCaster():GetTeamNumber() then
					
						local damage_table = {
							victim = hTarget,
							attacker = self:GetCaster(),
							damage = self:GetSpecialValueFor("damage_first"),
							damage_type = DAMAGE_TYPE_MAGICAL,
							ability=self,
							cover_reduction = 1
						}
					
										local blood_duration = 0
					
					if hTarget:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())==nil then
					
						hTarget:AddNewModifier(self:GetCaster(),self,"modifier_hit",{})
						
						if self:GetLevel()==3 then							
							--hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_movespeed_slow", { slow = self:GetSpecialValueFor("bonus_slow"),turns = self:GetSpecialValueFor("bonus_slow_duration") } )
							damage_table.scar_damage_direct = self:GetSpecialValueFor("bonus_scar_damage")
						end		
										
						local refference_modifier = hTarget:FindModifierByNameAndCaster("modifier_gondar_bleed",self:GetCaster())
						if refference_modifier~=nil then
							Alert_Damage({
								victim = hTarget,
								attacker = self:GetCaster(),
								damage = refference_modifier:GetAbility():GetSpecialValueFor("bonus_bleeding_damage"),
								damage_type = DAMAGE_TYPE_PURE,
								ability  = refference_modifier:GetAbility(),
								})
						end
										
										
										--[[local track_modifier = hTarget:FindModifierByNameAndCaster("modifier_gondar_tracked",self:GetCaster())
										if track_modifier~=nil then
											blood_duration = 1
											
											if track_modifier:GetAbility():GetLevel() == 4 then
												Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=track_modifier:GetAbility():GetSpecialValueFor("bonus_lifesteal")})										
											end
										end
										
										if self:GetCaster():HasModifier("modifier_gondar_bleeding_attack") then
											self:GetCaster():FindModifierByName("modifier_gondar_bleeding_attack"):SetStackCount(1)
											blood_duration=blood_duration+1
										end]]
					else
						damage_table.damage = self:GetSpecialValueFor("damage_second")
						projectile_energy = self:GetSpecialValueFor("energy_second")								
					end
												
						if IsTargetOverCover(instance,damage_table.attacker) then					
							damage_table.cover_reduction=1/2
						end
											if self:GetLevel() == 4 and hTarget:HasModifier("modifier_gondar_bleed") then
												damage_table.cover_reduction=1
											end
									
										--[[if blood_duration>0 then
										
											for i=1,blood_duration do	
												hTarget:AddNewModifier( self:GetCaster(), self:GetCaster():GetAbilityByIndex(2), "modifier_gondar_bleed", { turns = 2} )	
											end
										end]]
				
						Alert_Damage(damage_table)						
						Alert_Mana({unit=self:GetCaster(),manavalue=projectile_energy, energy=true});
						
						self.projectile_hit = self.projectile_hit+1
					end
end

--------------------------------------------------------------------------------

function ability_gondar_shurikens:getAbilityType()
	return ABILITY_TYPE_POINT_CONE_HYBRID;
end

--------------------------------------------------------------------------------

function ability_gondar_shurikens:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_gondar_shurikens:RequiresHitScanCheck()
	return true;
end

--------------------------------------------------------------------------------

function ability_gondar_shurikens:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_gondar_shurikens:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_gondar_shurikens:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_gondar_shurikens:GetConeArc(coneID,coneRange)

	if coneID == 1 then return 3 end	
	local start_cone_ang = self:GetSpecialValueFor("start_cone_ang")
	local end_cone_ang = self:GetSpecialValueFor("end_cone_ang")
	
	local min_cone_dist = self:GetSpecialValueFor("min_cone_dist")
	local max_cone_dist = self:GetSpecialValueFor("max_cone_dist")
	
	
	local cone_total = start_cone_ang

		if coneRange>=max_cone_dist then
			cone_total=end_cone_ang
		elseif coneRange>min_cone_dist then
		
			local delta = ((coneRange-min_cone_dist) / (max_cone_dist-min_cone_dist))
		
			cone_total = start_cone_ang + (end_cone_ang - start_cone_ang)	* delta			
		end	
	return cone_total
end

--------------------------------------------------------------------------------

function ability_gondar_shurikens:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_gondar_shurikens:GetForcedMinRange(coneID)	
	return self:GetMaxRange(coneID)
end

--------------------------------------------------------------------------------

function ability_gondar_shurikens:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_gondar_shurikens:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------