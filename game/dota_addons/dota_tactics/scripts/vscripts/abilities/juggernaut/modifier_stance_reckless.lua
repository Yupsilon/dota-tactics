modifier_stance_reckless = class({})

--------------------------------------------------------------------------------

function modifier_stance_reckless:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function modifier_stance_reckless:GetTexture()
	return "brewmaster_fire_permanent_immolation"
end

--------------------------------------------------------------------------------

function modifier_stance_reckless:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_stance_reckless:IsPermanent()
	return true
end

--------------------------------------------------------------------------------

function modifier_stance_reckless:RemoveOnDeath()
	return false
end

--------------------------------------------------------------------------------

function modifier_stance_reckless:Activate ( )

		if self:GetAbility():GetLevel() == 4 then	
			local might_modifier = self:GetParent():AddNewModifier( self:GetParent(), self, "generic_modifier_onslaught", {outgoing= self:GetAbility():GetSpecialValueFor("bonus_reckless_outgoing") , incoming= self:GetAbility():GetSpecialValueFor("bonus_reckless_incoming") , turns = self:GetAbility():GetSpecialValueFor("stance_duration") } )		
	else
		
		if self:GetAbility():GetLevel() == 2 then	
			self:GetParent():AddNewModifier( self:GetParent(), self, "generic_modifier_conversion", {value= self:GetAbility():GetSpecialValueFor("bonus_scar_damage") , turns = self:GetAbility():GetSpecialValueFor("stance_duration") } )
		end
	
		local might_modifier = self:GetParent():AddNewModifier( self:GetParent(), self, "generic_modifier_might", {value= self:GetAbility():GetSpecialValueFor("reckless_might") , turns = self:GetAbility():GetSpecialValueFor("stance_duration") } )
	end
			StartAnimation(self:GetCaster(), {duration=1, activity=ACT_DOTA_CAST_ABILITY_2})
			
			self:GetAbility().LastCastTurn = GameMode:GetCurrentTurn()+self:GetAbility():GetCooldown(self:GetAbility():GetLevel())+1;
			self:GetAbility():UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self:GetAbility(),"modifier_ability_cooldown",{})
				
	--[[if self:GetAbility():GetLevel() == 3 then
		
		if self:GetName() ~= self:GetParent().stance_last then
		
			Alert_Heal({caster=self:GetParent(),target=self:GetParent(),value=self:GetAbility():GetSpecialValueFor("bonus_self_heal")})
		
		end
		self:GetParent().stance_last = self:GetName()
	end			]]
end
	
--------------------------------------------------------------------------------

function modifier_stance_reckless:PrepAction()
	self:Activate();
	return MODIFIERACTION_SHORTPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
