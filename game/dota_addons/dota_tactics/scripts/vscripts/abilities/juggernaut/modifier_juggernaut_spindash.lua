modifier_juggernaut_spindash = class({})

--------------------------------------------------------------------------------

function modifier_juggernaut_spindash:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_juggernaut_spindash:OnCreated( kv )

	self.dash_start = self:GetParent():GetAbsOrigin()
	self.dash_end = Vector(kv.dash_destination_x,kv.dash_destination_y,0)
	self.dash_speed = (self:GetAbility():GetCastRange()/30) 
	self.distance_traveled = 0
	self.kb_height = 0

	if IsServer() then
		--AddAnimationTranslate(self:GetParent(), "forcestaff_friendly")
		--StartAnimation(self:GetCaster(), {duration=1, activity=ACT_DOTA_OVERRIDE_ABILITY_2})
		
		--local nFXIndex = ParticleManager:CreateParticle( "particles/econ/events/ti9/shovel_dig.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		--self:AddParticle( nFXIndex, false, false, -1, false, false )		
	
		--local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_spirit_breaker/spirit_breaker_charge_dust.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		--self:AddParticle( nFXIndex, false, false, -1, false, false )		
		self.dash_speed = self.dash_speed  / PLAYER_TIME_SIMULTANIOUS*3/2
		
		--StartAnimation(self:GetCaster(), {activity = ACT_DOTA_OVERRIDE_ABILITY_1, rate = 1.0})	
		self:GetCaster():EmitSound("Hero_Juggernaut.BladeFuryStart")	
				
		self.blade_fury_spin_pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_juggernaut/juggernaut_blade_fury.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster())
		ParticleManager:SetParticleControl(self.blade_fury_spin_pfx, 5, Vector(self:GetAbility():GetSpecialValueFor("spin_range") * 1.2, 0, 0))
		self:AddParticle( self.blade_fury_spin_pfx, false, false, -1, false, false )	
		
		self:StartIntervalThink( 1/30 )
		self:OnIntervalThink()
	end
end

--------------------------------------------------------------------------------

function modifier_juggernaut_spindash:OnIntervalThink()
	if IsServer() then

		self:Activate ( )
		local vectorDelta=self.dash_end-self.dash_start
		vectorDelta.z=0		
			
		if self.distance_traveled < vectorDelta:Length2D() then
			self:GetParent():SetAbsOrigin(self:GetParent():GetAbsOrigin() + vectorDelta:Normalized() * self.dash_speed)
			self.distance_traveled = self.distance_traveled + (vectorDelta:Normalized() * self.dash_speed):Length2D()
			self.kb_height=math.sin(self.distance_traveled/vectorDelta:Length2D()*math.pi)*1000
			
		else
			self.kb_height=0
			local animationTime= 1/2;
			FindClearSpaceForUnit(self:GetParent(), self.dash_end, true)
			
			self:GetParent():StopSound("Hero_Juggernaut.BladeFuryStart")
			self:GetParent():EmitSound("Hero_Juggernaut.BladeFuryStop")
			self:Destroy()
		end
	end
end

--------------------------------------------------------------------------------

function modifier_juggernaut_spindash:Activate ( )

		if not GameMode:GetCurrentGamePhase() == GAMEPHASE_DASH then return end

		local helix_aoe = self:GetAbility():GetSpecialValueFor("spin_range")
		local helix_damage = self:GetAbility():GetSpecialValueFor("spin_damage")
		local helix_energy =  self:GetAbility():GetSpecialValueFor("spin_energy")
		local caster = self:GetParent()
		local ability = self:GetAbility()
		
		if ability:GetLevel() == 2 then
			helix_damage = self:GetAbility():GetSpecialValueFor("spin_damage_lesser")
		elseif ability:GetLevel() == 3 then
			helix_damage = helix_damage + self:GetAbility():GetSpecialValueFor("bonus_damage")
		end
		
		local start = caster:GetAbsOrigin()

					local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), start, self:GetCaster(),helix_aoe , DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
					if #enemies > 0 then
						for _,enemy in pairs(enemies) do						
							if enemy ~= nil and ( not enemy:IsInvulnerable() ) and (enemy:HasModifier("generic_modifier_dashed_this_turn")==false)  then
						
												print("found unit")
									if enemy:FindModifierByNameAndCaster("modifier_hit",caster)==nil then
										enemy:AddNewModifier(caster,ability,"modifier_hit",{turns=1})
											
										local direction = enemy:GetAbsOrigin()-start
										direction.z=0
												print("make proj")
										
										local projectile = {		  			  
										vSpawnOrigin = start+Vector(0,0,30),
										fDistance = helix_aoe*2,
										fStartRadius = 50,
										fEndRadius = 50,
										fCollisionRadius = 30,
										Source = self:GetCaster(),
										vVelocity = direction:Normalized() * helix_aoe, 
										UnitBehavior = PROJECTILES_NOTHING,
										bMultipleHits = false,
										bIgnoreSource = true,
										TreeBehavior = PROJECTILES_NOTHING,
										bTreeFullCollision = false,
										WallBehavior = PROJECTILES_DESTROY,
										GroundBehavior = PROJECTILES_DESTROY,
										fGroundOffset = 0,
										bZCheck = false,
										bGroundLock = false,
										bUseFindUnitsInRadius = true,
										bHitScan=true,

										UnitTest = function(instance, unit) return unit==enemy end,
										OnUnitHit = function(instance, unit)   
										
											local damage_table = {
												victim = unit,
												attacker = caster,
												damage = helix_damage,
												damage_type = DAMAGE_TYPE_MAGICAL,
												ability  = ability,
												cover_reduction = 1,
											}
									
											if IsTargetOverCover(instance,damage_table.attacker) then
												damage_table.cover_reduction=1/2
											end
											
											if ability:GetLevel() == 4 then
												 Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=ability:GetSpecialValueFor("bonus_selfheal")})	
											end

											Alert_Damage( damage_table )
											Alert_Mana({unit=caster,manavalue=helix_energy, energy=true} );
										
										  end,
										}						
										Projectiles:CreateProjectile(projectile)
														
								end
							end
						end
					
					end	
end

--------------------------------------------------------------------------------

function modifier_juggernaut_spindash:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_OVERRIDE_ANIMATION
	}

	return funcs
end
--------------------------------------------------------------------------------

function modifier_juggernaut_spindash:GetOverrideAnimation( params )
	return ACT_DOTA_OVERRIDE_ABILITY_1
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------