modifier_juggernaut_omni_dash = class({})

--------------------------------------------------------------------------------

function modifier_juggernaut_omni_dash:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_juggernaut_omni_dash:OnCreated( kv )

	self.dash_start = self:GetParent():GetAbsOrigin()
	self.cleave_damage = self:GetAbility():GetSpecialValueFor("omnislash_damage_max")

	if IsServer() then		
		
		self.final_facing = self:GetParent():GetForwardVector()
		self.nearby_enemies = FindUnitsInRadius(
			self:GetCaster():GetTeamNumber(),
			self:GetCaster():GetAbsOrigin(),
			nil,
			self:GetAbility():GetSpecialValueFor("omnislash_aoe"),
			DOTA_UNIT_TARGET_TEAM_ENEMY,
			DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
			DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES + DOTA_UNIT_TARGET_FLAG_NO_INVIS + DOTA_UNIT_TARGET_FLAG_FOW_VISIBLE + DOTA_UNIT_TARGET_FLAG_INVULNERABLE,
			FIND_CLOSEST,
			false
		)
		for count = #self.nearby_enemies, 1, -1 do		
			if self.nearby_enemies[count] and (self.nearby_enemies[count]:HasModifier("generic_modifier_dashed_this_turn") or self.nearby_enemies[count]:IsInvulnerable()) then
				table.remove(self.nearby_enemies, count)
			end
		end
		
		if self:GetAbility():GetLevel()==4 then
			self.cleave_damage = self.cleave_damage - self:GetAbility():GetSpecialValueFor("bonus_damage_reduction")*(#self.nearby_enemies-1)
		else
			self.cleave_damage = self.cleave_damage - self:GetAbility():GetSpecialValueFor("omnislash_damage_reduction")*(#self.nearby_enemies-1)
		end
		
		self.cleave_damage = math.min(self:GetAbility():GetSpecialValueFor("omnislash_damage_min"),self.cleave_damage)
		
		self.slashRate=1/4
		
		self.sliceAttacks = math.max(3,#self.nearby_enemies)-1;
		if #self.nearby_enemies == 0 then
			self.sliceAttacks=-1
		elseif self.sliceAttacks>0 then
			self.slashRate = 1/self.sliceAttacks*PLAYER_TIME_SIMULTANIOUS*2/3
		end

		self:StartIntervalThink( self.slashRate )
		self:OnIntervalThink()
	end
end

--------------------------------------------------------------------------------

function modifier_juggernaut_omni_dash:OnIntervalThink()
	if IsServer() then

		self.target_enemy=nil		
				
		if self.sliceAttacks<#self.nearby_enemies then
			self.target_enemy = self.nearby_enemies[self.sliceAttacks+1]
		else
			local random_enemy = math.random()*(#self.nearby_enemies-1)+1
			self.target_enemy = self.nearby_enemies[math.floor((math.floor(random_enemy*2) + 1)/2)]
		end
				
		--[[if self.nearby_enemies then
			for count = #self.nearby_enemies, 1, -1 do		
				if self.nearby_enemies[count] and (self.nearby_enemies[count]:FindModifierByNameAndCaster("modifier_hit",self:GetCaster()) == nil) then
					self.target_enemy = self.nearby_enemies[count]
					break
				end
			end
		end]]
		
		if self.sliceAttacks<0 then			
			local trail_pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_juggernaut/juggernaut_omni_slash_trail.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())

			ParticleManager:SetParticleControl(trail_pfx, 0, self:GetCaster():GetAbsOrigin())
			ParticleManager:SetParticleControl(trail_pfx, 1, self.dash_start)
			ParticleManager:ReleaseParticleIndex(trail_pfx)
			
			if self:GetAbility():GetLevel()==2 then
				self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_invisible",{turns=self:GetAbility():GetSpecialValueFor("bonus_invisible_duration")})
			end
			FindClearSpaceForUnit(self:GetParent(), self.dash_start, true)
			self:GetParent():FaceTowards(self:GetParent():GetAbsOrigin()+self.final_facing)
		
			self:Destroy()
		else		
			self:Activate(self.target_enemy)	
			self.sliceAttacks	= self.sliceAttacks-1
		end
	end
end

--------------------------------------------------------------------------------

function modifier_juggernaut_omni_dash:Activate(enemy)

	if enemy == nil then return end

			local previous_position = self:GetParent():GetAbsOrigin()
			FindClearSpaceForUnit(self:GetParent(), enemy:GetAbsOrigin() + RandomVector(100), false)
			StartAnimation(self:GetParent(), {duration=self.slashRate, activity=ACT_DOTA_ATTACK, rate = 1/self.slashRate})
			
			local current_position = self:GetParent():GetAbsOrigin()

			self:GetParent():FaceTowards(enemy:GetAbsOrigin())
			self:GetAbility():CreateVisibilityNode(current_position, 300, 1.0)
			
			if (enemy:FindModifierByNameAndCaster("modifier_hit",self:GetCaster()) == nil) then
			
				local damage_table = {
												victim = enemy,
												attacker = self:GetCaster(),
												damage = self.cleave_damage,
												damage_type = DAMAGE_TYPE_MAGICAL,
												cover_reduction = 1,
											}	
				Alert_Damage( damage_table )
				
				enemy:AddNewModifier(self:GetCaster(),self,"modifier_hit",{turns=1})
				
				if self:GetAbility():GetLevel()==3 and #self.nearby_enemies == 1 then
					enemy:AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_slow",{turns=self:GetAbility():GetSpecialValueFor("bonus_modifier_duration"),value=self:GetAbility():GetSpecialValueFor("bonus_modifier_slow")})
					enemy:AddNewModifier(self:GetCaster(),self,"generic_modifier_reveal",{turns=self:GetAbility():GetSpecialValueFor("bonus_modifier_duration")})
				end
			end

			-- Play hit sound
			enemy:EmitSound("Hero_Juggernaut.OmniSlash.Damage")

			-- Play hit particle on the current target
			local hit_pfx = ParticleManager:CreateParticle("particles/frostivus_herofx/juggernaut_fs_omnislash_tgt.vpcf", PATTACH_ABSORIGIN_FOLLOW, enemy)
			ParticleManager:SetParticleControl(hit_pfx, 0, current_position)
			ParticleManager:SetParticleControl(hit_pfx, 1, current_position)
			ParticleManager:ReleaseParticleIndex(hit_pfx)

			-- Play particle trail when moving
			local trail_pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_juggernaut/juggernaut_omni_slash_trail.vpcf", PATTACH_ABSORIGIN, self:GetParent())
			ParticleManager:SetParticleControl(trail_pfx, 0, previous_position)
			ParticleManager:SetParticleControl(trail_pfx, 1, current_position)
			ParticleManager:ReleaseParticleIndex(trail_pfx)

--			if self.last_enemy ~= enemy then
--				local dash_pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_juggernaut/juggernaut_omni_slash_trail.vpcf", PATTACH_ABSORIGIN, self:GetParent())
--				ParticleManager:SetParticleControl(dash_pfx, 0, previous_position)
--				ParticleManager:SetParticleControl(dash_pfx, 2, current_position)
--				ParticleManager:ReleaseParticleIndex(dash_pfx)
--			end

			self.last_enemy = enemy
end

--------------------------------------------------------------------------------

function modifier_juggernaut_omni_dash:CheckState()
	local state = {
		[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
		[MODIFIER_STATE_INVULNERABLE] = true,
		[MODIFIER_STATE_NO_HEALTH_BAR] = true,
		[MODIFIER_STATE_MAGIC_IMMUNE] = true,
		[MODIFIER_STATE_DISARMED] = true,
		[MODIFIER_STATE_ROOTED] = true
	}

	return state
end

--------------------------------------------------------------------------------

function modifier_juggernaut_omni_dash:GetStatusEffectName()
	return "particles/status_fx/status_effect_omnislash.vpcf"	
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------
