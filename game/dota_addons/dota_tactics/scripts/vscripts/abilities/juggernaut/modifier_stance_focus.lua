modifier_stance_focus = class({})

--------------------------------------------------------------------------------

function modifier_stance_focus:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function modifier_stance_focus:GetTexture()
	return "brewmaster_earth_spell_immunity"
end

--------------------------------------------------------------------------------

function modifier_stance_focus:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_stance_focus:IsPermanent()
	return true
end

--------------------------------------------------------------------------------

function modifier_stance_focus:RemoveOnDeath()
	return false
end

--------------------------------------------------------------------------------

function modifier_stance_focus:Activate ( )

	local shield_modifier = self:GetParent():AddNewModifier(self:GetParent(),self,"generic_modifier_shield",{life= self:GetAbility():GetSpecialValueFor("focus_shield") , turns = self:GetAbility():GetSpecialValueFor("stance_duration")})	
		
	StartAnimation(self:GetCaster(), {duration=1, activity=ACT_DOTA_CAST_ABILITY_2})
	
			self:GetAbility().LastCastTurn = GameMode:GetCurrentTurn()+self:GetAbility():GetCooldown(self:GetAbility():GetLevel())+1;
			self:GetAbility():UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self:GetAbility(),"modifier_ability_cooldown",{})
				
	--[[if self:GetAbility():GetLevel() == 3 then
		
		if self:GetName() ~= self:GetParent().stance_last then
		
			Alert_Heal({caster=self:GetParent(),target=self:GetParent(),value=self:GetAbility():GetSpecialValueFor("bonus_self_heal")})
		
		end
		self:GetParent().stance_last = self:GetName()
	end]]
end
	
--------------------------------------------------------------------------------

function modifier_stance_focus:PrepAction()
	self:Activate();
	return MODIFIERACTION_SHORTPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
