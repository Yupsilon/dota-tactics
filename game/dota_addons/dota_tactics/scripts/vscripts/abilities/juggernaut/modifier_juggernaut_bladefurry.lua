modifier_juggernaut_bladefurry = class({})

--------------------------------------------------------------------------------

function modifier_juggernaut_bladefurry:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function modifier_juggernaut_bladefurry:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_juggernaut_bladefurry:OnCreated( kv )
	if not IsServer() then return end
	self:SetStackCount(kv.turns or 1)							
	StartAnimation(self:GetCaster(), {activity = ACT_DOTA_OVERRIDE_ABILITY_1, rate = 1.0})	
	self:GetCaster():EmitSound("Hero_Juggernaut.BladeFuryStart")	
				
				self.blade_fury_spin_pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_juggernaut/juggernaut_blade_fury.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster())
				ParticleManager:SetParticleControl(self.blade_fury_spin_pfx, 5, Vector(self:GetAbility():GetSpecialValueFor("spin_range") * 1.2, 0, 0))
		self:AddParticle( self.blade_fury_spin_pfx, false, false, -1, false, false )	
end
	
--------------------------------------------------------------------------------

function modifier_juggernaut_bladefurry:SolveAction()
		
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_SHORTPAUSE_SURVIVE;
	end
	--self:GetAbility():Activate(false);
	return MODIFIERACTION_SHORTPAUSE_DESTROY;
end
	
--------------------------------------------------------------------------------

function modifier_juggernaut_bladefurry:OnDestroy(kv)
	if not IsServer() then return end
	local caster = self:GetCaster()
	Timers:CreateTimer(0.5, function() 
		--StopAnimation(caster)
		caster:StopSound("Hero_Juggernaut.BladeFuryStart")
		caster:EmitSound("Hero_Juggernaut.BladeFuryStop")
	end)
end

--------------------------------------------------------------------------------

function modifier_juggernaut_bladefurry:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_OVERRIDE_ANIMATION,
	}

	return funcs
end

--------------------------------------------------------------------------------

function modifier_juggernaut_bladefurry:GetOverrideAnimation( params )
	return ACT_DOTA_OVERRIDE_ABILITY_1
end

--------------------------------------------------------------------------------
