ability_juggernaut_windslash = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_snare","abilities/generic/generic_modifier_snare.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_juggernaut_windslash:SolveAction(keys)

	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
							   

	if keys.target_point_A~=nil then				
			local animationTime = PLAYER_TIME_MODIFIER;
			self:GetCaster():FaceTowards(keys.target_point_A)		
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_ATTACK_EVENT, rate = 1/PLAYER_TIME_MODIFIER})
				
			local projectile_range = self:GetSpecialValueFor("projectile_range")
			local projectile_radius = self:GetSpecialValueFor("projectile_radius") 
			local projectile_speed = math.max(900,projectile_range/PLAYER_TIME_MODIFIER)*2
			
			local startPoint = self:GetCaster():GetAbsOrigin() + Vector(0,0,50)*self:GetCaster():GetModelScale();							
			
			local direction_A = (keys.target_point_A-startPoint)
			direction_A.z=0	
			--startPoint=startPoint+direction_A:Normalized()*projectile_radius/2
			local direction_B = Vector(0,0,0)
			
			local change_distance = projectile_range*2
			local changed = false
			self.hit_targets=0
			
			if keys.target_point_B~=nil then	
			
				direction_B = (keys.target_point_B-keys.target_point_A)
				change_distance = direction_A:Length2D() - projectile_radius/2
			
			end
			EmitSoundOn("n_creep_Wildkin.SummonTornado", self:GetCaster())
			
			self.soundDummy = CreateUnitByName("npc_dummy_unit", startPoint, true, nil, nil, DOTA_TEAM_NOTEAM)	
			self.soundDummy:FindAbilityByName("dummy_unit"):SetLevel(1)
			EmitSoundOn("n_creep_Wildkin.Tornado", self.soundDummy)
						
			Timers:CreateTimer(animationTime/2, function() 
			
				self.nChainParticleFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_invoker/invoker_tornado_funnel.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )		
				ParticleManager:SetParticleAlwaysSimulate( self.nChainParticleFXIndex )
				ParticleManager:SetParticleControl( self.nChainParticleFXIndex, 3, startPoint )
			
				EmitSoundOn( "Hero_Juggernaut.OmniSlash.Damage", self:GetCaster() )								
				local projectile = {
				--  EffectName = "particles/neutral_fx/tornado_ambient.vpcf",
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint,
				  fDistance = projectile_range,
				  fStartRadius = projectile_radius,
				  fEndRadius = projectile_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  vVelocity = direction_A:Normalized() * projectile_speed, 
				  UnitBehavior = PROJECTILES_NOTHING,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_DESTROY,
				  GroundBehavior = PROJECTILES_DESTROY,
				  fGroundOffset = 0,
				  bZCheck = false,
				  bGroundLock = false,
				  draw = false,
				  bUseFindUnitsInRadius = true,
				  bHitScan = false,

				  UnitTest = function(instance, unit) return unit:IsInvulnerable() == false and unit:GetTeamNumber() ~= self:GetCaster():GetTeamNumber() end,
				  OnUnitHit = function(instance, unit) 					
						self:OnProjectileHit(unit,instance:GetPosition())
				  end,
				 OnIntervalThink = function(instance)	
						ParticleManager:SetParticleControl( self.nChainParticleFXIndex, 3, instance:GetPosition() )					
						if (not changed) and instance:GetDistanceTraveled() > change_distance then
						
							instance:SetVelocity (direction_B:Normalized() * projectile_speed,keys.target_point_A)
							changed=true
						end				
						if self.soundDummy then
							self.soundDummy:SetAbsOrigin(instance:GetPosition())
						end
				  end,
				  OnFinish = function(instance, pos)		  
						ParticleManager:DestroyParticle( self.nChainParticleFXIndex, false )
						ParticleManager:ReleaseParticleIndex( self.nChainParticleFXIndex )
						
						if self:GetLevel() == 3 and self.hit_targets>0 then
							self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_shield",{life= self.hit_targets * self:GetSpecialValueFor("bonus_selfshield"), turns = self:GetSpecialValueFor("bonus_selfshield_duration"),nodraw=1})
						end	
						if self.soundDummy then
							StopSoundOn("n_creep_Wildkin.Tornado", self.soundDummy)
							self.soundDummy:Destroy()
						end
				  end
				}						
				Projectiles:CreateProjectile(projectile)
			end)		
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_juggernaut_windslash:GetPriority()
	return 5;
end

--------------------------------------------------------------------------------

function ability_juggernaut_windslash:OnProjectileHit(hTarget, vLocation)
					
					local damage_table = {					
							victim = hTarget,
							attacker = self:GetCaster(),
							damage = self:GetSpecialValueFor("projectile_damage") or 1,
							damage_type = DAMAGE_TYPE_MAGICAL,
							ability=self,
							cover_reduction = 1
						}
					local energy = self:GetSpecialValueFor("projectile_energy")
						
			EmitSoundOn( "Hero_Juggernaut.BladeFury.Impact", self:GetCaster() )
			
									local hit_particle = ParticleManager:CreateParticle("particles/items2_fx/shivas_guard_impact.vpcf", PATTACH_CUSTOMORIGIN, hTarget)
									ParticleManager:SetParticleControl(hit_particle, 0, hTarget:GetAbsOrigin())
									ParticleManager:ReleaseParticleIndex(hit_particle)
											
					if hTarget:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())==nil then
					
						hTarget:AddNewModifier(self:GetCaster(),self,"modifier_hit",{turns=1})															

						if self:GetLevel()==2 then
							hTarget:AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_slow",{								
								turns = self:GetSpecialValueFor("bonus_slow_duration")	,
								slow = self:GetSpecialValueFor("bonus_slow_strength")							
							})
						elseif self:GetLevel() == 4 and self.hit_targets>0 then
							damage_table.damage = damage_table.damage + self:GetSpecialValueFor("bonus_damage") * self.hit_targets
						end
							self.hit_targets = self.hit_targets + 1
					
						if IsTargetPointOverCover(vLocation,damage_table.attacker) then
							damage_table.cover_reduction=1/2
						end
				
						Alert_Damage(damage_table)						
						Alert_Mana({unit=self:GetCaster(), manavalue=energy, energy=true} );
					end
end

--------------------------------------------------------------------------------

function ability_juggernaut_windslash:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_juggernaut_windslash:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_juggernaut_windslash:RequiresHitScanCheck()
	return true;
end

--------------------------------------------------------------------------------

function ability_juggernaut_windslash:GetCastPoints()	
	return 2;
end

--------------------------------------------------------------------------------

function ability_juggernaut_windslash:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_juggernaut_windslash:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_juggernaut_windslash:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_juggernaut_windslash:GetMinRange(coneID)	
	if coneID==1 then return self:GetMaxRange(coneID) end
	return self:GetSpecialValueFor("projectile_range")/5
end

--------------------------------------------------------------------------------

function ability_juggernaut_windslash:TotalMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_juggernaut_windslash:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_juggernaut_windslash:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_juggernaut_windslash:GetCooldown(level)	
	local CD = 4
	--if self:GetLevel()==3 then return CD - self:GetSpecialValueFor("bonus_casttime") end
	return CD
end

--------------------------------------------------------------------------------