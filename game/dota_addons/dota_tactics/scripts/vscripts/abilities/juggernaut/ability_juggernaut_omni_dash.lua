ability_juggernaut_omni_dash = class({})
LinkLuaModifier( "modifier_juggernaut_omni_dash","abilities/juggernaut/modifier_juggernaut_omni_dash.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_dashed_this_turn","abilities/generic/generic_modifier_dashed_this_turn.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_invisible","abilities/generic/generic_modifier_invisible.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_reveal","abilities/generic/generic_modifier_reveal.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_juggernaut_omni_dash:DashAction(keys)

	if keys.target_point_A~=nil then
				
			local dash_range = self:GetSpecialValueFor("dash_range")
	
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,false)	
			Alert_Mana({unit=self:GetCaster(),manavalue=0-self:GetManaCost(self:GetLevel());} )
			--self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_dashed_this_turn",{turns = 1})
			
			StartAnimation(self:GetCaster(), {duration=1, activity=ACT_DOTA_CAST_ABILITY_4})
			local projectile_radius=self:GetCaster():GetHullRadius()
			
			local startPoint = keys.caster:GetAbsOrigin() + Vector(0,0,50);
			local endPoint = startPoint			
			local direction = (keys.target_point_A-startPoint)
			direction.z=0
	
			Timers:CreateTimer(1/4,function()
				local projectile = {
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint,
				  fDistance = direction:Length(),
				  fStartRadius = projectile_radius,
				  fEndRadius = projectile_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  vVelocity = direction:Normalized() * 1500, 
				  UnitBehavior = PROJECTILES_NOTHING,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_DESTROY,
				  GroundBehavior = PROJECTILES_DESTROY,
				  fGroundOffset = 0,
				  bZCheck = false,
				  bGroundLock = false,
				  draw = false,
				  bUseFindUnitsInRadius = true,
									  bHitScan = true,
				   OnFinish = function(instance, pos) 
					
						-- Play particle trail when moving
						local trail_pfx = ParticleManager:CreateParticle("particles/units/heroes/hero_juggernaut/juggernaut_omni_slash_trail.vpcf", PATTACH_ABSORIGIN, self:GetCaster())
						ParticleManager:SetParticleControl(trail_pfx, 0, startPoint)
						ParticleManager:SetParticleControl(trail_pfx, 1, pos)
						ParticleManager:ReleaseParticleIndex(trail_pfx)
						
						FindClearSpaceForUnit(self:GetCaster(), pos, true)
						self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_juggernaut_omni_dash",{})
					
				  end,
				}
				
				keys.caster:FaceTowards(keys.target_point_A)
						
				Projectiles:CreateProjectile(projectile)
			end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_juggernaut_omni_dash:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_juggernaut_omni_dash:disablesMovement()
	return true;
end

--------------------------------------------------------------------------------

function ability_juggernaut_omni_dash:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_juggernaut_omni_dash:RequiresHitScanCheck()
	return true;
end

--------------------------------------------------------------------------------

function ability_juggernaut_omni_dash:RequiresGridSnap()
	return true;
end

--------------------------------------------------------------------------------

function ability_juggernaut_omni_dash:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_juggernaut_omni_dash:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_juggernaut_omni_dash:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_juggernaut_omni_dash:GetConeArc(coneID,coneRange)
	return self:GetCaster():GetHullRadius()*2
end

--------------------------------------------------------------------------------

function ability_juggernaut_omni_dash:GetMinRange(coneID)	
	if coneID== 1 then return self:GetSpecialValueFor("omnislash_aoe") end
	return 100
end

--------------------------------------------------------------------------------

function ability_juggernaut_omni_dash:GetMaxRange(coneID)
	if coneID== 1 then return self:GetSpecialValueFor("omnislash_aoe") end
	return self:GetSpecialValueFor("dash_range")
end

--------------------------------------------------------------------------------

function ability_juggernaut_omni_dash:GetCastRange()	
	return self:GetSpecialValueFor("dash_range")
end

--------------------------------------------------------------------------------

function ability_juggernaut_omni_dash:GetAOERadius()
	return self:GetSpecialValueFor("omnislash_aoe")
end

--------------------------------------------------------------------------------