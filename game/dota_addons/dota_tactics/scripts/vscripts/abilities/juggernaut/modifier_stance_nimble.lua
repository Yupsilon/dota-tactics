modifier_stance_nimble = class({})

--------------------------------------------------------------------------------

function modifier_stance_nimble:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function modifier_stance_nimble:GetTexture()
	return "brewmaster_storm_dispel_magic"
end

--------------------------------------------------------------------------------

function modifier_stance_nimble:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_stance_nimble:IsPermanent()
	return true
end

--------------------------------------------------------------------------------

function modifier_stance_nimble:RemoveOnDeath()
	return false
end
	
--------------------------------------------------------------------------------

function modifier_stance_nimble:PrepAction()
	self:Activate();
	return MODIFIERACTION_SHORTPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function modifier_stance_nimble:Activate ( )
	local haste_modifier = self:GetParent():AddNewModifier(self:GetParent(),self,"generic_modifier_movespeed_haste",{speed=self:GetAbility():GetSpecialValueFor("nimble_haste"),turns = self:GetAbility():GetSpecialValueFor("stance_duration")})		
	self:GetParent():AddNewModifier(self:GetParent(),self,"generic_modifier_stealth",{turns = self:GetAbility():GetSpecialValueFor("stance_duration")})	
	StartAnimation(self:GetCaster(), {duration=1, activity=ACT_DOTA_CAST_ABILITY_2})	
	
	
			self:GetAbility().LastCastTurn = GameMode:GetCurrentTurn()+self:GetAbility():GetCooldown(self:GetAbility():GetLevel())+1;
			self:GetAbility():UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self:GetAbility(),"modifier_ability_cooldown",{})
				
	if self:GetAbility():GetLevel() == 3 then
		
		--[[if self:GetName() ~= self:GetParent().stance_last then
		
			Alert_Heal({caster=self:GetParent(),target=self:GetParent(),value=self:GetAbility():GetSpecialValueFor("bonus_self_heal")})
		
		end
		self:GetParent().stance_last = self:GetName()]]
		local unstop_modifier = self:GetParent():AddNewModifier(self:GetParent(),self,"generic_modifier_unstoppable",{turns = self:GetAbility():GetSpecialValueFor("stance_duration")})	
	end
end

--[[------------------------------------------------------------------------------

function modifier_stance_nimble:DeclareFunctions()
	
	local funcs = { MODIFIER_EVENT_ON_TAKEDAMAGE, }	

	return funcs
end
	
--------------------------------------------------------------------------------

function modifier_stance_nimble:OnTakeDamage ( params )
	if IsServer() and self:GetAbility():GetLevel()==4 then
		local hAttacker = params.attacker
		local hVictim = params.unit

		if hAttacker == self:GetParent() and params.damage_type~=DAMAGE_TYPE_PURE then
									
			if (self.lastHitTurn or 0) < GameMode:GetCurrentTurn() then
				self.lastHitTurn = GameMode:GetCurrentTurn()
				
				for abi = 0, 2, 1	do		
					self:GetCaster():GetAbilityByIndex(abi).LastCastTurn=self:GetCaster():GetAbilityByIndex(abi):GetLastCastTurn()-self:GetAbility():GetSpecialValueFor("bonus_refreshing")			
					self:GetCaster():GetAbilityByIndex(abi):StartCooldown(self:GetCaster():GetAbilityByIndex(abi):GetCooldownTimeRemaining())			
				end
			end
		end
	end
end

------]]--------------------------------------------------------------------------
