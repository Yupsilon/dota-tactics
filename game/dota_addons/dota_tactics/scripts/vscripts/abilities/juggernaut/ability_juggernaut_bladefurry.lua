ability_juggernaut_bladefurry = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_juggernaut_bladefurry","abilities/juggernaut/modifier_juggernaut_bladefurry.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_might","abilities/generic/generic_modifier_might.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_juggernaut_bladefurry:SolveAction(keys)
		
	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})												
						   		
	self:Activate(true);
			
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_juggernaut_bladefurry:Activate(beginning)
	
				local animationTime = 2*PLAYER_TIME_MODIFIER
				local cleave_damage = self:GetSpecialValueFor("spin_damage_full")
				local cleave_range = self:GetSpecialValueFor("spin_range")
				local cleave_energy = self:GetSpecialValueFor("spin_energy_full")
				local cleave_selfheal = 0
			
				if beginning then
				
					self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_juggernaut_bladefurry",{turns = 1})	
				
					if self:GetLevel() == 4 then 
						
						local link_ability = self:GetCaster():FindAbilityByName("ability_juggernaut_stance")
						
						if link_ability~=nil then
							local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), self:GetCaster(),self:GetSpecialValueFor("bonus_aura") , DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
							if #enemies > 0 then
								for _,hTarget in pairs(enemies) do
								
									if hTarget ~= nil and ( not hTarget:IsInvulnerable() )  then
									
										if self:GetCaster():HasModifier("modifier_stance_nimble") then
											hTarget:AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_haste",{value = link_ability:GetSpecialValueFor("nimble_haste"), turns = link_ability:GetSpecialValueFor("stance_duration")})
										elseif self:GetCaster():HasModifier("modifier_stance_focus") then
											hTarget:AddNewModifier(self:GetCaster(),self,"generic_modifier_shield",{value = link_ability:GetSpecialValueFor("reckless_might"), turns = link_ability:GetSpecialValueFor("stance_duration")})
										else
											hTarget:AddNewModifier(self:GetCaster(),self,"generic_modifier_might",{value = link_ability:GetSpecialValueFor("reckless_might"), turns = link_ability:GetSpecialValueFor("stance_duration")})
										end
										
									end
								end
							end		
						end
						
					elseif self:GetLevel() == 3 then 
						self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_unstoppable",{turns = self:GetSpecialValueFor("bonus_unstoppable_duration")})						
					elseif self:GetLevel() == 2 then 
						cleave_selfheal	 = self:GetSpecialValueFor("bonus_selfheal_full")
					end
				else
					cleave_damage = self:GetSpecialValueFor("spin_damage_half")			
					cleave_energy = self:GetSpecialValueFor("spin_energy_half")				
					if self:GetLevel() == 2 then 
						cleave_selfheal	 = self:GetSpecialValueFor("bonus_selfheal_half")
					end
				end	

				local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), self:GetCaster(), cleave_range, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
				if #enemies > 0 then
					for _,enemy in pairs(enemies) do
						if enemy ~= nil and ( not enemy:IsInvulnerable() )  then
					
							local enemy_direction = (enemy:GetAbsOrigin()-self:GetCaster():GetAbsOrigin())
								local projectile = {
								EffectAttach = PATTACH_WORLDORIGIN,			  			  
								vSpawnOrigin = self:GetCaster():GetAbsOrigin()+Vector(0,0,30),
								fDistance = cleave_range,
								fStartRadius = 50,
								fEndRadius = 50,
								fCollisionRadius = 30,
								  Source = self:GetCaster(),
								  vVelocity = enemy_direction:Normalized() * cleave_range*10, 
								  UnitBehavior = PROJECTILES_NOTHING,
								  bMultipleHits = true,
								  bIgnoreSource = true,
								  TreeBehavior = PROJECTILES_NOTHING,
								  bTreeFullCollision = false,
								  WallBehavior = PROJECTILES_DESTROY,
								  GroundBehavior = PROJECTILES_DESTROY,
								  fGroundOffset = 0,
								  bZCheck = false,
								  bGroundLock = false,
								  draw = false,
								  bUseFindUnitsInRadius = true,
									bHitScan=true,

								  UnitTest = function(instance, unit) return unit==enemy end,
								  OnUnitHit = function(instance, unit) 
														
										local damage_table = {
											victim = enemy,
											attacker = self:GetCaster(),
											damage = cleave_damage,
											damage_type = DAMAGE_TYPE_MAGICAL,
											cover_reduction = 1,
										}
										
										if IsTargetOverCover(instance,damage_table.attacker) then
											damage_table.cover_reduction=1/2
										end
			
										local hit_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_juggernaut/juggernaut_blade_fury_tgt.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())
										ParticleManager:SetParticleControl(hit_particle, 0, enemy:GetAbsOrigin())
										ParticleManager:ReleaseParticleIndex(hit_particle)
										EmitSoundOn( "Hero_Juggernaut.BladeFury.Impact", enemy )
										
										Alert_Damage( damage_table )
										Alert_Mana({unit=self:GetCaster(),manavalue=cleave_energy, energy=true});
										Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=cleave_selfheal})	
								  end,
								}						
								Projectiles:CreateProjectile(projectile)						
					
						end
					end
				end
end

--------------------------------------------------------------------------------

function ability_juggernaut_bladefurry:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_juggernaut_bladefurry:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_juggernaut_bladefurry:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_juggernaut_bladefurry:GetCastPoints()
	return 0;
end

--------------------------------------------------------------------------------

function ability_juggernaut_bladefurry:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_juggernaut_bladefurry:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_juggernaut_bladefurry:GetCastRange(coneID)	
	return self:GetSpecialValueFor("spin_range")
end

--------------------------------------------------------------------------------