ability_juggernaut_stance = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_stance_focus","abilities/juggernaut/modifier_stance_focus.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_stance_nimble","abilities/juggernaut/modifier_stance_nimble.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_stance_reckless","abilities/juggernaut/modifier_stance_reckless.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_might","abilities/generic/generic_modifier_might.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_onslaught","abilities/generic/generic_modifier_onslaught.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_conversion","abilities/generic/generic_modifier_conversion.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_scarred","abilities/generic/generic_modifier_scarred.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_stealth","abilities/generic/generic_modifier_stealth.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

--function ability_juggernaut_stance:OnUpgrade(keys)	
--	if self.stance_modifier == nil then
--		self.stance_modifier = self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_stance_reckless",{})
--	end
--end

--------------------------------------------------------------------------------

function ability_juggernaut_stance:HandleTactical(keys)
	
	if self:GetCaster():HasModifier("modifier_stance_reckless") then
	
		self:GetCaster():RemoveModifierByName("modifier_stance_reckless")
		self.stance_modifier = self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_stance_nimble",{})	
	
	elseif self:GetCaster():HasModifier("modifier_stance_nimble") then
	
		self:GetCaster():RemoveModifierByName("modifier_stance_nimble")
		self.stance_modifier = self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_stance_focus",{})	
	
	else --if self:GetCaster():HasModifier("modifier_stance_focus") then
	
		self:GetCaster():RemoveModifierByName("modifier_stance_focus")
		self.stance_modifier = self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_stance_reckless",{})		
	
	end
	
end

--------------------------------------------------------------------------------

function ability_juggernaut_stance:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_juggernaut_stance:IsDisabledBySilence()
	return true
end

--------------------------------------------------------------------------------

function ability_juggernaut_stance:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function ability_juggernaut_stance:GetPriority()
	return 0;
end

--------------------------------------------------------------------------------

function ability_juggernaut_stance:GetCastPoints()
	return 0;
end

--------------------------------------------------------------------------------

function ability_juggernaut_stance:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_juggernaut_stance:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_juggernaut_stance:GetConeArc(coneID,coneRange)	
	return 1
end

--------------------------------------------------------------------------------

function ability_juggernaut_stance:GetMinRange(coneID)	
	return self:GetSpecialValueFor("quake_range")
end

--------------------------------------------------------------------------------

function ability_juggernaut_stance:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("quake_range")
end

--------------------------------------------------------------------------------

function ability_juggernaut_stance:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_juggernaut_stance:GetHasteBonus()	
	if self:GetCaster():HasModifier("modifier_stance_nimble") then	
		return (100+(self:GetSpecialValueFor("nimble_haste")))/100
	end
	return 1
end

--------------------------------------------------------------------------------