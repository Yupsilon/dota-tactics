ability_juggernatu_doubleslice = class({})
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_energy_regeneration","abilities/modifier_energy_regeneration.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_juggernatu_doubleslice:GetIntrinsicModifierName()
	return "modifier_energy_regeneration"
end

--------------------------------------------------------------------------------

function ability_juggernatu_doubleslice:IsDisabledBySilence()
	return false
end

--------------------------------------------------------------------------------

function ability_juggernatu_doubleslice:GetRegenerationValue()
	return self:GetSpecialValueFor("base_energy_regen")
end

--------------------------------------------------------------------------------

function ability_juggernatu_doubleslice:GetDamageConversion()
	return 0
end

--------------------------------------------------------------------------------

function ability_juggernatu_doubleslice:SolveAction(keys)

	local nSlices = self:GetCastPoints()

	self.firstHit = false
	self.LastCastTurn = math.max(GameMode:GetCurrentTurn()+math.ceil(self:GetCooldown(self:GetLevel())),self.LastCastTurn or -1);	
	if keys.target_point_A~=nil then self:CastInstance(keys.target_point_A) end
	
	if keys.target_point_B~=nil then 
		Timers:CreateTimer(PLAYER_TIME_MODIFIER*1/nSlices, function() 
		self:CastInstance(keys.target_point_B) 
		end)
	end
	
	if keys.target_point_C~=nil then 
		Timers:CreateTimer(PLAYER_TIME_MODIFIER*2/nSlices, function() 
		self:CastInstance(keys.target_point_C) 
		end)
	end

	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_juggernatu_doubleslice:CastInstance(point)

			local cleave_range = self:GetSpecialValueFor("cleave_range")
			local cleave_angle = self:GetSpecialValueFor("cleave_angle")
				
			local startPoint = self:GetCaster():GetAbsOrigin()
			local direction = (point-startPoint):Normalized()
			direction.z=0
					
			local castPoints = self:GetCastPoints()
			local animationTime = PLAYER_TIME_MODIFIER*1/castPoints;
						   
				self:GetCaster():FaceTowards(point)			
			StartAnimation(self:GetCaster(), {duration=animationTime, rate=castPoints/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_ATTACK})	
								
				EmitSoundOn( "Hero_Juggernaut.PreAttack", self:GetCaster() )
			Timers:CreateTimer(animationTime/2, function() 
			
			EmitSoundOn( "Hero_Juggernaut.Attack", self:GetCaster() )
			
				local nFXIndex = ParticleManager:CreateParticle( "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave_gods_strength_crit.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )

				
				ParticleManager:SetParticleControl( nFXIndex, 0, startPoint )
				ParticleManager:SetParticleControlForward( nFXIndex, 0, (point-startPoint):Normalized());
				ParticleManager:SetParticleControl( nFXIndex, 2, startPoint )
				ParticleManager:SetParticleControl( nFXIndex, 3, startPoint )
				ParticleManager:SetParticleControl( nFXIndex, 4, startPoint )
				ParticleManager:SetParticleControl( nFXIndex, 5, startPoint )
				ParticleManager:ReleaseParticleIndex( nFXIndex )

				local enemies = FindUnitsInCone({				
					caster = self:GetCaster(),
					startPoint = startPoint,
					startAngle = math.atan2(direction.x,direction.y),
					search_range = cleave_range,
					search_angle = cleave_angle,
					searchteam = DOTA_UNIT_TARGET_TEAM_ENEMY,
					searchtype = DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
					searchflags = 0
				})
				
				if #enemies > 0 then
					for _,enemy in pairs(enemies) do
						if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
					
								local enemy_direction = (enemy:GetAbsOrigin()-startPoint)
								local projectile = {
								EffectAttach = PATTACH_WORLDORIGIN,			  			  
								vSpawnOrigin = startPoint+Vector(0,0,30),
								fDistance = cleave_range*2,
								fStartRadius = 50,
								fEndRadius = 50,
								fCollisionRadius = 30,
								  Source = self:GetCaster(),
								  vVelocity = enemy_direction:Normalized() * cleave_range, 
								  UnitBehavior = PROJECTILES_NOTHING,
								  bMultipleHits = true,
								  bIgnoreSource = true,
								  TreeBehavior = PROJECTILES_NOTHING,
								  bTreeFullCollision = false,
								  WallBehavior = PROJECTILES_DESTROY,
								  GroundBehavior = PROJECTILES_DESTROY,
								  fGroundOffset = 0,
								  bZCheck = false,
								  bGroundLock = false,
								  draw = false,
								  bUseFindUnitsInRadius = true,
								  bHitScan=true,
								  OnFinish = function(instance, pos) ParticleManager:ReleaseParticleIndex( nFXIndex ) end,
								  UnitTest = function(instance, unit) return unit==enemy end,
								  OnUnitHit = function(instance, unit) 
															
														
										local damage_table = {
											victim = enemy,
											attacker = self:GetCaster(),
											damage = self:GetSpecialValueFor("cleave_damage_full"),
											damage_type = DAMAGE_TYPE_MAGICAL,
											cover_reduction = 1
										}	
										local energyGain = self:GetSpecialValueFor("cleave_energy")
										
									EmitSoundOn( "Hero_Juggernaut.Attack.Ring", enemy )
									
									local hit_particle = ParticleManager:CreateParticle("particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave_impact.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())
									ParticleManager:SetParticleControl(hit_particle, 0, enemy:GetAbsOrigin())
									ParticleManager:ReleaseParticleIndex(hit_particle)
										
										if enemy:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())~=nil then
											energyGain=0
											damage_table.damage=self:GetSpecialValueFor("cleave_damage_half")
											
											if enemy:FindModifierByNameAndCaster("modifier_hit",self:GetCaster()):IsFirstHitTarget() then
												damage_table.damage_type = DAMAGE_TYPE_PHYSICAL
											end

											if self:GetLevel()==4 then
												damage_table.damage = damage_table.damage-self:GetSpecialValueFor("bonus_damage")
											end
										else
											local hitmod = enemy:AddNewModifier(self:GetCaster(),self,"modifier_hit",{turns=1})

											if self.firstHit == false then
												hitmod:SetFirstHitTarget()
												damage_table.damage_type = DAMAGE_TYPE_PHYSICAL
												self.firstHit = true
											end
											
											if self:GetLevel()==2 then
												local selfheal = self:GetSpecialValueFor("bonus_selfheal")
												Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=selfheal})
											--elseif self:GetLevel()==4 then
												--damage_table.damage = damage_table.damage-self:GetSpecialValueFor("bonus_damage")
											elseif self:GetLevel()==3 then
												energyGain = energyGain + self:GetSpecialValueFor("bonus_energy")
											end
										end										
																
										if IsTargetOverCover(instance,damage_table.attacker) then
											damage_table.cover_reduction=1/2
										end

										Alert_Damage( damage_table )
										Alert_Mana({unit=self:GetCaster(),manavalue=energyGain, energy=true});
								  end,
								}						
								Projectiles:CreateProjectile(projectile)
							
							end
					
					end
				end
			end)
end

--------------------------------------------------------------------------------

function ability_juggernatu_doubleslice:getAbilityType()
	return ABILITY_TYPE_CONE;
end

--------------------------------------------------------------------------------

function ability_juggernatu_doubleslice:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_juggernatu_doubleslice:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_juggernatu_doubleslice:GetCastPoints()
	if self:GetLevel() == 4 then
		return 3;
	end
	return 2;
end

--------------------------------------------------------------------------------

function ability_juggernatu_doubleslice:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_juggernatu_doubleslice:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_juggernatu_doubleslice:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("cleave_angle")*2
end

--------------------------------------------------------------------------------

function ability_juggernatu_doubleslice:GetMinRange(coneID)	
	return self:GetSpecialValueFor("cleave_range")
end

--------------------------------------------------------------------------------

function ability_juggernatu_doubleslice:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("cleave_range")
end

--------------------------------------------------------------------------------

function ability_juggernatu_doubleslice:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------