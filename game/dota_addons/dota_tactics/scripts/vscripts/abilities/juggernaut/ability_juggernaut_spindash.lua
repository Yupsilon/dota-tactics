ability_juggernaut_spindash = class({})
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_juggernaut_spindash","abilities/juggernaut/modifier_juggernaut_spindash.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_dashed_this_turn","abilities/generic/generic_modifier_dashed_this_turn.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_juggernaut_spindash:DashAction(keys)

	if keys.target_point_A~=nil then
				
			local dash_range = self:GetCastRange(0)	
			local projectile_radius = self:GetSpecialValueFor("spin_range")*2 
	
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_dashed_this_turn",{turns = 1})
			
			local startPoint = keys.caster:GetAbsOrigin() + Vector(0,0,50);
			local endPoint = startPoint			
			local direction = (keys.target_point_A-startPoint)
			direction.z=0
						
				keys.caster:FaceTowards(keys.target_point_A)
			Timers:CreateTimer(0.1, function()
				local hitunit=false
				local projectile = {
				  --EffectName = projectile_model,	
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint,
				  fDistance = math.min(direction:Length2D(),dash_range),
				  fStartRadius = projectile_radius,
				  fEndRadius = projectile_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  --fExpireTime = 8.0,
				  vVelocity = direction:Normalized() * dash_range , 
				  UnitBehavior = PROJECTILES_NOTHING,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  --bCutTrees = true,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_NOTHING,
				  GroundBehavior = PROJECTILES_NOTHING,
				  fGroundOffset = 0,
				  bZCheck = false,
				  bGroundLock = false,
				  draw = false,
				  bHitScan=true,
				   OnFinish = function(instance, pos) 
					
						if not self:GetCaster():HasModifier("modifier_juggernaut_spindash") then
							self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_juggernaut_spindash",{dash_destination_x=instance:GetPosition().x,dash_destination_y=instance:GetPosition().y})
						end
				  end,
				}
				
						
				Projectiles:CreateProjectile(projectile)
			end);
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_juggernaut_spindash:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_juggernaut_spindash:RequiresGridSnap()
	return true;
end

--------------------------------------------------------------------------------

function ability_juggernaut_spindash:RequiresHitScanCheck()
	return true;
end

--------------------------------------------------------------------------------

function ability_juggernaut_spindash:disablesMovement()
	return false;
end

--------------------------------------------------------------------------------

function ability_juggernaut_spindash:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_juggernaut_spindash:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_juggernaut_spindash:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_juggernaut_spindash:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_juggernaut_spindash:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("spin_range")
end

--------------------------------------------------------------------------------

function ability_juggernaut_spindash:GetMinRange(coneID)	
	return 0--self:GetSpecialValueFor("dash_range")
end

--------------------------------------------------------------------------------

function ability_juggernaut_spindash:GetMaxRange(coneID)	
	if self:GetLevel() == 2 then
		return self:GetSpecialValueFor("dash_range")+self:GetSpecialValueFor("bonus_range")	
	end
	return self:GetSpecialValueFor("dash_range")
end

--------------------------------------------------------------------------------

function ability_juggernaut_spindash:ChangesCasterPosition()
	return 0;
end

--------------------------------------------------------------------------------

function ability_juggernaut_spindash:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_juggernaut_spindash:GetAOERadius()	
	return self:GetSpecialValueFor("spin_range")
end

--------------------------------------------------------------------------------