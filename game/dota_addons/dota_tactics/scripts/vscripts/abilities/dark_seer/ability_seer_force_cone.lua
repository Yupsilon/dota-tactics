 ability_seer_force_cone = class({})
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_energy_regeneration","abilities/modifier_energy_regeneration.lua", LUA_MODIFIER_MOTION_NONE )
--LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_seer_force_cone:GetIntrinsicModifierName()
	return "modifier_energy_regeneration"
end

--------------------------------------------------------------------------------

function ability_seer_force_cone:IsDisabledBySilence()
	return false
end


--------------------------------------------------------------------------------

function ability_seer_force_cone:GetDamageConversion()
	return 0
end

--------------------------------------------------------------------------------

function ability_seer_force_cone:GetRegenerationValue()	
	if self:GetLevel() == 3 then
		return self:GetSpecialValueFor("base_energy_regen")+self:GetSpecialValueFor("bonus_energy_regen")
	end
	return self:GetSpecialValueFor("base_energy_regen")
end

--------------------------------------------------------------------------------

function ability_seer_force_cone:SolveAction(keys)

	if keys.target_point_A~=nil then
				
			local projectile_range = self:GetSpecialValueFor("projectile_range")
			local projectile_radius = self:GetSpecialValueFor("projectile_radius")*2
			local projectile_angle = (self:GetSpecialValueFor("projectile_angle")*math.pi/180)/2
	
			self.firstHit = false
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			--self:UseResources(false,false,true)	
			--self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			
			local startPoint = keys.caster:GetAbsOrigin() + Vector(0,0,50)*keys.caster:GetModelScale();		
			local direction = (keys.target_point_A-startPoint):Normalized()
			direction.z=0
			startPoint=startPoint+direction*projectile_radius/2
			
			local cone_arc = self:GetConeArc(0,(keys.target_point_A-keys.caster:GetAbsOrigin()):Length2D())
			local projectile_count = cone_arc/self:GetSpecialValueFor("projectile_angle")
			
			local vectorZ= Vector(0,0,100)
			local force_delta = (cone_arc-self:GetSpecialValueFor("end_cone_ang"))/(self:GetSpecialValueFor("start_cone_ang")-self:GetSpecialValueFor("end_cone_ang"))
			
			local animationTime = PLAYER_TIME_MODIFIER;
			local decalSpeed = math.max(900,projectile_range/PLAYER_TIME_MODIFIER)*2
			local projectile_model = "particles/units/heroes/hero_enigma/enigma_base_attack_eidolon.vpcf"		

			--special hack to show power
			if projectile_count <= math.ceil(self:GetSpecialValueFor("end_cone_ang")/self:GetSpecialValueFor("projectile_angle")) then
				projectile_model = "particles/neutral_fx/black_dragon_attack.vpcf"
			elseif projectile_count > math.ceil(self:GetSpecialValueFor("start_cone_ang")/self:GetSpecialValueFor("projectile_angle")/2) then
				projectile_model = "particles/units/heroes/hero_enigma/enigma_base_attack.vpcf"				
			end
			
			if self:GetLevel()==4 then
				self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_haste",{speed=self:GetSpecialValueFor("bonus_haste"),turns = self:GetSpecialValueFor("bonus_haste_duration"),applythisturn=1})
			end
			
			keys.caster:FaceTowards(keys.target_point_A)
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_ATTACK, rate = 1/PLAYER_TIME_MODIFIER})
			
			Timers:CreateTimer(animationTime/2,function()
	
			EmitSoundOn( "Hero_Dark_Seer.Vacuum", self:GetCaster() )
			
	
			local startangle = math.atan2(direction.x,direction.y)
	
				for i=0,projectile_count-1 do											
				   
						local nChainParticleFXIndex = ParticleManager:CreateParticle( projectile_model, PATTACH_CUSTOMORIGIN, self:GetCaster() )		
						ParticleManager:SetParticleAlwaysSimulate( nChainParticleFXIndex )
						ParticleManager:SetParticleControlEnt( nChainParticleFXIndex, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_attack1",  startPoint+vectorZ, true )
						ParticleManager:SetParticleControl( nChainParticleFXIndex, 1, startPoint+vectorZ )
						ParticleManager:SetParticleControl( nChainParticleFXIndex, 2, Vector( decalSpeed, 0, 0 ) )
						ParticleManager:SetParticleControl( nChainParticleFXIndex, 4, Vector( 1, 0, 0 ) )
						ParticleManager:SetParticleControl( nChainParticleFXIndex, 5, Vector( 0, 0, 0 ) )
						ParticleManager:SetParticleControlEnt( nChainParticleFXIndex, 7, self:GetCaster(), PATTACH_CUSTOMORIGIN, nil, self:GetCaster():GetOrigin(), true )	
	
					local fireAngle = startangle-projectile_angle*(projectile_count-1)/2+projectile_angle*i
					local newdirection = Vector(math.sin(fireAngle),math.cos(fireAngle),0)
			
					local projectile = {			  			  
					  vSpawnOrigin = startPoint+vectorZ,
					  fDistance = projectile_range-projectile_radius,
					  fStartRadius = projectile_radius,
					  fEndRadius = projectile_radius,
					  fCollisionRadius = 30,
					  Source = self:GetCaster(),
					  vVelocity = newdirection * decalSpeed, 
					  UnitBehavior = PROJECTILES_NOTHING,
					  bMultipleHits = false,
					  bIgnoreSource = true,
					  TreeBehavior = PROJECTILES_NOTHING,
					  bTreeFullCollision = false,
					  WallBehavior = PROJECTILES_DESTROY,
					  GroundBehavior = PROJECTILES_DESTROY,
					  fGroundOffset = 0,
					  bZCheck = false,
					  bGroundLock = false,
					  draw = false,
					  bUseFindUnitsInRadius = true,

					  UnitTest = function(instance, unit) return (not unit:IsInvulnerable()) and unit:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())==nil end,
					  OnUnitHit = function(instance, unit) 
						
							self:OnProjectileHit(unit,instance,1-force_delta)
						
					  end,
					  OnIntervalThink = function(instance)
							ParticleManager:SetParticleControl( nChainParticleFXIndex, 1, instance:GetPosition() )
					  end,
					  OnFinish = function(instance, pos)		  
								ParticleManager:DestroyParticle( nChainParticleFXIndex, false )
								ParticleManager:ReleaseParticleIndex( nChainParticleFXIndex )
					  end
					}
							
						
							
					Projectiles:CreateProjectile(projectile)
				end
			end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_seer_force_cone:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_seer_force_cone:OnProjectileHit(hTarget, instance, fDelta)
					
					if hTarget:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())~=nil then
						return
					else
						hTarget:AddNewModifier(self:GetCaster(),self,"modifier_hit",{turns=1})
					end
					
					if self:AffectsAllies() and hTarget:GetTeamNumber() == self:GetCaster():GetTeamNumber() then
						Alert_Heal({caster=self:GetCaster(),target=hTarget,value=self:GetSpecialValueFor("bonus_heal")})
					elseif hTarget:GetTeamNumber() ~= self:GetCaster():GetTeamNumber() then
					
						local damage_table = {
							victim = hTarget,
							attacker = self:GetCaster(),
							damage = self:GetSpecialValueFor("min_damage")+ (self:GetSpecialValueFor("max_damage")-self:GetSpecialValueFor("min_damage"))*fDelta,
							damage_type = DAMAGE_TYPE_MAGICAL,
							ability=self,
							cover_reduction = 1
						}
						if self.firstHit == false then
							self.firstHit = true
							damage_table.damage_type = DAMAGE_TYPE_PHYSICAL
						end
						--EmitSoundOn( "Hero_DrowRanger.ProjectileImpact", unit )
												
						if IsTargetOverCover(instance,damage_table.attacker) then					
							damage_table.cover_reduction=1/2
						end
				
						Alert_Damage(damage_table)						
						Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("projectile_energy"), energy=true});
					end
end

--------------------------------------------------------------------------------

function ability_seer_force_cone:AffectsAllies()
	return false	--unused	self:GetLevel()==4 and self:GetCaster():GetAbilityByIndex(3):GetCooldownTimeRemaining()<=0;
end

--------------------------------------------------------------------------------

function ability_seer_force_cone:getAbilityType()
	return ABILITY_TYPE_CONE;
end

--------------------------------------------------------------------------------

function ability_seer_force_cone:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_seer_force_cone:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_seer_force_cone:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_seer_force_cone:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_seer_force_cone:GetConeArc(coneID,coneRange)

	local start_cone_ang = self:GetSpecialValueFor("start_cone_ang")
	local end_cone_ang = self:GetSpecialValueFor("end_cone_ang")
	
	local min_cone_dist = self:GetSpecialValueFor("min_cone_dist")
	local max_cone_dist = self:GetSpecialValueFor("max_cone_dist")
	
	if self:GetLevel() == 2 then
		start_cone_ang = start_cone_ang+self:GetSpecialValueFor("bonus_cone_ang")
	end
	
	local cone_total = start_cone_ang

		if coneRange>=max_cone_dist then
			cone_total=end_cone_ang
		elseif coneRange>min_cone_dist then
		
			local delta = ((coneRange-min_cone_dist) / (max_cone_dist-min_cone_dist))
		
			cone_total = start_cone_ang + (end_cone_ang - start_cone_ang)	* delta			
		end

	if coneID == 1 then return cone_total / self:GetSpecialValueFor("projectile_angle") end		
	return cone_total
end

--------------------------------------------------------------------------------

function ability_seer_force_cone:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_seer_force_cone:GetForcedMinRange(coneID)	
	return self:GetMaxRange(coneID)
end

--------------------------------------------------------------------------------

function ability_seer_force_cone:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_seer_force_cone:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------