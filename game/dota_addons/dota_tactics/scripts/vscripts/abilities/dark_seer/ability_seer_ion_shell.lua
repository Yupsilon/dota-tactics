ability_seer_ion_shell = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "modifier_seer_ion_shell","abilities/dark_seer/modifier_seer_ion_shell.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_might","abilities/generic/generic_modifier_might.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_seer_ion_shell:PrepAction(keys)
			
	local target_unit = EntIndexToHScript(keys.target_unit)
	local animationTime = PLAYER_TIME_MODIFIER;
	if target_unit~=nil then	

		self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,false)	
			Alert_Mana({unit=self:GetCaster(),manavalue=0-self:GetManaCost(self:GetLevel());} )
		--self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})			
	
		self:GetCaster():FaceTowards(target_unit:GetAbsOrigin())	
		StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_2,rate = 1/PLAYER_TIME_MODIFIER})
	
		EmitSoundOn( "Hero_Enigma.Midnight_Pulse", self:GetCaster() )
	
		self:OnProjectileHit(target_unit)			
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_seer_ion_shell:OnProjectileHit(hTarget)

	self.nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_dark_seer/dark_seer_surge_start.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )
	ParticleManager:SetParticleControl( self.nFXIndex, 0, hTarget:GetAbsOrigin() )							
	ParticleManager:ReleaseParticleIndex(self.nFXIndex)
	
	local damage_modifier = hTarget:AddNewModifier( self:GetCaster(), self, "modifier_seer_ion_shell", {turns = self:GetSpecialValueFor("shell_duration") } )
	local shield_modifier = hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_shield", {life= self:GetSpecialValueFor("shell_life") , turns = self:GetSpecialValueFor("shell_duration"), nodraw=1 } )
	
	table.insert(shield_modifier.linked_modifiers,damage_modifier)
	
	if self:GetLevel() == 3 then
		hTarget:AddNewModifier(self:GetCaster(),self,"generic_modifier_unstoppable",{turns=self:GetSpecialValueFor("unstoppable_duration")})
	elseif self:GetLevel()==4 then
		local might = hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_might", {value= self:GetSpecialValueFor("bonus_might") , turns = self:GetSpecialValueFor("shell_duration") , nodraw=1,applythisturn=1} )
		table.insert(shield_modifier.linked_modifiers,might)
	end
	
	--local nFXIndex = ParticleManager:CreateParticle( "particles/econ/items/abaddon/abaddon_alliance/abaddon_aphotic_shield_alliance.vpcf", PATTACH_ABSORIGIN_FOLLOW, hTarget )
	--ParticleManager:SetParticleControl( self.nFXIndex, 1, Vector(hTarget:GetHullRadius(),0,0) )		
	--other_modifier:AddParticle( nFXIndex, false, false, -1, false, false )					
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_seer_ion_shell:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_seer_ion_shell:GetPriority()
	return 4;
end

--------------------------------------------------------------------------------

function ability_seer_ion_shell:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_seer_ion_shell:GetCastPoints()

	return 1;
end

--------------------------------------------------------------------------------

function ability_seer_ion_shell:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_seer_ion_shell:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_seer_ion_shell:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_seer_ion_shell:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_seer_ion_shell:GetMaxRange(coneID)	
	if self:GetLevel() == 2 then return self:GetSpecialValueFor("projectile_range")+self:GetSpecialValueFor("bonus_range") end
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_seer_ion_shell:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_seer_ion_shell:GetAOERadius()
	return self:GetSpecialValueFor( "burst_aoe" )
end

--------------------------------------------------------------------------------