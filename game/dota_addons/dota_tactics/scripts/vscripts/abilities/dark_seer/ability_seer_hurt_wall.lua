ability_seer_hurt_wall = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "modifier_seer_ionwall_target","abilities/dark_seer/modifier_seer_ionwall_target.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_seer_ionwall_caster","abilities/dark_seer/modifier_seer_ionwall_caster.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_weak","abilities/generic/generic_modifier_weak.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_seer_hurt_wall:PrepAction(keys)

	if keys.target_point_A~=nil and keys.target_point_B~=nil then
	
		self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
		self:UseResources(false,false,true)	
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})

		local animationTime = PLAYER_TIME_MODIFIER;		
		keys.caster:FaceTowards((keys.target_point_A+keys.target_point_B)/2)
		StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_4, rate = 1/PLAYER_TIME_MODIFIER})	

		self:CastInstance(keys.target_point_A, keys.target_point_B)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_seer_hurt_wall:CastInstance(pointA, pointB)


	EmitSoundOn( "Hero_OgreMagi.Ignite.Cast", self:GetCaster() )			

	local mod_duration = self:GetSpecialValueFor("wall_duration")
	
	
	local direction = (pointB-pointA)
	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_seer_ionwall_caster",{turns = mod_duration, point_x=pointA.x, point_y=pointA.y, ang_rotation = math.atan2(direction.x,direction.y), wall_length = self:GetWallSize()})							
	
end

--------------------------------------------------------------------------------

function ability_seer_hurt_wall:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_seer_hurt_wall:GetPriority()
	return 2;
end

--------------------------------------------------------------------------------

function ability_seer_hurt_wall:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_seer_hurt_wall:GetCastPoints()

	return 2;
end

--------------------------------------------------------------------------------

function ability_seer_hurt_wall:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_seer_hurt_wall:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_seer_hurt_wall:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_seer_hurt_wall:GetMinRange(coneID)	
	if coneID == 1 then return self:GetWallSize() end
	return 0
end

--------------------------------------------------------------------------------

function ability_seer_hurt_wall:GetMaxRange(coneID)	
	if coneID == 1 then return self:GetWallSize() end
	if self:GetLevel() == 2 then return self:GetSpecialValueFor("projectile_range")+self:GetSpecialValueFor("bonus_range") end
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_seer_hurt_wall:GetWallSize()	
	return self:GetSpecialValueFor("wall_length") 
end

--------------------------------------------------------------------------------

function ability_seer_hurt_wall:GetAOERadius()	
	return self:GetSpecialValueFor("projectile_radius") 
end

--------------------------------------------------------------------------------

function ability_seer_hurt_wall:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_seer_hurt_wall:GetCooldown(level)	
	if self:GetLevel()==4 then return 4-self:GetSpecialValueFor("bonus_cooldown") end
	return 4
end

--------------------------------------------------------------------------------