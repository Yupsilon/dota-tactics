modifier_seer_ionwall_target = class({})

--------------------------------------------------------------------------------

function modifier_seer_ionwall_target:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_seer_ionwall_target:OnCreated( kv )
	
	local activate = ((kv.activate or 1) == 1)
	
	if IsServer() and activate then
	
			Alert_Mana({unit=self:GetCaster(),manavalue=self:GetAbility():GetSpecialValueFor("wall_energy"), energy=true} );
			EmitSoundOn( "Hero_Dark_Seer.Ion_Shield_Start.TI8", self:GetParent() )	
			Alert_Damage( {
											victim = self:GetParent(),
												attacker = self:GetCaster(),
												damage = self:GetAbility():GetSpecialValueFor("wall_damage"),
												damage_type = DAMAGE_TYPE_PURE,
												ability = self:GetAbility()
											} )	
			
		--local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_ogre_magi/ogre_magi_ignite_debuff.vpcf", PATTACH_CUSTOMORIGIN, self:GetParent() )				
		--self:AddParticle( nFXIndex, false, false, -1, false, false )		
	end
end
	
--------------------------------------------------------------------------------

function modifier_seer_ionwall_target:EndOfTurnAction()		
	
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------