modifier_seer_ion_shell = class({})

--------------------------------------------------------------------------------

function modifier_seer_ion_shell:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_seer_ion_shell:GetStatusEffectName()
	return "particles/status_fx/status_effect_dark_seer_illusion.vpcf"
end

--------------------------------------------------------------------------------

function modifier_seer_ion_shell:OnCreated( kv )
	
	self:SetStackCount(kv.turns or 2	)	
	
	if IsServer() then
	
							
							local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_dark_seer/dark_seer_ion_shell.vpcf", PATTACH_POINT_FOLLOW, self:GetParent() )
							ParticleManager:SetParticleControlEnt(nFXIndex, 0, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
							ParticleManager:SetParticleControl( nFXIndex, 1, Vector(self:GetParent():GetHullRadius() ,self:GetParent():GetHullRadius()  ,0))		
							self:AddParticle( nFXIndex, false, false, -1, false, false )	
							
	end
end

--------------------------------------------------------------------------------

function modifier_seer_ion_shell:Activate ( )

		local helix_aoe = self:GetAbility():GetSpecialValueFor("burst_aoe")
		local helix_damage = self:GetAbility():GetSpecialValueFor("burst_damage")
			EmitSoundOn( "Hero_Enigma.MaleficeTick", self:GetParent() )
		
			local helix = ParticleManager:CreateParticle("particles/units/heroes/hero_disruptor/disruptor_thunder_strike_bolt.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
			ParticleManager:SetParticleControl(helix, 1, self:GetParent():GetAbsOrigin())		
			ParticleManager:SetParticleControl(helix, 2, self:GetParent():GetAbsOrigin())		
			ParticleManager:ReleaseParticleIndex(helix)	

			
				
					local enemies = FindUnitsInRadius( self:GetParent():GetTeamNumber(), self:GetParent():GetAbsOrigin(), self:GetParent(),helix_aoe , DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
					if #enemies > 0 then
						for _,enemy in pairs(enemies) do
							if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
						
									local direction = enemy:GetAbsOrigin()-self:GetParent():GetAbsOrigin()
									direction.z=0
									local projectile = {
									EffectAttach = PATTACH_WORLDORIGIN,			  			  
									vSpawnOrigin = self:GetParent():GetAbsOrigin()+Vector(0,0,30),
									fDistance = helix_aoe*2,
									fStartRadius = 50,
									fEndRadius = 50,
									fCollisionRadius = 30,
									  Source = self:GetParent(),
									  vVelocity = direction:Normalized() * helix_aoe*10, 
									  UnitBehavior = PROJECTILES_NOTHING,
									  bMultipleHits = true,
									  bIgnoreSource = true,
									  TreeBehavior = PROJECTILES_NOTHING,
									  bTreeFullCollision = false,
									  WallBehavior = PROJECTILES_DESTROY,
									  GroundBehavior = PROJECTILES_DESTROY,
									  fGroundOffset = 0,
									  bZCheck = false,
									  bGroundLock = false,
									  draw = false,
									  bUseFindUnitsInRadius = true,
									  bHitScan = true,

									  UnitTest = function(instance, unit) return unit==enemy end,
									  OnUnitHit = function(instance, unit) 
															
											local damage_table = {
												victim = enemy,
												attacker = self:GetCaster(),
												damage = helix_damage,
												damage_type = DAMAGE_TYPE_MAGICAL,
												ability = self:GetAbility(),
												cover_reduction = 1
											}
									
											if IsTargetOverCover(instance,damage_table.attacker) then
												damage_table.cover_reduction=1/2
											end

											Alert_Damage( damage_table )
										
									  end,
									}						
									Projectiles:CreateProjectile(projectile)
														
							end
						end
					end							
				

end
	
--------------------------------------------------------------------------------

function modifier_seer_ion_shell:SolveAction()
	if IsServer() then		
		self:Activate();	
	end
	return MODIFIERACTION_SHORTPAUSE_SURVIVE;	
end

--------------------------------------------------------------------------------

function modifier_seer_ion_shell:EndOfTurnAction()	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
