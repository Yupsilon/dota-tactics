modifier_seer_ionwall_caster = class({})

--------------------------------------------------------------------------------

function modifier_seer_ionwall_caster:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_seer_ionwall_caster:OnCreated( kv )

	local turnsDuration = kv.turns or 2
	local ang_rot = kv.ang_rotation or 90
	self:SetStackCount(turnsDuration)
	
	self.wall_thickness= self:GetAbility():GetAOERadius()
	
	if IsServer() then	
					
		self.start_point = Vector(kv.point_x,kv.point_y,0)
		self.end_point = self.start_point+Vector(math.sin(ang_rot),math.cos(ang_rot),0)*kv.wall_length
		EmitSoundOnLocationWithCaster(self.start_point, "Hero_Dark_Seer.Wall_of_Replica_Start", self:GetCaster())	
		
		local enemies =  self:GetHitTargets()
			
		if #enemies > 0 then
			for _,enemy in pairs(enemies) do
				if enemy ~= nil and ( not enemy:IsInvulnerable() ) and not enemy:HasModifier("modifier_seer_ionwall_target") then
					enemy:AddNewModifier(self:GetCaster(),self:GetAbility(),"modifier_seer_ionwall_target",{activate=0})						
				end
			end
		end	
							
		local wall_particle = ParticleManager:CreateParticle( "particles/units/heroes/hero_dark_seer/dark_seer_wall_of_replica.vpcf", PATTACH_CUSTOMORIGIN, self:GetParent() )
		ParticleManager:SetParticleControl(wall_particle, 0, self.start_point)
		ParticleManager:SetParticleControl(wall_particle, 1, self.end_point)
		ParticleManager:SetParticleControl( wall_particle, 60, self:GetWallColor() )
		self:AddParticle( wall_particle, false, false, -1, false, false )	
		
		self:StartIntervalThink( 1/15 )
		self:OnIntervalThink()	
	end
end
	
--------------------------------------------------------------------------------

function modifier_seer_ionwall_caster:GetWallColor()
	
	return GameMode.PlayerColors[self:GetCaster():GetPlayerOwnerID()] or Vector(255,255,255);
end
	
--------------------------------------------------------------------------------

function modifier_seer_ionwall_caster:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end
	
--------------------------------------------------------------------------------

function modifier_seer_ionwall_caster:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function modifier_seer_ionwall_caster:GetHitTargets()
	return FindUnitsInLine(self:GetCaster():GetTeamNumber(), self.start_point, self.end_point, nil, self.wall_thickness, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, DOTA_UNIT_TARGET_FLAG_MAGIC_IMMUNE_ENEMIES)
end

--------------------------------------------------------------------------------

function modifier_seer_ionwall_caster:OnIntervalThink()
	if IsServer() then
					   
			if GameMode:GetCurrentGamePhase()<GAMEPHASE_DASH or GameMode:GetCurrentGamePhase()>GAMEPHASE_WALK then return end			
					
			local enemies =  self:GetHitTargets()
			
			if #enemies > 0 then
				for _,enemy in pairs(enemies) do
					if enemy ~= nil and ( not enemy:IsInvulnerable() ) and not enemy:HasModifier("modifier_seer_ionwall_target") then
						enemy:AddNewModifier(self:GetCaster(),self:GetAbility(),"modifier_seer_ionwall_target",{})
						
						enemy:AddNewModifier(self:GetCaster(),self,"generic_modifier_weak",{
								value=self:GetAbility():GetSpecialValueFor("weaken_duration"),
								turns=self:GetAbility():GetSpecialValueFor("weaken_strength"),
								applythisturn=0
							})
							
							if self:GetAbility():GetLevel()==3 then								
								enemy:AddNewModifier(self:GetCaster(),self:GetAbility(),"generic_modifier_movespeed_slow",{								
									turns = self:GetAbility():GetSpecialValueFor("bonus_slow_duration")	,
									slow = self:GetAbility():GetSpecialValueFor("bonus_slow_strength")	,
									applythisturn=0								
								})
							end
					end
				end
			end	
	
	end
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------