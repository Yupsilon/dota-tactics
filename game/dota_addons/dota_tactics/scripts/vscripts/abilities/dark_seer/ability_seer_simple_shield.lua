ability_seer_simple_shield = class({})
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_seer_simple_shield:PrepAction(keys)
			
	local target_unit = EntIndexToHScript(keys.target_unit)
	if target_unit~=nil then	

		self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
		self:UseResources(false,false,true)	
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})			
	
		self:GetCaster():FaceTowards(target_unit:GetAbsOrigin())	
		StartAnimation(self:GetCaster(), {duration=PLAYER_TIME_MODIFIER, activity=ACT_DOTA_CAST_ABILITY_2, rate = 1/PLAYER_TIME_MODIFIER})
	
		EmitSoundOn( "Hero_Dark_Seer.Ion_Shield_Start ", self:GetCaster() )
	
		self:OnProjectileHit(target_unit)			
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_seer_simple_shield:OnProjectileHit(hTarget)

						self.nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_dark_seer/dark_seer_surge_start.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )
						ParticleManager:SetParticleControl( self.nFXIndex, 0, hTarget:GetAbsOrigin() )							
						ParticleManager:ReleaseParticleIndex(self.nFXIndex)
							
						local other_shield = self:GetSpecialValueFor("shield_strength")
						
						if self:GetLevel()==2 then
							other_shield = self:GetSpecialValueFor("shield_strength_lesser")
						elseif self:GetLevel()==3 then
							hTarget:AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_haste",{
								speed=self:GetSpecialValueFor("bonus_haste"),
								turns=self:GetSpecialValueFor("shield_duration"),
								nodraw=1
							})
						elseif self:GetLevel()==4 and hTarget~=self:GetCaster() then
						
							local self_modifier = self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_shield", {life= self:GetSpecialValueFor("bonus_selfshield") , turns = self:GetSpecialValueFor("shield_duration") , nodraw=1} )
							
							local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_dark_seer/dark_seer_ion_shell.vpcf", PATTACH_POINT_FOLLOW, self:GetCaster() )
							ParticleManager:SetParticleControlEnt(nFXIndex, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetCaster():GetAbsOrigin(), true)
							ParticleManager:SetParticleControl( nFXIndex, 1, Vector(self:GetCaster():GetHullRadius() ,0 ,self:GetCaster():GetHullRadius() ))		
							self_modifier:AddParticle( nFXIndex, false, false, -1, false, false )	
						end
						
					local other_modifier = hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_shield", {life= other_shield , turns = self:GetSpecialValueFor("shield_duration") , nodraw=1} )
						
					local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_dark_seer/dark_seer_ion_shell.vpcf", PATTACH_ABSORIGIN_FOLLOW, hTarget )
							ParticleManager:SetParticleControlEnt(nFXIndex, 0, hTarget, PATTACH_POINT_FOLLOW, "attach_hitloc", hTarget:GetAbsOrigin(), true)
							ParticleManager:SetParticleControl( nFXIndex, 1, Vector(hTarget:GetHullRadius(),0,hTarget:GetHullRadius()) )		
							other_modifier:AddParticle( nFXIndex, false, false, -1, false, false )	
					
					Alert_Mana({unit=self:GetCaster(),  manavalue=self:GetSpecialValueFor("projectile_energy") , energy=true});					
end

--------------------------------------------------------------------------------

function ability_seer_simple_shield:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_seer_simple_shield:GetPriority()
	return 2;
end

--------------------------------------------------------------------------------

function ability_seer_simple_shield:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function ability_seer_simple_shield:GetCooldown(level)	
	if self:GetLevel()==2 then return self:GetSpecialValueFor("cooldown_lesser") end
	return 3
end

--------------------------------------------------------------------------------

function ability_seer_simple_shield:GetCastPoints()

	return 1;
end

--------------------------------------------------------------------------------

function ability_seer_simple_shield:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_seer_simple_shield:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_seer_simple_shield:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_seer_simple_shield:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_seer_simple_shield:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_seer_simple_shield:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------