modifier_seer_void = class({})

--------------------------------------------------------------------------------

function modifier_seer_void:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function modifier_seer_void:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_seer_void:OnCreated( kv )
	if not IsServer() then return end
	self:SetStackCount(kv.turns or 2)
							
	--local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_dark_seer/dark_seer_surge.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )
	--self:AddParticle( nFXIndex, false, false, -1, false, false )		
end

--------------------------------------------------------------------------------

function modifier_seer_void:Activate ( )

		local helix_aoe = self:GetAbility():GetSpecialValueFor("void_aoe")
		local helix_damage = self:GetAbility():GetSpecialValueFor("void_damage")
		local berserk_helix_energy =  self:GetAbility():GetSpecialValueFor("target_energy")
		local helix_knockback = 1
		
		local helix_center = self:GetParent():GetAbsOrigin()
								
		local kb_max = self:GetAbility():GetSpecialValueFor("knockback_range")	
											
		if self:GetAbility():GetLevel() == 2 then
			kb_max = kb_max + self:GetAbility():GetSpecialValueFor("bonus_knockback_range")	
		end
		
			EmitSoundOn( "Hero_Dark_Seer.Vacuum", self:GetParent() )
		
			local helix = ParticleManager:CreateParticle("particles/units/heroes/hero_dark_seer/dark_seer_vacuum.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent())
			ParticleManager:SetParticleControl(helix, 0, helix_center)		
			ParticleManager:ReleaseParticleIndex(helix)	
				
					local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), helix_center, self:GetCaster(),helix_aoe , DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
					if #enemies > 0 then
						for _,target in pairs(enemies) do
						
							local enemy = target
							if enemy ~= nil and ( not enemy:IsInvulnerable() )  then
						
									local direction = enemy:GetAbsOrigin()-helix_center
									direction.z=0
									
									local projectile = {	  			  
									vSpawnOrigin = helix_center+Vector(0,0,30),
									fDistance = helix_aoe*2,
									fStartRadius = 50,
									fEndRadius = 50,
									fCollisionRadius = 30,
									  Source = self:GetCaster(),
									  vVelocity = direction:Normalized() * helix_aoe, 
									  UnitBehavior = PROJECTILES_NOTHING,
									  bMultipleHits = true,
									  bIgnoreSource = true,
									  TreeBehavior = PROJECTILES_NOTHING,
									  bTreeFullCollision = false,
									  WallBehavior = PROJECTILES_DESTROY,
									  GroundBehavior = PROJECTILES_DESTROY,
									  fGroundOffset = 0,
									  bZCheck = false,
									  bGroundLock = false,
									  draw = false,
									  bUseFindUnitsInRadius = true,
									  bHitScan = true,

									  UnitTest = function(instance, unit) return unit==enemy end,
									  OnUnitHit = function(instance, unit) 
															
											local damage_table = {
												victim = enemy,
												attacker = self:GetCaster(),
												damage = helix_damage,
												damage_type = DAMAGE_TYPE_MAGICAL,
												ability = self:GetAbility(),
												cover_reduction = 1,
											}
									
											if IsTargetOverCover(instance,damage_table.attacker) then
												damage_table.cover_reduction=1/2
											end
							
											local kb_direction = -direction:Normalized() * direction:Length2D() * helix_knockback
											
											if kb_direction:Length2D() > kb_max then
												kb_direction = kb_direction * (kb_max / kb_direction:Length2D())
											end
																		
											enemy:AddNewModifier(self:GetCaster(),nil,"modifier_order_knockback",{
												direction_x=kb_direction.x,
												direction_y=kb_direction.y
											})

											Alert_Damage( damage_table )									
											if self:GetAbility():GetLevel() == 4 then
												enemy:AddNewModifier(self:GetCaster(),nil,"generic_modifier_movespeed_slow",{
												slow = self:GetAbility():GetSpecialValueFor("bonus_slow")	,
												turns = self:GetAbility():GetSpecialValueFor("bonus_slow_duration")	
											})
											end
									  end,
									}						
									Projectiles:CreateProjectile(projectile)
														
							end
						end
					end							
				

end
	
--------------------------------------------------------------------------------

function modifier_seer_void:SolveAction()
	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
		self:Activate();
	return MODIFIERACTION_SHORTPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
