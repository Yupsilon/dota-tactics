ability_seer_void = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "modifier_seer_void","abilities/dark_seer/modifier_seer_void.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_order_knockback","mechanics/heroes/modifier_order_knockback.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_action_knockback","mechanics/heroes/modifier_action_knockback.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_combobreaker","abilities/generic/generic_modifier_combobreaker.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_seer_void:PrepAction(keys)
			
	local target_unit = EntIndexToHScript(keys.target_unit)
	local animationTime = PLAYER_TIME_MODIFIER;
	if target_unit~=nil then	

		self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
		self:UseResources(false,false,true)	
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})			
	
		self:GetCaster():FaceTowards(target_unit:GetAbsOrigin())	
		StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_3,rate = 1/PLAYER_TIME_MODIFIER})
	
		EmitSoundOn( "Hero_Dark_Seer.Surge", self:GetCaster() )
	
		self:OnProjectileHit(target_unit)			
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_seer_void:OnProjectileHit(hTarget)
																				
	--EmitSoundOn( "Hero_OgreMagi.Bloodlust.Target", self:GetCaster() )
	
						
						self.nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_dark_seer/dark_seer_surge_start.vpcf", PATTACH_ABSORIGIN_FOLLOW, hTarget )
						ParticleManager:SetParticleControl( self.nFXIndex, 0, hTarget:GetAbsOrigin() )							
						ParticleManager:ReleaseParticleIndex(self.nFXIndex)
								
		local other_modifier = hTarget:AddNewModifier( self:GetCaster(), self, "modifier_seer_void", {turns = self:GetSpecialValueFor("void_duration")} )
					
		if self:GetLevel()==4 then
						
			local shield_modifier = self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_shield", {life= self:GetSpecialValueFor("bonus_shield") , turns = self:GetSpecialValueFor("bonus_shield_duration"), applythisturn=1} )							
		end

						Alert_Mana({unit=self:GetCaster(), manavalue= self:GetSpecialValueFor("cast_energy"), energy=true} );
					if self:GetLevel() == 2 then
						Alert_Mana({unit=self:GetCaster(), manavalue= self:GetSpecialValueFor("energy_lesser"), energy=true} );
					end																	
	
end

--------------------------------------------------------------------------------

function ability_seer_void:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_seer_void:GetPriority()
	return 2;
end

--------------------------------------------------------------------------------

function ability_seer_void:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_seer_void:GetCastPoints()

	return 1;
end

--------------------------------------------------------------------------------

function ability_seer_void:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_seer_void:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_seer_void:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_seer_void:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_seer_void:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_seer_void:GetAOERadius()
	return self:GetSpecialValueFor( "void_aoe" )
end

--------------------------------------------------------------------------------

function ability_seer_void:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_seer_void:GetHasteBonus()	
	if self:GetLevel()==4 then			
		return (100+(self:GetSpecialValueFor("bonus_haste")))/100
	end
	return 1
end

--------------------------------------------------------------------------------