modifier_phoenix_fireshield = class({})

--------------------------------------------------------------------------------

function modifier_phoenix_fireshield:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function modifier_phoenix_fireshield:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_phoenix_fireshield:OnCreated( kv )

	self.activateSwitch = 0;
	self.damage =  0
	self:SetStackCount(kv.turns or 2)
	
	if IsServer() then
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_phoenix/phoenix_sunray_tgt.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		self:AddParticle( nFXIndex, false, false, -1, false, false )		
	end
end

--------------------------------------------------------------------------------

function modifier_phoenix_fireshield:Activate ( )


		self.activateSwitch=2
		local helix_aoe = self:GetAbility():GetSpecialValueFor("burst_area")
		local berserk_helix_energy =  0
		local helix_damage = self:GetAbility():GetSpecialValueFor("burst_damage")
		
		if self:GetAbility():GetLevel() == 4 then
			berserk_helix_energy = self:GetAbility():GetSpecialValueFor("bonus_energy")
		end
		
		EmitSoundOn( "Hero_Phoenix.FireSpirits.Target", self:GetParent() )
			local helix = ParticleManager:CreateParticle("particles/units/heroes/hero_phoenix/phoenix_fire_spirit_ground.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
			ParticleManager:SetParticleControl(helix, 0, self:GetParent():GetAbsOrigin())
			ParticleManager:SetParticleControl(helix, 1, Vector(helix_aoe,helix_aoe,helix_aoe))		
			ParticleManager:ReleaseParticleIndex(helix)	

			
				--StartAnimation(self:GetCaster(), {duration=1/2, activity=ACT_DOTA_CAST_ABILITY_3})
				
					local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), self:GetCaster(),helix_aoe , DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
					if #enemies > 0 then
						for _,enemy in pairs(enemies) do
							if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
						
									local direction = enemy:GetAbsOrigin()-self:GetParent():GetAbsOrigin()
									direction.z=0
									local projectile = {
									EffectAttach = PATTACH_WORLDORIGIN,			  			  
									vSpawnOrigin = self:GetParent():GetAbsOrigin()+Vector(0,0,30),
									fDistance = helix_aoe,
									fStartRadius = 50,
									fEndRadius = 50,
									fCollisionRadius = 30,
									  Source = self:GetCaster(),
									  vVelocity = direction:Normalized() * helix_aoe*10, 
									  UnitBehavior = PROJECTILES_NOTHING,
									  bMultipleHits = true,
									  bIgnoreSource = true,
									  TreeBehavior = PROJECTILES_NOTHING,
									  bTreeFullCollision = false,
									  WallBehavior = PROJECTILES_DESTROY,
									  GroundBehavior = PROJECTILES_DESTROY,
									  fGroundOffset = 0,
									  bZCheck = false,
									  bGroundLock = false,
									  draw = false,
									  bUseFindUnitsInRadius = true,
									  bHitScan = true,

									  UnitTest = function(instance, unit) return unit==enemy end,
									  OnUnitHit = function(instance, unit) 
															
											local damage_table = {
												victim = enemy,
												attacker = self:GetCaster(),
												damage = helix_damage,
												damage_type = DAMAGE_TYPE_PURE,
												ability = self:GetAbility(),
												cover_reduction = 1
											}
									
											if IsTargetOverCover(instance,self:GetParent()) then
												damage_table.cover_reduction=1/2
											end
															
											--local hit_particle=ParticleManager:CreateParticle("particles/units/heroes/hero_beastmaster/beastmaster_wildaxes_hit.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())
											--ParticleManager:SetParticleControl(hit_particle, 0, unit:GetAbsOrigin())
											--ParticleManager:ReleaseParticleIndex(hit_particle)

											Alert_Damage( damage_table )
											Alert_Mana({unit=self:GetCaster(),manavalue=berserk_helix_energy,energy=true});
										
									  end,
									}						
									Projectiles:CreateProjectile(projectile)
														
							end
						end
					end							
				

end

--------------------------------------------------------------------------------

function modifier_phoenix_fireshield:DeclareFunctions()
	local funcs = {
		MODIFIER_EVENT_ON_TAKEDAMAGE ,
	}	

	return funcs
end

--------------------------------------------------------------------------------

function modifier_phoenix_fireshield:OnTakeDamage ( params )
		if IsServer() then
				local hAttacker = params.attacker
				local hVictim = params.unit

			if hVictim == self:GetParent() then
						
				self.damage = self.damage+params.damage
				
				if self.activateSwitch==0  then					
				
					self.activateSwitch=1;
					--if GameMode:GetCurrentGamePhase() >= GAMEPHASE_ACTION then					
					--	self:Activate();						
					--end			
				end
			end
		end
end
	
--------------------------------------------------------------------------------

function modifier_phoenix_fireshield:SolveAction()
	if IsServer() then
		if self.activateSwitch==1 then
			self:Activate();
			return MODIFIERACTION_NOPAUSE_DESTROY;
		end
	end
	return MODIFIERACTION_NOPAUSE_SURVIVE;
end
	
--------------------------------------------------------------------------------

function modifier_phoenix_fireshield:GetPriority()
	
	return 5;
end

--------------------------------------------------------------------------------

function modifier_phoenix_fireshield:EndOfTurnAction()
	
	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
