ability_phoenix_supernova = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "modifier_phoenix_firespirits","abilities/phoenix/modifier_phoenix_firespirits.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_scar_immunity","abilities/generic/generic_modifier_scar_immunity.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_stun","abilities/generic/generic_modifier_stun.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

--model change
----models/heroes/phoenix/phoenix_egg.vmdl

function ability_phoenix_supernova:SolveAction(keys)

	local nova_aoe = self:GetSpecialValueFor("nova_aoe")

	EmitSoundOn( "Hero_Phoenix.SuperNova.Explode", self:GetCaster() )
	local hit_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_phoenix/phoenix_supernova_reborn.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())
	ParticleManager:SetParticleControl(hit_particle, 0, self:GetCaster():GetAbsOrigin())
	ParticleManager:SetParticleControl(hit_particle, 1, Vector(nova_aoe,nova_aoe,nova_aoe))	
	ParticleManager:ReleaseParticleIndex(hit_particle)
	
	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;			
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
	Alert_Mana({unit=self:GetCaster(),manavalue=0-self:GetManaCost(self:GetLevel());} )
			
	local animationTime = 2*PLAYER_TIME_MODIFIER;			
	StartAnimation(self:GetCaster(), {duration=animationTime, rate = 1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_INTRO})
		
	local nova_heal = self:GetSpecialValueFor("total_healing_self")
	
	Alert_HealScars({unit = self:GetCaster(), value = self:GetSpecialValueFor("healing_perma")})	
	if self:GetLevel() == 2 then
		
		
		--self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_scar_immunity", { turns = self:GetSpecialValueFor("bonus_duration_perma") } )
	
	--elseif self:GetLevel() == 3 then
		nova_heal = self:GetSpecialValueFor("bonus_healing_self")
	end
	
	--if self:GetLevel() == 4 then
	--	self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_regenerate", {value = self:GetSpecialValueFor("bonus_regen_heal"), turns = self:GetSpecialValueFor("bonus_regen_turns"),initial = 0} )
	--else
		Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=nova_heal})
	--end
	
	--self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_unstoppable", { turns = self:GetSpecialValueFor("unstoppable_duration") } )
	
	local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), self:GetCaster(), nova_aoe, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, 0, FIND_CLOSEST, false )
	
	local timerint = 1;
	if #enemies > 0 then
		for _,enemy in pairs(enemies) do
			if enemy ~= nil and ( not enemy:IsInvulnerable() )  then
						self:StrikeTarget(enemy)
			end
		end	
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_phoenix_supernova:StrikeTarget(hTarget)

	local startPoint = self:GetCaster():GetAbsOrigin()
	local enemy_direction = (hTarget:GetAbsOrigin()-startPoint)
							
	local selfheal = 0
	local nova_damage = self:GetSpecialValueFor("nova_damage")
	
	if self:GetLevel() == 2 then
		nova_damage = self:GetSpecialValueFor("bonus_nova_damage")
	end
										 
										
	local projectile = {
								EffectAttach = PATTACH_WORLDORIGIN,			  			  
								vSpawnOrigin = startPoint+Vector(0,0,30),
								fDistance =  enemy_direction:Length2D(),
								fStartRadius = 50,
								fEndRadius = 50,
								fCollisionRadius = 30,
								  Source = self:GetCaster(),
								  vVelocity = enemy_direction:Normalized() * enemy_direction:Length2D()*2, 
								  UnitBehavior = PROJECTILES_NOTHING,
								  bMultipleHits = true,
								  bIgnoreSource = true,
								  TreeBehavior = PROJECTILES_NOTHING,
								  bTreeFullCollision = false,
								  WallBehavior = PROJECTILES_DESTROY,
								  GroundBehavior = PROJECTILES_DESTROY,
								  fGroundOffset = 0,
								  bZCheck = false,
								  bGroundLock = false,
								  draw = false,
								  bUseFindUnitsInRadius = true,
								  bHitScan=true,
								  UnitTest = function(instance, unit) return unit==hTarget end,
								  OnUnitHit = function(instance, unit) 
															 
									local hit_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_phoenix/phoenix_supernova_hit.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())
									ParticleManager:SetParticleControl(hit_particle, 0, unit:GetAbsOrigin())
									ParticleManager:ReleaseParticleIndex(hit_particle)
														
										local damage_table = {
											victim = hTarget,
											attacker = self:GetCaster(),
											damage = nova_damage,
											damage_type = DAMAGE_TYPE_MAGICAL,
											cover_reduction = 1,
										}	
										if self.firstHit == false then
											self.firstHit = true
											--damage_table.damage_type = DAMAGE_TYPE_PHYSICAL
										end						
																
										if IsTargetOverCover(instance,damage_table.attacker) then
											damage_table.cover_reduction=1/2
										end
										
										if self:GetLevel() == 4 then
											hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_stun", {turns = self:GetSpecialValueFor("bonus_stun_turns")} )
											end

										Alert_Damage( damage_table )
										
								  end,
								}						
								Projectiles:CreateProjectile(projectile)
end
--------------------------------------------------------------------------------

function ability_phoenix_supernova:GetIntrinsicModifierName()
	return "modifier_phoenix_firespirits"
end

--------------------------------------------------------------------------------

function ability_phoenix_supernova:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_phoenix_supernova:GetPriority()
	return 1;
end

--------------------------------------------------------------------------------

function ability_phoenix_supernova:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("nova_aoe") 
end

--------------------------------------------------------------------------------

function ability_phoenix_supernova:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_phoenix_supernova:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_phoenix_supernova:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_phoenix_supernova:GetCastRange()	
	return self:GetMaxRange(0)
end


--------------------------------------------------------------------------------