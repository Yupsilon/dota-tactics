modifier_phoenix_protect = class({})

-----------------------------------------------------------------------------

function modifier_phoenix_protect:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function modifier_phoenix_protect:GetStatusEffectName()
	return "particles/units/heroes/hero_lina/lina_fiery_soul.vpcf"
end

--------------------------------------------------------------------------------

function modifier_phoenix_protect:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function modifier_phoenix_protect:GetPriority()
	return 3
end

--------------------------------------------------------------------------------

function modifier_phoenix_protect:IsHidden()
	return self.turns == 0 
end

--------------------------------------------------------------------------------

function modifier_phoenix_protect:OnCreated( kv )

	self.nodraw = kv.nodraw or 0
	self:SetStackCount(kv.turns or 2	)	
	
	self.damage=0
	self.absorption = kv.value or 75
	
	if IsServer() then
			
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_phoenix/phoenix_fire_spirit_burn.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		--ParticleManager:SetParticleControlEnt(nFXIndex, 1, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
		--ParticleManager:SetParticleControlEnt(nFXIndex, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetCaster():GetAbsOrigin(), true)	
		self:AddParticle( nFXIndex, false, false, -1, false, false )	
	end
end
	
--------------------------------------------------------------------------------

function modifier_phoenix_protect:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self.turns=self.turns+1
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function modifier_phoenix_protect:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_TOTAL_CONSTANT_BLOCK
	}	

	return funcs
end
		
--------------------------------------------------------------------------------

function modifier_phoenix_protect:GetModifierTotal_ConstantBlock(kv)
	if IsServer() then
		local target = self:GetParent()
		local damage = kv.damage
		
		if kv.damage > 0 then

			local absorbed_damage = self.absorption/100 * damage
			self.damage = self.damage + absorbed_damage
			
			local damage_table = {
				victim = self:GetCaster(),
				attacker = kv.attacker,
				damage = absorbed_damage,
				damage_type = DAMAGE_TYPE_PURE,
				ability = self:GetAbility()
			}
			Alert_Damage( damage_table )
			Alert_Mana({unit=self:GetCaster(),manavalue=absorbed_damage, energy=false});
			
			--local hit_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_beastmaster/beastmaster_wildaxes_hit.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())									
			--ParticleManager:SetParticleControl(hit_particle, 0, unit:GetAbsOrigin())
			--ParticleManager:ReleaseParticleIndex(hit_particle)

			
			return absorbed_damage
		end
	end
	return 0
end
	
--------------------------------------------------------------------------------

function modifier_phoenix_protect:OnDestroy()

	if IsServer() then	
		if self.damage==0 and self:GetAbility():GetLevel()==2 then
			Alert_CooldownRefresh(self:GetCaster(),self:GetAbility(),-self:GetAbility():GetSpecialValueFor("bonus_refreshing"))	
		end	
		if self.linked_modifiers ~= nil and #self.linked_modifiers>0 then
			for index, modifier in pairs (self.linked_modifiers) do
				if modifier~=nil then
					modifier:Destroy()
				end
			end
		end
	end
end

--------------------------------------------------------------------------------