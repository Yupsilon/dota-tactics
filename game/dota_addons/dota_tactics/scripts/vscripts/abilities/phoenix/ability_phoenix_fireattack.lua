ability_phoenix_fireattack = class({})
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_snare","abilities/generic/generic_modifier_snare.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_phoenix_fireattack:SolveAction(keys)

	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
	EmitSoundOn("Hero_Phoenix.PreAttack", self:GetCaster())
	
	
	if keys.target_point_A~=nil then				
			local animationTime = PLAYER_TIME_MODIFIER;
			self:GetCaster():FaceTowards(keys.target_point_A)		
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_OVERRIDE_ABILITY_2, rate = 1/PLAYER_TIME_MODIFIER})
				
			local projectile_range = self:GetSpecialValueFor("projectile_range")
			local projectile_radius = self:GetSpecialValueFor("projectile_radius") 
			
			local startPoint = self:GetCaster():GetAbsOrigin() + Vector(0,0,50)*self:GetCaster():GetModelScale();							
			
			local direction_A = (keys.target_point_A-startPoint)
			direction_A.z=0	
						
							local helix = ParticleManager:CreateParticle("particles/units/heroes/hero_lina/lina_spell_light_strike_array.vpcf", PATTACH_WORLDORIGIN, self:GetCaster())
							ParticleManager:SetParticleControl( helix, 0, keys.target_point_A )	
							ParticleManager:SetParticleControl(helix, 1, Vector(bonus_aoe,bonus_aoe,bonus_aoe))		
							ParticleManager:ReleaseParticleIndex(helix)	
							EmitSoundOn("Hero_Phoenix.SuperNova.Death", self:GetCaster())
			
			Timers:CreateTimer(animationTime/2, function() 		
			
							
				local projectile = {
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint,
				  fDistance = math.min(direction_A:Length2D(),projectile_range),
				  fStartRadius = projectile_radius,
				  fEndRadius = projectile_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  vVelocity = direction_A:Normalized() * projectile_radius, 
				  UnitBehavior = PROJECTILES_NOTHING,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_DESTROY,
				  GroundBehavior = PROJECTILES_DESTROY,
				  fGroundOffset = 0,
				  bZCheck = false,
				  bGroundLock = false,
				  draw = false,
				  bUseFindUnitsInRadius = true,
				  bHitScan = true,

				  --UnitTest = function(instance, unit) return unit:IsInvulnerable() == false and unit:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())==nil end,
				  --OnUnitHit = function(instance, unit) 		

				--			instance.fDistance = 0
				  --end,
				  OnFinish = function(instance, pos)		  						
				  
						local bonus_aoe = self:GetAOERadius()
						local selfheal = 0
							
							local targets = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), pos, self:GetCaster(), bonus_aoe, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
							if #targets > 0 then
								for _,target in pairs(targets) do
									if target ~= nil and ( not target:IsInvulnerable() and target:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())==nil  )  then	
										self:OnProjectileHit(target,pos)		
										if self:GetLevel() == 2 then
											selfheal = selfheal+self:GetSpecialValueFor("bonus_selfheal")
										end
									end
								end
							end	
				  if selfheal>0 then
						Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=selfheal})
					end
				  end
				}						
				Projectiles:CreateProjectile(projectile)
			end)		
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_phoenix_fireattack:GetPriority()
	return 0;
end

--------------------------------------------------------------------------------

function ability_phoenix_fireattack:OnProjectileHit(hTarget, vLocation)
	
	local diff = vLocation - hTarget:GetAbsOrigin()
	diff.z = 0
						
	local aoe_damage = self:GetSpecialValueFor("outer_damage")
						
	if diff:Length2D()<=self:GetSpecialValueFor("inner_radius") then
		aoe_damage = self:GetSpecialValueFor("inner_damage")
		
	end
		if self:GetLevel() == 4 then
			local nSpirits =  self:GetCaster():GetModifierStackCount("modifier_phoenix_firespirits",self:GetCaster())
			aoe_damage = aoe_damage + self:GetSpecialValueFor("bonus_spirit_damage") * nSpirits
		end
	
	hTarget:AddNewModifier(self:GetCaster(),self,"modifier_hit",{turns=1})				
	
					local damage_table = {					
							victim = hTarget,
							attacker = self:GetCaster(),
							damage = aoe_damage,
							damage_type = DAMAGE_TYPE_MAGICAL,
							ability=self,
							cover_reduction = 1
						}
						
			--EmitSoundOn( "Hero_Juggernaut.BladeFury.Impact", self:GetCaster() )
			
				--					local hit_particle = ParticleManager:CreateParticle("particles/items2_fx/shivas_guard_impact.vpcf", PATTACH_CUSTOMORIGIN, hTarget)
					--				ParticleManager:SetParticleControl(hit_particle, 0, hTarget:GetAbsOrigin())
						--			ParticleManager:ReleaseParticleIndex(hit_particle)
											
					
												
					
						--if IsTargetPointOverCover(vLocation,damage_table.attacker) then
						--	damage_table.cover_reduction=1/2
						--end
				
						Alert_Damage(damage_table)			
	if self:GetLevel() == 3 then
		Alert_Mana({unit=self:GetCaster(), manavalue=self:GetSpecialValueFor("bonus_energy"), energy=true} );
	end
end

--------------------------------------------------------------------------------

function ability_phoenix_fireattack:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_phoenix_fireattack:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_phoenix_fireattack:RequiresHitScanCheck()
	return self:GetLevel()~=3;
end

--------------------------------------------------------------------------------

function ability_phoenix_fireattack:GetCastPoints()	
	return 1;
end

--------------------------------------------------------------------------------

function ability_phoenix_fireattack:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_phoenix_fireattack:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_phoenix_fireattack:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_phoenix_fireattack:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_phoenix_fireattack:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_phoenix_fireattack:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_phoenix_fireattack:GetAOERadius(coneID)	
	local radius = self:GetSpecialValueFor("outer_radius")
	
	local nSpirits =  self:GetCaster():GetModifierStackCount("modifier_phoenix_firespirits",self:GetCaster())	
	radius = radius + self:GetSpecialValueFor("outer_radius_extension") * nSpirits
	
	return radius
end

--------------------------------------------------------------------------------