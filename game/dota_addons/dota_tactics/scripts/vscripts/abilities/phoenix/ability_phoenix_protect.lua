ability_phoenix_protect = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "modifier_phoenix_protect","abilities/phoenix/modifier_phoenix_protect.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_phoenix_protect_caster","abilities/phoenix/modifier_phoenix_protect_caster.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_energy_regeneration","abilities/modifier_energy_regeneration.lua", LUA_MODIFIER_MOTION_NONE )


--------------------------------------------------------------------------------

function ability_phoenix_protect:PrepAction(keys)
			
	local target_unit = EntIndexToHScript(keys.target_unit)
	if target_unit~=nil then	

		self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
		self:UseResources(true,false,true)	
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})	
		StartAnimation(self:GetCaster(), {duration=animationTime, rate = 1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_ATTACK})		
		self:GetCaster():FaceTowards(target_unit:GetAbsOrigin())				
		
		--Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("cast_energy"), energy=true});
		self:OnProjectileHit(target_unit,true)		

		local selfShield = self:GetSpecialValueFor("self_shield")
		
		local nSpirits =  self:GetCaster():GetModifierStackCount("modifier_phoenix_firespirits",self:GetCaster())			
		selfShield = selfShield + self:GetSpecialValueFor("spirit_shield") * nSpirits
		
		if self:GetLevel() == 3 then									
			selfShield = selfShield + self:GetSpecialValueFor("bonus_spirit_shield") * nSpirits
		end
		
		self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_shield", {life= selfShield , turns = self:GetSpecialValueFor("modifier_duration"), applythisturn=1} )
		self:GetCaster():AddNewModifier( self:GetCaster(), self, "modifier_phoenix_protect_caster", {} )		
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_phoenix_protect:CastFilterResultTarget( hTarget )
	if self:GetCaster() == hTarget then
		return UF_FAIL_CUSTOM
	end

	return UF_SUCCESS
end

--------------------------------------------------------------------------------

function ability_phoenix_protect:GetCustomCastErrorTarget( hTarget )
	if self:GetCaster() == hTarget then
		return "#dota_hud_error_cant_cast_on_self"
	end

	return ""
end
--------------------------------------------------------------------------------

function ability_phoenix_protect:GetIntrinsicModifierName()
	return "modifier_energy_regeneration"
end

--------------------------------------------------------------------------------

function ability_phoenix_protect:GetDamageConversion()
	if self:GetCaster():HasModifier("modifier_phoenix_protect_caster") then return 0 end
	return self:GetSpecialValueFor("base_energy_absorbtion")
end

--------------------------------------------------------------------------------

function ability_phoenix_protect:GetRegenerationValue()
	return self:GetSpecialValueFor("base_energy_regen")
end

--------------------------------------------------------------------------------

function ability_phoenix_protect:OnProjectileHit(hTarget)

	local absorption = self:GetSpecialValueFor("modifier_protection")
	local shield_modifier = hTarget:AddNewModifier( self:GetCaster(), self, "modifier_phoenix_protect", {value= absorption , turns = self:GetSpecialValueFor("modifier_duration") } )

	if self:GetLevel() == 4 then	
		hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_unstoppable", { turns = self:GetSpecialValueFor("bonus_unstoppable_duration") } )
	end
	EmitSoundOn( "Hero_Phoenix.IcarusDive.Cast", hTarget )	
end

--------------------------------------------------------------------------------

function ability_phoenix_protect:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_phoenix_protect:GetPriority()
	return 5;
end

--------------------------------------------------------------------------------

function ability_phoenix_protect:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_phoenix_protect:GetCastPoints()

	return 1;
end

--------------------------------------------------------------------------------

function ability_phoenix_protect:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_phoenix_protect:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_phoenix_protect:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_phoenix_protect:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_phoenix_protect:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_phoenix_protect:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------