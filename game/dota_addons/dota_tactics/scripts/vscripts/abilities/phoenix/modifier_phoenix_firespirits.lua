modifier_phoenix_firespirits = class({})

--------------------------------------------------------------------------------

function modifier_phoenix_firespirits:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function modifier_phoenix_firespirits:GetTexture()
	return "phoenix_fire_spirits"
end

--------------------------------------------------------------------------------

function modifier_phoenix_firespirits:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_phoenix_firespirits:IsPermanent()
	return true
end

--------------------------------------------------------------------------------

function modifier_phoenix_firespirits:RemoveOnDeath()
	return false
end
	
--------------------------------------------------------------------------------

function modifier_phoenix_firespirits:PrepAction()
	if self:GetAbility():GetLevel()==3 and self:GetCaster():GetMana() == 1000 then
		--Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=self:GetAbility():GetSpecialValueFor("bonus_regen_heal")})
		self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_unstoppable", { turns = self:GetAbility():GetSpecialValueFor("unstoppable_duration") } )
	end
	return MODIFIERACTION_NOPAUSE_SURVIVE;
end
--------------------------------------------------------------------------------

function modifier_phoenix_firespirits:OnCreated( kv )

	self.baseRot = 0
	  
	if IsServer() then
		
		self.firespiritsFX = {}
		for i=1,5 do
			
			self.firespiritsFX[i] = ParticleManager:CreateParticle("particles/units/heroes/hero_phoenix/phoenix_fire_spirits_birda.vpcf", PATTACH_CUSTOMORIGIN, self:GetParent())		
			self:AddParticle( self.firespiritsFX[i], false, false, -1, false, false )	
		end
		
		self:StartIntervalThink( 1/30 )
		self:OnIntervalThink()		
	end
end

--------------------------------------------------------------------------------

function modifier_phoenix_firespirits:OnIntervalThink()
	
	if IsServer() then 

		local nspirits = math.floor(self:GetParent():GetMana()/self:GetParent():GetMaxMana()*5)		
		if (not self:GetParent():IsAlive()) or self:GetParent():IsInvisible() or self:GetParent():GetMana()==0 then
			nspirits=0
		end
			
		self:SetStackCount(nspirits)
		
		self.baseRot = self.baseRot+2
		local distancex = 120
		local distancez = 75
		local parent_center = self:GetParent():GetAbsOrigin()
		
		--if nspirits>0 then
			for i=1,5 do
				
				if i > nspirits then
					 ParticleManager:SetParticleControl(self.firespiritsFX[i], 0, parent_center+Vector (0,0,-999))				
				else
					local localrot = (self.baseRot + 360/nspirits*i ) * 3.14/180
					local vectordelta = Vector (math.cos(localrot)*distancex,math.sin(localrot)*distancex,distancez)
				
					ParticleManager:SetParticleControl(self.firespiritsFX[i], 0, parent_center+vectordelta)
					ParticleManager:SetParticleControl(self.firespiritsFX[i], 5, parent_center)
				end
			end
		--end
	end
end

--------------------------------------------------------------------------------

function modifier_phoenix_firespirits:DeclareFunctions()
	local funcs = {
		MODIFIER_EVENT_ON_DEATH
	}	

	return funcs
end

--------------------------------------------------------------------------------

function modifier_phoenix_firespirits:OnDeath()
	local mana_mod = 1 - self:GetAbility():GetSpecialValueFor("mana_lost") / 100
	
	self:GetCaster():SetMana(self:GetCaster():GetMana()*mana_mod)
end	

--------------------------------------------------------------------------------
