ability_omniknight_helbeam = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_snare","abilities/generic/generic_modifier_snare.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_omniknight_helbeam:SolveAction(keys)

	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
	
	if keys.target_point_A~=nil then				
			local animationTime = PLAYER_TIME_MODIFIER;
			self:GetCaster():FaceTowards(keys.target_point_A)		
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_1, rate = 1/PLAYER_TIME_MODIFIER})
				
			local projectile_range = self:GetSpecialValueFor("projectile_range")
			local projectile_radius = self:GetSpecialValueFor("projectile_radius") 
			local projectile_speed = math.max(900,projectile_range/PLAYER_TIME_MODIFIER)
			
			local startPoint = self:GetCaster():GetAbsOrigin() + Vector(0,0,50)*self:GetCaster():GetModelScale();							
			
			local direction_A = (keys.target_point_A-startPoint)
			direction_A.z=0	
			--startPoint=startPoint+direction_A:Normalized()*projectile_radius/2
			
			self:OnProjectileHit(self:GetCaster(),startPoint)	
			Timers:CreateTimer(animationTime, function() 		
			
				self.soundDummy = CreateUnitByName("npc_dummy_unit", startPoint, true, nil, nil, DOTA_TEAM_NOTEAM)	
				self.soundDummy:FindAbilityByName("dummy_unit"):SetLevel(1)						
			
				local projthink =  projectile_radius/projectile_speed*2/3
				local lastthink = GameRules:GetGameTime()
				local wall_beh = PROJECTILES_DESTROY 
				if self:GetLevel() == 3 then wall_beh = PROJECTILES_NOTHING end
				
				--EmitSoundOn( "Hero_Juggernaut.OmniSlash.Damage", self:GetCaster() )								
				local projectile = {
				--  EffectName = "particles/neutral_fx/tornado_ambient.vpcf",
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint,
				  fDistance = projectile_range,
				  fStartRadius = projectile_radius,
				  fEndRadius = projectile_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  vVelocity = direction_A:Normalized() * projectile_speed, 
				  UnitBehavior = PROJECTILES_NOTHING,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  bTreeFullCollision = false,
				  WallBehavior = wall_beh,
				  GroundBehavior = wall_beh,
				  fGroundOffset = 0,
				  bZCheck = false,
				  bGroundLock = false,
				  draw = false,
				  bUseFindUnitsInRadius = true,
				  bHitScan = false,

				  UnitTest = function(instance, unit) return unit:IsInvulnerable() == false and unit:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())==nil end,
				  OnUnitHit = function(instance, unit) 		

						if self:GetLevel() == 2 then	
						
							local bonus_aoe = self:GetSpecialValueFor("bonus_aoe")
						
							local helix = ParticleManager:CreateParticle("particles/units/heroes/hero_omniknight/omniknight_purification.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())
							ParticleManager:SetParticleControl( helix, 0, unit:GetAbsOrigin() )	
							ParticleManager:SetParticleControl(helix, 1, Vector(bonus_aoe,bonus_aoe,bonus_aoe)/2)		
							ParticleManager:ReleaseParticleIndex(helix)	
							EmitSoundOn("Hero_Omniknight.DivineLight", self.soundDummy)
							
							local targets = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), unit:GetAbsOrigin(), self:GetCaster(), bonus_aoe, DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
							if #targets > 0 then
								for _,target in pairs(targets) do
									if target ~= nil and ( not target:IsInvulnerable() and target:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())==nil  )  then	
										self:OnProjectileHit(target,unit:GetAbsOrigin())										
									end
								end
							end	
							
							instance.fDistance = 0
						else
							self:OnProjectileHit(unit,instance:GetPosition())
						end
				  end,
				 OnIntervalThink = function(instance)	
				 
						if GameRules:GetGameTime() > lastthink then
							lastthink = GameRules:GetGameTime() + projthink
							
							local helix = ParticleManager:CreateParticle("particles/units/heroes/hero_omniknight/omniknight_purification.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())
							ParticleManager:SetParticleControl( helix, 0, instance:GetPosition() )	
							ParticleManager:SetParticleControl(helix, 1, Vector(projectile_radius,projectile_radius,projectile_radius)/2)		
							ParticleManager:ReleaseParticleIndex(helix)	
							
							if self.soundDummy then
								self.soundDummy:SetAbsOrigin(instance:GetPosition())
								EmitSoundOn("Hero_Omniknight.DivineLight", self.soundDummy)
							end
						end
				  end,
				  OnFinish = function(instance, pos)		  
						
						if self.soundDummy then
							self.soundDummy:Destroy()
						end
				  end
				}						
				Projectiles:CreateProjectile(projectile)
			end)		
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_omniknight_helbeam:GetPriority()
	return 0;
end

--------------------------------------------------------------------------------

function ability_omniknight_helbeam:OnProjectileHit(hTarget, vLocation)
	
	local target_damage = self:GetSpecialValueFor("projectile_damage")
	local target_healing = self:GetSpecialValueFor("projectile_healing")
	
	hTarget:AddNewModifier(self:GetCaster(),self,"modifier_hit",{turns=1})									
	if self:GetLevel() == 4 then
		target_damage = target_damage+self:GetSpecialValueFor("bonus_damage")
		target_healing = target_healing+self:GetSpecialValueFor("bonus_healing")
	end
	
	if  hTarget:GetTeamNumber() ~= self:GetCaster():GetTeamNumber() then
					local damage_table = {					
							victim = hTarget,
							attacker = self:GetCaster(),
							damage = target_damage,
							damage_type = DAMAGE_TYPE_MAGICAL,
							ability=self,
							cover_reduction = 1
						}
						
			--EmitSoundOn( "Hero_Juggernaut.BladeFury.Impact", self:GetCaster() )
			
				--					local hit_particle = ParticleManager:CreateParticle("particles/items2_fx/shivas_guard_impact.vpcf", PATTACH_CUSTOMORIGIN, hTarget)
					--				ParticleManager:SetParticleControl(hit_particle, 0, hTarget:GetAbsOrigin())
						--			ParticleManager:ReleaseParticleIndex(hit_particle)
											
					
												
					
						if IsTargetPointOverCover(vLocation,damage_table.attacker) then
							damage_table.cover_reduction=1/2
						end
				
						Alert_Damage(damage_table)						
						Alert_Mana({unit=self:GetCaster(), manavalue=self:GetSpecialValueFor("enemy_energy"), energy=true} );
					
	else
	
		Alert_Heal({caster=self:GetCaster(),target=hTarget,value=target_healing})
		Alert_Mana({unit=self:GetCaster(), manavalue=self:GetSpecialValueFor("ally_energy"), energy=true} );
	end
end

--------------------------------------------------------------------------------

function ability_omniknight_helbeam:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_omniknight_helbeam:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_omniknight_helbeam:RequiresHitScanCheck()
	return self:GetLevel()~=3;
end

--------------------------------------------------------------------------------

function ability_omniknight_helbeam:GetCastPoints()	
	return 1;
end

--------------------------------------------------------------------------------

function ability_omniknight_helbeam:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_omniknight_helbeam:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_omniknight_helbeam:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_omniknight_helbeam:GetMinRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_omniknight_helbeam:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_omniknight_helbeam:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------