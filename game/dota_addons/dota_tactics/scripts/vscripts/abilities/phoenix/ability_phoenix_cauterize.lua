ability_phoenix_cauterize = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_regenerate","abilities/generic/generic_modifier_regenerate.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_phoenix_cauterize:PrepAction(keys)
			
	local target_unit = EntIndexToHScript(keys.target_unit)
	if target_unit~=nil then	

		self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
		self:UseResources(true,false,true)	
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})	
		StartAnimation(self:GetCaster(), {duration=animationTime, rate = 1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_ATTACK})		
	
		if self:GetCaster()~= target_unit then
	
			self:GetCaster():FaceTowards(target_unit:GetAbsOrigin())				
			self:OnProjectileHit(self:GetCaster(),false)	
		end
		
	
		--Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("burst_energy"), energy=true});
		self:OnProjectileHit(target_unit,true)			
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_phoenix_cauterize:OnProjectileHit(hTarget,main_target)
	
	local base_heal = self:GetSpecialValueFor("base_heal") 
	local spirit_heal = self:GetSpecialValueFor("spirit_heal") 
	
	if self:GetLevel() == 3 then
		spirit_heal = spirit_heal + self:GetSpecialValueFor("bonus_spirit_heal") 
	elseif self:GetLevel() == 4 then	
		--Alert_HealScars({unit = self:GetCaster(), value = self:GetSpecialValueFor("bonus_healing_perma")})	
		--self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_scar_immunity", { turns = self:GetSpecialValueFor("bonus_duration_perma") } )
		self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_shield", {life= self:GetSpecialValueFor("bonus_healing_perma") , turns = self:GetSpecialValueFor("bonus_duration_perma")} )
		
	elseif self:GetLevel() == 2 and hTarget:HasModifier("modifier_phoenix_fireshield") and main_target then
		Alert_Heal({caster=self:GetCaster(),target=hTarget,value=self:GetSpecialValueFor("bonus_base_heal") })
	end
		
	local nSpirits =  self:GetCaster():GetModifierStackCount("modifier_phoenix_firespirits",self:GetCaster())			
	local total_healing = base_heal + spirit_heal * nSpirits

	if main_target==true then
			local healMod = hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_regenerate", {value = total_healing, turns = self:GetSpecialValueFor("modifier_duration"), initial = total_healing, nodraw=1 } )
			local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_jakiro/jakiro_liquid_fire_debuff.vpcf", PATTACH_ABSORIGIN_FOLLOW, hTarget )
			healMod:AddParticle( nFXIndex, false, false, -1, false, false )	
			EmitSoundOn( "Hero_Phoenix.FireSpirits.Cast", hTarget )	
	else
		Alert_Heal({caster=self:GetCaster(),target=hTarget,value=total_healing})
	end
end

--------------------------------------------------------------------------------

function ability_phoenix_cauterize:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_phoenix_cauterize:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_phoenix_cauterize:GetCastPoints()

	return 1;
end

--------------------------------------------------------------------------------

function ability_phoenix_cauterize:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_phoenix_cauterize:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_phoenix_cauterize:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_phoenix_cauterize:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_phoenix_cauterize:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_phoenix_cauterize:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------