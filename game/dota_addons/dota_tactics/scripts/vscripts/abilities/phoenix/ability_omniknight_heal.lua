ability_omniknight_heal = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )

--LinkLuaModifier( "modifier_omniknight_heal","abilities/omniknight/modifier_omniknight_heal.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_regenerate","abilities/generic/generic_modifier_regenerate.lua", LUA_MODIFIER_MOTION_NONE )

--LinkLuaModifier( "modifier_order_knockback","mechanics/heroes/modifier_order_knockback.lua", LUA_MODIFIER_MOTION_NONE )
--LinkLuaModifier( "modifier_action_knockback","mechanics/heroes/modifier_action_knockback.lua", LUA_MODIFIER_MOTION_NONE )
--LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
--LinkLuaModifier( "generic_modifier_combobreaker","abilities/generic/generic_modifier_combobreaker.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_omniknight_heal:PrepAction(keys)
			
	local target_unit = EntIndexToHScript(keys.target_unit)
	if target_unit~=nil then	

		self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
		self:UseResources(true,false,true)	
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})	
		StartAnimation(self:GetCaster(), {duration=animationTime, rate = 1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_CAST_ABILITY_1})		
	
		if self:GetCaster()~= target_unit then
	
			self:GetCaster():FaceTowards(target_unit:GetAbsOrigin())				
			--self:OnProjectileHit(self:GetCaster(),false)	
		end
		
	
		Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("burst_energy"), energy=true});
		self:OnProjectileHit(target_unit,true)			
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_omniknight_heal:OnProjectileHit(hTarget,main_target)

	if main_target==true then
		--local damage_modifier = hTarget:AddNewModifier( self:GetCaster(), self, "modifier_omniknight_heal", {turns = 1 } )	
		
		local shield_modifier = hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_shield", {life= self:GetSpecialValueFor("healing_base") , turns = self:GetSpecialValueFor("modifier_duration"), nodraw=1 } )
		if self:GetLevel()==3 then
			hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_unstoppable", { turns = self:GetSpecialValueFor("bonus_unstoppable_duration") } )
		else
			local unstopp = hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_unstoppable", { turns = self:GetSpecialValueFor("modifier_duration") } )
			table.insert(shield_modifier.linked_modifiers,unstopp)
		end
		
		if self:GetLevel()==4 then
			local regen = hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_regenerate", {value = self:GetSpecialValueFor("bonus_healing"), turns = self:GetSpecialValueFor("modifier_duration"), initial = 0 } )
			regen.turns = 1
			table.insert(shield_modifier.linked_modifiers,regen)
		elseif self:GetLevel()==2 then
			--damage_modifier:Activate();
			Alert_CooldownRefresh(self:GetCaster(),self,-self:GetSpecialValueFor("bonus_refresh"))			
		end
		
		EmitSoundOn( "Hero_Omniknight.Repel", hTarget )
		local nFXIndex = ParticleManager:CreateParticle( "particles/econ/items/omniknight/omni_ti8_head/omniknight_repel_buff_ti8.vpcf", PATTACH_ABSORIGIN_FOLLOW, hTarget )
		ParticleManager:SetParticleControlEnt(nFXIndex, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_origin", hTarget:GetAbsOrigin(), true)
		shield_modifier:AddParticle( nFXIndex, false, false, -1, false, false )	
	end
		
	--[[local min_life = self:GetSpecialValueFor("healing_base")
	local healing_missing = self:GetSpecialValueFor("healing_missing")
	if self:GetLevel()==4 and main_target then
		healing_missing = healing_missing+self:GetSpecialValueFor("bonus_healing")
	end
	
	local hPercentage = hTarget:GetHealth()/(hTarget:GetMaxHealth()+hTarget:GetModifierStackCount("generic_modifier_scarred",nil)-self:GetSpecialValueFor("tresspass_health"))
	
	local total_healing = min_life+(healing_missing)*math.min(1,1-hPercentage)
	
		EmitSoundOn( "Hero_Omniknight.Repel.TI8", hTarget )
	Alert_Heal({caster=self:GetCaster(),target=hTarget,value=total_healing})]]
end

--------------------------------------------------------------------------------

function ability_omniknight_heal:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_omniknight_heal:GetPriority()
	return 5;
end

--------------------------------------------------------------------------------

function ability_omniknight_heal:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_omniknight_heal:GetCastPoints()

	return 1;
end

--------------------------------------------------------------------------------

function ability_omniknight_heal:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_omniknight_heal:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_omniknight_heal:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_omniknight_heal:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_omniknight_heal:GetMaxRange(coneID)	
	--if self:GetLevel() == 3 then return self:GetSpecialValueFor("projectile_range")+self:GetSpecialValueFor("bonus_range") end
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_omniknight_heal:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_omniknight_heal:GetAOERadius()
	return self:GetSpecialValueFor( "burst_aoe" )
end

--------------------------------------------------------------------------------