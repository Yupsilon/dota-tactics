modifier_phoenix_protect_caster = class({})


--------------------------------------------------------------------------------

function modifier_phoenix_protect_caster:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function modifier_phoenix_protect_caster:IsHidden()
	return true
end
	
--------------------------------------------------------------------------------

function modifier_phoenix_protect_caster:EndOfTurnAction()
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------