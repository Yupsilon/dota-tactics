ability_phoenix_fireshield = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "modifier_phoenix_fireshield","abilities/phoenix/modifier_phoenix_fireshield.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_scar_immunity","abilities/generic/generic_modifier_scar_immunity.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_phoenix_fireshield:PrepAction(keys)
			
	local target_unit = EntIndexToHScript(keys.target_unit)
	local animationTime = PLAYER_TIME_MODIFIER;
	if target_unit~=nil then	

		self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
		self:UseResources(false,false,true)	
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})			
	
		self:GetCaster():FaceTowards(target_unit:GetAbsOrigin())	
		StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_2,rate = 1/PLAYER_TIME_MODIFIER})
		
		if self:GetLevel() == 2 and self:GetCaster():GetModifierStackCount("modifier_phoenix_firespirits",self:GetCaster()) == 5 then			
			Alert_CooldownRefresh(self:GetCaster(),self,-self:GetSpecialValueFor("bonus_refreshing"))	
		end
		
		self:OnProjectileHit(target_unit)			
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_phoenix_fireshield:OnProjectileHit(hTarget)
																
		
		EmitSoundOn( "Hero_Phoenix.FireSpirits.Launch", hTarget )
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_batrider/batrider_firefly_startflash.vpcf", PATTACH_ABSORIGIN_FOLLOW, hTarget )
		ParticleManager:SetParticleControlEnt(nFXIndex, 0, hTarget, PATTACH_POINT_FOLLOW, "attach_origin", hTarget:GetAbsOrigin(), true)
		ParticleManager:ReleaseParticleIndex( nFXIndex )	
								
		local other_modifier = hTarget:AddNewModifier( self:GetCaster(), self, "modifier_phoenix_fireshield", {turns = self:GetSpecialValueFor("modifier_duration")} )
					
		if self:GetLevel() == 3 then
						
			local nSpirits =  self:GetCaster():GetModifierStackCount("modifier_phoenix_firespirits",self:GetCaster())
			
			if (nSpirits>0) then
				self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_shield", {life= self:GetSpecialValueFor("bonus_shield") * nSpirits , turns = self:GetSpecialValueFor("bonus_shield_duration"), applythisturn=1} )							
			end
		end

		--Alert_Mana({unit=self:GetCaster(), manavalue= self:GetSpecialValueFor("cast_energy"), energy=true} );
																				
	
end

--------------------------------------------------------------------------------

function ability_phoenix_fireshield:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_phoenix_fireshield:GetPriority()
	return 2;
end

--------------------------------------------------------------------------------

function ability_phoenix_fireshield:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function ability_phoenix_fireshield:GetCastPoints()

	return 1;
end

--------------------------------------------------------------------------------

function ability_phoenix_fireshield:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_phoenix_fireshield:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_phoenix_fireshield:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_phoenix_fireshield:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_phoenix_fireshield:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_phoenix_fireshield:GetAOERadius()
	return self:GetSpecialValueFor( "burst_area" )
end

--------------------------------------------------------------------------------

function ability_phoenix_fireshield:GetCastRange()	
	return self:GetMaxRange(0)
end


--------------------------------------------------------------------------------