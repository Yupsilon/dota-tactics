modifier_axe_berserker_selfheal = class({})

--------------------------------------------------------------------------------

function modifier_axe_berserker_selfheal:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function modifier_axe_berserker_selfheal:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_axe_berserker_selfheal:OnCreated( kv )

	self.activateSwitch = 1;
	self.maxlife =  self:GetAbility().berserk_max_regen
	self.damage =  0
	
	if IsServer() then
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_axe/axe_beserkers_call.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		self:AddParticle( nFXIndex, false, false, -1, false, false )		
	end
end

--------------------------------------------------------------------------------

function modifier_axe_berserker_selfheal:GetStatusEffectName()
	return "particles/status_fx/status_effect_gods_strength.vpcf"
end

--------------------------------------------------------------------------------

function modifier_axe_berserker_selfheal:Activate ( )


		self.activateSwitch=2
		local helix_aoe = self:GetAbility().berserk_helix_aoe
		local berserk_helix_energy =  self:GetAbility():GetSpecialValueFor("berserk_helix_energy")
			EmitSoundOn( "Hero_Axe.CounterHelix", self:GetCaster() )
		
		local helix_damage_min = self:GetAbility():GetSpecialValueFor("berserk_helix_damage_min")
		local helix_damage_max = self:GetAbility():GetSpecialValueFor("berserk_helix_damage_max")
		
		local helix_damage = helix_damage_min + (helix_damage_max - helix_damage_min) * math.min(self.damage/self:GetAbility().berserk_helix_aoe,1)
		
			local helix = ParticleManager:CreateParticle("particles/units/heroes/hero_axe/axe_attack_blur_counterhelix.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
			ParticleManager:SetParticleControl(helix, 0, self:GetParent():GetAbsOrigin())		
			ParticleManager:ReleaseParticleIndex(helix)	

			
				StartAnimation(self:GetCaster(), {duration=1/2, activity=ACT_DOTA_CAST_ABILITY_3})
				
					local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), self:GetCaster(),helix_aoe , DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
					if #enemies > 0 then
						for _,enemy in pairs(enemies) do
							if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
						
									local direction = enemy:GetAbsOrigin()-self:GetCaster():GetAbsOrigin()
									direction.z=0
									local projectile = {
									EffectAttach = PATTACH_WORLDORIGIN,			  			  
									vSpawnOrigin = self:GetParent():GetAbsOrigin()+Vector(0,0,30),
									fDistance = helix_aoe,
									fStartRadius = 50,
									fEndRadius = 50,
									fCollisionRadius = 30,
									  Source = self:GetCaster(),
									  vVelocity = direction:Normalized() * helix_aoe*10, 
									  UnitBehavior = PROJECTILES_NOTHING,
									  bMultipleHits = true,
									  bIgnoreSource = true,
									  TreeBehavior = PROJECTILES_NOTHING,
									  bTreeFullCollision = false,
									  WallBehavior = PROJECTILES_DESTROY,
									  GroundBehavior = PROJECTILES_DESTROY,
									  fGroundOffset = 0,
									  bZCheck = false,
									  bGroundLock = false,
									  draw = false,
									  bUseFindUnitsInRadius = true,
									  bHitScan = true,

									  UnitTest = function(instance, unit) return unit==enemy end,
									  OnUnitHit = function(instance, unit) 
															
											local damage_table = {
												victim = enemy,
												attacker = self:GetCaster(),
												damage = helix_damage,
												damage_type = DAMAGE_TYPE_MAGICAL,
												ability = self:GetAbility(),
												cover_reduction = 1
											}
									
											if IsTargetOverCover(instance,damage_table.attacker) then
												damage_table.cover_reduction=1/2
											end
															
											local hit_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_beastmaster/beastmaster_wildaxes_hit.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())									
											ParticleManager:SetParticleControl(hit_particle, 0, unit:GetAbsOrigin())
											ParticleManager:ReleaseParticleIndex(hit_particle)

											Alert_Damage( damage_table )
											Alert_Mana({unit=self:GetCaster(),manavalue=berserk_helix_energy,energy=true});
										
									  end,
									}						
									Projectiles:CreateProjectile(projectile)
														
							end
						end
					end							
				

end

--------------------------------------------------------------------------------

function modifier_axe_berserker_selfheal:DeclareFunctions()
	local funcs = {
		MODIFIER_EVENT_ON_TAKEDAMAGE ,
	}	

	return funcs
end

--------------------------------------------------------------------------------

function modifier_axe_berserker_selfheal:OnTakeDamage ( params )
		if IsServer() then
				local hAttacker = params.attacker
				local hVictim = params.unit

			if hVictim == self:GetParent() then
						
				self.damage = self.damage+params.damage
				
				if self.activateSwitch==0 and params.damage_type~=DAMAGE_TYPE_PURE then					
				
					self.activateSwitch=1;
					--if GameMode:GetCurrentGamePhase() >= GAMEPHASE_ACTION then					
					--	self:Activate();						
					--end			
				end
			end
		end
end
	
--------------------------------------------------------------------------------

function modifier_axe_berserker_selfheal:SolveAction()
	if IsServer() then
		if self.activateSwitch==1 then
			self:Activate();
			return MODIFIERACTION_SHORTPAUSE_SURVIVE;
		end
	end
	return MODIFIERACTION_NOPAUSE_SURVIVE;
end
	
--------------------------------------------------------------------------------

function modifier_axe_berserker_selfheal:GetPriority()
	
	return 5;
end

--------------------------------------------------------------------------------

function modifier_axe_berserker_selfheal:EndOfTurnAction()
	
	if self.damage>0 then 
		local selfheal = math.min(self.maxlife, self.damage)/2
		--self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_regenerate",{
		--	turns = 2,
		--	health = selfheal,
		--	initial=selfheal
		--})
		Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=selfheal})
	elseif self:GetAbility():GetLevel() == 2 then
			Alert_CooldownRefresh(self:GetCaster(),self:GetAbility(),-self:GetAbility():GetSpecialValueFor("bonus_refreshing"))	
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
