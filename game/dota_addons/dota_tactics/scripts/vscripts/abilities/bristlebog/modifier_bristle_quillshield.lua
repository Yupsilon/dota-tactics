modifier_bristle_quillshield = class({})

--------------------------------------------------------------------------------

function modifier_bristle_quillshield:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function modifier_bristle_quillshield:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_bristle_quillshield:OnCreated( kv )

	self.activateSwitch = 1;
	self.hit_targets =  {}
	

	if IsServer() then
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_bristleback/bristleback_quill_spray_hit.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		self:AddParticle( nFXIndex, false, false, -1, false, false )	
	end
end

--------------------------------------------------------------------------------

function modifier_bristle_quillshield:GetStatusEffectName()
	return "particles/status_fx/status_effect_gods_strength.vpcf"
end

--------------------------------------------------------------------------------

function modifier_bristle_quillshield:DeclareFunctions()
	local funcs = {
		MODIFIER_EVENT_ON_TAKEDAMAGE ,
	}	

	return funcs
end

--------------------------------------------------------------------------------

function modifier_bristle_quillshield:OnTakeDamage ( params )
		if IsServer() then
				local hAttacker = params.attacker
				local hVictim = params.unit

			if hVictim == self:GetParent() then
										
				if params.damage_type~=DAMAGE_TYPE_PURE then					
				
					  for _, value in pairs(self.hit_targets) do
						if value == hAttacker then
						  return
						end
					  end
						
					local damage_table = {
						victim = hAttacker,
						attacker = self:GetCaster(),
						damage = cleave_damage,
						damage_type = DAMAGE_TYPE_PURE,
						cover_reduction = 1,
					}										
					Alert_Damage( damage_table )		
					
					if self:GetAbility():GetLevel() == 4 then
						
						if hAttacker:HasModifier("modifier_bristle_thorns") then
							hAttacker:FindModifierByNameAndCaster("modifier_bristle_thorns", self:GetCaster()):Activate()
						else
							hAttacker:AddNewModifier(self:GetCaster(),self:GetCaster():GetAbilityByIndex(0),"modifier_bristle_thorns",{turns=self:GetCaster():GetAbilityByIndex(0):GetSpecialValueFor("quills_turns")})											
						end
					end
										
					self.activateSwitch=2;					
					table.insert(self.hit_targets ,hAttacker)
				end
			end
		end
end
	
--------------------------------------------------------------------------------

function modifier_bristle_quillshield:GetPriority()
	
	return 5;
end

--------------------------------------------------------------------------------

function modifier_bristle_quillshield:EndOfTurnAction()
	
	
	if self:GetAbility():GetLevel() == 2 and self.activateSwitch<2 then
			self:GetAbility().LastCastTurn=self:GetAbility():GetLastCastTurn()-self:GetAbility():GetSpecialValueFor("bonus_refreshing")	
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
