modifier_bristle_thorns = class({})
-----------------------------------------------------------------------------

function modifier_bristle_thorns:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end


--------------------------------------------------------------------------------

function modifier_bristle_thorns:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_bristle_thorns:OnCreated( kv )

	local turnsDuration = kv.turns or 2
	self:SetStackCount(turnsDuration)

	if IsServer() then
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_bristleback/bristleback_quill_spray_hit.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		self:AddParticle( nFXIndex, false, false, -1, false, false )	
	end
end

--------------------------------------------------------------------------------

function modifier_bristle_thorns:Activate(  )

		local damage_table = {
			victim = self:GetParent(),
			attacker = self:GetCaster(),
			damage = self:GetAbility():GetSpecialValueFor("quill_damage"),
			damage_type = DAMAGE_TYPE_PURE,
			ability = self:GetAbility()
		}
		if self:GetAbility():GetLevel()==4 then
			damage_table.damage = damage_table.damage + self:GetAbility():GetSpecialValueFor("bonus_quill_damage")
		end
		Alert_Damage( damage_table )
		self:SetStackCount(self:GetStackCount()+1)
end
	
--------------------------------------------------------------------------------

function modifier_bristle_thorns:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end