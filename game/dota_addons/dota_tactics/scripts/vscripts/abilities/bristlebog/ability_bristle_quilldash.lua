ability_bristle_quilldash = class({})
LinkLuaModifier( "modifier_bristle_quilldash","abilities/bristlebog/modifier_bristle_quilldash.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_dashed_this_turn","abilities/generic/generic_modifier_dashed_this_turn.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_snare","abilities/generic/generic_modifier_snare.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_bristle_quilldash:DashAction(keys)

	if keys.target_point_A~=nil then
				
			EmitSoundOn( "Hero_Axe.JungleWeapon.Dunk", self:GetCaster() )
			local dash_range = self:GetSpecialValueFor("dash_range")
			local projectile_radius = self:GetSpecialValueFor("axe_butt")
	
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_dashed_this_turn",{turns = 1})
			
			local startPoint = keys.caster:GetAbsOrigin() + Vector(0,0,50);
			local endPoint = startPoint			
			local direction = (keys.target_point_A-startPoint)
			direction.z=0
			
			local animationTime = 1;
			Timers:CreateTimer(0.1, function()
				local hitunit=false
				local projectile = {
				  --EffectName = projectile_model,	
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint+direction:Normalized()*projectile_radius/2,--{unit=hero, attach="attach_attack1", offset=Vector(0,0,0)},
				  fDistance = direction:Length2D(),
				  fStartRadius = projectile_radius,
				  fEndRadius = projectile_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  --fExpireTime = 8.0,
				  vVelocity = direction:Normalized() * 100, 
				  UnitBehavior = PROJECTILES_DESTROY,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  --bCutTrees = true,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_DESTROY,
				  GroundBehavior = PROJECTILES_DESTROY,
				  fGroundOffset = 0,
				  --nChangeMax = 1,
				  --bRecreateOnChange = true,
				  bZCheck = false,
				  bGroundLock = false,
				  --bProvidesVision = true,
				  --iVisionRadius = 350,
				  --iVisionTeamNumber = keys.caster:GetTeam(),
				  --bFlyingVision = false,
				  --fVisionTickTime = .1,
				  --fVisionLingerDuration = 1,
				  draw = false,--             draw = {alpha=1, color=Vector(200,0,0)},
				  --iPositionCP = 0,
				  --iVelocityCP = 1,
				  --ControlPoints = {[5]=Vector(100,0,0), [10]=Vector(0,0,1)},
				  --ControlPointForwards = {[4]=hero:GetForwardVector() * -1},
				  --ControlPointOrientations = {[1]={hero:GetForwardVector() * -1, hero:GetForwardVector() * -1, hero:GetForwardVector() * -1}},
				  --[[ControlPointEntityAttaches = {[0]={
					unit = hero,
					pattach = PATTACH_ABSORIGIN_FOLLOW,
					attachPoint = "attach_attack1", -- nil
					origin = Vector(0,0,0)
				  }},]]
				  --fRehitDelay = .3,
				  --fChangeDelay = 1,
				  --fRadiusStep = 10,
				  bUseFindUnitsInRadius = true,
				  bHitScan=true,

				   OnFinish = function(instance, pos) 
					
							self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_bristle_quilldash",{dash_destination_x=instance:GetPosition().x,dash_destination_y=instance:GetPosition().y})
						
				  end,
				}
				
				keys.caster:FaceTowards(keys.target_point_A)
						
				Projectiles:CreateProjectile(projectile)
			end);
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_bristle_quilldash:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_bristle_quilldash:disablesMovement()
	return false;
end

--------------------------------------------------------------------------------

function ability_bristle_quilldash:ChangesCasterPosition()
	return 1;
end

--------------------------------------------------------------------------------

function ability_bristle_quilldash:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_bristle_quilldash:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_bristle_quilldash:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_bristle_quilldash:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_bristle_quilldash:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("axe_butt")
end

--------------------------------------------------------------------------------

function ability_bristle_quilldash:GetMinRange(coneID)	

	return 0
end

--------------------------------------------------------------------------------

function ability_bristle_quilldash:GetMaxRange(coneID)
	if self:GetLevel()==3 then
		return self:GetSpecialValueFor("dash_range")+self:GetSpecialValueFor("bonus_dash_range")
	end
	return self:GetSpecialValueFor("dash_range")
end

--------------------------------------------------------------------------------

function ability_bristle_quilldash:GetCastRange()	
	return self:GetMaxRange(0)
end


--------------------------------------------------------------------------------

function ability_bristle_quilldash:GetAOERadius(coneID)	
	return self:GetAbility():GetSpecialValueFor("helix_aoe")
end

--------------------------------------------------------------------------------