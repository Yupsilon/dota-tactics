ability_bristle_quillshield = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_bristle_quillshield","abilities/bristlebog/modifier_bristle_quillshield.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_regenerate","abilities/generic/generic_modifier_regenerate.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_bristle_quillshield:PrepAction(keys)

			EmitSoundOn( "Hero_Omniknight.Repel", self:GetCaster() )
	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;			
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			
	local animationTime = 1.25;			
	StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_1})
	
	local shieldValue = self:GetSpecialValueFor("base_shield")
	if self:GetLevel() == 3  then
		shieldValue = shieldValue + self:GetSpecialValueFor("bonus_selfshield")
	end
	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_shield",{life=  shieldValue, turns = self:GetSpecialValueFor("modifier_duration")})		
	local quillmodifier = self:GetCaster():AddNewModifier( self:GetCaster(), self, "modifier_bristle_quillshield", {turns = self:GetSpecialValueFor("modifier_duration")} )

						
						--local nFXIndex = ParticleManager:CreateParticle( "particles/econ/items/omniknight/omni_ti8_head/omniknight_repel_buff_ti8.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )
						
							--ParticleManager:SetParticleControlEnt(nFXIndex, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_origin", self:GetCaster():GetAbsOrigin(), true)
							--mhaste:AddParticle( nFXIndex, false, false, -1, false, false )	
	
	
	
	Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("self_energy"), energy=true} )
	return MODIFIERACTION_SHORTPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function ability_bristle_quillshield:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_bristle_quillshield:GetPriority()
	return 1;
end

--------------------------------------------------------------------------------

function ability_bristle_quillshield:GetMaxRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_bristle_quillshield:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function ability_bristle_quillshield:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_bristle_quillshield:GetHasteBonus()		
	return (100+(self:GetSpecialValueFor("modifier_speed")))/100
end

--------------------------------------------------------------------------------

function ability_bristle_quillshield:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end


--------------------------------------------------------------------------------