ability_bristle_quillburst = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_bristle_thorns","abilities/bristlebog/modifier_bristle_thorns.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_bristle_quillburst:GetIntrinsicModifierName()
	return "modifier_energy_regeneration"
end

--------------------------------------------------------------------------------

function ability_bristle_quillburst:GetDamageConversion()
	if self:GetLevel()==3 then
		return self:GetSpecialValueFor("bonus_energy_absorbtion")
	end
	return 0
end

--------------------------------------------------------------------------------

function ability_bristle_quillburst:GetRegenerationValue()
	return self:GetSpecialValueFor("base_energy_regen")
end

--------------------------------------------------------------------------------

function ability_bristle_quillburst:SolveAction(keys)
		
				
				
			local cleave_range = self:GetSpecialValueFor("burst_range")
			local cleave_damage = self:GetSpecialValueFor("burst_damage")
				
			local startPoint = keys.caster:GetAbsOrigin()
			--local direction = (keys.target_point_A-startPoint):Normalized()
			--direction.z=0
			--local centerangle = math.atan2(direction.x,direction.y)
	
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			--self:UseResources(false,false,true)	
			--self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			
			--local selfshield = self:GetSpecialValueFor("quake_selfshield")
								
			local animationTime = 1;
						   	
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_2})	
				EmitSoundOn( "Hero_EarthShaker.Woosh", self:GetCaster() )
			
			Timers:CreateTimer(animationTime*1/2, function() 
			
								
				EmitSoundOn( "Hero_EarthShaker.Totem", self:GetCaster() )
			
			local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_bristleback/bristleback_quill_spray.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )				
			ParticleManager:SetParticleControl( nFXIndex, 0, startPoint )			
			ParticleManager:SetParticleControl( nFXIndex, 1, Vector(cleave_range,cleave_range,cleave_range) )
			ParticleManager:ReleaseParticleIndex( nFXIndex )	

				local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), startPoint, self:GetCaster(), cleave_range, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
				if #enemies > 0 then
					for _,enemy in pairs(enemies) do
						if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
					
							local enemy_direction = (enemy:GetAbsOrigin()-startPoint)
								local projectile = {
								EffectAttach = PATTACH_WORLDORIGIN,			  			  
								vSpawnOrigin = startPoint+Vector(0,0,30),
								fDistance = cleave_range,
								fStartRadius = 50,
								fEndRadius = 50,
								fCollisionRadius = 30,
								  Source = self:GetCaster(),
								  vVelocity = enemy_direction:Normalized() * cleave_range*10, 
								  UnitBehavior = PROJECTILES_NOTHING,
								  bMultipleHits = true,
								  bIgnoreSource = true,
								  TreeBehavior = PROJECTILES_NOTHING,
								  bTreeFullCollision = false,
								  WallBehavior = PROJECTILES_DESTROY,
								  GroundBehavior = PROJECTILES_DESTROY,
								  fGroundOffset = 0,
								  bZCheck = false,
								  bGroundLock = false,
								  draw = false,
								  bUseFindUnitsInRadius = true,
									bHitScan=true,

								  UnitTest = function(instance, unit) return unit==enemy end,
								  OnUnitHit = function(instance, unit) 
								  
										local hit_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_bristleback/bristleback_quill_spray_impact.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster()									
										ParticleManager:SetParticleControl(hit_particle, 0, unit:GetAbsOrigin())
										ParticleManager:ReleaseParticleIndex(hit_particle)
														
										local damage_table = {
											victim = enemy,
											attacker = self:GetCaster(),
											damage = cleave_damage,
											damage_type = DAMAGE_TYPE_MAGICAL,
											cover_reduction = 1,
										}
										
										if IsTargetOverCover(instance,damage_table.attacker) then
											damage_table.cover_reduction=1/2
										end

										Alert_Damage( damage_table )										
										Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("quake_energy"), energy=true} );
									
										if enemy:HasModifier("modifier_bristle_thorns") then
											enemy:FindModifierByNameAndCaster("modifier_bristle_thorns", self:GetCaster()):Activate()
										else
											enemy:AddNewModifier(self:GetCaster(),self,"modifier_bristle_thorns",{turns=self:GetSpecialValueFor("quills_turns")})										
										end
										
										if self:GetLevel()==2 then
											Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=self:GetSpecialValueFor("bonus_selfheal")})
										end
								  end,
								}						
								Projectiles:CreateProjectile(projectile)						
					
						end
					end
				end
			end)
			
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_bristle_quillburst:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_bristle_quillburst:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_bristle_quillburst:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_bristle_quillburst:GetCastPoints()
	return 0;
end

--------------------------------------------------------------------------------

function ability_bristle_quillburst:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_bristle_quillburst:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_bristle_quillburst:GetConeArc(coneID,coneRange)	
	return 1
end

--------------------------------------------------------------------------------

function ability_bristle_quillburst:GetMinRange(coneID)	
	return self:GetSpecialValueFor("burst_range")
end

--------------------------------------------------------------------------------

function ability_bristle_quillburst:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("burst_range")
end

--------------------------------------------------------------------------------

function ability_bristle_quillburst:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------