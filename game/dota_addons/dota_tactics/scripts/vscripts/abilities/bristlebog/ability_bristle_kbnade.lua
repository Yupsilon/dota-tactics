ability_bristle_kbnade = class({})
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_silence","abilities/generic/generic_modifier_silence.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_reveal","abilities/generic/generic_modifier_reveal.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_bristle_kbnade:SolveAction(keys)

	if keys.target_point_A~=nil then
				
			local projectile_range = self:GetSpecialValueFor("projectile_range")
			local projectile_radius = self:GetSpecialValueFor("projectile_radius") * 2
	
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,false)	
			Alert_Mana({unit=self:GetCaster(),manavalue=0-self:GetManaCost(self:GetLevel());} )
			--self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			
			local startPoint = keys.caster:GetAbsOrigin() + Vector(0,0,50)*keys.caster:GetModelScale();	
			local direction = (keys.target_point_A-startPoint)
			direction.z=0		
			startPoint=startPoint+direction:Normalized()*projectile_radius/2
			
			local animationTime = 1;
			local decalSpeed = 1800
									   
				keys.caster:FaceTowards(keys.target_point_A)
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_VICTORY})	
			
			if self:GetLevel() == 2 then
				self:GetParent():AddNewModifier(self:GetParent(),self,"generic_modifier_unstoppable",{turns = self:GetSpecialValueFor("bonus_unstoppable")})		
			end
						   
			Timers:CreateTimer(animationTime*3/4, function() 
			
				StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_ATTACK})	
				EmitSoundOn( "Hero_DrowRanger.Attack", self:GetCaster() )

				self.nChainParticleFXIndex = ParticleManager:CreateParticle( "particles/neutral_fx/mud_golem_hurl_boulder.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster() )		
					ParticleManager:SetParticleAlwaysSimulate( self.nChainParticleFXIndex )
					ParticleManager:SetParticleControlEnt( self.nChainParticleFXIndex, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_attack1",  startPoint, true )
					ParticleManager:SetParticleControl( self.nChainParticleFXIndex, 1, startPoint )
					ParticleManager:SetParticleControl( self.nChainParticleFXIndex, 2, Vector( decalSpeed, 9999, 0 ) )-- self.fireball_speed, self.fireball_distance, self.fireball_width
					--ParticleManager:SetParticleControl( self.nChainParticleFXIndex, 3, Vector( 1, 0, 0 ) )	--killswitch
					ParticleManager:SetParticleControl( self.nChainParticleFXIndex, 4, Vector( 1, 0, 0 ) )
					ParticleManager:SetParticleControl( self.nChainParticleFXIndex, 5, Vector( 0, 0, 0 ) )
					ParticleManager:SetParticleControlEnt( self.nChainParticleFXIndex, 7, self:GetCaster(), PATTACH_CUSTOMORIGIN, nil, self:GetCaster():GetOrigin(), true )	
					
	
				local projectile = {
				  --EffectName = projectile_model,	
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint,--{unit=hero, attach="attach_attack1", offset=Vector(0,0,0)},
				  fDistance = direction:Length2D(),
				  fStartRadius = projectile_radius,
				  fEndRadius = projectile_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  --fExpireTime = 8.0,
				  vVelocity = direction:Normalized() * decalSpeed, 
				  UnitBehavior = PROJECTILES_DESTROY,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  --bCutTrees = true,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_DESTROY,
				  GroundBehavior = PROJECTILES_DESTROY,
				  fGroundOffset = 0,
				  --nChangeMax = 1,
				  --bRecreateOnChange = true,
				  bZCheck = false,
				  bGroundLock = false,
				  --bProvidesVision = true,
				  --iVisionRadius = 350,
				  --iVisionTeamNumber = keys.caster:GetTeam(),
				  --bFlyingVision = false,
				  --fVisionTickTime = .1,
				  --fVisionLingerDuration = 1,
				  draw = false,--             draw = {alpha=1, color=Vector(200,0,0)},
				  --iPositionCP = 0,
				  --iVelocityCP = 1,
				  --ControlPoints = {[5]=Vector(100,0,0), [10]=Vector(0,0,1)},
				  --ControlPointForwards = {[4]=hero:GetForwardVector() * -1},
				  --ControlPointOrientations = {[1]={hero:GetForwardVector() * -1, hero:GetForwardVector() * -1, hero:GetForwardVector() * -1}},
				  --[[ControlPointEntityAttaches = {[0]={
					unit = hero,
					pattach = PATTACH_ABSORIGIN_FOLLOW,
					attachPoint = "attach_attack1", -- nil
					origin = Vector(0,0,0)
				  }},]]
				  --fRehitDelay = .3,
				  --fChangeDelay = 1,
				  --fRadiusStep = 10,
				  bUseFindUnitsInRadius = true,
									  bHitScan = true,

				  UnitTest = function(instance, unit) return unit:IsInvulnerable() == false and unit:GetTeamNumber() ~= keys.caster:GetTeamNumber() end,
				  OnUnitHit = function(instance, unit) 
					
						self:OnProjectileHit(unit,instance:GetPosition())
						EmitSoundOnLocationWithCaster(instance:GetPosition(), "Hero_DeathProphet.Silence", self:GetCaster() )
					
				  end,
				  OnIntervalThink = function(instance)
						ParticleManager:SetParticleControl( self.nChainParticleFXIndex, 1, instance:GetPosition() )
				  end,
				  OnFinish = function(instance, pos)		  
						ParticleManager:DestroyParticle( self.nChainParticleFXIndex, false )
						ParticleManager:ReleaseParticleIndex( self.nChainParticleFXIndex )
				  end
				  --OnTreeHit = function(self, tree) ... end,
				  --OnWallHit = function(self, gnvPos) ... end,
				  --OnGroundHit = function(self, groundPos) ... end,
				  --OnFinish = function(self, pos) ... end,
				}						
				Projectiles:CreateProjectile(projectile)
			end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_bristle_kbnade:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_bristle_kbnade:OnProjectileHit(hTarget, vLocation)
					
					local dealt_damage = self:GetSpecialValueFor("projectile_damage") or 1
					local projectile_aoe = self:GetSpecialValueFor("projectile_aoe") or 1
					local silence_duration = self:GetSpecialValueFor("silence_duration") or 2
					
						
		local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), vLocation, hTarget, projectile_aoe, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
		if #enemies > 0 then
			for _,enemy in pairs(enemies) do
				if enemy ~= nil and ( not enemy:IsMagicImmune() ) and ( not enemy:IsInvulnerable() )  then

					local direction = (enemy:GetAbsOrigin()-vLocation):Normalized()
					direction.z=0
					
					local damage = dealt_damage
					if (enemy:GetAbsOrigin()-vLocation):Length2D() < self:GetSpecialValueFor("bonus_damage_aoe") then
						damage=damage+self:GetSpecialValueFor("bonus_damage") 
					end
				
					local projectile = {
					  EffectAttach = PATTACH_WORLDORIGIN,			  			  
					  vSpawnOrigin = vLocation+Vector(0,0,30),
					  fDistance = projectile_aoe,
					  fStartRadius = 50,
					  fEndRadius = 50,
					  fCollisionRadius = 30,
					  Source = self:GetCaster(),
					  vVelocity = direction * projectile_aoe*10, 
					  UnitBehavior = PROJECTILES_NOTHING,
					  bMultipleHits = true,
					  bIgnoreSource = true,
					  TreeBehavior = PROJECTILES_NOTHING,
					  bTreeFullCollision = false,
					  WallBehavior = PROJECTILES_DESTROY,
					  GroundBehavior = PROJECTILES_DESTROY,
					  fGroundOffset = 0,
					  bZCheck = false,
					  bGroundLock = false,
					  draw = false,
					  bUseFindUnitsInRadius = true,

					  UnitTest = function(instance, unit) return unit==enemy end,
					  OnUnitHit = function(instance, unit) 
					  			
											
							local damage_table = {
								victim = enemy,
								attacker = self:GetCaster(),
								damage = dealt_damage,
								damage_type = DAMAGE_TYPE_MAGICAL,
								ability=self,
								cover_reduction = 1
							}
					
							if IsTargetOverCover(instance,damage_table.attacker) then
								damage_table.cover_reduction=1/2
							end

							Alert_Damage( damage_table )
							
							if enemy:HasModifier("modifier_bristle_thorns") then
								enemy:FindModifierByNameAndCaster("modifier_bristle_thorns", self:GetCaster()):Activate()
							else
								enemy:AddNewModifier(self:GetCaster(),self:GetCaster():GetAbilityByIndex(0),"modifier_bristle_thorns",{turns=self:GetCaster():GetAbilityByIndex(0):GetSpecialValueFor("quills_turns")})											
							end
						
							if self:GetLevel() == 3 then
								local kbDirection = direction:Normalized()*self:GetSpecialValueFor("bonus_knockback")
													
								enemy:AddNewModifier(self:GetCaster(),nil,"modifier_order_knockback",{
									direction_x = kbDirection.x,
									direction_y = kbDirection.y		
								})
							else
								if self:GetLevel() == 3 then
									Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=self:GetSpecialValueFor("bonus_selfheal")})				
								end
							
								enemy:AddNewModifier(self:GetCaster(),self,"generic_modifier_snare",{								
									turns = self:GetSpecialValueFor("snare_duration")						
								})
							end
						
					  end,
					}						
					Projectiles:CreateProjectile(projectile)
				
				end
			end
		end
										
				local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_death_prophet/death_prophet_silence.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )

				ParticleManager:SetParticleControl( nFXIndex, 0, vLocation )
				ParticleManager:SetParticleControl( nFXIndex, 1, Vector (projectile_aoe,0,0) )
				ParticleManager:ReleaseParticleIndex( nFXIndex )	
end

--------------------------------------------------------------------------------

function ability_bristle_kbnade:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_bristle_kbnade:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_bristle_kbnade:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_bristle_kbnade:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_bristle_kbnade:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_bristle_kbnade:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	
end

--------------------------------------------------------------------------------

function ability_bristle_kbnade:GetMinRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_bristle_kbnade:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_bristle_kbnade:GetAOERadius()
	return self:GetSpecialValueFor( "projectile_aoe" )
end

--------------------------------------------------------------------------------

function ability_bristle_kbnade:GetCastRange()
	return self:GetMaxRange( 0 )
end

--------------------------------------------------------------------------------