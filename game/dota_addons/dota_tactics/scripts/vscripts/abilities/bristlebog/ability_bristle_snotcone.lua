ability_bristle_snotcone = class({})
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_weak","abilities/generic/generic_modifier_weak.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_stun","abilities/generic/generic_modifier_stun.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_bristle_snotcone:PrepAction(keys)

	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;			
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})	
	
	if keys.target_point_A~=nil then self:CastInstance(keys.target_point_A) end	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_bristle_snotcone:CastInstance(point)

			local cleave_range = self:GetSpecialValueFor("cleave_range")
			local cleave_angle = self:GetSpecialValueFor("cleave_angle")
				
			local startPoint = self:GetCaster():GetAbsOrigin()
			local direction = (point-startPoint):Normalized()
			direction.z=0
			local centerangle = math.atan2(direction.x,direction.y)
					
			local animationTime = 1;
						   
				self:GetCaster():FaceTowards(point)			
			StartAnimation(self:GetCaster(), {duration=animationTime, rate=1, activity=ACT_DOTA_CAST_ABILITY_1})	
								
				EmitSoundOn( "Hero_Omniknight.PreAttack", self:GetCaster() )
			Timers:CreateTimer(animationTime*3/5, function() 
			
			EmitSoundOn( "Hero_Omniknight.Attack", self:GetCaster() )
			
				
						
				if self:GetLevel() == 2 then
					local haste_modifier = self:GetParent():AddNewModifier(self:GetParent(),self,"generic_modifier_unstoppable",{turns = self:GetAbility():GetSpecialValueFor("bonus_unstoppable")})		
				end
			
				local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_mars/mars_shield_bash.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )

				
				ParticleManager:SetParticleControl( nFXIndex, 0, startPoint )
				ParticleManager:SetParticleControlForward( nFXIndex, 0, (point-startPoint):Normalized());
				ParticleManager:SetParticleControl( nFXIndex, 1, Vector(1,1,1)*cleave_range )
				ParticleManager:SetParticleControl( nFXIndex, 60, Vector(240,20,20) )
				ParticleManager:SetParticleControl( nFXIndex, 61, Vector(1,0,0) )
				ParticleManager:ReleaseParticleIndex( nFXIndex )

				local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), startPoint, self:GetCaster(), cleave_range, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
				if #enemies > 0 then
					for _,enemy in pairs(enemies) do
						if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
					
							local enemy_direction = (enemy:GetAbsOrigin()-startPoint)
							enemy_direction.z=0				
							local enemy_deltaangle = math.atan2(enemy_direction.x,enemy_direction.y)
							local enemy_angle = math.abs(AngleDiff(centerangle*180/math.pi,enemy_deltaangle*180/math.pi))
							
							if enemy_angle <= cleave_angle/2 then					
							
							
								local projectile = {
								EffectAttach = PATTACH_WORLDORIGIN,			  			  
								vSpawnOrigin = startPoint+Vector(0,0,30),
								fDistance = cleave_range*2,
								fStartRadius = 50,
								fEndRadius = 50,
								fCollisionRadius = 30,
								  Source = self:GetCaster(),
								  vVelocity = enemy_direction:Normalized() * cleave_range, 
								  UnitBehavior = PROJECTILES_NOTHING,
								  bMultipleHits = true,
								  bIgnoreSource = true,
								  TreeBehavior = PROJECTILES_NOTHING,
								  bTreeFullCollision = false,
								  WallBehavior = PROJECTILES_DESTROY,
								  GroundBehavior = PROJECTILES_DESTROY,
								  fGroundOffset = 0,
								  bZCheck = false,
								  bGroundLock = false,
								  draw = false,
								  bUseFindUnitsInRadius = true,
								  bHitScan=true,
								  OnFinish = function(instance, pos) ParticleManager:ReleaseParticleIndex( nFXIndex ) end,
								  UnitTest = function(instance, unit) return unit==enemy end,
								  OnUnitHit = function(instance, unit) 
												
									
										local slowModifier = enemy:AddNewModifier( self:GetCaster(), self, "generic_modifier_movespeed_slow", {
											turns = duration,
											move_slow = self:GetSpecialValueFor("ice_slow")
										} )			
										if self:GetAbility():GetLevel()==3 then
											enemy:AddNewModifier( self:GetCaster(), self:GetAbility(), "generic_modifier_weak", {value = self:GetAbility():GetSpecialValueFor("bonus_weakness"),turns = self:GetAbility():GetSpecialValueFor("bonus_weaken_duration")} )					
										end

										Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("cleave_energy"), energy=true});
								  end,
								}						
								Projectiles:CreateProjectile(projectile)
							
							end
					
						end
					end
				end
			end)
end

--------------------------------------------------------------------------------

function ability_bristle_snotcone:getAbilityType()
	return ABILITY_TYPE_CONE;
end

--------------------------------------------------------------------------------

function ability_bristle_snotcone:isFreeAction()
	if self:GetLevel() == 2 then return false end
	return true
end

--------------------------------------------------------------------------------

function ability_bristle_snotcone:GetPriority()
	return 1;
end

--------------------------------------------------------------------------------

function ability_bristle_snotcone:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_bristle_snotcone:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_bristle_snotcone:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_bristle_snotcone:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("cleave_angle")*2
end

--------------------------------------------------------------------------------

function ability_bristle_snotcone:GetMinRange(coneID)	
	return self:GetMaxRange(coneID)
end

--------------------------------------------------------------------------------

function ability_bristle_snotcone:GetMaxRange(coneID)	
	if self:GetLevel()==4 then
		return self:GetSpecialValueFor("cleave_range")+self:GetSpecialValueFor("cleave_range")
	end
	return self:GetSpecialValueFor("cleave_range")
end

--------------------------------------------------------------------------------

function ability_bristle_snotcone:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------