modifier_hit = class({})
-----------------------------------------------------------------------------

function modifier_hit:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end


--------------------------------------------------------------------------------

function modifier_hit:IsHidden()
	return true
end


--------------------------------------------------------------------------------

function modifier_hit:SetFirstHitTarget()
	self.fh = true
end

--------------------------------------------------------------------------------

function modifier_hit:IsFirstHitTarget()
	return self.fh or false
end
	
--------------------------------------------------------------------------------

--[[function modifier_hit:PrepAction()
	
	return MODIFIERACTION_NOPAUSE_DESTROY;
end
	
--------------------------------------------------------------------------------

function modifier_hit:DashAction()
	
	return MODIFIERACTION_NOPAUSE_DESTROY;
end
	
--------------------------------------------------------------------------------

function modifier_hit:SolveAction()
	
	return MODIFIERACTION_NOPAUSE_DESTROY;
end
	
--------------------------------------------------------------------------------

function modifier_hit:WalkAction()
	
	return MODIFIERACTION_NOPAUSE_DESTROY;
end]]
	
--------------------------------------------------------------------------------

function modifier_hit:EndOfTurnAction()
	
	return MODIFIERACTION_NOPAUSE_DESTROY;
end