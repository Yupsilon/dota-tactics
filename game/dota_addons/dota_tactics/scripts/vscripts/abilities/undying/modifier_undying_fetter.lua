modifier_undying_fetter = class({})

--------------------------------------------------------------------------------

function modifier_undying_fetter:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_undying_fetter:OnCreated( kv )

	self.dash_start = self:GetParent():GetAbsOrigin()
	self.dash_end = Vector(kv.dash_destination_x,kv.dash_destination_y,0)
	self.dash_speed = self:GetAbility():GetSpecialValueFor("dash_range")/30 
	self.distance_traveled = 0

	if IsServer() then
		AddAnimationTranslate(self:GetParent(), "forcestaff_friendly")
		StartAnimation(self:GetCaster(), {duration=1, activity=ACT_DOTA_FLAIL})
		self.dash_speed = (self.dash_end-self.dash_start):Length2D() / PLAYER_TIME_SIMULTANIOUS * 1/15
		
		local nFXIndex = ParticleManager:CreateParticle( "particles/items_fx/ethereal_blade.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		self:AddParticle( nFXIndex, false, false, -1, false, false )		
	
		self:StartIntervalThink( 1/30 )
		self:OnIntervalThink()
	end
end

--------------------------------------------------------------------------------

function modifier_undying_fetter:GetStatusEffectName()
	return "particles/status_fx/status_effect_wraithking_ghosts.vpcf"
end

--------------------------------------------------------------------------------

function modifier_undying_fetter:OnIntervalThink()
	if IsServer() then

		local vectorDelta=self.dash_end-self.dash_start
		vectorDelta.z=0

		if self.distance_traveled < vectorDelta:Length2D() then
			self:GetParent():SetAbsOrigin(self:GetParent():GetAbsOrigin() + vectorDelta:Normalized() * self.dash_speed)
			self.distance_traveled = self.distance_traveled + (vectorDelta:Normalized() * self.dash_speed):Length2D()
		else
			local animationTime= 1/2;
			FindClearSpaceForUnit(self:GetParent(), self.dash_end, true)
			
			self:Activate ( )
			EndAnimation(self:GetCaster())
			
			RemoveAnimationTranslate(self:GetParent())
			self:Destroy()
		end
	end
end

--------------------------------------------------------------------------------

function modifier_undying_fetter:Activate ( )

		local helix_aoe = self:GetAbility():GetSpecialValueFor("helix_aoe")
		local helix_damage = self:GetAbility():GetSpecialValueFor("helix_damage")
		local caster = self:GetCaster()
		local ability = self:GetAbility()
			
			EmitSoundOn( "Hero_Undying_Golem.Attack", self:GetCaster() )	
			local helix = ParticleManager:CreateParticle("particles/units/heroes/hero_undying/undying_decay.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
			ParticleManager:SetParticleControl(helix, 0, self:GetParent():GetAbsOrigin())		
			ParticleManager:SetParticleControl(helix, 1, Vector(helix_aoe,0,0))		
			ParticleManager:ReleaseParticleIndex(helix)	
							
					local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), self:GetCaster(),helix_aoe , DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
					if #enemies > 0 then
						for _,enemy in pairs(enemies) do						
							if enemy ~= nil and ( not enemy:IsInvulnerable() )then
						
									local direction = enemy:GetAbsOrigin()-self:GetCaster():GetAbsOrigin()
									direction.z=0
									local projectile = {		  			  
									vSpawnOrigin = self:GetParent():GetAbsOrigin()+Vector(0,0,30),
									fDistance = helix_aoe,
									fStartRadius = 50,
									fEndRadius = 50,
									fCollisionRadius = 30,
									  Source = self:GetCaster(),
									  vVelocity = direction:Normalized() * helix_aoe*10, 
									  UnitBehavior = PROJECTILES_NOTHING,
									  bMultipleHits = true,
									  bIgnoreSource = true,
									  TreeBehavior = PROJECTILES_NOTHING,
									  bTreeFullCollision = false,
									  WallBehavior = PROJECTILES_DESTROY,
									  GroundBehavior = PROJECTILES_DESTROY,
									  fGroundOffset = 0,
									  bZCheck = false,
									  bGroundLock = false,
									  draw = false,
									  bUseFindUnitsInRadius = true,
									bHitScan=true,

									  UnitTest = function(instance, unit) return unit==enemy end,
									  OnUnitHit = function(instance, unit) 
														
											
											--if self:GetCaster():FindModifierByName("modifier_undying_giant") == nil then
												local damage_table = {
													victim = unit,
													attacker = caster,
													damage = helix_damage,
													damage_type = DAMAGE_TYPE_MAGICAL,
													ability  = ability,
													cover_reduction = 1,
												}
										
												if IsTargetOverCover(instance,damage_table.attacker) then
													damage_table.cover_reduction = 1/2
												end

												Alert_Damage (damage_table)
												
												--self:GetCaster():FindModifierByName("modifier_undying_giant"):Grow(self:GetAbility():GetSpecialValueFor("helix_energy"))
											--end
											
											Alert_Mana({unit=self:GetCaster(),manavalue=self:GetAbility():GetSpecialValueFor("helix_energy"), energy=true});
										
											if self:GetAbility():GetLevel()==4 then	
											
												enemy:AddNewModifier(caster,ability,"generic_modifier_snare",{turns=self:GetAbility():GetSpecialValueFor("bonus_root_duration")})
												
											else 
												
												local slow_turns = self:GetAbility():GetSpecialValueFor("slow_duration")
												if self:GetCaster():FindModifierByName("modifier_undying_giant") == nil then
													slow_turns = self:GetAbility():GetSpecialValueFor("slow_duration_long")
												end
												
												local slomo = enemy:AddNewModifier(caster,ability,"generic_modifier_movespeed_slow",{value = self:GetAbility():GetSpecialValueFor("slow_strength"),turns=slow_turns,nodraw = 1})
												
												
												if slomo~=nil then
													local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_warlock/warlock_upheaval_debuff.vpcf", PATTACH_ABSORIGIN_FOLLOW, enemy )
													slomo:AddParticle( nFXIndex, false, false, -1, false, false )
												end
											end
											
											local dust = ParticleManager:CreateParticle("particles/units/heroes/hero_undying/undying_zombie_spawn.vpcf", PATTACH_ABSORIGIN_FOLLOW, unit)
											ParticleManager:SetParticleControl(dust, 0, self:GetParent():GetAbsOrigin())		
											ParticleManager:ReleaseParticleIndex(dust)	
									  end,
									}						
									Projectiles:CreateProjectile(projectile)
														
							end
						end
					
					end	


										
				

end

--------------------------------------------------------------------------------

function modifier_undying_fetter:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_OVERRIDE_ANIMATION,
	}

	return funcs
end

--------------------------------------------------------------------------------

function modifier_undying_fetter:GetOverrideAnimation( params )
	return ACT_DOTA_FLAIL
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------