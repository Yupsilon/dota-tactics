ability_undying_lifesap = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_scarred","abilities/generic/generic_modifier_scarred.lua", LUA_MODIFIER_MOTION_NONE )


--------------------------------------------------------------------------------

function ability_undying_lifesap:PrepAction(keys)
			
	local target_unit = self:GetCaster()--EntIndexToHScript(keys.target_unit) or 
	if target_unit~=nil then	

		self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
		self:UseResources(true,false,true)	
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})		
	
		if self:GetCaster():FindModifierByName("modifier_undying_giant") == nil then
			Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("burst_energy"), energy=true});
		else
			self:GetCaster():FindModifierByName("modifier_undying_giant"):Grow(self:GetSpecialValueFor("burst_energy"))
		end
	
		self:OnProjectileHit(target_unit)			
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_undying_lifesap:OnProjectileHit(hTarget)

	if self:GetCaster()~= hTarget then	
		self:GetCaster():FaceTowards(hTarget:GetAbsOrigin())					
	end

	local animationTime = PLAYER_TIME_MODIFIER*2;
	StartAnimation(self:GetCaster(), {duration=PLAYER_TIME_MODIFIER, rate = 1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_TELEPORT})
	EmitSoundOn( "Hero_Undying.SoulRip.Cast", self:GetCaster() )
			
	local total_healing = self:GetSpecialValueFor("base_healing")
	
	if self:GetLevel()==3 then
				
	 total_healing = total_healing + self:GetSpecialValueFor("bonus_base_healing")
	elseif self:GetLevel()==2 then 
		Alert_HealScars({unit = hTarget, value = self:GetSpecialValueFor("bonus_healing_perma")})	
	end
	
	local AoE = self:GetSpecialValueFor("burst_aoe")
	
	--[[if self:GetCaster() == hTarget then
		AoE = AoE*2
	else
		local helix = ParticleManager:CreateParticle("particles/units/heroes/hero_undying/undying_soul_rip_heal.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster())
		ParticleManager:SetParticleControl( helix, 0, self:GetCaster():GetAbsOrigin() )	
		ParticleManager:SetParticleControl(helix, 1, hTarget:GetAbsOrigin())		
		ParticleManager:ReleaseParticleIndex(helix)	
	end]]
	if self:GetCaster():FindModifierByName("modifier_undying_giant") == nil then
		AoE = 9999
	end
	
	EmitSoundOn( "Hero_Undying.SoulRip.Ally", hTarget )	
	
	
	
					local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), self:GetCaster(), AoE, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
					if #enemies > 0 then
						for _,enemy in pairs(enemies) do
							if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
						
								local helix = ParticleManager:CreateParticle("particles/units/heroes/hero_undying/undying_soul_rip_heal.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())
								ParticleManager:SetParticleControl( helix, 0, enemy:GetAbsOrigin() )	
								ParticleManager:SetParticleControl(helix, 1, hTarget:GetAbsOrigin())		
								ParticleManager:ReleaseParticleIndex(helix)	
								EmitSoundOn( "Hero_Undying.SoulRip.Enemy", enemy )
						
								Alert_Damage({
									victim = enemy,
									attacker = self:GetCaster(),
									damage = 0,
									damage_type = DAMAGE_TYPE_PURE,
									ability  = self,
									scar_damage_absolute = self:GetSpecialValueFor("burst_damage"),
									cover_reduction = 1
								})
									
								total_healing = total_healing + self:GetSpecialValueFor("burst_healing")
								if self:GetLevel() == 4 then 
									total_healing = total_healing + self:GetSpecialValueFor("bonus_healing")
								end
							end
						end
					end			
	
	Alert_Heal({caster=self:GetCaster(),target=hTarget,value=total_healing})				
				
end

--------------------------------------------------------------------------------

function ability_undying_lifesap:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_undying_lifesap:GetPriority()
	return 5;
end

--------------------------------------------------------------------------------

function ability_undying_lifesap:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function ability_undying_lifesap:GetCastPoints()

	return 1;
end

--------------------------------------------------------------------------------

function ability_undying_lifesap:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_undying_lifesap:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_undying_lifesap:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_undying_lifesap:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_undying_lifesap:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("burst_aoe")
end

--------------------------------------------------------------------------------

function ability_undying_lifesap:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_undying_lifesap:GetAOERadius()
	return self:GetSpecialValueFor( "burst_aoe" )
end

--------------------------------------------------------------------------------