ability_warlock_summonimp = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "modifier_turn_restricted_creature","mechanics/heroes/modifier_turn_restricted_creature.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_warlock_summonedimp","abilities/warlock/modifier_warlock_summonedimp.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_warlock_demonstatus","abilities/warlock/modifier_warlock_demonstatus.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_summonedunit","abilities/generic/generic_modifier_summonedunit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_snare","abilities/generic/generic_modifier_snare.lua", LUA_MODIFIER_MOTION_NONE )



--------------------------------------------------------------------------------

function ability_warlock_summonimp:SolveAction(keys)

	if keys.target_point_A~=nil then			
		
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
				
			local summon_duration = self:GetSpecialValueFor("summon_duration")
			local arate=.6
				
			local startPoint = keys.caster:GetAbsOrigin()
			local direction = (keys.target_point_A-startPoint):Normalized()
			direction.z=0
			local centerangle = math.atan2(direction.x,direction.y)
	
			self.LastCastTurn = math.max(GameMode:GetCurrentTurn()+math.ceil(self:GetCooldown(self:GetLevel())),self.LastCastTurn or -1);			
			local animationTime = 26/30*PLAYER_TIME_MODIFIER/arate;
						   
			keys.caster:FaceTowards(keys.target_point_A)	
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_ATTACK,rate = arate/PLAYER_TIME_MODIFIER})	
			
			EmitSoundOn( "n_black_dragon.Fireball.Cast", self:GetCaster() )
			Timers:CreateTimer(animationTime*.5, function() 
			
			--EmitSoundOn( "Hero_OgreMagi.Attack", self:GetCaster() )
				
				self:SummonCreature(keys.target_point_A,summon_duration)
	
				
			end)
			
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_warlock_summonimp:SummonCreature(point,duration)

	local main_modifier = self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_warlock_summonedimp",{x=point.x,y=point.y})		   
	main_modifier.AssignedUnit:FaceTowards(main_modifier.AssignedUnit:GetAbsOrigin()+point-self:GetCaster():GetAbsOrigin())
				
	local aiModifier = self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_summonedunit",{turns = duration})
	aiModifier:AssignUnit(main_modifier.AssignedUnit)
				
	main_modifier.AssignedUnit:AddNewModifier(self:GetCaster(),self,"modifier_turn_restricted_creature",{})
	main_modifier.AssignedUnit:AddNewModifier(self:GetCaster(),self,"modifier_warlock_demonstatus",{})
	local particlemodifier = main_modifier.AssignedUnit:AddNewModifier(self:GetCaster(),self,"generic_modifier_snare",{turns=duration+1,nodraw=1})
	main_modifier:Activate()
end

--------------------------------------------------------------------------------

function ability_warlock_summonimp:GetPriority()
	return 3;
end


--------------------------------------------------------------------------------

function ability_warlock_summonimp:getAbilityType()
	return ABILITY_TYPE_POINT_MULTIPLE;
end

--------------------------------------------------------------------------------

function ability_warlock_summonimp:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_warlock_summonimp:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_warlock_summonimp:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_warlock_summonimp:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_warlock_summonimp:GetConeArc(coneID,coneRange)
	return self:GetAOERadius()
end

--------------------------------------------------------------------------------

function ability_warlock_summonimp:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_warlock_summonimp:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("summon_range")
end

--------------------------------------------------------------------------------

function ability_warlock_summonimp:GetAOERadius()	
	return self:GetSpecialValueFor("spergout_aoe")
end

--------------------------------------------------------------------------------

function ability_warlock_summonimp:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------