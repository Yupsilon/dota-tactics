ability_undying_strengthdrain = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_weak","abilities/generic/generic_modifier_weak.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_might","abilities/generic/generic_modifier_might.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_silence","abilities/generic/generic_modifier_silence.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_undying_strengthdrain:PrepAction(keys)

	if keys.target_point_A~=nil then
		
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
						
			local arate = 1
			local animationTime = 29/30*PLAYER_TIME_MODIFIER/arate;		
			keys.caster:FaceTowards(keys.target_point_A)
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_UNDYING_DECAY, rate = arate/PLAYER_TIME_MODIFIER})	
			
			Timers:CreateTimer(animationTime/2,function()
				 
				EmitSoundOn( "Hero_Undying.Decay.Cast", self:GetCaster() )
				local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_undying/undying_decay.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster() )
				ParticleManager:SetParticleControl( nFXIndex, 0, keys.target_point_A )	
				ParticleManager:SetParticleControl( nFXIndex, 1, Vector (self:GetAOERadius(),0,0) )	
				ParticleManager:SetParticleControl( nFXIndex, 2, self:GetCaster():GetAbsOrigin() )		
				ParticleManager:ReleaseParticleIndex(nFXIndex)
			
				if self:GetLevel()==3 then
					
					self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_unstoppable", { turns = self:GetSpecialValueFor("bonus_modifier_duration") } )
				end
				
				local nHitTargets = 0						   
				local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), keys.target_point_A, self:GetCaster(), self:GetAOERadius(), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
				
				if #enemies > 0 then
					for _,enemy in pairs(enemies) do
						if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
							nHitTargets=nHitTargets+1
							enemy:AddNewModifier(self:GetCaster(),self,"generic_modifier_weak",{value = self:GetSpecialValueFor("weaken_str"),turns = self:GetSpecialValueFor("weaken_duration")})
							EmitSoundOn( "Hero_Undying.Decay.Transfer", enemy )
							
							if self:GetCaster():FindModifierByName("modifier_undying_giant") == nil then
								Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("burst_energy"), energy=true});
							else
								self:GetCaster():FindModifierByName("modifier_undying_giant"):Grow(self:GetSpecialValueFor("burst_energy"))
							end
							
							if self:GetLevel()==4 and #enemies == 1 then
								enemy:AddNewModifier( self:GetCaster(), self, "generic_modifier_silence", { turns = self:GetSpecialValueFor("bonus_modifier_duration") } )
							end
						end
					end
				end		
						
				if nHitTargets>0 then
					local might_mod = self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_might",{value = self:GetSpecialValueFor("might_str")*nHitTargets,turns = self:GetSpecialValueFor("might_duration"),nodraw = 1})										
					local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_undying/undying_decay_strength_buff.vpcf", PATTACH_ABSORIGIN_FOLLOW, hTarget )
					might_mod:AddParticle( nFXIndex, false, false, -1, false, false )
				end
			end)	
	end
	return ABILITYACTION_COMPLETE_SHORT;
end

--------------------------------------------------------------------------------

function ability_undying_strengthdrain:GetPriority()
	return 4;
end

--------------------------------------------------------------------------------

function ability_undying_strengthdrain:GetAOERadius()
	if self:GetLevel()==2 then
		return self:GetSpecialValueFor( "hunger_aoe" )+self:GetSpecialValueFor( "bonus_aoe")
	end
	return self:GetSpecialValueFor( "hunger_aoe" )
end

--------------------------------------------------------------------------------

function ability_undying_strengthdrain:getAbilityType()
	return ABILITY_TYPE_POINT_MULTIPLE;
end

--------------------------------------------------------------------------------

function ability_undying_strengthdrain:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_undying_strengthdrain:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_undying_strengthdrain:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_undying_strengthdrain:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_undying_strengthdrain:GetConeArc(coneID,coneRange)
	return 100
end

--------------------------------------------------------------------------------

function ability_undying_strengthdrain:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_undying_strengthdrain:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("hunger_range")
end

--------------------------------------------------------------------------------

function ability_undying_strengthdrain:GetCastRange()	
	return self:GetSpecialValueFor("hunger_range")
end	

--------------------------------------------------------------------------------