modifier_undying_giant = class({})

-----------------------------------------------------------------------------

function modifier_undying_giant:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function modifier_undying_giant:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function modifier_undying_giant:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_undying_giant:OnCreated( kv )

	self.turns=0	
	if IsServer() then
			
		self.linked_modifiers = {}
		self:SetStackCount(kv.turns or 0)
		StartAnimation(self:GetCaster(), {duration=PLAYER_TIME_MODIFIER*3, rate = 1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_UNDYING_TOMBSTONE})		
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_undying/undying_fg_transform.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )	
		ParticleManager:ReleaseParticleIndex(nFXIndex)							
		EmitSoundOn( "Hero_Undying.FleshGolem.Cast", self:GetParent() )			
	
		self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_shield", {
			value = self:GetAbility():GetSpecialValueFor( "initial_shield" ),
			turns = 1
		} )	
		Alert_Mana({unit=self:GetCaster(),manavalue=-self:GetCaster():GetMaxMana()} )
	end
end
	
--------------------------------------------------------------------------------

--[[function modifier_undying_giant:Grow(growth)

	if not  IsServer() then return end
	local real_value = growth * self:GetAbility():GetSpecialValueFor( "percent_conversion" )/100 * GetIncomingEnergizedPercentage(self:GetCaster())/100

	self.shield_remaining =  self.shield_remaining + real_value 
			
			Alert_SendOverheadEventMessage{
				unit = self:GetParent(),
				value = real_value,
				message = ALERT_SHIELD_ADD
			}
end]]
	
--------------------------------------------------------------------------------

function modifier_undying_giant:PrepAction()
	
	
	local golem_modifier = self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_shield", {
			value = self:GetAbility():GetSpecialValueFor( "shield_block" ),
			turns = 1
		} )	
		
	return MODIFIERACTION_NOPAUSE_SURVIVE;
end
	
--------------------------------------------------------------------------------

function modifier_undying_giant:EndOfTurnAction()
	
		--[[local decay = self:GetAbility():GetSpecialValueFor("shield_decay")
		
		if decay>0 then
			self.shield_remaining = self.shield_remaining-decay
			Alert_SendOverheadEventMessage{
					unit = self:GetParent(),
					value = decay,
					message = ALERT_SHIELD_DAMAGE
				}
		end]]
			
	if self:GetStackCount()>1 then	
		self.turns=self.turns+1
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
		
	return MODIFIERACTION_NOPAUSE_DESTROY;
end
	
--------------------------------------------------------------------------------

--[[function modifier_undying_giant:OnDestroy()

	if IsServer() then
		if self.shield_remaining>0 then		
			Alert_SendOverheadEventMessage{
				unit = self:GetParent(),
				value = self.shield_remaining,
				message = ALERT_SHIELD_DAMAGE
			}
		end
		
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_undying/undying_fg_transform_reverse.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )	
		ParticleManager:ReleaseParticleIndex(nFXIndex)			
		StartAnimation(self:GetCaster(), {duration=PLAYER_TIME_MODIFIER*3, rate = 1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_UNDYING_TOMBSTONE})
		
		EmitSoundOn( "Hero_Undying.FleshGolem.End", self:GetParent() )	
		
		if self.linked_modifiers ~= nil and #self.linked_modifiers>0 then
			for index, modifier in pairs (self.linked_modifiers) do
				if modifier~=nil then
					modifier:Destroy()
				end
			end
		end
	end
end]]

--------------------------------------------------------------------------------

function modifier_undying_giant:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MODEL_CHANGE
	}	

	return funcs
end

--------------------------------------------------------------------------------

function modifier_undying_giant:OnDestroy()	 -- BURST OPEN

	if IsServer() then
		self:GetAbility().LastCastTurn = GameMode:GetCurrentTurn()+math.ceil(self:GetAbility():GetCooldown(self:GetAbility():GetLevel()));
		self:GetAbility():UseResources(false,false,true)	
		self:GetCaster():AddNewModifier(self:GetCaster(),self:GetAbility(),"modifier_ability_cooldown",{})	
		
		
		local aura_aoe = self:GetAbility():GetSpecialValueFor("aura_aoe")			
		local aura_center = self:GetParent():GetAbsOrigin()
		
		
		local current_mana = self:GetCaster():GetMana()
		Alert_Mana({unit=self:GetCaster(),manavalue=-current_mana} )
		
		--[[EmitSoundOn( "Hero_Omniknight.GuardianAngel", self:GetParent() )
		
		local helix = ParticleManager:CreateParticle("particles/items_fx/arcane_boots.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
		ParticleManager:SetParticleControl(helix, 0, helix_center)		
		ParticleManager:ReleaseParticleIndex(helix)	]]
				
		local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), aura_center, nil,aura_aoe , DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
					if #enemies > 0 then
						for _,hTarget in pairs(enemies) do
						
							if hTarget ~= nil and ( not hTarget:IsInvulnerable() )  then
							
								Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=current_mana*self:GetAbility():GetSpecialValueFor("percent_conversion")/100})
								
								local helix = ParticleManager:CreateParticle("particles/units/heroes/hero_undying/undying_soul_rip_heal.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster())
								ParticleManager:SetParticleControl( helix, 1, self:GetCaster():GetAbsOrigin() )	
								ParticleManager:SetParticleControl(helix, 0, hTarget:GetAbsOrigin())		
								ParticleManager:ReleaseParticleIndex(helix)	
								
								if self:GetAbility():GetLevel()==2 then
									Alert_HealScars({unit = self:GetCaster(), value = self:GetAbility():GetSpecialValueFor("bonus_healing_perma")})	
								end
							end
						end
					end							
		
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_undying/undying_fg_transform_reverse.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )	
		ParticleManager:ReleaseParticleIndex(nFXIndex)			
		StartAnimation(self:GetCaster(), {duration=PLAYER_TIME_MODIFIER*3, rate = 1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_UNDYING_TOMBSTONE})
		
		EmitSoundOn( "Hero_Undying.FleshGolem.End", self:GetParent() )	
		
		if self.linked_modifiers ~= nil and #self.linked_modifiers>0 then
			for index, modifier in pairs (self.linked_modifiers) do
				if modifier~=nil then
					modifier:Destroy()
				end
			end
		end
	end
end
		
--------------------------------------------------------------------------------

--[[function modifier_undying_giant:GetModifierTotal_ConstantBlock(kv)
	if IsServer() then
		local target 					= self:GetParent()
		local original_shield_amount	= self.shield_remaining
		if kv.damage > 0 then

			self.shield_remaining = self.shield_remaining - kv.damage
			
			if kv.damage < original_shield_amount then
			
				Alert_SendOverheadEventMessage{
					unit = self:GetParent(),
					value = kv.damage,
					message = ALERT_SHIELD_DAMAGE
				}
				return kv.damage
			else			
			
				Alert_SendOverheadEventMessage{
					unit = self:GetParent(),
					value = original_shield_amount,
					message = ALERT_SHIELD_DAMAGE
				}
				--nope self:Destroy()
				return original_shield_amount
			end
		end
	end
end]]
	
--------------------------------------------------------------------------------

function modifier_undying_giant:GetModifierModelChange()
	return "models/heroes/undying/undying_flesh_golem.vmdl"
end

--------------------------------------------------------------------------------