ability_undying_giant = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "modifier_undying_giant","abilities/undying/modifier_undying_giant.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_energized","abilities/generic/generic_modifier_energized.lua", LUA_MODIFIER_MOTION_NONE )
--LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )
--LinkLuaModifier( "generic_modifier_snare","abilities/generic/generic_modifier_snare.lua", LUA_MODIFIER_MOTION_NONE )
--LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
--LinkLuaModifier( "generic_modifier_combobreaker","abilities/generic/generic_modifier_combobreaker.lua", LUA_MODIFIER_MOTION_NONE )
--LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
--LinkLuaModifier( "generic_modifier_scar_immunity","abilities/generic/generic_modifier_scar_immunity.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

--[[function ability_undying_giant:PrepAction(keys)
			
	local animationTime = 1;
	
	
		local shield_remaining =  self:GetSpecialValueFor( "initial_shield" )	
		
		if self:GetLevel() == 3 then
			shield_remaining = shield_remaining + self:GetSpecialValueFor( "bonus_initial_shield" )	
		end
	
	local golem_modifier = self:GetCaster():AddNewModifier( self:GetCaster(), self, "modifier_undying_giant", {
			value = shield_remaining,
			turns = self:GetSpecialValueFor( "modifier_duration" )
		} )	
	table.insert(golem_modifier.linked_modifiers,self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_unstoppable", { turns = 9 } ))
	
	local linked_modifier = nil
	
	if self:GetLevel() == 4 then
		linked_modifier = self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_might", { value = self:GetSpecialValueFor("bonus_might"),turns = 9 } )
	elseif self:GetLevel() == 3 then
		local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), self:GetCaster(), self:GetSpecialValueFor("bonus_root_aoe"), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
			if #enemies > 0 then
				for _,enemy in pairs(enemies) do
					if enemy ~= nil and ( not enemy:IsInvulnerable() )  then
									
						enemy:AddNewModifier( self:GetCaster(), self, "generic_modifier_snare", {
							turns = self:GetSpecialValueFor("bonus_root_duration")
						} )		
						
					end
				end
			end
	elseif self:GetLevel() == 2 then
		Alert_HealScars({unit = self:GetCaster(), value = self:GetSpecialValueFor("bonus_healing_perma")})	
		linked_modifier = self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_scar_immunity", { turns = 9 } )
	end
	
	if linked_modifier~=nil then
		table.insert(golem_modifier.linked_modifiers,linked_modifier)
	end
		
		self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;			
		self:UseResources(false,false,true)	
		Alert_Mana({unit=self:GetCaster(),manavalue=-self:GetCaster():GetMana()} )
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})			
	
	
	return ABILITYACTION_COMPLETE;
end]]

--------------------------------------------------------------------------------

function ability_undying_giant:PrepAction(keys)
			
	local animationTime = 1;
	
	if self:GetCaster():HasModifier("modifier_undying_giant") then
	
		self:GetCaster():RemoveModifierByNameAndCaster("modifier_undying_giant",self:GetCaster())
		
	else
		self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
		--self:UseResources(false,false,false)	
		--self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})			
	
		--EmitSoundOn( "Hero_Dark_Seer.Surge", self:GetCaster() )		
								
		local golem_modifier = self:GetCaster():AddNewModifier( self:GetCaster(), self, "modifier_undying_giant", { turns = self:GetSpecialValueFor( "modifier_duration" )} )	
		--golem_modifier:Activate();
		if self:GetLevel() == 3 then
			for i=0, 5 do
				Alert_CooldownRefresh(self:GetCaster(),i,-self:GetSpecialValueFor("bonus_ability_refresh"))	
			end
		elseif self:GetLevel() == 4 then
			linked_modifier = self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_energized", { value = self:GetSpecialValueFor("bonus_energy_boost"),turns = 9 } )
			table.insert(golem_modifier.linked_modifiers,linked_modifier)
		end
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_undying_giant:GetManaCost()
			
	
	if self:GetCaster():HasModifier("modifier_undying_giant") then
	
		return 0
	end
	
	return 1000;
end

--------------------------------------------------------------------------------

function ability_undying_giant:IsDisabledBySilence()
	
	if self:GetCaster():HasModifier("modifier_undying_giant") then
		return false
	end
	return true
end

--------------------------------------------------------------------------------

function ability_undying_giant:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_undying_giant:GetPriority()
	return 2;
end

--------------------------------------------------------------------------------

function ability_undying_giant:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function ability_undying_giant:GetCastPoints()

	return 1;
end

--------------------------------------------------------------------------------

function ability_undying_giant:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_undying_giant:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------