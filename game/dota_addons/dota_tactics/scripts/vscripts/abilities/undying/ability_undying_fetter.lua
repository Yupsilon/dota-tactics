ability_undying_fetter = class({})
LinkLuaModifier( "modifier_undying_fetter","abilities/undying/modifier_undying_fetter.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_dashed_this_turn","abilities/generic/generic_modifier_dashed_this_turn.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_snare","abilities/generic/generic_modifier_snare.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_combobreaker","abilities/generic/generic_modifier_combobreaker.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_scarred","abilities/generic/generic_modifier_scarred.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_undying_fetter:DashAction(keys)

	if keys.target_point_A~=nil then
				
			EmitSoundOn( "Hero_Undying.Tombstone", self:GetCaster() )
			local dash_range = self:GetSpecialValueFor("dash_range")
			local projectile_radius = self:GetSpecialValueFor("corpse_butt") * 2
	
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_dashed_this_turn",{turns = 1})
			
			local startPoint = keys.caster:GetAbsOrigin() + Vector(0,0,50);
			local endPoint = startPoint			
			local direction = (keys.target_point_A-startPoint):Normalized()
			direction.z=0
			
			local wall_behavior = PROJECTILES_DESTROY			
			if self:GetLevel()==3 then
				
				wall_behavior = PROJECTILES_NOTHING
			end
			
			keys.caster:FaceTowards(keys.target_point_A)
			local animationTime = 1;
			Timers:CreateTimer(0.1, function()
				local hitunit=false
				local projectile = {
				  --EffectName = projectile_model,	
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint+direction:Normalized()*projectile_radius/2,--{unit=hero, attach="attach_attack1", offset=Vector(0,0,0)},
				  fDistance = math.min((keys.target_point_A-startPoint):Length2D(),dash_range),
				  fStartRadius = projectile_radius,
				  fEndRadius = projectile_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  --fExpireTime = 8.0,
				  vVelocity = direction * 1500, 
				  UnitBehavior = PROJECTILES_NOTHING,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  --bCutTrees = true,
				  bTreeFullCollision = false,
				  WallBehavior = wall_behavior,
				  GroundBehavior = wall_behavior,
				  fGroundOffset = 0,
				  --nChangeMax = 1,
				  --bRecreateOnChange = true,
				  bZCheck = false,
				  bGroundLock = false,
				  --bProvidesVision = true,
				  --iVisionRadius = 350,
				  --iVisionTeamNumber = keys.caster:GetTeam(),
				  --bFlyingVision = false,
				  --fVisionTickTime = .1,
				  --fVisionLingerDuration = 1,
				  draw = false,--             draw = {alpha=1, color=Vector(200,0,0)},
				  --iPositionCP = 0,
				  --iVelocityCP = 1,
				  --ControlPoints = {[5]=Vector(100,0,0), [10]=Vector(0,0,1)},
				  --ControlPointForwards = {[4]=hero:GetForwardVector() * -1},
				  --ControlPointOrientations = {[1]={hero:GetForwardVector() * -1, hero:GetForwardVector() * -1, hero:GetForwardVector() * -1}},
				  --[[ControlPointEntityAttaches = {[0]={
					unit = hero,
					pattach = PATTACH_ABSORIGIN_FOLLOW,
					attachPoint = "attach_attack1", -- nil
					origin = Vector(0,0,0)
				  }},]]
				  --fRehitDelay = .3,
				  --fChangeDelay = 1,
				  --fRadiusStep = 10,
				  bUseFindUnitsInRadius = true,
				  bHitScan=true,

				  --[[UnitTest = function(instance, unit) return ( not unit:IsInvulnerable() ) and (not unit:HasModifier("generic_modifier_dashed_this_turn")) and unit:GetTeamNumber() ~= keys.caster:GetTeamNumber() end,
				  OnUnitHit = function(instance, unit) 
					
						
						self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_undying_fetter",{dash_destination_x=instance:GetPosition().x,dash_destination_y=instance:GetPosition().y,hit=true})
						
						if self:GetLevel()==4 then
							hTarget:AddNewModifier(self:GetCaster(),self,"generic_modifier_snare",{								
								turns = self:GetSpecialValueFor("bonus_snare_duration")						
							})
						end
				  end,]]
				  --OnTreeHit = function(self, tree) ... end,
				  --OnWallHit = function(self, gnvPos) ... end,
				  --OnGroundHit = function(self, groundPos) ... end,
				  --OnFinish = function(self, pos) ... end,
				   OnFinish = function(instance, pos) 
					
						if not self:GetCaster():HasModifier("modifier_undying_fetter") then
							self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_undying_fetter",{dash_destination_x=instance:GetPosition().x,dash_destination_y=instance:GetPosition().y})
						end
				  end,
				}
						
				Projectiles:CreateProjectile(projectile)
			end);
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_undying_fetter:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_undying_fetter:disablesMovement()
	return true;
end

--------------------------------------------------------------------------------

function ability_undying_fetter:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_undying_fetter:RequiresHitScanCheck()
	return self:GetLevel()~=3;
end

--------------------------------------------------------------------------------

function ability_undying_fetter:RequiresGridSnap()
	return true;
end


--------------------------------------------------------------------------------

function ability_undying_fetter:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_undying_fetter:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_undying_fetter:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_undying_fetter:GetConeArc(coneID,coneRange)
	return self:GetAOERadius(coneID)
end

--------------------------------------------------------------------------------

function ability_undying_fetter:GetMinRange(coneID)	

	return 0
end

--------------------------------------------------------------------------------

function ability_undying_fetter:GetMaxRange(coneID)	
	if self:GetLevel()==2 then return self:GetSpecialValueFor("dash_range") + self:GetSpecialValueFor("bonus_dash_range") end
	return self:GetSpecialValueFor("dash_range")
end

--------------------------------------------------------------------------------

function ability_undying_fetter:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_undying_fetter:GetAOERadius()	
	return self:GetSpecialValueFor("helix_aoe")
end

--------------------------------------------------------------------------------