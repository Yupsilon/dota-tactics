ability_omniknight_divineshield = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_regenerate","abilities/generic/generic_modifier_regenerate.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_omniknight_divineshield:PrepAction(keys)

			EmitSoundOn( "Hero_Omniknight.Repel", self:GetCaster() )
	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;			
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			
	local animationTime = 1.25*PLAYER_TIME_MODIFIER;			
	StartAnimation(self:GetCaster(), {duration=animationTime, rate = 1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_CAST_ABILITY_1})
	
	local munstoppable = self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_unstoppable", {turns = self:GetSpecialValueFor("modifier_duration") , nodraw=1} )
	local mhaste = self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_movespeed_haste", {speed= self:GetSpecialValueFor("modifier_speed") , turns = self:GetSpecialValueFor("modifier_duration"), nodraw=1} )
						
						local nFXIndex = ParticleManager:CreateParticle( "particles/econ/items/omniknight/omni_ti8_head/omniknight_repel_buff_ti8.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )
						
							ParticleManager:SetParticleControlEnt(nFXIndex, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_origin", self:GetCaster():GetAbsOrigin(), true)
							mhaste:AddParticle( nFXIndex, false, false, -1, false, false )	
	
	if self:GetLevel() == 3 then
			local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), self:GetCaster(), self:GetSpecialValueFor("bonus_slow_aoe"), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
			if #enemies > 0 then
				for _,enemy in pairs(enemies) do
					if enemy ~= nil and ( not enemy:IsInvulnerable() )  then
									
						local slowModifier = enemy:AddNewModifier( self:GetCaster(), self, "generic_modifier_movespeed_slow", {
							turns = self:GetSpecialValueFor("bonus_slow_duration"),
							move_slow = self:GetSpecialValueFor("bonus_slow"),
							nodraw = 1
						} )		
						
					end
				end
			end	
	elseif self:GetLevel() == 2 then
		--self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_regenerate",{
		--	turns = 2,
		--	health = self:GetSpecialValueFor("bonus_healing_total")/2,
		--	initial= self:GetSpecialValueFor("bonus_healing_total")/2
		--})	'
		Alert_HealScars({unit = self:GetCaster(), value = self:GetSpecialValueFor("bonus_healing_total")})
	elseif self:GetLevel() == 4 then
							self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_energized",{
								energy=self:GetSpecialValueFor("bonus_energy_boost"),
								turns=self:GetSpecialValueFor("modifier_duration"),
								nodraw=1
							})
	end
	
	Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("self_energy"), energy=true} )
	return MODIFIERACTION_SHORTPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function ability_omniknight_divineshield:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_omniknight_divineshield:GetPriority()
	return 1;
end

--------------------------------------------------------------------------------

function ability_omniknight_divineshield:GetMaxRange(coneID)	
	if self:GetLevel()==2 then return self:GetSpecialValueFor("bonus_slow_aoe") end
	return 0
end

--------------------------------------------------------------------------------

function ability_omniknight_divineshield:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function ability_omniknight_divineshield:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_omniknight_divineshield:GetHasteBonus()		
	return (100+(self:GetSpecialValueFor("modifier_speed")))/100
end

--------------------------------------------------------------------------------

function ability_omniknight_divineshield:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_omniknight_divineshield:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end


--------------------------------------------------------------------------------