generic_modifier_dot = class({})

--------------------------------------------------------------------------------

function generic_modifier_dot:IsDebuff()
	return true
end

-----------------------------------------------------------------------------

function generic_modifier_dot:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function generic_modifier_dot:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_dot:OnCreated( kv )
	
	local turnsDuration = kv.turns or 1
	self:SetStackCount(turnsDuration)
	
	if IsServer() then		

		self.damage = kv.value or kv.damage
	
		local mFXIndex = ParticleManager:CreateParticle( "particles/items2_fx/orb_of_venom.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent() )
		self:AddParticle( mFXIndex, false, false, -1, false, false )		
	end
end

--------------------------------------------------------------------------------

function generic_modifier_dot:WalkMovementAction()
	
	
		local damage_table = {
			victim = self:GetParent(),
			attacker = self:GetCaster(),
			damage = self.damage,
			damage_type = DAMAGE_TYPE_PURE
		}
	
		Alert_Damage( damage_table )
	
		
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
