generic_modifier_stealth = class({})

-----------------------------------------------------------------------------

function generic_modifier_stealth:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function generic_modifier_stealth:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_stealth:GetTexture()
	return "item_ninja_gear"
end

--------------------------------------------------------------------------------

function generic_modifier_stealth:GetPriority()
	
	return 3
end

--------------------------------------------------------------------------------

function generic_modifier_stealth:OnCreated( kv )

	local turnsDuration = kv.turns or 1
	self.applythisturn = kv.applythisturn or 0
		
	self:SetStackCount(turnsDuration)

	if IsServer() then
	
			local nFXIndex = ParticleManager:CreateParticle( "particles/generic_gameplay/rune_invisibility.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
			self:AddParticle( nFXIndex, false, false, -1, false, false )
			self.invisible = false
	end

end
	
--------------------------------------------------------------------------------

function generic_modifier_stealth:WalkMovementAction()
	
	self.invisible = true
	return MODIFIERACTION_NOPAUSE_SURVIVE;
end
	
--------------------------------------------------------------------------------

function generic_modifier_stealth:EndOfTurnAction()
	
	self.invisible = false
	if self:GetStackCount()>1 then
		self.applythisturn=1	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function generic_modifier_stealth:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_stealth:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_INVISIBILITY_LEVEL,
	}

	return funcs
end

--------------------------------------------------------------------------------

function generic_modifier_stealth:GetPriority()
	
	return 2
end

--------------------------------------------------------------------------------

function generic_modifier_stealth:CheckState()
	local state = {
	[MODIFIER_STATE_INVISIBLE] = (self.invisible or false),
	}

	return state
end

--------------------------------------------------------------------------------

function generic_modifier_stealth:GetModifierInvisibilityLevel( params )
	return 50
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------