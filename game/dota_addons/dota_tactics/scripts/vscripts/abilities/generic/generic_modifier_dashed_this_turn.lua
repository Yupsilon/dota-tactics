generic_modifier_dashed_this_turn = class({})
-----------------------------------------------------------------------------

function generic_modifier_dashed_this_turn:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end


--------------------------------------------------------------------------------

function generic_modifier_dashed_this_turn:IsHidden()
	return not BAREBONES_DEBUG_SPEW -- show cooldown modifier in debug mode
end

--------------------------------------------------------------------------------

function generic_modifier_dashed_this_turn:OnCreated( kv )

	if IsServer() then
		Alert_SendOverheadEventMessage({
			unit = self:GetParent(),
			message = ALERT_DASH
		})
	
		--local parent = self:GetParent()
		--Timers:CreateTimer(PLAYER_TIME_SIMULTANIOUS, function()	
		--	if not IsPointOverBrush(parent:GetAbsOrigin()) then
		--		parent:RemoveModifierByName("modifier_brush_invisibility")
		--	end
		--end)
	end
end
	
--------------------------------------------------------------------------------

function generic_modifier_dashed_this_turn:EndOfTurnAction()		
	return MODIFIERACTION_NOPAUSE_DESTROY;
end