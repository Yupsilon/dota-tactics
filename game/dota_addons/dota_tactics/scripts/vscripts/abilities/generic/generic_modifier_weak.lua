generic_modifier_weak = class({})

-----------------------------------------------------------------------------

function generic_modifier_weak:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function generic_modifier_weak:IsHidden()
	return self.turns == 0 
end

--------------------------------------------------------------------------------

function generic_modifier_weak:IsDebuff()
	return true
end

--------------------------------------------------------------------------------

function generic_modifier_weak:GetTexture()
	return "life_stealer_assimilate"
end

--------------------------------------------------------------------------------

function generic_modifier_weak:OnCreated( kv )

	local turnsDuration = kv.turns or 2
	self.modifierStrength = kv.value or -25
	self.nodraw = kv.nodraw or 0
	self:SetStackCount(turnsDuration)
	
	self.turns = kv.applythisturn or 1

	if IsServer() and self.nodraw == 0 then
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_sniper/sniper_headshot_slow.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent() )
		self:AddParticle( nFXIndex, false, false, -1, false, false )		
	end
end

--------------------------------------------------------------------------------

function generic_modifier_weak:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_TOTALDAMAGEOUTGOING_PERCENTAGE,
		MODIFIER_PROPERTY_TRANSLATE_ACTIVITY_MODIFIERS,
	}

	return funcs
end

--------------------------------------------------------------------------------

function generic_modifier_weak:GetActivityTranslationModifiers( params )
	return "injured"
end

--------------------------------------------------------------------------------

function generic_modifier_weak:GetModifierTotalDamageOutgoing_Percentage(params)
	if self.turns==0 then return 0 end	
	return 0-math.abs(self.modifierStrength)
end
	
--------------------------------------------------------------------------------

function generic_modifier_weak:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self.turns=self.turns+1
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------