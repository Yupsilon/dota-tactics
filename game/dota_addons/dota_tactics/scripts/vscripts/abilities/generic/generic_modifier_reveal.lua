generic_modifier_reveal = class({})

-----------------------------------------------------------------------------

function generic_modifier_reveal:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function generic_modifier_reveal:GetTexture()
	return "keeper_of_the_light_blinding_light"
end

--------------------------------------------------------------------------------

function generic_modifier_reveal:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_reveal:IsDebuff()
	return true
end

--------------------------------------------------------------------------------

function generic_modifier_reveal:GetPriority()
	
	return 4
end

--------------------------------------------------------------------------------

function generic_modifier_reveal:OnCreated( kv )

	local turnsDuration = kv.turns or 2
	self:SetStackCount(turnsDuration)
	self.interval = 1/3
	  
	if IsServer() then
		local nFXIndex = ParticleManager:CreateParticle( "particles/items_fx/dust_of_appearance_debuff.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent() )
		self:AddParticle( nFXIndex, false, false, -1, false, false )	
		--	EmitSoundOn( "n_creep_TrollWarlord.Ensnare", self:GetCaster() )	

		self:StartIntervalThink( self.interval )
		self:OnIntervalThink()		
	end
end

--------------------------------------------------------------------------------

function generic_modifier_reveal:OnIntervalThink()

	AddFOWViewer(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), 40, self.interval, true)

end

--------------------------------------------------------------------------------

function generic_modifier_reveal:CheckState()
	local state = {
	[MODIFIER_STATE_INVISIBLE] = false,
	[MODIFIER_PROPERTY_PROVIDES_FOW_POSITION] = true,
	}

	return state
end

--------------------------------------------------------------------------------

function generic_modifier_reveal:PrepAction()
	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------