generic_modifier_movespeed_slow = class({})
-----------------------------------------------------------------------------

function generic_modifier_movespeed_slow:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end


--------------------------------------------------------------------------------

function generic_modifier_movespeed_slow:IsHidden()
	return self.turns == 0 
end

--------------------------------------------------------------------------------

function generic_modifier_movespeed_slow:IsDebuff()
	return true
end

--------------------------------------------------------------------------------

function generic_modifier_movespeed_slow:GetTexture()
	return "visage_grave_chill"
end

--------------------------------------------------------------------------------

function generic_modifier_movespeed_slow:OnCreated( kv )

	local turnsDuration = kv.turns or 1
	self.move_slow = kv.slow or -100
	self.nodraw = kv.nodraw or 0
	
	self.turns = kv.applythisturn or 1

	if IsServer() then
	
		self:SetStackCount(turnsDuration)
		if self:GetParent():HasModifier("generic_modifier_unstoppable")==true then
			self:Destroy()
		elseif self.nodraw == 0 then
			local nFXIndex = ParticleManager:CreateParticle( "particles/econ/events/ti9/ti9_monkey_debuff_puddle.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
			self:AddParticle( nFXIndex, false, false, -1, false, false )	
			EmitSoundOn( "SeasonalConsumable.TI9.Monkey.ProjectileImpact", self:GetCaster() )			
		end
	end

end

--------------------------------------------------------------------------------

function generic_modifier_movespeed_slow:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
	}

	return funcs
end

--------------------------------------------------------------------------------

function generic_modifier_movespeed_slow:GetModifierMoveSpeedBonus_Percentage( params )
	if self.turns==0 then return 0 end;		
	return self.move_slow
end
	
--------------------------------------------------------------------------------

function generic_modifier_movespeed_slow:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self.turns=self.turns+1
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------