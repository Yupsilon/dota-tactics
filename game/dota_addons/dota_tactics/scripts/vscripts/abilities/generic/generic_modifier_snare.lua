generic_modifier_snare = class({})

-----------------------------------------------------------------------------

function generic_modifier_snare:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function generic_modifier_snare:GetTexture()
	return "dark_troll_warlord_ensnare"
end

--------------------------------------------------------------------------------

function generic_modifier_snare:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_snare:IsDebuff()
	return true
end

--------------------------------------------------------------------------------

function generic_modifier_snare:OnCreated( kv )

	local turnsDuration = kv.turns or 2
	self:SetStackCount(turnsDuration)
	 local nodraw = kv.nodraw or 0
	  
	if IsServer() then
		
		if self:GetParent():HasModifier("generic_modifier_combobreaker") then	
			self:GetParent():AddNewModifier(self:GetCaster(),self:GetAbility(),"generic_modifier_movespeed_slow",{turns=1})
			self:Destroy()
		elseif self:GetParent():HasModifier("generic_modifier_unstoppable") then
			self:Destroy()
		elseif nodraw == 0 then
			local nFXIndex = ParticleManager:CreateParticle( "particles/neutral_fx/dark_troll_ensnare.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
			self:AddParticle( nFXIndex, false, false, -1, false, false )	
			EmitSoundOn( "n_creep_TrollWarlord.Ensnare", self:GetCaster() )		
		end
	end
end

--------------------------------------------------------------------------------

function generic_modifier_snare:CheckState()
	local state = {
	[MODIFIER_STATE_ROOTED] = true,
	}

	return state
end

--------------------------------------------------------------------------------

function generic_modifier_snare:WalkMovementAction()
	if self:GetParent():HasModifier("generic_modifier_unstoppable") then	
		return MODIFIERACTION_NOPAUSE_DESTROY;
	end
	return MODIFIERACTION_NOPAUSE_SURVIVE;
end

--------------------------------------------------------------------------------

function generic_modifier_snare:EndOfTurnAction()
	
	if not self:GetParent():HasModifier("generic_modifier_combobreaker") then	
		self:GetParent():AddNewModifier(self:GetCaster(),self:GetAbility(),"generic_modifier_combobreaker",{turns=1})
	end
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------