generic_modifier_conversion = class({})

-----------------------------------------------------------------------------

function generic_modifier_conversion:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function generic_modifier_conversion:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_conversion:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function generic_modifier_conversion:GetTexture()
	return "giant_wolf_critical_strike"
end

--------------------------------------------------------------------------------

function generic_modifier_conversion:OnCreated( kv )

	local turnsDuration = kv.turns or 2
	self.modifierStrength = kv.value or kv.power or 25
	self:SetStackCount(turnsDuration)
	local nodraw = kv.nodraw or 0
	
	self.turns = 1
	
	--[[if IsServer() then
		
		if kv.applythisturn ~= nil then
			self.turns = kv.applythisturn
		elseif GameMode:GetCurrentGamePhase() == GAMEPHASE_ACTION and self:GetStackCount() > 1 then
			self.turns = 0
		end
		if nodraw~=1 then
			local nFXIndex = ParticleManager:CreateParticle( "particles/generic_gameplay/rune_doubledamage.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
			self:AddParticle( nFXIndex, false, false, -1, false, false )		
			EmitSoundOn( "Rune.DD", self:GetCaster() )				
		end
	end]]
end

--------------------------------------------------------------------------------

function generic_modifier_conversion:GetPermanentDamageConversion(params)
	return math.abs(self.modifierStrength)
end
	
--------------------------------------------------------------------------------

function generic_modifier_conversion:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self.turns=self.turns+1
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------