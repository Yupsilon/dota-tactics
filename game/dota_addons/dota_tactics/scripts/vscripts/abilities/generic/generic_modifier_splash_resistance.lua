generic_modifier_splash_resistance = class({})

-----------------------------------------------------------------------------

function generic_modifier_splash_resistance:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function generic_modifier_splash_resistance:IsHidden()
	return self.turns==0
end

--------------------------------------------------------------------------------

function generic_modifier_splash_resistance:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_splash_resistance:GetTexture()
	return "mudgolem_cloak_aura"
end

--------------------------------------------------------------------------------

function generic_modifier_splash_resistance:OnCreated( kv )

	local turnsDuration = kv.turns or 1
	self.power = kv.value or 100
	self.nodraw = kv.nodraw or 0
		
	self:SetStackCount(turnsDuration)
	
	self.turns = 1
	
	if IsServer() then
		
		if kv.applythisturn ~= nil then
			self.turns = kv.applythisturn
		elseif GameMode:GetCurrentGamePhase() == GAMEPHASE_ACTION and self:GetStackCount() > 1 then
			self.turns = 0
		end
		if nodraw~=1 then
			local nFXIndex = ParticleManager:CreateParticle( "particles/items2_fx/pipe_of_insight.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
				ParticleManager:SetParticleControl(nFXIndex, 0, self:GetParent():GetAbsOrigin())
				ParticleManager:SetParticleControlEnt(nFXIndex, 5, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_overhead", self:GetParent():GetAbsOrigin(), true)
			self:AddParticle( nFXIndex, false, false, -1, false, false )				
		end
	end

end

--------------------------------------------------------------------------------

function generic_modifier_splash_resistance:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MAGICAL_RESISTANCE_BONUS,
	}

	return funcs
end

--------------------------------------------------------------------------------

function generic_modifier_splash_resistance:GetModifierMagicalResistanceBonus( params )
	if self.turns==0 then return 0 end;		
	return self.power
end
	
--------------------------------------------------------------------------------

function generic_modifier_splash_resistance:EndOfTurnAction()
	
	if self:GetStackCount()>1 then
		self.turns=self.turns+1	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------