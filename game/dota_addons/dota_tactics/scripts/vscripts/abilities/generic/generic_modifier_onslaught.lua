generic_modifier_onslaught = class({})

-----------------------------------------------------------------------------

function generic_modifier_onslaught:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function generic_modifier_onslaught:IsHidden()
	return self.turns == 0 
end

--------------------------------------------------------------------------------

function generic_modifier_onslaught:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_onslaught:GetTexture()
	return "satyr_hellcaller_unholy_aura"
end

--------------------------------------------------------------------------------

function generic_modifier_onslaught:OnCreated( kv )

	local turnsDuration = kv.turns or 2
	self.inc_damage = kv.incoming or 25
	self.out_damage = kv.outgoing or 25
	self:SetStackCount(turnsDuration)
	local nodraw = kv.nodraw or 0
	
	self.turns = 1
	
	if IsServer() then
		
		if kv.applythisturn ~= nil then
			self.turns = kv.applythisturn
		elseif GameMode:GetCurrentGamePhase() == GAMEPHASE_ACTION and self:GetStackCount() > 1 then
			self.turns = 0
		end
		if nodraw~=1 then
			local nFXIndex = ParticleManager:CreateParticle( "particles/items2_fx/mask_of_madness.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
			self:AddParticle( nFXIndex, false, false, -1, false, false )		
			EmitSoundOn( "DOTA_Item.MaskOfMadness.Activate", self:GetCaster() )				
		end
	end
end

--------------------------------------------------------------------------------

function generic_modifier_onslaught:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_TOTALDAMAGEOUTGOING_PERCENTAGE,
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
	}

	return funcs
end

--------------------------------------------------------------------------------

function generic_modifier_onslaught:GetModifierTotalDamageOutgoing_Percentage(params)
	if self.turns==0 then return 0 end;		
	return math.abs(self.out_damage)
end

--------------------------------------------------------------------------------

function generic_modifier_onslaught:GetModifierIncomingDamage_Percentage(params)
	if self.turns==0 then return 0 end;		
	return math.abs(self.inc_damage)
end
	
--------------------------------------------------------------------------------

function generic_modifier_onslaught:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self.turns=self.turns+1
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------