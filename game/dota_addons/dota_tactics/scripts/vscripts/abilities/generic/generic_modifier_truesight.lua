generic_modifier_truesight = class({})

-----------------------------------------------------------------------------

function generic_modifier_truesight:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

-----------------------------------------------------------------------------

function generic_modifier_truesight:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function generic_modifier_truesight:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_truesight:GetPriority()
	
	return 4
end

--------------------------------------------------------------------------------

function generic_modifier_truesight:OnCreated( kv )

	local turnsDuration = kv.turns or 2
	self:SetStackCount(turnsDuration)
	self.sightrange = kv.range or 2000
	self.interval = 1/3
	  
	if IsServer() then
		--local nFXIndex = ParticleManager:CreateParticle( "particles/items_fx/dust_of_appearance_debuff.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent() )
		--self:AddParticle( nFXIndex, false, false, -1, false, false )	
		--	EmitSoundOn( "n_creep_TrollWarlord.Ensnare", self:GetCaster() )	

		self:StartIntervalThink( self.interval )
		self:OnIntervalThink()		
	end
end

--------------------------------------------------------------------------------

function generic_modifier_truesight:OnIntervalThink()

	AddFOWViewer(self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), self.sightrange, self.interval, false)
	
	local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), self:GetParent(),self.sightrange , DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
	if #enemies > 0 then
		for _,enemy in pairs(enemies) do
			if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
				enemy:AddNewModifier(self:GetCaster(),self:GetAbility(),"modifier_temp_reveal",{duration = self.interval*3/2})
			end
		end
	end
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------