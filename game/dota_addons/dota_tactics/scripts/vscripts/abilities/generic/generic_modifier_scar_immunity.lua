generic_modifier_scar_immunity = class({})
-----------------------------------------------------------------------------

function generic_modifier_scar_immunity:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end


--------------------------------------------------------------------------------

function generic_modifier_scar_immunity:IsHidden()
	return not BAREBONES_DEBUG_SPEW -- show cooldown modifier in debug mode
end

--------------------------------------------------------------------------------

function generic_modifier_scar_immunity:OnCreated( kv )

	self:SetStackCount(kv.turns or 1)

end
-----------------------------------------------------------------------------

function generic_modifier_scar_immunity:GetTexture()
	return "dazzle_shadow_wave"
end
	
--------------------------------------------------------------------------------

function generic_modifier_scar_immunity:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end