generic_modifier_healing_reduced = class({})

-----------------------------------------------------------------------------

function generic_modifier_healing_reduced:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function generic_modifier_healing_reduced:IsHidden()
	return self.turns==0
end

--------------------------------------------------------------------------------

function generic_modifier_healing_reduced:IsDebuff()
	return true
end

--------------------------------------------------------------------------------

function generic_modifier_healing_reduced:GetTexture()
	return "spawnlord_master_stomp"
end

--------------------------------------------------------------------------------

function generic_modifier_healing_reduced:OnCreated( kv )

	local turnsDuration = kv.turns or 1
	self.wounds = 0-math.abs(kv.value or 100)
	self.nodraw = kv.nodraw or 0
		
	self:SetStackCount(turnsDuration)
	
	self.turns = 1
	
	if IsServer() then
		
		if kv.applythisturn ~= nil then
			self.turns = kv.applythisturn
		elseif GameMode:GetCurrentGamePhase() == GAMEPHASE_ACTION and self:GetStackCount() > 1 then
			self.turns = 0
		end
		if nodraw~=1 then
			local nFXIndex = ParticleManager:CreateParticle( "particles/items4_fx/spirit_vessel_damage.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
			self:AddParticle( nFXIndex, false, false, -1, false, false )				
		end
	end

end

--------------------------------------------------------------------------------

function generic_modifier_healing_reduced:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_HP_REGEN_AMPLIFY_PERCENTAGE,
	}

	return funcs
end

--------------------------------------------------------------------------------

function generic_modifier_healing_reduced:GetModifierHPRegenAmplify_Percentage( params )
	if self.turns==0 then return 0 end;		
	return self.wounds
end
	
--------------------------------------------------------------------------------

function generic_modifier_healing_reduced:EndOfTurnAction()
	
	if self:GetStackCount()>1 then
		self.turns=self.turns+1	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------