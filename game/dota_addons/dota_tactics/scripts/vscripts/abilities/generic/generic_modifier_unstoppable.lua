generic_modifier_unstoppable = class({})

-----------------------------------------------------------------------------

function generic_modifier_unstoppable:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function generic_modifier_unstoppable:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_unstoppable:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_unstoppable:GetTexture()
	return "neutral_spell_immunity"
end

--------------------------------------------------------------------------------

function generic_modifier_unstoppable:OnCreated( kv )

	local turnsDuration = kv.turns or 1
	self:SetStackCount(turnsDuration)
	
	if IsServer() then

		while self:GetParent():HasModifier("generic_modifier_movespeed_slow") do
			self:GetParent():RemoveModifierByName("generic_modifier_movespeed_slow")
		end
		while self:GetParent():HasModifier("generic_modifier_movespeed_snare") do
			self:GetParent():RemoveModifierByName("generic_modifier_movespeed_snare")
		end
		local nFXIndex = ParticleManager:CreateParticle( "particles/items_fx/black_king_bar_avatar.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		self:AddParticle( nFXIndex, false, false, -1, false, false )		
			EmitSoundOn( "DOTA_Item.BlackKingBar.Activate", self:GetCaster() )
	end
end
	
--------------------------------------------------------------------------------

function generic_modifier_unstoppable:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end