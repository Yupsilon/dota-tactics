generic_modifier_energized = class({})
-----------------------------------------------------------------------------

function generic_modifier_energized:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function generic_modifier_energized:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_energized:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_energized:GetTexture()
	return "spawnlord_aura"
end

--------------------------------------------------------------------------------

function generic_modifier_energized:GetEnergizedValue()
	if self.turns==0 then return 0 end;		
	return math.abs(self.multiplier)
end

--------------------------------------------------------------------------------

function generic_modifier_energized:OnCreated( kv )

	local turnsDuration = kv.turns or 2
	self.multiplier = kv.energy or 25
	self.nodraw = kv.nodraw or 0
	self:SetStackCount(turnsDuration)
	
	self.turns = 1

	if IsServer() then
		
		if kv.applythisturn ~= nil then
			self.turns = kv.applythisturn
		elseif GameMode:GetCurrentGamePhase() == GAMEPHASE_ACTION and self:GetStackCount() > 1 then
			self.turns = 0
		end
		if self.nodraw == 0 then
			local nFXIndex = ParticleManager:CreateParticle( "particles/generic_gameplay/rune_arcane.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
			self:AddParticle( nFXIndex, false, false, -1, false, false )	
			EmitSoundOn( "Rune.Arcane", self:GetCaster() )					
		end
	end

end
	
--------------------------------------------------------------------------------

function generic_modifier_energized:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function generic_modifier_energized:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_TOOLTIP
	}	
	return funcs
end

--------------------------------------------------------------------------------

function generic_modifier_energized:OnTooltip( params )
	return self.multiplier
end