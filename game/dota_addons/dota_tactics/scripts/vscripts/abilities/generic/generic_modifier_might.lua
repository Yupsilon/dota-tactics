generic_modifier_might = class({})

-----------------------------------------------------------------------------

function generic_modifier_might:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function generic_modifier_might:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_might:IsHidden()
	return self.turns == 0 
end

--------------------------------------------------------------------------------

function generic_modifier_might:GetTexture()
	return "giant_wolf_critical_strike"
end

--------------------------------------------------------------------------------

function generic_modifier_might:OnCreated( kv )

	local turnsDuration = kv.turns or 2
	self.modifierStrength = kv.value or kv.power or 25
	self:SetStackCount(turnsDuration)
	local nodraw = kv.nodraw or 0
	
	self.turns = 1
	
	if IsServer() then
		
		if kv.applythisturn ~= nil then
			self.turns = kv.applythisturn
		elseif GameMode:GetCurrentGamePhase() == GAMEPHASE_ACTION and self:GetStackCount() > 1 then
			self.turns = 0
		end
		if nodraw~=1 then
			local nFXIndex = ParticleManager:CreateParticle( "particles/generic_gameplay/rune_doubledamage.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
			self:AddParticle( nFXIndex, false, false, -1, false, false )		
			EmitSoundOn( "Rune.DD", self:GetCaster() )				
		end
	end
end

--------------------------------------------------------------------------------

function generic_modifier_might:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_TOTALDAMAGEOUTGOING_PERCENTAGE,
	}

	return funcs
end

--------------------------------------------------------------------------------

function generic_modifier_might:GetModifierTotalDamageOutgoing_Percentage(params)
	if self.turns==0 then return 0 end;		
	return math.abs(self.modifierStrength)
end
	
--------------------------------------------------------------------------------

function generic_modifier_might:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self.turns=self.turns+1
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------