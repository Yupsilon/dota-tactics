generic_modifier_regenerate = class({})
-----------------------------------------------------------------------------

function generic_modifier_regenerate:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function generic_modifier_regenerate:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_regenerate:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_regenerate:GetTexture()
	return "huskar_inner_vitality"
end

--------------------------------------------------------------------------------

function generic_modifier_regenerate:OnCreated( kv )

	self.health_per_turn = kv.health or kv.value or 10
	self.initial = kv.initial or self.health_per_turn
	self.nodraw = kv.nodraw or 0
	self.turns = 0
	self:SetStackCount(kv.turns or 2)

	if IsServer() then  
	
		local caster = self:GetCaster() or self:GetParent()
		self:GetParent():Heal(self.initial,self:GetParent())
		Alert_Heal({caster=caster,target=self:GetParent(),value=self.initial})
		if self.nodraw ~= 1 then
			local nFXIndex = ParticleManager:CreateParticle( "particles/generic_gameplay/rune_regen_owner.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
			self:AddParticle( nFXIndex, false, false, -1, false, false )	
			EmitSoundOn( "Rune.Regen", self:GetParent() )			
		end
	end

end
	
--------------------------------------------------------------------------------

function generic_modifier_regenerate:EndOfTurnAction()
	
	if (self.turns > 0) then
		local caster = self:GetCaster() or self:GetParent()
		Alert_Heal({caster=caster,target=self:GetParent(),value=self.health_per_turn})
	end
	self.turns=self.turns+1
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function generic_modifier_regenerate:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_TOOLTIP
	}	

	return funcs
end

--------------------------------------------------------------------------------

function generic_modifier_regenerate:OnTooltip( params )
	return self.health_per_turn
end