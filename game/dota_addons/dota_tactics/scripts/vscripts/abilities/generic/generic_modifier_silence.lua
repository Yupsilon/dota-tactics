generic_modifier_silence = class({})
-----------------------------------------------------------------------------

function generic_modifier_silence:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end


--------------------------------------------------------------------------------

function generic_modifier_silence:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_silence:IsDebuff()
	return true
end

--------------------------------------------------------------------------------

function generic_modifier_silence:GetTexture()
	return "silencer_last_word"
end

--------------------------------------------------------------------------------

function generic_modifier_silence:OnCreated( kv )

	local turnsDuration = kv.turns or 2
	self.nodraw = kv.nodraw or 0
	self:SetStackCount(turnsDuration)
	self.startabil = 0
	if kv.ultimatesonly == 1 then
		self.startabil = 5
	end

	if IsServer() and self.nodraw == 0 then
	
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_pangolier/pangolier_luckyshot_silence_debuff.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent() )
		self:AddParticle( nFXIndex, false, false, -1, false, false )		
			EmitSoundOn( "DOTA_Item.Orchid.Activate", self:GetCaster() )			
	end
end
	
--------------------------------------------------------------------------------

function generic_modifier_silence:EndOfTurnAction()
	
	
	
		 for i=self.startabil, 6 do
			local abil = self:GetParent():GetAbilityByIndex(i)
			if abil ~= nil and abil.IsDisabledBySilence == nil or abil:IsDisabledBySilence() == true then
			  
				abil.LastCastTurn = math.max(abil.LastCastTurn or -1,GameMode:GetCurrentTurn()+self:GetStackCount()+1);
				abil:UseResources(false,false,true)	
				self:GetParent():AddNewModifier(self:GetCaster(),abil,"modifier_ability_cooldown",{})
			  
			end
		  end
	
	--if self:GetStackCount()>1 then	
	--	self:DecrementStackCount()
	--	return MODIFIERACTION_NOPAUSE_SURVIVE;
	--end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------