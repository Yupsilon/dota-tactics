generic_modifier_scarred = class({})

-----------------------------------------------------------------------------

function generic_modifier_scarred:GetAttributes()
	return MODIFIER_ATTRIBUTE_PERMANENT
end


--------------------------------------------------------------------------------

function generic_modifier_scarred:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_scarred:IsPermanent()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_scarred:RemoveOnDeath()
	return true
end


--------------------------------------------------------------------------------

function generic_modifier_scarred:IsDebuff()
	return true
end

--------------------------------------------------------------------------------

function generic_modifier_scarred:GetTexture()
	return "life_stealer_open_wounds"
end

--------------------------------------------------------------------------------

function generic_modifier_scarred:OnCreated(kv)
	if not IsServer() then return end
	
	self.minusLife = 0
	--self:GetParent():SetMaxHealth(self:GetParent():GetMaxHealth()-self.minusLife)
	self:StartIntervalThink( 1/30 )
	self:OnIntervalThink()		
end

--------------------------------------------------------------------------------

function generic_modifier_scarred:OnDestroy()
	if not IsServer() then return end
	self:GetParent():SetBaseMaxHealth(self:GetParent():GetBaseMaxHealth()+self.minusLife)
end

--------------------------------------------------------------------------------

function generic_modifier_scarred:Refresh()
	if not IsServer() then return end
	
	local newlife = self:GetStackCount()
	
	if (newlife~=self.minusLife) then
		self:GetParent():SetBaseMaxHealth(self:GetParent():GetBaseMaxHealth()+self.minusLife-newlife)
		self.minusLife = newlife
	end
end

--------------------------------------------------------------------------------

function generic_modifier_scarred:OnIntervalThink()
	if not IsServer() then return end
	
	self:GetParent():SetMaxHealth(self:GetParent():GetBaseMaxHealth()-self:GetStackCount())
	self:GetParent():SetHealth(math.min(self:GetParent():GetHealth(),self:GetParent():GetMaxHealth()))	
end

--[[------------------------------------------------------------------------------

function generic_modifier_scarred:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_EXTRA_HEALTH_BONUS
	}

	return funcs
end

--------------------------------------------------------------------------------

function generic_modifier_scarred:GetModifierExtraHealthBonus( params )
	
	return 0-self:GetStackCount()
end]]

--------------------------------------------------------------------------------