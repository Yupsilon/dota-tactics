generic_modifier_movespeed_absolute = class({})

--------------------------------------------------------------------------------

function generic_modifier_movespeed_absolute:IsHidden()
	return true 
end

--------------------------------------------------------------------------------

function generic_modifier_movespeed_absolute:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_movespeed_absolute:GetTexture()
	return "kobold_taskmaster_speed_aura"
end

--------------------------------------------------------------------------------

function generic_modifier_movespeed_absolute:OnCreated( kv )

	self.move_speed = kv.speed or kv.value or 50
	
	if IsServer() then
		
		self:SetStackCount(self.move_speed)
		
	end

end

--------------------------------------------------------------------------------

function generic_modifier_movespeed_absolute:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MOVESPEED_ABSOLUTE,
	}

	return funcs
end

--------------------------------------------------------------------------------

function generic_modifier_movespeed_absolute:GetModifierMoveSpeed_Absolute( params )
	
	return self:GetStackCount()
end
	
--------------------------------------------------------------------------------
