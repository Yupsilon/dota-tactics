generic_modifier_combobreaker = class({})

-----------------------------------------------------------------------------

function generic_modifier_combobreaker:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function generic_modifier_combobreaker:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_combobreaker:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_combobreaker:GetTexture()
	return "courier_shield"
end

--------------------------------------------------------------------------------

function generic_modifier_combobreaker:OnCreated( kv )

	local turnsDuration = kv.turns or 1
	self:SetStackCount(turnsDuration)
	
	if IsServer() then

		local nFXIndex = ParticleManager:CreateParticle( "particles/items5_fx/minotaur_horn_swirl.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		self:AddParticle( nFXIndex, false, false, -1, false, false )		
		--EmitSoundOn( "DOTA_Item.BlackKingBar.Activate", self:GetCaster() )
	end
end
	
--------------------------------------------------------------------------------

function generic_modifier_combobreaker:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end