generic_modifier_invisible = class({})

-----------------------------------------------------------------------------

function generic_modifier_invisible:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function generic_modifier_invisible:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_invisible:GetTexture()
	return "brewmaster_storm_wind_walk"
end

--------------------------------------------------------------------------------

function generic_modifier_invisible:GetPriority()
	
	return 3
end

--------------------------------------------------------------------------------

function generic_modifier_invisible:OnCreated( kv )

	local turnsDuration = kv.turns or 1
	self.applythisturn = kv.applythisturn or 0
		
	self:SetStackCount(turnsDuration)

	if IsServer() then
	
			local nFXIndex = ParticleManager:CreateParticle( "particles/generic_gameplay/rune_invisibility.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
			self:AddParticle( nFXIndex, false, false, -1, false, false )
		
	end

end
	
--------------------------------------------------------------------------------

function generic_modifier_invisible:EndOfTurnAction()
	
	if self:GetStackCount()>1 then
		self.applythisturn=1	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function generic_modifier_invisible:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_invisible:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_INVISIBILITY_LEVEL,
	}

	return funcs
end

--------------------------------------------------------------------------------

function generic_modifier_invisible:GetPriority()
	
	return 2
end

--------------------------------------------------------------------------------

function generic_modifier_invisible:CheckState()
	local state = {
	[MODIFIER_STATE_INVISIBLE] = true,
	}

	return state
end

--------------------------------------------------------------------------------

function generic_modifier_invisible:GetModifierInvisibilityLevel( params )
	return 50
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------