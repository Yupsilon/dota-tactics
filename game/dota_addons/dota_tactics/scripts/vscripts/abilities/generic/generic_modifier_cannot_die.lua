generic_modifier_cannot_die = class({})
-----------------------------------------------------------------------------

function generic_modifier_cannot_die:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function generic_modifier_cannot_die:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_cannot_die:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_cannot_die:GetTexture()
	return "dazzle_shallow_grave"
end

--------------------------------------------------------------------------------

function generic_modifier_cannot_die:OnCreated( kv )

	local turnsDuration = kv.turns or 1
	self.nodraw = kv.nodraw or 0
	self:SetStackCount(turnsDuration)

	if IsServer() and self.nodraw == 0 then
		--local nFXIndex = ParticleManager:CreateParticle( "particles/generic_gameplay/rune_arcane.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		--self:AddParticle( nFXIndex, false, false, -1, false, false )	
		--	EmitSoundOn( "Rune.Arcane", self:GetCaster() )					
	end

end
	
--------------------------------------------------------------------------------

function generic_modifier_cannot_die:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end
