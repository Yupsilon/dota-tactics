generic_modifier_stun = class({})

-----------------------------------------------------------------------------

function generic_modifier_stun:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function generic_modifier_stun:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_stun:IsDebuff()
	return true
end

--------------------------------------------------------------------------------

function generic_modifier_stun:GetTexture()
	return "roshan_bash"
end

--------------------------------------------------------------------------------

function generic_modifier_stun:OnCreated( kv )

	local turnsDuration = kv.turns or 2
	self:SetStackCount(turnsDuration)
	
	if IsServer() then
		local nFXIndex = ParticleManager:CreateParticle( "particles/generic_gameplay/generic_stunned.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent() )
		self:AddParticle( nFXIndex, false, false, -1, false, false )	
			EmitSoundOn( "DOTA_Item.SkullBasher", self:GetCaster() )				
	end
end

--------------------------------------------------------------------------------

function generic_modifier_stun:EndOfTurnAction()
	
	
		local abil = self:GetParent():GetAbilityByIndex(0)
		if abil ~= nil then
				  
			abil.LastCastTurn = math.max(abil.LastCastTurn or -1,GameMode:GetCurrentTurn()+self:GetStackCount()+1);
			abil:UseResources(false,false,true)	
			self:GetParent():AddNewModifier(self:GetCaster(),abil,"modifier_ability_cooldown",{})
				  
		end
	--if self:GetStackCount()>1 then	
	--	self:DecrementStackCount()
		
	--	return MODIFIERACTION_NOPAUSE_SURVIVE;
	--end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------