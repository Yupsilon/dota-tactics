generic_modifier_shield = class({})

-----------------------------------------------------------------------------

function generic_modifier_shield:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function generic_modifier_shield:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_shield:GetPriority()
	return 2
end

--------------------------------------------------------------------------------

function generic_modifier_shield:IsHidden()
	return self.turns == 0 
end

--------------------------------------------------------------------------------

function generic_modifier_shield:GetShieldLife()
	return self.shield_remaining
end

--------------------------------------------------------------------------------

function generic_modifier_shield:OnCreated( kv )

	self.nodraw = kv.nodraw or 0
	self.shield_remaining =  kv.life or kv.value or 10
	self:SetStackCount(kv.turns or 2	)	
	
	self.turns = kv.applythisturn or 1
	
	if IsServer() then
			
			--SendOverheadEventMessage( nil, OVERHEAD_ALERT_MAGICAL_BLOCK, self:GetParent(),self.shield_remaining, self:GetParent():GetPlayerOwner() )--nil)
			Alert_SendOverheadEventMessage{
				unit = self:GetParent(),
				value = self.shield_remaining,
				message = ALERT_SHIELD_ADD
			}
			self.linked_modifiers = {}
	
			if self.nodraw == 0 then
					local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_sven/sven_warcry_buff_shield.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
							ParticleManager:SetParticleControlEnt(nFXIndex, 0, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
							ParticleManager:SetParticleControl( nFXIndex, 1, Vector(self:GetParent():GetHullRadius(),self:GetParent():GetHullRadius(),self:GetParent():GetHullRadius()) )		
							self:AddParticle( nFXIndex, false, false, -1, false, false )	
							EmitSoundOn( "DOTA_Item.MagicWand.Activate", self:GetParent() )			
			end
	end
end
	
--------------------------------------------------------------------------------

function generic_modifier_shield:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self.turns=self.turns+1
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	Alert_SendOverheadEventMessage{
		unit = self:GetParent(),
		value = self.shield_remaining,
		message = ALERT_SHIELD_DAMAGE
	}
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function generic_modifier_shield:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_TOTAL_CONSTANT_BLOCK
	}	

	return funcs
end
	
--------------------------------------------------------------------------------

function generic_modifier_shield : OnTakeDamage( event )

	if IsServer() and self.turns > 0 then
			local hVictim = event.unit

			
			if hVictim == self:GetParent() then		
			
				self.shieldLife = self.shieldLife-event.damage
				event.damage=0			
				if self.shieldLife <= 0 then
				
					self:GetParent():SetHealth(self.unitLife+self.shieldLife);	
					self.shieldLife = nil						
					self:Destroy()
				else
					self:GetParent():SetHealth(self.unitLife);		
				end
		end
	end
	return 0
end
	
--------------------------------------------------------------------------------

function generic_modifier_shield:GetModifierTotal_ConstantBlock(kv)
	if IsServer() then
		local target 					= self:GetParent()
		local original_shield_amount	= self.shield_remaining
		if kv.damage > 0 then

			self.shield_remaining = self.shield_remaining - kv.damage
			
			if kv.damage < original_shield_amount then
			
				Alert_SendOverheadEventMessage{
					unit = self:GetParent(),
					value = kv.damage,
					message = ALERT_SHIELD_DAMAGE
				}
				return kv.damage
			else			
			
				Alert_SendOverheadEventMessage{
					unit = self:GetParent(),
					value = original_shield_amount,
					message = ALERT_SHIELD_DAMAGE
				}
				self:Destroy()
				return original_shield_amount
			end
		end
	end
end
	
--------------------------------------------------------------------------------

function generic_modifier_shield:OnDestroy()

	if IsServer() then
		
		if self.shield_remaining>0 then
			Alert_SendOverheadEventMessage{
				unit = self:GetParent(),
				value = self.shield_remaining,
				message = ALERT_SHIELD_DAMAGE
			}
		end
		if self.linked_modifiers ~= nil and #self.linked_modifiers>0 then
			for index, modifier in pairs (self.linked_modifiers) do
				if modifier~=nil then
					modifier:Destroy()
				end
			end
		end
	end
end

--------------------------------------------------------------------------------