generic_modifier_movespeed_haste = class({})

-----------------------------------------------------------------------------

function generic_modifier_movespeed_haste:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end


--------------------------------------------------------------------------------

function generic_modifier_movespeed_haste:IsHidden()
	return self.turns == 0 
end

--------------------------------------------------------------------------------

function generic_modifier_movespeed_haste:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_movespeed_haste:GetTexture()
	return "kobold_taskmaster_speed_aura"
end

--------------------------------------------------------------------------------

function generic_modifier_movespeed_haste:OnCreated( kv )

	local turnsDuration = kv.turns or 1
	self.move_speed = kv.speed or kv.value or 50
	self.nodraw = kv.nodraw or 0
	
	
	self.turns = 1

	if IsServer() then
		
		self:SetStackCount(turnsDuration)
		if kv.applythisturn ~= nil then
			self.turns = kv.applythisturn
		elseif GameMode:GetCurrentGamePhase() == GAMEPHASE_ACTION and self:GetStackCount() > 1 then
			self.turns = 0
		end
	
		if	self.nodraw == 0 then
			local nFXIndex = ParticleManager:CreateParticle( "particles/generic_gameplay/rune_haste_owner.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
			self:AddParticle( nFXIndex, false, false, -1, false, false )		
			EmitSoundOn( "Rune.Haste", self:GetCaster() )			
		end
	end

end

--------------------------------------------------------------------------------

function generic_modifier_movespeed_haste:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_MOVESPEED_BONUS_PERCENTAGE,
	}

	return funcs
end

--------------------------------------------------------------------------------

function generic_modifier_movespeed_haste:GetModifierMoveSpeedBonus_Percentage( params )
	if self.turns==0 then return 0 end;			
	return self.move_speed
end
	
--------------------------------------------------------------------------------

function generic_modifier_movespeed_haste:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self.turns=self.turns+1
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------