generic_modifier_summonedunit = class({})
-----------------------------------------------------------------------------

function generic_modifier_summonedunit:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end


--------------------------------------------------------------------------------

function generic_modifier_summonedunit:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_summonedunit:IsPermanent()
	return true
end

--------------------------------------------------------------------------------

function generic_modifier_summonedunit:RemoveOnDeath()
	return false
end

--------------------------------------------------------------------------------

function generic_modifier_summonedunit:OnCreated( kv )

	self.AssignedUnit=nil
	self:SetStackCount(kv.turns or 1)

	self.nodraw = kv.nodraw or 0
	self.destroyonend = kv.destroyonend or 0
end

--------------------------------------------------------------------------------

function generic_modifier_summonedunit:AssignUnit( unit )
	if not IsServer() then return end

	self.AssignedUnit=unit
	self.AssignedUnit:SetModelScale(self:GetCaster():GetModelScale()*self.AssignedUnit:GetModelScale())
	
	  local item = CreateItem("item_ready_up", self.AssignedUnit, self.AssignedUnit)
	  self.AssignedUnit:AddItem(item)
	    
	  self.AssignedUnit:SwapItems(DOTA_ITEM_SLOT_1,TP_SCROLL_SLOT)
	  
	  if self.nodraw ~= 1 then
		
			local nFXIndex = ParticleManager:CreateParticle( "particles/items5_fx/neutral_treasurebox_spawn.vpcf", PATTACH_WORLDORIGIN, self.AssignedUnit )
			ParticleManager:SetParticleControl( nFXIndex, 0, self.AssignedUnit:GetAbsOrigin() )
			ParticleManager:ReleaseParticleIndex( nFXIndex )
	  end
end

--------------------------------------------------------------------------------

function generic_modifier_summonedunit:PrepAction()
	
	return self:Resolve()
end

--------------------------------------------------------------------------------

function generic_modifier_summonedunit:DashAction()
	
	return self:Resolve()
end

--------------------------------------------------------------------------------

function generic_modifier_summonedunit:SolveAction()
	
	return self:Resolve()
end

--------------------------------------------------------------------------------

function generic_modifier_summonedunit:WalkMovementAction()
	return self:Resolve()
end
	
--------------------------------------------------------------------------------

function generic_modifier_summonedunit:Resolve()

	if self.AssignedUnit==nil or self.AssignedUnit:IsNull() or not self.AssignedUnit:IsAlive() then 
		return MODIFIERACTION_NOPAUSE_DESTROY;
	end

	local currentGamePhase = GameMode:GetCurrentGamePhase()
	local modifierPause = 0
	
	for index,modifierorder in pairs(self.AssignedUnit:FindAllModifiers()) do	
		local mod = MODIFIERACTION_NOPAUSE_SURVIVE
		
			if currentGamePhase == GAMEPHASE_PREP and modifierorder.PrepAction~=nil then
					mod = modifierorder:PrepAction()
			elseif currentGamePhase == GAMEPHASE_DASH and modifierorder.DashAction~=nil then
					mod = modifierorder:DashAction()
			elseif currentGamePhase == GAMEPHASE_ACTION and modifierorder.SolveAction~=nil then
					mod = modifierorder:SolveAction()
			elseif currentGamePhase == GAMEPHASE_WALK and modifierorder.WalkMovementAction~=nil then
					mod = modifierorder:WalkMovementAction()
			elseif currentGamePhase > GAMEPHASE_WALK and modifierorder.EndOfTurnAction~=nil then
					mod = modifierorder:EndOfTurnAction()
			elseif modifierorder.Resolve~=nil then			
				mod = modifierorder:Resolve()		
			end
			
		modifierPause = math.max( GetModifierPause(mod) , modifierPause )
				
		if GetModifierDestroyed(mod)  then
			modifierorder:Destroy();											
		end			
	end
	
	if modifierPause== 2 then
		return MODIFIERACTION_PAUSE_SURVIVE;
	elseif modifierPause == 1 then
		return MODIFIERACTION_SHORTPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_SURVIVE;
end
	
--------------------------------------------------------------------------------

function generic_modifier_summonedunit:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return self:Resolve();
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function generic_modifier_summonedunit:OnDestroy( kv )
	if self.AssignedUnit~=nil and not self.AssignedUnit:IsNull() then
		  if self.nodraw ~= 1 then			
				local nFXIndex = ParticleManager:CreateParticle( "particles/items5_fx/neutral_treasurebox_spawn.vpcf", PATTACH_WORLDORIGIN, self.AssignedUnit )
				ParticleManager:SetParticleControl( nFXIndex, 0, self.AssignedUnit:GetAbsOrigin() )
				ParticleManager:ReleaseParticleIndex( nFXIndex )
		  end
		if self.destroyonend then
			self.AssignedUnit:Destroy()
		else
			self.AssignedUnit:Kill(nil,nil)
		end
	end		
end
	
--------------------------------------------------------------------------------