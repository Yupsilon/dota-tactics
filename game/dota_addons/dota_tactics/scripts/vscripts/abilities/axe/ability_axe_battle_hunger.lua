ability_axe_battle_hunger = class({})
LinkLuaModifier( "modifier_axe_battlehunger","abilities/axe/modifier_axe_battlehunger.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_silence","abilities/generic/generic_modifier_silence.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_axe_battle_hunger:PrepAction(keys)

	if keys.target_point_A~=nil then
				 
			EmitSoundOn( "Hero_Axe.Battle_Hunger", self:GetCaster() )
		
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
						
			local arate = 1
			local animationTime = 29/30*PLAYER_TIME_MODIFIER/arate;		
				keys.caster:FaceTowards(keys.target_point_A)
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_OVERRIDE_ABILITY_2, rate = arate/PLAYER_TIME_MODIFIER})	

			local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_axe/axe_battle_hunger_cast.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )
			ParticleManager:SetParticleControlEnt(nFXIndex, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_attack1 ", self:GetCaster():GetAbsOrigin(), true)	
			ParticleManager:ReleaseParticleIndex(nFXIndex)
						   
			local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), keys.target_point_A, self:GetCaster(), self:GetAOERadius(), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
					if #enemies > 0 then
						for _,enemy in pairs(enemies) do
							if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
								enemy:AddNewModifier(self:GetCaster(),self,"modifier_axe_battlehunger",{turns = self:GetSpecialValueFor("hunger_duration")})
								Alert_Mana({unit = self:GetCaster(),manavalue = self:GetSpecialValueFor("hunger_energy_caster"),energy = true});
								if self:GetLevel()==4 then
									enemy:AddNewModifier( self:GetCaster(), self, "generic_modifier_silence", { turns = self:GetSpecialValueFor("bonus_reveal_duration"), ultimatesonly = 1 } )
								end
							end
						end
					end		
					
			if self:GetLevel()==2 then
				self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_haste",{speed=self:GetSpecialValueFor("bonus_haste"),turns = 1})
			end
				
	end
	return ABILITYACTION_COMPLETE_SHORT;
end

--------------------------------------------------------------------------------

function ability_axe_battle_hunger:GetPriority()
	return 4;
end

--------------------------------------------------------------------------------

function ability_axe_battle_hunger:GetAOERadius()
	if self:GetLevel()==3 then
		return self:GetSpecialValueFor( "hunger_aoe" )+self:GetSpecialValueFor( "bonus_aoe")
	end
	return self:GetSpecialValueFor( "hunger_aoe" )
end

--------------------------------------------------------------------------------

function ability_axe_battle_hunger:getAbilityType()
	return ABILITY_TYPE_POINT_MULTIPLE;
end

--------------------------------------------------------------------------------

function ability_axe_battle_hunger:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_axe_battle_hunger:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_axe_battle_hunger:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_axe_battle_hunger:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_axe_battle_hunger:GetConeArc(coneID,coneRange)
	return 100
end

--------------------------------------------------------------------------------

function ability_axe_battle_hunger:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_axe_battle_hunger:GetMaxRange(coneID)	
	if self:GetLevel()==3 then
		return self:GetSpecialValueFor("hunger_range")+self:GetSpecialValueFor("bonus_range")
	end
	return self:GetSpecialValueFor("hunger_range")
end

--------------------------------------------------------------------------------

function ability_axe_battle_hunger:GetCastRange()	
	return self:GetSpecialValueFor("hunger_range")
end

--------------------------------------------------------------------------------

function ability_axe_battle_hunger:GetHasteBonus()	
	if self:GetLevel()==2 then			
		return (100+(self:GetSpecialValueFor("bonus_haste")))/100
	end
	return 1
end

--------------------------------------------------------------------------------