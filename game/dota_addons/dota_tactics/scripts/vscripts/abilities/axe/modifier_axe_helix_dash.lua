modifier_axe_helix_dash = class({})

--------------------------------------------------------------------------------

function modifier_axe_helix_dash:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_axe_helix_dash:OnCreated( kv )

	self.dash_start = self:GetParent():GetAbsOrigin()
	self.dash_end = Vector(kv.dash_destination_x,kv.dash_destination_y,0)
	self.dash_speed = self:GetAbility():GetSpecialValueFor("dash_range")/30 
	self.distance_traveled = 0
	self.hit = kv.hit or false	

	if IsServer() then
		AddAnimationTranslate(self:GetParent(), "forcestaff_friendly")
		StartAnimation(self:GetCaster(), {duration=1, activity=ACT_DOTA_FLAIL})
		self.dash_speed = self.dash_speed / PLAYER_TIME_SIMULTANIOUS * 3/2
		
		local nFXIndex = ParticleManager:CreateParticle( "particles/generic_gameplay/rune_haste.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		self:AddParticle( nFXIndex, false, false, -1, false, false )		
	
		self:StartIntervalThink( 1/30 )
		self:OnIntervalThink()
	end
end

--------------------------------------------------------------------------------

function modifier_axe_helix_dash:OnIntervalThink()
	if IsServer() then

		local vectorDelta=self.dash_end-self.dash_start
		vectorDelta.z=0

		if self.distance_traveled < vectorDelta:Length2D() then
			self:GetParent():SetAbsOrigin(self:GetParent():GetAbsOrigin() + vectorDelta:Normalized() * self.dash_speed)
			self.distance_traveled = self.distance_traveled + (vectorDelta:Normalized() * self.dash_speed):Length2D()
		else
			local animationTime= 1/2;
			FindClearSpaceForUnit(self:GetParent(), self.dash_end, true)
			if self.hit==true or self.hit== 1 then
				StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_3})	
				self:Activate ( )
			else
				StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_FORCESTAFF_END})
			end
			RemoveAnimationTranslate(self:GetParent())
			self:Destroy()
		end
	end
end

--------------------------------------------------------------------------------

function modifier_axe_helix_dash:Activate ( )

		local helix_aoe = self:GetAbility():GetSpecialValueFor("helix_aoe")
		local helix_damage = self:GetAbility():GetSpecialValueFor("helix_damage")
		local helix_energy =  self:GetAbility():GetSpecialValueFor("helix_energy")
		local caster = self:GetCaster()
		local ability = self:GetAbility()
			EmitSoundOn( "Hero_Axe.CounterHelix", self:GetCaster() )
		
			local helix = ParticleManager:CreateParticle("particles/units/heroes/hero_axe/axe_attack_blur_counterhelix.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
			ParticleManager:SetParticleControl(helix, 0, self:GetParent():GetAbsOrigin())		
			ParticleManager:ReleaseParticleIndex(helix)	

							
					local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), self:GetCaster(),helix_aoe , DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
					if #enemies > 0 then
						for _,enemy in pairs(enemies) do						
							if enemy ~= nil and ( not enemy:IsInvulnerable() )then
						
									local direction = enemy:GetAbsOrigin()-self:GetCaster():GetAbsOrigin()
									direction.z=0
									local projectile = {		  			  
									vSpawnOrigin = self:GetParent():GetAbsOrigin()+Vector(0,0,30),
									fDistance = helix_aoe,
									fStartRadius = 50,
									fEndRadius = 50,
									fCollisionRadius = 30,
									  Source = self:GetCaster(),
									  vVelocity = direction:Normalized() * helix_aoe*10, 
									  UnitBehavior = PROJECTILES_NOTHING,
									  bMultipleHits = true,
									  bIgnoreSource = true,
									  TreeBehavior = PROJECTILES_NOTHING,
									  bTreeFullCollision = false,
									  WallBehavior = PROJECTILES_DESTROY,
									  GroundBehavior = PROJECTILES_DESTROY,
									  fGroundOffset = 0,
									  bZCheck = false,
									  bGroundLock = false,
									  draw = false,
									  bUseFindUnitsInRadius = true,
									bHitScan=true,

									  UnitTest = function(instance, unit) return unit==enemy end,
									  OnUnitHit = function(instance, unit) 
														
											local damage_table = {
												victim = unit,
												attacker = caster,
												damage = helix_damage,
												damage_type = DAMAGE_TYPE_MAGICAL,
												ability  = ability,
												cover_reduction = 1,
											}
									
											if IsTargetOverCover(instance,damage_table.attacker) then
												damage_table.cover_reduction = 1/2
											end

											Alert_Damage (damage_table)
											Alert_Mana ({unit=caster,manavalue=helix_energy, energy=true});
										
										
											local slowDuration = self:GetAbility():GetSpecialValueFor("slow_duration")
											if self:GetAbility():GetLevel()==3 then	
												slowDuration = self:GetAbility():GetSpecialValueFor("bonus_slow_duration")
											end
											unit:AddNewModifier(caster,ability,"generic_modifier_movespeed_slow",{value=self:GetAbility():GetSpecialValueFor("bonus_slow"),turns=slowDuration})
									  end,
									}						
									Projectiles:CreateProjectile(projectile)
														
							end
						end
					
						if self:GetAbility():GetLevel()==2 then	
							Alert_CooldownRefresh(self:GetCaster(),self:GetAbility(),-self:GetAbility():GetSpecialValueFor("bonus_refresh"))	
						end
					end	


										
				

end

--------------------------------------------------------------------------------

function modifier_axe_helix_dash:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_OVERRIDE_ANIMATION,
	}

	return funcs
end

--------------------------------------------------------------------------------

function modifier_axe_helix_dash:GetOverrideAnimation( params )
	return ACT_DOTA_FLAIL
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------