ability_axe_berserker_call = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_axe_berserker_selfheal","abilities/axe/modifier_axe_berserker_selfheal.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_regenerate","abilities/generic/generic_modifier_regenerate.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_axe_berserker_call:PrepAction(keys)
				
				
			self.berserk_max_regen = self:GetSpecialValueFor("berserk_max_regen")
			self.berserk_helix_aoe = self:GetSpecialValueFor("berserk_helix_aoe")
			self.berserk_helix_energy = self:GetSpecialValueFor("berserk_helix_energy")
			
			EmitSoundOn( "Hero_Axe.Berserkers_Call", self:GetCaster() )
			EmitSoundOn( "Hero_Axe.BerserkersCall.Start", self:GetCaster() )
	
	
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
				
			local arate = .9
			local animationTime = 24/30*PLAYER_TIME_MODIFIER/arate;		
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_OVERRIDE_ABILITY_1, rate = arate/PLAYER_TIME_MODIFIER})	
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_axe_berserker_selfheal",{})
			
			local particle = ParticleManager:CreateParticle("particles/units/heroes/hero_axe/axe_beserkers_call_owner.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster())
			ParticleManager:SetParticleControl(particle, 2, Vector(berserk_helix_aoe, berserk_helix_aoe, berserk_helix_aoe))
			ParticleManager:ReleaseParticleIndex(particle)
			
			Alert_Mana({unit = self:GetCaster(),manavalue = self:GetSpecialValueFor("berserk_energy"),energy = true})	
			
			if self:GetLevel()==3 then
				self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_shield",{life=self:GetSpecialValueFor("bonus_shield") , turns = 1})
			elseif self:GetLevel()==4 then
				self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_unstoppable",{turns = self:GetSpecialValueFor("bonus_unstoppable")})
			end
			
		return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_axe_berserker_call:GetPriority()
	return 1;
end

--------------------------------------------------------------------------------

function ability_axe_berserker_call:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_axe_berserker_call:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_axe_berserker_call:GetCastPoints()
	return 0;
end

--------------------------------------------------------------------------------

function ability_axe_berserker_call:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_axe_berserker_call:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_axe_berserker_call:GetConeArc(coneID,coneRange)
	if coneID ~= 0 then return 1 end
	return self:GetMaxRange(0)	*math.sin(self:GetSpecialValueFor("cleave_angle")*math.pi/180)
end

--------------------------------------------------------------------------------

function ability_axe_berserker_call:GetMinRange(coneID)	
	return self:GetSpecialValueFor("cleave_range")
end

--------------------------------------------------------------------------------

function ability_axe_berserker_call:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("cleave_range")
end

--------------------------------------------------------------------------------

function ability_axe_berserker_call:GetCastRange(coneID)	
	return self:GetSpecialValueFor("berserk_helix_aoe")
end

--------------------------------------------------------------------------------