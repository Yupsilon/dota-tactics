modifier_axe_battlehunger = class({})

--------------------------------------------------------------------------------

function modifier_axe_battlehunger:IsDebuff()
	return true
end

--------------------------------------------------------------------------------

function modifier_axe_battlehunger:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_axe_battlehunger:OnCreated( kv )
	
	local turnsDuration = kv.turns or 1
	self:SetStackCount(turnsDuration)
	self.lastHitTurn=0
	if IsServer() then
		
	
		local mFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_axe/axe_battle_hunger.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent() )
		self:AddParticle( mFXIndex, false, false, -1, false, false )		
	end
end

--------------------------------------------------------------------------------

function modifier_axe_battlehunger:DeclareFunctions()
	local funcs = {
		MODIFIER_EVENT_ON_TAKEDAMAGE ,
	}	

	return funcs
end

--------------------------------------------------------------------------------

function modifier_axe_battlehunger:OnTakeDamage ( params )
		if IsServer() then
				local hAttacker = params.attacker
				local hVictim = params.unit
				local currentTurn = GameMode:GetCurrentTurn()
				
			if hAttacker == self:GetParent() then
										
				if self.lastHitTurn<currentTurn and params.damage_type~=DAMAGE_TYPE_PURE then					
					--self.lastHitTurn=currentTurn
					--self:DecrementStackCount()						
					self:Destroy()
				end
			end
		end
end
	
--------------------------------------------------------------------------------

function modifier_axe_battlehunger:WalkMovementAction()
	
	if self:GetStackCount()>0 then	
											local damage_table = {
												victim = self:GetParent(),
												attacker = self:GetCaster(),
												damage = self:GetAbility():GetSpecialValueFor("hunger_damage"),
												damage_type = DAMAGE_TYPE_PURE,
												ability = self:GetAbility()
											}
															
									local hit_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_beastmaster/beastmaster_wildaxes_hit.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())									
									ParticleManager:SetParticleControl(hit_particle, 0, self:GetParent():GetAbsOrigin())
									ParticleManager:ReleaseParticleIndex(hit_particle)
		Alert_Damage( damage_table )
	end
		
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
