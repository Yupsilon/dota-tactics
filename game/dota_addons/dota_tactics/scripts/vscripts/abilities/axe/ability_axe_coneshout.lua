 ability_axe_coneshout = class({})
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_weak","abilities/generic/generic_modifier_weak.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_combobreaker","abilities/generic/generic_modifier_combobreaker.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_follow","mechanics/heroes/modifier_follow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_order_forcefollow","mechanics/heroes/modifier_order_forcefollow.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_axe_coneshout:SolveAction(keys)

	if keys.target_point_A~=nil then
				
			local cone_full_range = self:GetMaxRange(0)
			local projectile_radius = self:GetSpecialValueFor("projectile_radius")
			local projectile_angle = (self:GetSpecialValueFor("projectile_angle")*math.pi/180)/2
			local projectile_count = self:GetConeArc(1,0)
	
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,false)	
			Alert_Mana({unit=self:GetCaster(),manavalue=0-self:GetManaCost(self:GetLevel());} )
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			
			local startPoint = keys.caster:GetAbsOrigin() + Vector(0,0,50)*keys.caster:GetModelScale();		
			local direction = (keys.target_point_A-startPoint):Normalized()
			direction.z=0
			startPoint=startPoint+direction*projectile_radius/2
			
			local animationTime = 2 * PLAYER_TIME_MODIFIER;
			local decalSpeed = math.max(900,cone_full_range/PLAYER_TIME_MODIFIER)*2
			
			if self:GetLevel() == 4 then
				self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_shield",{life=self:GetSpecialValueFor("bonus_shield") , turns = self:GetSpecialValueFor("bonus_shield_duration")})	
			end
			
				keys.caster:FaceTowards(keys.target_point_A)
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_TAUNT, translate = "come_get_it", rate = 2/PLAYER_TIME_MODIFIER})
			
			--Timers:CreateTimer(animationTime/4,function()
	
			--EmitSoundOn( "Hero_Axe.Culling_Blade_Success", self:GetCaster() )
	
				local startangle = math.atan2(direction.x,direction.y)
	
				for i=0,projectile_count-1 do
					
					local fireAngle = startangle+projectile_angle*((projectile_count-1)/2-i)
					local newdirection = Vector(math.sin(fireAngle),math.cos(fireAngle),0)
				
					local nChainParticleFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_queenofpain/queen_scream_of_pain.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )			ParticleManager:SetParticleControl( nChainParticleFXIndex, 0, startPoint + Vector (0,0,100) + newdirection*cone_full_range )
					ParticleManager:SetParticleControl( nChainParticleFXIndex, 1, startPoint + Vector (0,0,100)  )
					ParticleManager:SetParticleControl( nChainParticleFXIndex, 2, Vector( decalSpeed, 0, 0 ) )
			
					local projectile = {
					  --EffectName = projectile_model,			  			  
					  vSpawnOrigin = startPoint,
					  fDistance = cone_full_range,
					  fStartRadius = projectile_radius,
					  fEndRadius = projectile_radius,
					  fCollisionRadius = 30,
					  Source = self:GetCaster(),
					  vVelocity = newdirection * decalSpeed, 
					  UnitBehavior = PROJECTILES_NOTHING,
					  bMultipleHits = false,
					  bIgnoreSource = true,
					  TreeBehavior = PROJECTILES_NOTHING,
					  bTreeFullCollision = false,
					  WallBehavior = PROJECTILES_DESTROY,
					  GroundBehavior = PROJECTILES_DESTROY,
					  fGroundOffset = 0,
					  bZCheck = false,
					  bGroundLock = false,
					  draw = false,
					  bUseFindUnitsInRadius = true,
					  bHitScan=false,

					  UnitTest = function(instance, unit) return unit:IsInvulnerable() == false and unit:GetTeamNumber() ~= keys.caster:GetTeamNumber() and unit:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())==nil end,
					  OnUnitHit = function(instance, unit) 
						
							self:OnProjectileHit(unit,instance:GetPosition())
							--EmitSoundOn( "Hero_DrowRanger.ProjectileImpact", unit )
						
					  end,
					  OnFinish = function(instance, pos)		  
						ParticleManager:DestroyParticle( nChainParticleFXIndex, false )
						ParticleManager:ReleaseParticleIndex( nChainParticleFXIndex )
								
					  end
					}
							
						
							
					Projectiles:CreateProjectile(projectile)
				end
			--end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_axe_coneshout:GetPriority()
	return 5;
end

--------------------------------------------------------------------------------

function ability_axe_coneshout:OnProjectileHit(hTarget, vLocation)
					
					if hTarget:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())~=nil then
						return
					else
						hTarget:AddNewModifier(self:GetCaster(),self,"modifier_hit",{turns=1})
					end
					
					local damage_table = {
							victim = hTarget,
							attacker = self:GetCaster(),
							damage = self:GetSpecialValueFor("cleave_damage_full") ,
							damage_type = DAMAGE_TYPE_MAGICAL,
							ability=self,
							cover_reduction = 1
						}
							
					
					if IsTargetPointOverCover(vLocation,damage_table.attacker) then
						damage_table.cover_reduction=1/2					
					end
				
					Alert_Damage(damage_table)												
					local ffollow = hTarget:AddNewModifier(self:GetCaster(),nil,"modifier_order_forcefollow",{  })			
					ffollow:SetFollowTarget(  self:GetCaster() )
					
					if self:GetLevel() == 3 then
						hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_weak", {value = self:GetSpecialValueFor("bonus_weaken"),turns = self:GetSpecialValueFor("bonus_weaken_duration"),applythisturn=0} )						
					end
end

--------------------------------------------------------------------------------

function ability_axe_coneshout:getAbilityType()
	return ABILITY_TYPE_CONE;
end

--------------------------------------------------------------------------------

function ability_axe_coneshout:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_axe_coneshout:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_axe_coneshout:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_axe_coneshout:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_axe_coneshout:GetConeArc(coneID,coneRange)
	local cone_angle = self:GetSpecialValueFor("cone_angle")
	if self:GetLevel()== 2 then cone_angle = self:GetSpecialValueFor("bonus_cone_angle") end
	
	if coneID == 1 then return cone_angle / self:GetSpecialValueFor("projectile_angle") end	
	return cone_angle
end

--------------------------------------------------------------------------------

function ability_axe_coneshout:GetMinRange(coneID)	
	if self:GetLevel()== 2 then return self:GetSpecialValueFor("bonus_cone_range") end
	return self:GetSpecialValueFor("cone_range")
end

--------------------------------------------------------------------------------

function ability_axe_coneshout:GetMaxRange(coneID)	
	if self:GetLevel()== 2 then return self:GetSpecialValueFor("bonus_cone_range") end
	return self:GetSpecialValueFor("cone_range")
end

--------------------------------------------------------------------------------

function ability_axe_coneshout:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_axe_coneshout:GetHasteBonus()	
	if self:GetLevel()==4 then			
		return (100+(self:GetSpecialValueFor("bonus_haste")))/100
	end
	return 1
end

--------------------------------------------------------------------------------