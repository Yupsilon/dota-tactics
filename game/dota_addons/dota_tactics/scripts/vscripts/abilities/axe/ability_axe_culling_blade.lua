ability_axe_culling_blade = class({})
LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_scarred","abilities/generic/generic_modifier_scarred.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_axe_culling_blade:PrepAction(keys)
	if self:GetLevel()==4 then
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_shield",{life=self:GetSpecialValueFor("bonus_shield") , turns = self:GetSpecialValueFor("bonus_shield_duration")})
		return MODIFIERACTION_SHORTPAUSE_SURVIVE
	end			
	return MODIFIERACTION_NOPAUSE_SURVIVE
end

--------------------------------------------------------------------------------

function ability_axe_culling_blade:SolveAction(keys)

	if keys.target_point_A~=nil then
				
			local cleave_range = self:GetSpecialValueFor("cleave_range")
			local cleave_radius = self:GetSpecialValueFor("cleave_radius")
			local cleave_damage = self:GetSpecialValueFor("cleave_damage_max")
	
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,false)	
			Alert_Mana({unit=self:GetCaster(),manavalue=0-self:GetManaCost(self:GetLevel())} )
			--self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			
			local startPoint = keys.caster:GetAbsOrigin() + Vector(0,0,50);	
			local direction = (keys.target_point_A-startPoint):Normalized()
			direction.z=0	

			startPoint = startPoint+direction*cleave_radius/2
			
			local arate=.9
			local animationTime = 25/30*PLAYER_TIME_MODIFIER/arate;
			local decalSpeed = 900
						   
				keys.caster:FaceTowards(keys.target_point_A)
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_4, rate = arate/PLAYER_TIME_MODIFIER})	
						  	
			
			Timers:CreateTimer(animationTime*.5, function() 

			local target = nil
	
				local projectile = {
				  --EffectName = projectile_model,	
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint,--{unit=hero, attach="attach_attack1", offset=Vector(0,0,0)},
				  fDistance = cleave_range,
				  fStartRadius = cleave_radius,
				  fEndRadius = cleave_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  --fExpireTime = 8.0,
				  vVelocity = direction * decalSpeed, 
				  UnitBehavior = PROJECTILES_DESTROY,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  --bCutTrees = true,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_DESTROY,
				  GroundBehavior = PROJECTILES_DESTROY,
				  fGroundOffset = 0,
				  --nChangeMax = 1,
				  --bRecreateOnChange = true,
				  bZCheck = false,
				  bGroundLock = false,
				  --bProvidesVision = true,
				  --iVisionRadius = 350,
				  --iVisionTeamNumber = keys.caster:GetTeam(),
				  --bFlyingVision = false,
				  --fVisionTickTime = .1,
				  --fVisionLingerDuration = 1,
				  draw = false,--             draw = {alpha=1, color=Vector(200,0,0)},
				  --iPositionCP = 0,
				  --iVelocityCP = 1,
				  --ControlPoints = {[5]=Vector(100,0,0), [10]=Vector(0,0,1)},
				  --ControlPointForwards = {[4]=hero:GetForwardVector() * -1},
				  --ControlPointOrientations = {[1]={hero:GetForwardVector() * -1, hero:GetForwardVector() * -1, hero:GetForwardVector() * -1}},
				  --[[ControlPointEntityAttaches = {[0]={
					unit = hero,
					pattach = PATTACH_ABSORIGIN_FOLLOW,
					attachPoint = "attach_attack1", -- nil
					origin = Vector(0,0,0)
				  }},]]
				  --fRehitDelay = .3,
				  --fChangeDelay = 1,
				  --fRadiusStep = 10,
				  bUseFindUnitsInRadius = true,
				  bHitScan=true,

				  UnitTest = function(instance, unit) return ( not unit:IsInvulnerable() ) and unit:GetTeamNumber() ~= keys.caster:GetTeamNumber() end,
				  OnUnitHit = function(instance, unit) 
					
					
					
										local damage_table = {
											victim = unit,
											attacker = self:GetCaster(),
											damage = math.min(unit:GetHealth()/2,cleave_damage),
											damage_type = DAMAGE_TYPE_PHYSICAL,
											cover_reduction = 1
										}
										target=unit
										
										if not (damage_table.damage>=cleave_damage) then																					
											--[[ if self:GetLevel()==3 then
												local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), self:GetCaster(), self:GetSpecialValueFor("bonus_haste_aoe"), DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
												if #enemies > 0 then
													for _,enemy in pairs(enemies) do
														if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
															
															enemy:AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_haste",{speed=self:GetSpecialValueFor("bonus_haste"),turns = self:GetSpecialValueFor("bonus_haste_duration"),applythisturn=1})
															
														end
													end
												end	
											end
										else]]
											self:GetCaster():GiveMana(self:GetSpecialValueFor("tresspass_energy"))
										end
										
										if self:GetLevel() == 4 and self:GetCaster():GetHealth()<=self:GetSpecialValueFor("bonus_tresspass") then
											damage_table.damage=damage_table.damage+self:GetSpecialValueFor("bonus_damage_tresspass")
										elseif self:GetLevel() == 2 then 
											damage_table.scar_damage_percent = 10 * 100;
										end
										Alert_Damage( damage_table )										
															
									local culling_kill_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_axe/axe_culling_blade_kill.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())
									ParticleManager:SetParticleControl(culling_kill_particle, 4, self:GetCaster():GetAbsOrigin())
									ParticleManager:ReleaseParticleIndex(culling_kill_particle)
					
				  end,
				  --OnTreeHit = function(self, tree) ... end,
				  --OnWallHit = function(self, gnvPos) ... end,
				  --OnGroundHit = function(self, groundPos) ... end,
				  OnFinish = function(instance, pos) 
						if target==nil then
							EmitSoundOn( "Hero_Axe.Culling_Blade_Fail", self:GetCaster() )
						else
							EmitSoundOn( "Hero_Axe.Culling_Blade_Success", self:GetCaster() )
						end
				  end
				}						
				Projectiles:CreateProjectile(projectile)
			end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_axe_culling_blade:GetPriority()
	return 1;
end

--------------------------------------------------------------------------------

function ability_axe_culling_blade:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_axe_culling_blade:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_axe_culling_blade:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_axe_culling_blade:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_axe_culling_blade:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_axe_culling_blade:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("cleave_radius")
end

--------------------------------------------------------------------------------

function ability_axe_culling_blade:GetMinRange(coneID)	
	return self:GetSpecialValueFor("cleave_range")
end

--------------------------------------------------------------------------------

function ability_axe_culling_blade:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("cleave_range")
end

--------------------------------------------------------------------------------

function ability_axe_culling_blade:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_axe_culling_blade:GetHasteBonus()	
	if self:GetLevel()==3 then			
		return (100+(self:GetSpecialValueFor("bonus_haste")))/100
	end
	return 1
end

--------------------------------------------------------------------------------