ability_axe_cleave_attack = class({})
LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_energy_regeneration","abilities/modifier_energy_regeneration.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_axe_cleave_attack:GetIntrinsicModifierName()
	return "modifier_energy_regeneration"
end

--------------------------------------------------------------------------------

function ability_axe_cleave_attack:GetDamageConversion()
	if self:GetLevel()==3 then
		return self:GetSpecialValueFor("bonus_energy_absorbtion")
	end
	return 0
end

--------------------------------------------------------------------------------

function ability_axe_cleave_attack:GetRegenerationValue()
	return self:GetSpecialValueFor("base_energy_regen")
end

--------------------------------------------------------------------------------

function ability_axe_cleave_attack:SolveAction(keys)

	if keys.target_point_A~=nil then				
				
				
			local cleave_range = self:GetSpecialValueFor("cleave_range")
			local cleave_angle = self:GetSpecialValueFor("cleave_angle")
			local cleave_damage = self:GetSpecialValueFor("cleave_damage")
			local cleave_close_range = self:GetSpecialValueFor("cleave_close_range")
			local cleave_far_damage = self:GetSpecialValueFor("cleave_far_damage")
				
			local startPoint = keys.caster:GetAbsOrigin()
			local direction = (keys.target_point_A-startPoint):Normalized()
			direction.z=0
	
			self.LastCastTurn = math.max(GameMode:GetCurrentTurn()+math.ceil(self:GetCooldown(self:GetLevel())),self.LastCastTurn or -1);
			self.firstHit = false			
			
			local arate=.8
			local animationTime = 35/30*PLAYER_TIME_MODIFIER/arate
				keys.caster:FaceTowards(keys.target_point_A)	
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_ATTACK, rate = arate/PLAYER_TIME_MODIFIER})	
			
			Timers:CreateTimer(animationTime*0.5, function() 
			
				EmitSoundOn( "Hero_Axe.Attack", self:GetCaster() )
				local nFXIndex = ParticleManager:CreateParticle( "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave_gods_strength_crit.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )

				
				ParticleManager:SetParticleControl( nFXIndex, 0, startPoint )
				ParticleManager:SetParticleControlForward( nFXIndex, 0, (keys.target_point_A-startPoint):Normalized());
				ParticleManager:SetParticleControl( nFXIndex, 2, startPoint )
				ParticleManager:SetParticleControl( nFXIndex, 3, startPoint )
				ParticleManager:SetParticleControl( nFXIndex, 4, startPoint )
				ParticleManager:SetParticleControl( nFXIndex, 5, startPoint )
				ParticleManager:ReleaseParticleIndex( nFXIndex )	

				local enemies = FindUnitsInCone({				
					caster = self:GetCaster(),
					startPoint = startPoint,
					startAngle = math.atan2(direction.x,direction.y),
					search_range = cleave_range,
					search_angle = cleave_angle,
					searchteam = DOTA_UNIT_TARGET_TEAM_ENEMY,
					searchtype = DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
					searchflags = 0
				})
				
				if #enemies > 0 then
					for _,enemy in pairs(enemies) do
						if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
									
								local enemy_direction = (enemy:GetAbsOrigin()-startPoint)
								local projectile = {
								EffectAttach = PATTACH_WORLDORIGIN,			  			  
								vSpawnOrigin = startPoint+Vector(0,0,30),
								fDistance = cleave_range,
								fStartRadius = 50,
								fEndRadius = 50,
								fCollisionRadius = 30,
								  Source = self:GetCaster(),
								  vVelocity = enemy_direction:Normalized() * cleave_range*10, 
								  UnitBehavior = PROJECTILES_NOTHING,
								  bMultipleHits = true,
								  bIgnoreSource = true,
								  TreeBehavior = PROJECTILES_NOTHING,
								  bTreeFullCollision = false,
								  WallBehavior = PROJECTILES_DESTROY,
								  GroundBehavior = PROJECTILES_DESTROY,
								  fGroundOffset = 0,
								  bZCheck = false,
								  bGroundLock = false,
								  draw = false,
								  bUseFindUnitsInRadius = true,
									bHitScan=true,

								  UnitTest = function(instance, unit) return unit==enemy end,
								  OnUnitHit = function(instance, unit) 
														
										local hit_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_beastmaster/beastmaster_wildaxes_hit.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())				
										ParticleManager:SetParticleControl(hit_particle, 0, unit:GetAbsOrigin())
										ParticleManager:ReleaseParticleIndex(hit_particle)
														
										local damage_table = {
											victim = enemy,
											attacker = self:GetCaster(),
											damage = cleave_damage,
											damage_type = DAMAGE_TYPE_MAGICAL,
											cover_reduction = 1,
										}
										if self.firstHit == false then
											self.firstHit = true
											damage_table.damage_type = DAMAGE_TYPE_PHYSICAL
										end
										
										if enemy_direction:Length()>cleave_close_range then
											damage_table.damage=cleave_far_damage
											if self:GetLevel() == 4 then
												damage_table.damage=damage_table.damage+self:GetSpecialValueFor("bonus_damage_close")
												--enemy:AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_slow",{turns=self:GetSpecialValueFor("bonus_slow_duration")})	
											end
										end
											
										if self:GetLevel()==2 and self:GetCaster():GetHealth()<=self:GetSpecialValueFor("bonus_tresspass") then
											damage_table.damage=damage_table.damage+self:GetSpecialValueFor("bonus_damage_tresspass")									
										end
										--if self:GetLevel()==4 then
										--	if not self:GetCaster():CanEntityBeSeenByMyTeam(enemy) then
										--		damage_table.damage=damage_table.damage+self:GetSpecialValueFor("bonus_damage")
										--	end
										--end
								
										if IsTargetOverCover(instance,damage_table.attacker) then
											damage_table.cover_reduction=1/2
										end

										Alert_Damage( damage_table )
										Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("cleave_energy"),energy=true});
									
								  end,
								}						
								Projectiles:CreateProjectile(projectile)
							
							end
						end
					end
				--end
			end)
	end
	return ABILITYACTION_COMPLETE_SHORT;
end

--------------------------------------------------------------------------------

function ability_axe_cleave_attack:getAbilityType()
	return ABILITY_TYPE_DOUBLE_CONE;
end

--------------------------------------------------------------------------------

function ability_axe_cleave_attack:IsDisabledBySilence()
	return false
end


--------------------------------------------------------------------------------

function ability_axe_cleave_attack:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_axe_cleave_attack:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_axe_cleave_attack:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_axe_cleave_attack:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_axe_cleave_attack:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_axe_cleave_attack:GetConeArc(coneID,coneRange)
	if coneID == 1 then return 5 end
	return self:GetSpecialValueFor("cleave_angle")
end

--------------------------------------------------------------------------------

function ability_axe_cleave_attack:GetMinRange(coneID)	
	if coneID == 1 then return self:GetSpecialValueFor("cleave_close_range") end
	return self:GetSpecialValueFor("cleave_range")
end

--------------------------------------------------------------------------------

function ability_axe_cleave_attack:GetMaxRange(coneID)	
	if coneID == 1 then return self:GetSpecialValueFor("cleave_close_range") end
	return self:GetSpecialValueFor("cleave_range")
end

--------------------------------------------------------------------------------

function ability_axe_cleave_attack:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------