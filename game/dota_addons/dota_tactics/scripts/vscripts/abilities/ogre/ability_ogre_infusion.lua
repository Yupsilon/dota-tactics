ability_ogre_infusion = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_scar_immunity","abilities/generic/generic_modifier_scar_immunity.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_ogre_infusion:PrepAction(keys)

	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
							   
	if keys.target_point_A~=nil then				
		self:CastInstance(keys.target_point_A)			
	end
	if keys.target_point_B~=nil and (self:GetCaster():HasModifier("modifier_ogre_multicast") or self:GetCastPoints()>1 ) then	
		Timers:CreateTimer(PLAYER_TIME_MODIFIER,function()	self:CastInstance(keys.target_point_B)
			EmitSoundOn( "Hero_OgreMagi.Fireblast.x2", self:GetCaster() )
			local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_ogre_magi/ogre_magi_multicast.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetCaster() )					
			ParticleManager:SetParticleControl( nFXIndex, 1, Vector(2,2,2) )	
			ParticleManager:ReleaseParticleIndex(nFXIndex)		end)
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_ogre_infusion:CastInstance(point)

	
	EmitSoundOn( "TI9_ConsumableInstrument.Drum.Hit", self:GetCaster() )
	local nFXDrums = ParticleManager:CreateParticle( "particles/models/items/beastmaster/beastmaster_taunt_ti9_drums.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetCaster() )
	
	local nFXCaster = ParticleManager:CreateParticle( "particles/econ/events/ti9/ti9_drums_musicnotes.vpcf", PATTACH_OVERHEAD_FOLLOW, enemy )
	ParticleManager:ReleaseParticleIndex( nFXCaster )	
			
	local animationTime = 25/30*PLAYER_TIME_MODIFIER;
	self:GetCaster():FaceTowards(point)		
	StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_GENERIC_CHANNEL_1, rate = 1/PLAYER_TIME_MODIFIER})
	
	Timers:CreateTimer(animationTime,function() ParticleManager:DestroyParticle(nFXDrums,false) ParticleManager:ReleaseParticleIndex( nFXDrums ) end)
				
			local projectile_range = self:GetMaxRange(0)
			local projectile_radius = self:GetSpecialValueFor("projectile_radius") 
			
			local startPoint = self:GetCaster():GetAbsOrigin() + Vector(0,0,50)*self:GetCaster():GetModelScale();	
			local direction = (point-startPoint):Normalized()
			direction.z=0		
			startPoint=startPoint+direction*projectile_radius/4
			
			local decalSpeed = 2400	
			
			Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=self:GetSpecialValueFor("self_heal")})
			Alert_Mana({unit=self:GetCaster(),  manavalue=self:GetSpecialValueFor("projectile_energy") , energy=true});
			
			Timers:CreateTimer(animationTime*1/2, function() 

				local projectile = {
				  --EffectName = projectile_model,	
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint,--{unit=hero, attach="attach_attack1", offset=Vector(0,0,0)},
				  fDistance = projectile_range-projectile_radius*3/4,
				  fStartRadius = projectile_radius,
				  fEndRadius = projectile_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  --fExpireTime = 8.0,
				  vVelocity = direction * decalSpeed, 
				  UnitBehavior = PROJECTILES_NOTHING,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  --bCutTrees = true,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_DESTROY,
				  GroundBehavior = PROJECTILES_DESTROY,
				  fGroundOffset = 0,
				  --nChangeMax = 1,
				  --bRecreateOnChange = true,
				  bZCheck = false,
				  bGroundLock = false,
				  draw = false,
				  bUseFindUnitsInRadius = true,
				  bHitScan = true,

				  UnitTest = function(instance, unit) return unit~= self:GetCaster() and unit:IsInvulnerable() == false and unit:GetTeamNumber() == self:GetCaster():GetTeamNumber() end,
				  OnUnitHit = function(instance, unit) 
					
						self:OnProjectileHit(unit,instance:GetPosition())						
						
							local nFXIndex = ParticleManager:CreateParticle( "particles/econ/events/ti9/ti9_drums_musicnotes.vpcf", PATTACH_OVERHEAD_FOLLOW, unit )
							ParticleManager:ReleaseParticleIndex( nFXIndex )	
				
							EmitSoundOn( "TI9_ConsumableInstrument.Drum.Hit", unit )
					
				  end,
				 --[[OnIntervalThink = function(instance)
						ParticleManager:SetParticleControl( self.nChainParticleFXIndex, 1, instance:GetPosition() )
				  end,
				  OnFinish = function(instance, pos)		  
						
				  end]]
				  --OnTreeHit = function(self, tree) ... end,
				  --OnWallHit = function(self, gnvPos) ... end,
				  --OnGroundHit = function(self, groundPos) ... end,
				  --OnFinish = function(self, pos) ... end,
				}						
				Projectiles:CreateProjectile(projectile)
			end)
end


--------------------------------------------------------------------------------

function ability_ogre_infusion:OnProjectileHit(hTarget, vLocation)
																										
				EmitSoundOn( "Hero_OgreMagi.Bloodlust.Target", self:GetCaster() )
					
				local heal = self:GetSpecialValueFor("base_heal")
					
					if hTarget:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())~=nil or (self:GetLevel()==3 and self:GetCaster():HasModifier("modifier_ogre_multicast")) then
						hTarget:AddNewModifier(self:GetCaster(),self,"generic_modifier_unstoppable",{turns=self:GetSpecialValueFor("unstoppable_duration")})	
						if self:GetLevel() == 2 then
							self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_scar_immunity", { turns = 1 } )
						end
						if hTarget:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())~=nil then
							heal = heal/2
						end
					else
						hTarget:AddNewModifier(self:GetCaster(),self,"modifier_hit",{turns=1})
							
						
						--if self:GetLevel()==4 then
							--enemy:AddNewModifier( self:GetCaster(), self, "generic_modifier_splash_resistance", {value = self:GetSpecialValueFor("bonus_resistance") ,turns = self:GetSpecialValueFor("shield_duration") } )
							--hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_shield", {life= self:GetSpecialValueFor("shield_life") , turns = self:GetSpecialValueFor("shield_duration")} )
						--end
					end
					
					if self:GetLevel()==2 then
						Alert_HealScars({unit = self:GetCaster(), value = self:GetSpecialValueFor("bonus_healing_perma")})	
					elseif self:GetLevel() == 4 then
								local overheal = heal
						local diff_life = hTarget:GetMaxHealth() - hTarget:GetHealth()
						
						if (diff_life>overheal) then
							Alert_Heal({caster=self:GetCaster(),target=hTarget,value=overheal})
						else
						
							hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_shield", {life= diff_life-overheal , turns = self:GetSpecialValueFor("overheal_duration")} )
							Alert_Heal({caster=self:GetCaster(),target=hTarget,value=diff_life})
						
						end
					else
						Alert_Heal({caster=self:GetCaster(),target=hTarget,value=heal})
					end
					

					Alert_Mana({unit=self:GetCaster(),  manavalue=self:GetSpecialValueFor("projectile_energy") , energy=true});
end

--------------------------------------------------------------------------------

function ability_ogre_infusion:getAbilityType()
	return ABILITY_TYPE_POINT_MULTIPLE;
end

--------------------------------------------------------------------------------

function ability_ogre_infusion:GetPriority()
	return 2;
end

--------------------------------------------------------------------------------

function ability_ogre_infusion:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_ogre_infusion:RequiresHitScanCheck()
	return true;
end

--------------------------------------------------------------------------------

function ability_ogre_infusion:GetCastPoints()

	for _,modifierorder in pairs(self:GetCaster():FindAllModifiers()) do	

				if modifierorder:GetAbility()~=nil and modifierorder:GetName() == "modifier_order_cast" and modifierorder:GetAbility():GetAbilityName()=="ability_ogre_multicast" then
					return 2
				end
	end
	return 1;
end

--------------------------------------------------------------------------------

function ability_ogre_infusion:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_ogre_infusion:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_ogre_infusion:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_ogre_infusion:GetMinRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_ogre_infusion:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_ogre_infusion:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------