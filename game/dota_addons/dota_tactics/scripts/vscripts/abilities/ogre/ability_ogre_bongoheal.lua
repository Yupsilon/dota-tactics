ability_ogre_bongoheal = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_splash_resistance","abilities/generic/generic_modifier_splash_resistance.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_ogre_bongoheal:PrepAction(keys)

	EmitSoundOn( "TI9_ConsumableInstrument.Drum.Hit", self:GetCaster() )
	local nFXDrums = ParticleManager:CreateParticle( "particles/models/items/beastmaster/beastmaster_taunt_ti9_drums.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetCaster() )
	ParticleManager:ReleaseParticleIndex( nFXDrums )
	local nFXCaster = ParticleManager:CreateParticle( "particles/econ/events/ti9/ti9_drums_musicnotes.vpcf", PATTACH_OVERHEAD_FOLLOW, enemy )
	ParticleManager:ReleaseParticleIndex( nFXCaster )	
			
	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;			
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			
	local animationTime = 2*PLAYER_TIME_MODIFIER;			
	StartAnimation(self:GetCaster(), {duration=animationTime, rate = 1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_GENERIC_CHANNEL_1})
	Timers:CreateTimer(animationTime,function() ParticleManager:DestroyParticle(nFXDrums,false) end)
	
	local total_healing_self = self:GetSpecialValueFor("total_healing_self")
	local total_healing_other = self:GetSpecialValueFor("total_healing_other")
	
	if self:GetLevel() == 2 then
	
		self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_movespeed_haste", {value = self:GetSpecialValueFor("bonus_haste") ,turns = self:GetSpecialValueFor("bonus_haste_duration") , nodraw=0} )
	
	elseif self:GetLevel() == 3 then
		
		total_healing_other = self:GetSpecialValueFor("bonus_healing_self")
		total_healing_self = self:GetSpecialValueFor("bonus_healing_other")
	end
	
	Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=total_healing_self})
	local energygain = self:GetSpecialValueFor("other_energy");
	
	local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), self:GetCaster(), self:GetSpecialValueFor("heal_aoe"), DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO, 0, FIND_CLOSEST, false )
	
	local timerint = 1;
	if #enemies > 0 then
		for _,enemy in pairs(enemies) do
			if enemy ~= nil and ( not enemy:IsInvulnerable() )  then
						energygain = energygain + self:GetSpecialValueFor("other_energy")
						Alert_Heal({caster=self:GetCaster(),target=enemy,value=total_healing_other})
						if self:GetLevel() == 4 then
							enemy:AddNewModifier( self:GetCaster(), self, "generic_modifier_splash_resistance", {value = self:GetSpecialValueFor("bonus_resistance") ,turns = self:GetSpecialValueFor("bonus_resistance_duration") } )
						end
						Timers:CreateTimer(timerint * animationTime/4, function()
						
							local nFXIndex = ParticleManager:CreateParticle( "particles/econ/events/ti9/ti9_drums_musicnotes.vpcf", PATTACH_OVERHEAD_FOLLOW, enemy )
							ParticleManager:ReleaseParticleIndex( nFXIndex )	
				
							EmitSoundOn( "TI9_ConsumableInstrument.Drum.Hit", enemy )
						end)
					end
				end
			end	
	
	
	Alert_Mana({unit=self:GetCaster(),manavalue=energygain, energy=true} )
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_ogre_bongoheal:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_ogre_bongoheal:GetPriority()
	return 1;
end

--------------------------------------------------------------------------------

function ability_ogre_bongoheal:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("heal_aoe") 
end

--------------------------------------------------------------------------------

function ability_ogre_bongoheal:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function ability_ogre_bongoheal:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_ogre_bongoheal:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_ogre_bongoheal:GetCastRange()	
	return self:GetMaxRange(0)
end


--------------------------------------------------------------------------------