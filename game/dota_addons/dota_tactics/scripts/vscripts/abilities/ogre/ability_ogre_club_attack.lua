ability_ogre_club_attack = class({})
LinkLuaModifier( "generic_modifier_stun","abilities/generic/generic_modifier_stun.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_energy_regeneration","abilities/modifier_energy_regeneration.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_scarred","abilities/generic/generic_modifier_scarred.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_ogre_club_attack:GetIntrinsicModifierName()
	return "modifier_energy_regeneration"
end

--------------------------------------------------------------------------------

function ability_ogre_club_attack:IsDisabledBySilence()
	return false
end


--------------------------------------------------------------------------------

function ability_ogre_club_attack:GetDamageConversion()
	return 0
end

--------------------------------------------------------------------------------

function ability_ogre_club_attack:GetRegenerationValue()	
	if self:GetLevel() == 3 then
		return self:GetSpecialValueFor("base_energy_regen")+self:GetSpecialValueFor("bonus_energy_regen")
	end
	return self:GetSpecialValueFor("base_energy_regen")
end

--------------------------------------------------------------------------------

function ability_ogre_club_attack:SolveAction(keys)

	if keys.target_point_A~=nil then				
				
				
			local cleave_range = self:GetSpecialValueFor("cleave_range")
			local cleave_radius = self:GetSpecialValueFor("cleave_radius")
			local cleave_damage = self:GetSpecialValueFor("cleave_damage")
			local first_hit = false
			local arate=.6
				
			local startPoint = keys.caster:GetAbsOrigin()
			local direction = (keys.target_point_A-startPoint):Normalized()
			direction.z=0
			local centerangle = math.atan2(direction.x,direction.y)
	
			self.LastCastTurn = math.max(GameMode:GetCurrentTurn()+math.ceil(self:GetCooldown(self:GetLevel())),self.LastCastTurn or -1);			
			local animationTime = 26/30*PLAYER_TIME_MODIFIER/arate;
						   
				keys.caster:FaceTowards(keys.target_point_A)	
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_ATTACK,rate = arate/PLAYER_TIME_MODIFIER})	
	
			for index,modifier in pairs(self:GetCaster():FindAllModifiersByName("modifier_order_cast")) do	
				if modifier:GetAbility():GetAbilityName() == "ability_ogre_multicast" then
					modifier:Destroy();
				end
			end
			
			EmitSoundOn( "Hero_OgreMagi.PreAttack", self:GetCaster() )
			Timers:CreateTimer(animationTime*.5, function() 
			
			EmitSoundOn( "Hero_OgreMagi.Attack", self:GetCaster() )
				local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_tiny/tiny_craggy_cleave_ring.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )

				
				--ParticleManager:SetParticleControl( nFXIndex, 0, startPoint )
				
				local faceAngle = 30;
				
				ParticleManager:SetParticleControl( nFXIndex, 1, Vector(0,faceAngle,0));
				--ParticleManager:SetParticleControlForward( nFXIndex, 0, (keys.target_point_A-startPoint):Normalized());
				--ParticleManager:SetParticleControl( nFXIndex, 2, startPoint )
				--ParticleManager:SetParticleControl( nFXIndex, 3, startPoint )
				--ParticleManager:SetParticleControl( nFXIndex, 4, startPoint )
				--ParticleManager:SetParticleControl( nFXIndex, 5, startPoint )
				ParticleManager:ReleaseParticleIndex( nFXIndex )	
	
				local projectile = {
				  --EffectName = projectile_model,	
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint,--{unit=hero, attach="attach_attack1", offset=Vector(0,0,0)},
				  fDistance = cleave_range,
				  fStartRadius = cleave_radius,
				  fEndRadius = cleave_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  --fExpireTime = 8.0,
				  vVelocity = direction * 900, 
				  UnitBehavior = PROJECTILES_NOTHING,
				  bMultipleHits = true,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  --bCutTrees = true,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_DESTROY,
				  GroundBehavior = PROJECTILES_DESTROY,
				  fGroundOffset = 0,
				  --nChangeMax = 1,
				  --bRecreateOnChange = true,
				  bZCheck = false,
				  bGroundLock = false,
				  --bProvidesVision = true,
				  --iVisionRadius = 350,
				  --iVisionTeamNumber = keys.caster:GetTeam(),
				  --bFlyingVision = false,
				  --fVisionTickTime = .1,
				  --fVisionLingerDuration = 1,
				  draw = false,--             draw = {alpha=1, color=Vector(200,0,0)},
				  --iPositionCP = 0,
				  --iVelocityCP = 1,
				  --ControlPoints = {[5]=Vector(100,0,0), [10]=Vector(0,0,1)},
				  --ControlPointForwards = {[4]=hero:GetForwardVector() * -1},
				  --ControlPointOrientations = {[1]={hero:GetForwardVector() * -1, hero:GetForwardVector() * -1, hero:GetForwardVector() * -1}},
				  --[[ControlPointEntityAttaches = {[0]={
					unit = hero,
					pattach = PATTACH_ABSORIGIN_FOLLOW,
					attachPoint = "attach_attack1", -- nil
					origin = Vector(0,0,0)
				  }},]]
				  --fRehitDelay = .3,
				  --fChangeDelay = 1,
				  --fRadiusStep = 10,
				  bUseFindUnitsInRadius = true,
				  bHitScan=true,

				  UnitTest = function(instance, unit) return unit:IsInvulnerable() == false and unit:GetTeamNumber() ~= keys.caster:GetTeamNumber() end,
				  OnUnitHit = function(instance, unit) 
															
									local hit_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_spirit_breaker/spirit_breaker_greater_bash.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())
									ParticleManager:SetParticleControl(hit_particle, 0, unit:GetAbsOrigin())
									ParticleManager:ReleaseParticleIndex(hit_particle)
					
										local damage_table = {
											victim = unit,
											attacker = self:GetCaster(),
											damage = cleave_damage,
											damage_type = DAMAGE_TYPE_MAGICAL,
											cover_reduction = 1,
										}
										
										if IsTargetOverCover(instance,damage_table.attacker) then
											damage_table.cover_reduction=1/2
										end
										unit:AddNewModifier(self:GetCaster(),self,"generic_modifier_stun",{turns = self:GetSpecialValueFor("bonus_ability_refresh")})
										
										if self:GetLevel()==2 then
											damage_table.scar_damage_direct = self:GetSpecialValueFor("bonus_club_scar")
										elseif self:GetLevel()==4 and not first_hit then
											self:GetCaster():GetAbilityByIndex(1).LastCastTurn=self:GetCaster():GetAbilityByIndex(1):GetLastCastTurn()-self:GetSpecialValueFor("bonus_ability_refresh")
											self:GetCaster():GetAbilityByIndex(1):StartCooldown(self:GetCaster():GetAbilityByIndex(1):GetCooldownTimeRemaining())	
											first_hit=true
										end

										Alert_Damage( damage_table )
										Alert_Mana({unit = self:GetCaster(),manavalue = self:GetSpecialValueFor("cleave_energy"), energy=true});
					
			EmitSoundOn( "Hero_OgreMagi.Attack.Impact", self:GetCaster() )
				  end
				}						
				Projectiles:CreateProjectile(projectile)
			end)
			
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_ogre_club_attack:GetPriority()
	return 3;
end


--------------------------------------------------------------------------------

function ability_ogre_club_attack:getAbilityType()
	return ABILITY_TYPE_POINT_MULTIPLE;
end

--------------------------------------------------------------------------------

function ability_ogre_club_attack:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_ogre_club_attack:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_ogre_club_attack:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_ogre_club_attack:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_ogre_club_attack:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("cleave_radius")
end

--------------------------------------------------------------------------------

function ability_ogre_club_attack:GetMinRange(coneID)	
	return self:GetSpecialValueFor("cleave_range")
end

--------------------------------------------------------------------------------

function ability_ogre_club_attack:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("cleave_range")
end

--------------------------------------------------------------------------------

function ability_ogre_club_attack:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------