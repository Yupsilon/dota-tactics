ability_ogre_blood_lust = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_might","abilities/generic/generic_modifier_might.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_ogre_blood_lust:PrepAction(keys)

	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
							   
	if keys.target_point_A~=nil then				
		self:CastInstance(keys.target_point_A)			
	end
	if keys.target_point_B~=nil and (self:GetCaster():HasModifier("modifier_ogre_multicast") or self:GetCastPoints()>1 ) then	
		Timers:CreateTimer(PLAYER_TIME_MODIFIER,function()	self:CastInstance(keys.target_point_B)
			EmitSoundOn( "Hero_OgreMagi.Fireblast.x2", self:GetCaster() )
			local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_ogre_magi/ogre_magi_multicast.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetCaster() )					
			ParticleManager:SetParticleControl( nFXIndex, 1, Vector(2,2,2) )	
			ParticleManager:ReleaseParticleIndex(nFXIndex)		end)
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_ogre_blood_lust:CastInstance(point)

			local animationTime = 25/30*PLAYER_TIME_MODIFIER;
	self:GetCaster():FaceTowards(point)		
	StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_3, rate = 1/PLAYER_TIME_MODIFIER})
	
				EmitSoundOn( "Hero_OgreMagi.Bloodlust.Cast", self:GetCaster() )
				
			local projectile_range = self:GetSpecialValueFor("projectile_range")
			local projectile_radius = self:GetSpecialValueFor("projectile_radius") 
			
			local startPoint = self:GetCaster():GetAbsOrigin() + Vector(0,0,50)*self:GetCaster():GetModelScale();	
			local direction = (point-startPoint):Normalized()
			direction.z=0		
			startPoint=startPoint+direction*projectile_radius/2
			
			local decalSpeed = 2400	
						   
			
			Timers:CreateTimer(animationTime*1/2, function() 

			
			
	
				local projectile = {
				  --EffectName = projectile_model,	
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint,--{unit=hero, attach="attach_attack1", offset=Vector(0,0,0)},
				  fDistance = projectile_range,
				  fStartRadius = projectile_radius,
				  fEndRadius = projectile_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  --fExpireTime = 8.0,
				  vVelocity = direction * decalSpeed, 
				  UnitBehavior = PROJECTILES_DESTROY,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  --bCutTrees = true,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_DESTROY,
				  GroundBehavior = PROJECTILES_DESTROY,
				  fGroundOffset = 0,
				  --nChangeMax = 1,
				  --bRecreateOnChange = true,
				  bZCheck = false,
				  bGroundLock = false,
				  draw = false,
				  bUseFindUnitsInRadius = true,
				  bHitScan = true,

				  UnitTest = function(instance, unit) return unit~= self:GetCaster() and unit:IsInvulnerable() == false and unit:GetTeamNumber() == self:GetCaster():GetTeamNumber() end,
				  OnUnitHit = function(instance, unit) 
					
						self:OnProjectileHit(unit,instance:GetPosition())						
						
						self.nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_ogre_magi/ogre_magi_bloodlust_cast.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )

						ParticleManager:SetParticleControl( self.nFXIndex, particleID or 2, unit:GetAbsOrigin() )						
						ParticleManager:SetParticleControl( self.nFXIndex, particleID or 3, unit:GetAbsOrigin() )	
						
						ParticleManager:ReleaseParticleIndex(self.nFXIndex)
					
				  end,
				 --[[OnIntervalThink = function(instance)
						ParticleManager:SetParticleControl( self.nChainParticleFXIndex, 1, instance:GetPosition() )
				  end,
				  OnFinish = function(instance, pos)		  
						
				  end]]
				  --OnTreeHit = function(self, tree) ... end,
				  --OnWallHit = function(self, gnvPos) ... end,
				  --OnGroundHit = function(self, groundPos) ... end,
				  --OnFinish = function(self, pos) ... end,
				}						
				Projectiles:CreateProjectile(projectile)
			end)
end


--------------------------------------------------------------------------------

function ability_ogre_blood_lust:OnProjectileHit(hTarget, vLocation)
																				
				EmitSoundOn( "Hero_OgreMagi.Bloodlust.Target", self:GetCaster() )
					
					local heal = self:GetSpecialValueFor("target_heal")
					if hTarget:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())~=nil or (self:GetLevel()==4 and self:GetCaster():HasModifier("modifier_ogre_multicast")) then
						--hTarget:AddNewModifier(self:GetCaster(),self,"generic_modifier_unstoppable",{turns=self:GetSpecialValueFor("bonus_unstoppable_duration")})
							for i=0, 5 do
								Alert_CooldownRefresh(hTarget,i,-self:GetSpecialValueFor("overcast_ability_refresh"))	
							end
						if hTarget:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())~=nil then
							heal = heal/2
						end
					else
						hTarget:AddNewModifier(self:GetCaster(),self,"modifier_hit",{turns=1})
							
						local might_modifier = hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_might", {value= self:GetSpecialValueFor("bloodlust_strength") , turns = self:GetSpecialValueFor("bloodlust_duration") , nodraw=1} )
					
						if self:GetLevel()==2 then
						
							local haste_modifier = hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_movespeed_haste", {value= self:GetSpecialValueFor("bloodlust_speed") , turns = self:GetSpecialValueFor("bloodlust_duration") , nodraw=1} )
						end
						
						local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_ogre_magi/ogre_magi_bloodlust_buff.vpcf", PATTACH_ABSORIGIN_FOLLOW, hTarget )
						might_modifier:AddParticle( nFXIndex, false, false, -1, false, false )	
					end
									
						if self:GetLevel()==3 then
							--hTarget:AddNewModifier(self:GetCaster(),self,"generic_modifier_energized",{
							--	energy=self:GetSpecialValueFor("bonus_energy_boost"),
							--	turns=self:GetSpecialValueFor("bonus_energy_duration"),
							--	nodraw=1
							--})
							--Alert_Mana({unit=hTarget,  manavalue=self:GetSpecialValueFor("bonus_energy_boost") , energy=true});
							Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=self:GetSpecialValueFor("bonus_selfheal")})
						end
					
						Alert_Heal({caster=self:GetCaster(),target=hTarget,value=heal})
						--hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_shield", {life= self:GetSpecialValueFor("shield_laif") , turns = self:GetSpecialValueFor("shield_duration") , nodraw=1} )

					Alert_Mana({unit=self:GetCaster(),  manavalue=self:GetSpecialValueFor("projectile_energy") , energy=true});
end

--------------------------------------------------------------------------------

function ability_ogre_blood_lust:getAbilityType()
	return ABILITY_TYPE_POINT_MULTIPLE;
end

--------------------------------------------------------------------------------

function ability_ogre_blood_lust:GetPriority()
	return 2;
end

--------------------------------------------------------------------------------

function ability_ogre_blood_lust:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_ogre_blood_lust:RequiresHitScanCheck()
	return true;
end

--------------------------------------------------------------------------------

function ability_ogre_blood_lust:GetCastPoints()

	for _,modifierorder in pairs(self:GetCaster():FindAllModifiers()) do	

				if modifierorder:GetAbility()~=nil and modifierorder:GetName() == "modifier_order_cast" and modifierorder:GetAbility():GetAbilityName()=="ability_ogre_multicast" then
					return 2
				end
	end
	return 1;
end

--------------------------------------------------------------------------------

function ability_ogre_blood_lust:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_ogre_blood_lust:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_ogre_blood_lust:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_ogre_blood_lust:GetMinRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_ogre_blood_lust:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_ogre_blood_lust:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------