ability_ogre_ignite = class({})
LinkLuaModifier( "modifier_ogre_ignite_caster","abilities/ogre/modifier_ogre_ignite_caster.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ogre_ignite_target","abilities/ogre/modifier_ogre_ignite_target.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_weak","abilities/generic/generic_modifier_weak.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_ogre_ignite:SolveAction(keys)

	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})

	local animationTime = PLAYER_TIME_MODIFIER;		
	keys.caster:FaceTowards(keys.target_point_A)
	StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_2, rate = 1/PLAYER_TIME_MODIFIER})	

	
	if keys.target_point_A~=nil then
		self:CastInstance(keys.target_point_A)
	end
	if keys.target_point_B~=nil and self:GetCaster():HasModifier("modifier_ogre_multicast") then
			
			EmitSoundOn( "Hero_OgreMagi.Fireblast.x2", self:GetCaster() )
			local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_ogre_magi/ogre_magi_multicast.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetCaster() )					
			ParticleManager:SetParticleControl( nFXIndex, 1, Vector(2,2,2) )	
			ParticleManager:ReleaseParticleIndex(nFXIndex)	
			
		self:CastInstance(keys.target_point_B)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_ogre_ignite:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_ogre_ignite:CastInstance(point)

	   --if self:GetLevel()==2 then
			local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), point, self:GetCaster(), self:GetSpecialValueFor("ignite_aoe"), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
			if #enemies > 0 then
				for _,enemy in pairs(enemies) do
					if enemy ~= nil and ( not enemy:IsInvulnerable() )  then
						
						if (self:GetLevel()==2 and self:GetCaster():HasModifier("modifier_ogre_multicast")) or enemy:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())~=nil then
							
							enemy:AddNewModifier(self:GetCaster(),self,"generic_modifier_weak",{
								value=self:GetSpecialValueFor("bonus_weaken_duration"),
								turns=self:GetSpecialValueFor("bonus_weaken_strength"),
								applythisturn=0
							})
						else
							enemy:AddNewModifier(self:GetCaster(),self,"modifier_hit",{})
						end
					end
				end
			end	
		--end

	Timers:CreateTimer(20/30*PLAYER_TIME_MODIFIER,function()
	EmitSoundOn( "Hero_OgreMagi.Ignite.Cast", self:GetCaster() )			

	local mod_duration = self:GetSpecialValueFor("ignite_duration")
	if self:GetLevel() == 3 then
		mod_duration = self:GetSpecialValueFor("ignite_duration")+self:GetSpecialValueFor("bonus_ignite_duration")
	end
	
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ogre_ignite_caster",{turns = mod_duration, point_x=point.x, point_y=point.y})					
		
		--if not self:GetCaster():HasModifier("modifier_ogre_multicast") then
			Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("ignite_energy_caster"),energy=true});				
		--end																			
	end)
end

--------------------------------------------------------------------------------

function ability_ogre_ignite:GetAOERadius()
	return self:GetSpecialValueFor( "ignite_aoe" )
end

--------------------------------------------------------------------------------

function ability_ogre_ignite:getAbilityType()
	return ABILITY_TYPE_POINT_MULTIPLE;
end

--------------------------------------------------------------------------------

function ability_ogre_ignite:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_ogre_ignite:RequiresGridSnap()
	return true;
end

--------------------------------------------------------------------------------

function ability_ogre_ignite:GetCastPoints()

	for _,modifierorder in pairs(self:GetCaster():FindAllModifiers()) do	

				if modifierorder:GetAbility()~=nil and modifierorder:GetName() == "modifier_order_cast" and modifierorder:GetAbility():GetAbilityName()=="ability_ogre_multicast" then
					return 2
				end
	end
	return 1;
end

--------------------------------------------------------------------------------

function ability_ogre_ignite:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_ogre_ignite:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_ogre_ignite:GetConeArc(coneID,coneRange)
	return 100
end

--------------------------------------------------------------------------------

function ability_ogre_ignite:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_ogre_ignite:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("ignite_range")
end

--------------------------------------------------------------------------------

function ability_ogre_ignite:GetCastRange()	
	return self:GetSpecialValueFor("ignite_range")
end

--------------------------------------------------------------------------------