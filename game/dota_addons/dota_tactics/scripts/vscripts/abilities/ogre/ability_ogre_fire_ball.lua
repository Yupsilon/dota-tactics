ability_ogre_fire_ball = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_snare","abilities/generic/generic_modifier_snare.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_combobreaker","abilities/generic/generic_modifier_combobreaker.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_energy_regeneration","abilities/modifier_energy_regeneration.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_ogre_fire_ball:GetIntrinsicModifierName()
	return "modifier_energy_regeneration"
end

--------------------------------------------------------------------------------

function ability_ogre_fire_ball:IsDisabledBySilence()
	return false
end


--------------------------------------------------------------------------------

function ability_ogre_fire_ball:GetDamageConversion()
	return 0
end

--------------------------------------------------------------------------------

function ability_ogre_fire_ball:GetRegenerationValue()	
	if self:GetLevel() == 3 then
		return self:GetSpecialValueFor("base_energy_regen")+self:GetSpecialValueFor("bonus_energy_regen")
	end
	return self:GetSpecialValueFor("base_energy_regen")
end

--------------------------------------------------------------------------------

function ability_ogre_fire_ball:SolveAction(keys)

	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
	self.firstHit = false

	if keys.target_point_A~=nil then				
		self:CastInstance(keys.target_point_A)			
	end
	if keys.target_point_B~=nil and self:GetCaster():HasModifier("modifier_ogre_multicast") then				
		Timers:CreateTimer(PLAYER_TIME_MODIFIER,function()	self:CastInstance(keys.target_point_B)	
			EmitSoundOn( "Hero_OgreMagi.Fireblast.x2", self:GetCaster() )
			local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_ogre_magi/ogre_magi_multicast.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetCaster() )					
			ParticleManager:SetParticleControl( nFXIndex, 1, Vector(2,2,2) )	
			ParticleManager:ReleaseParticleIndex(nFXIndex)	end)		
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_ogre_fire_ball:GetPriority()
	return 3;
end


--------------------------------------------------------------------------------

function ability_ogre_fire_ball:CastInstance(point)

			local animationTime = PLAYER_TIME_MODIFIER;
			local delay = 0*PLAYER_TIME_MODIFIER;
			self:GetCaster():FaceTowards(point)		
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_1, rate = 1/PLAYER_TIME_MODIFIER})
				
			local projectile_range = self:GetSpecialValueFor("projectile_range")
			local projectile_radius = self:GetSpecialValueFor("projectile_radius") 
			
			local startPoint = self:GetCaster():GetAbsOrigin() + Vector(0,0,50)*self:GetCaster():GetModelScale();	
			local direction = (point-startPoint):Normalized()
			direction.z=0		
			startPoint=startPoint+direction*projectile_radius/2
									  
					local hit = false 
			
			Timers:CreateTimer(animationTime*.66, function() 
			
				EmitSoundOn( "Hero_OgreMagi.Fireblast.Cast", self:GetCaster() )								
				local projectile = {
				  --EffectName = projectile_model,	
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint,--{unit=hero, attach="attach_attack1", offset=Vector(0,0,0)},
				  fDistance = projectile_range-projectile_radius,
				  fStartRadius = projectile_radius,
				  fEndRadius = projectile_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  --fExpireTime = 8.0,
				  vVelocity = direction * 900, 
				  UnitBehavior = PROJECTILES_DESTROY,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  --bCutTrees = true,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_DESTROY,
				  GroundBehavior = PROJECTILES_DESTROY,
				  fGroundOffset = 0,
				  --nChangeMax = 1,
				  --bRecreateOnChange = true,
				  bZCheck = false,
				  bGroundLock = false,
				  --bProvidesVision = true,
				  --iVisionRadius = 350,
				  --iVisionTeamNumber = self:GetCaster():GetTeam(),
				  --bFlyingVision = false,
				  --fVisionTickTime = .1,
				  --fVisionLingerDuration = 1,
				  draw = false,--             draw = {alpha=1, color=Vector(200,0,0)},
				  --iPositionCP = 0,
				  --iVelocityCP = 1,
				  --ControlPoints = {[5]=Vector(100,0,0), [10]=Vector(0,0,1)},
				  --ControlPointForwards = {[4]=hero:GetForwardVector() * -1},
				  --ControlPointOrientations = {[1]={hero:GetForwardVector() * -1, hero:GetForwardVector() * -1, hero:GetForwardVector() * -1}},
				  --[[ControlPointEntityAttaches = {[0]={
					unit = hero,
					pattach = PATTACH_ABSORIGIN_FOLLOW,
					attachPoint = "attach_attack1", -- nil
					origin = Vector(0,0,0)
				  }},]]
				  --fRehitDelay = .3,
				  --fChangeDelay = 1,
				  --fRadiusStep = 10,
				  bUseFindUnitsInRadius = true,
				  bHitScan = true,

				  UnitTest = function(instance, unit) return unit:IsInvulnerable() == false and unit:GetTeamNumber() ~= self:GetCaster():GetTeamNumber() end,
				  OnUnitHit = function(instance, unit) 
					
						self:OnProjectileHit(unit,instance:GetPosition())	 
						
						Timers:CreateTimer(delay, function()
							local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_ogre_magi/ogre_magi_fireblast.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )
							ParticleManager:SetParticleControlEnt(nFXIndex, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_attack1 ", self:GetCaster():GetAbsOrigin(), true)	
							ParticleManager:SetParticleControl( nFXIndex, 1, unit:GetAbsOrigin() )
							ParticleManager:ReleaseParticleIndex( nFXIndex )	
						end)
						hit = true
					
				  end,
				 --[[OnIntervalThink = function(instance)
						ParticleManager:SetParticleControl( self.nChainParticleFXIndex, 1, instance:GetPosition() )
				  end,]]
				  OnFinish = function(instance, pos)		
						if hit == true then
							return
						end
						Timers:CreateTimer(delay, function()
							local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_ogre_magi/ogre_magi_fireblast.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )
							ParticleManager:SetParticleControl( nFXIndex, 1, Vector(9999,9999,9999) )
							ParticleManager:ReleaseParticleIndex( nFXIndex )	
						end)
				  end
				  --OnTreeHit = function(self, tree) ... end,
				  --OnWallHit = function(self, gnvPos) ... end,
				  --OnGroundHit = function(self, groundPos) ... end,
				  --OnFinish = function(self, pos) ... end,
				}						
				Projectiles:CreateProjectile(projectile)
			end)
end


--------------------------------------------------------------------------------

function ability_ogre_fire_ball:OnProjectileHit(hTarget, vLocation)
					
					local damage_table = {
							victim = hTarget,
							attacker = self:GetCaster(),
							damage = self:GetSpecialValueFor("projectile_damage") or 1,
							damage_type = DAMAGE_TYPE_PHYSICAL,
							ability=self,
							cover_reduction=1
						} 
						local energy = self:GetSpecialValueFor("projectile_energy")
						
				EmitSoundOn( "Hero_OgreMagi.Fireblast.Target", hTarget )	
							
						
						if self:GetLevel()==5 then
							damage_table.damage = self:GetSpecialValueFor("bonus_damage_upfront")
						end
					
					if hTarget:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())~=nil then
						damage_table.damage=self:GetSpecialValueFor("projectile_multi_damage")						
						
						if self:GetLevel()==5 then
							damage_table.damage = self:GetSpecialValueFor("bonus_damage_multiple")
						end
						
						if self:GetLevel()~=2 then
							hTarget:AddNewModifier(self:GetCaster(),self,"generic_modifier_snare",{								
								turns = self:GetSpecialValueFor("bonus_snare_duration")						
							})
						end
					else
						hTarget:AddNewModifier(self:GetCaster(),self,"modifier_hit",{turns=1})	
						
						if self.firstHit == false then
							self.firstHit = true
						else
							damage_table.damage_type = DAMAGE_TYPE_MAGICAL
						end														

						if self:GetLevel()==4 then
							self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_haste",{								
								turns = self:GetSpecialValueFor("bonus_slow_duration")	,
								slow = self:GetSpecialValueFor("bonus_slow_strength")
							})
						end
						
						if self:GetLevel()==2 and self:GetCaster():HasModifier("modifier_ogre_multicast") then
							hTarget:AddNewModifier(self:GetCaster(),self,"generic_modifier_snare",{								
								turns = self:GetSpecialValueFor("bonus_snare_duration")						
							})
						end
								
							damage_table.scar_damage_direct = self:GetSpecialValueFor("fireball_roast") 
						--hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_healing_reduced", {
						--	turns = self:GetSpecialValueFor("fireball_duration"),
						--	value = self:GetSpecialValueFor("fireball_roast")
						--} )	
					end
					
					if IsTargetPointOverCover(vLocation,damage_table.attacker) then
						damage_table.cover_reduction=1/2
										end
				
						Alert_Damage(damage_table)
						

					--if not self:GetCaster():HasModifier("modifier_ogre_multicast") then
						Alert_Mana({unit=self:GetCaster(), manavalue=energy, energy=true} );
					--end
end

--------------------------------------------------------------------------------

function ability_ogre_fire_ball:getAbilityType()
	return ABILITY_TYPE_POINT_MULTIPLE;
end

--------------------------------------------------------------------------------

function ability_ogre_fire_ball:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_ogre_fire_ball:RequiresHitScanCheck()
	return true;
end

--------------------------------------------------------------------------------

function ability_ogre_fire_ball:GetCastPoints()

	for _,modifierorder in pairs(self:GetCaster():FindAllModifiers()) do	

				if modifierorder:GetAbility()~=nil and modifierorder:GetName() == "modifier_order_cast" and modifierorder:GetAbility():GetAbilityName()=="ability_ogre_multicast" then
					return 2
				end
	end
				
	return 1;
end

--------------------------------------------------------------------------------

function ability_ogre_fire_ball:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_ogre_fire_ball:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_ogre_fire_ball:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_ogre_fire_ball:GetMinRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_ogre_fire_ball:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_ogre_fire_ball:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------