modifier_ogre_ignite_target = class({})

--------------------------------------------------------------------------------

function modifier_ogre_ignite_target:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_ogre_ignite_target:OnCreated( kv )
	
	if IsServer() then
	
		if self:GetCaster():GetTeamNumber() == self:GetParent():GetTeamNumber() then
		
			if self:GetAbility():GetLevel()==4 then
				Alert_Heal({caster=self:GetCaster(),target=self:GetParent(),value=self:GetAbility():GetSpecialValueFor("bonus_ignite_healing")})
			end
		else--if self:GetCaster():GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
	
			EmitSoundOn( "Hero_OgreMagi.Ignite.Damage", self:GetParent() )	
			Alert_Damage( {
											victim = self:GetParent(),
												attacker = self:GetCaster(),
												damage = self:GetAbility():GetSpecialValueFor("fire_damage"),
												damage_type = DAMAGE_TYPE_PURE,
												ability = self:GetAbility()
											} )	
		end
	
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_ogre_magi/ogre_magi_ignite_debuff.vpcf", PATTACH_CUSTOMORIGIN, self:GetParent() )
		self:AddParticle( nFXIndex, false, false, -1, false, false )		
	end
end
	
--------------------------------------------------------------------------------

function modifier_ogre_ignite_target:EndOfTurnAction()		
	
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------