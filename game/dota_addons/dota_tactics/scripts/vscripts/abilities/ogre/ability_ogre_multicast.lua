
ability_ogre_multicast = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ogre_multicast","abilities/ogre/modifier_ogre_multicast.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_energized","abilities/generic/generic_modifier_energized.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_ogre_multicast:PrepAction(keys)

	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ogre_multicast",{})
	
	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;			
	self:UseResources(false,false,true)	
	Alert_Mana({unit=self:GetCaster(),manavalue=0-self:GetManaCost(self:GetLevel());} )
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			
	local animationTime = 1.25;			
	--StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_4})
					
	local energy_fatigue = self:GetSpecialValueFor("energy_fatigue")
					
	if self:GetLevel()==3 then
		energy_fatigue=energy_fatigue+self:GetSpecialValueFor("bonus_energy_boost")/2
	end
						
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_energized",{
		energy=energy_fatigue,
		turns=1
	})
					
	if self:GetLevel()==2 then
		--self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_shield",{life=self:GetSpecialValueFor("bonus_self_heal") , turns = 1})
		Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=self:GetSpecialValueFor("bonus_self_heal")})
	elseif self:GetLevel()==4 then
		for i=0, 5 do
			Alert_CooldownRefresh(self:GetCaster(),i,-self:GetSpecialValueFor("bonus_ability_refresh"))	
		end
	end
	
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function ability_ogre_multicast:GetPriority()
	return 1;
end

--------------------------------------------------------------------------------

function ability_ogre_multicast:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_ogre_multicast:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function ability_ogre_multicast:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_ogre_multicast:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------