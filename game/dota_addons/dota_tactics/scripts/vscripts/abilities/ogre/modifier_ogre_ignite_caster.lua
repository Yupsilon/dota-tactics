modifier_ogre_ignite_caster = class({})

--------------------------------------------------------------------------------

function modifier_ogre_ignite_caster:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_ogre_ignite_caster:OnCreated( kv )

	local turnsDuration = kv.turns or 2
	self:SetStackCount(turnsDuration)
	self.ignitionPoint = Vector(kv.point_x,kv.point_y,0)
	
	if IsServer() then	

		local speed = self:GetAbility():GetCastRange()/PLAYER_TIME_MODIFIER
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_ogre_magi/ogre_magi_ignite.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		--ParticleManager:SetParticleControl( nFXIndex, 0, self:GetParent():GetAbsOrigin() )
		ParticleManager:SetParticleControl( nFXIndex, 1, self.ignitionPoint )
		ParticleManager:SetParticleControl( nFXIndex, 2, Vector (speed,0,0) )
		ParticleManager:SetParticleControl( nFXIndex, 3, self.ignitionPoint )
		ParticleManager:SetParticleControl( nFXIndex, 4, self.ignitionPoint )
		
		Timers:CreateTimer(((self.ignitionPoint-self:GetParent():GetAbsOrigin()):Length()/speed),function()
					ParticleManager:DestroyParticle( nFXIndex, false )
					ParticleManager:ReleaseParticleIndex( nFXIndex )	
					
					EmitSoundOnLocationWithCaster(self.ignitionPoint, "Hero_OgreMagi.Ignite.Target", self:GetCaster())	
							
					local groundFire = ParticleManager:CreateParticle( "particles/units/heroes/hero_jakiro/jakiro_macropyre.vpcf", PATTACH_CUSTOMORIGIN, self:GetParent() )
					ParticleManager:SetParticleControl( groundFire, 0, self.ignitionPoint )
					ParticleManager:SetParticleControl( groundFire, 1, self.ignitionPoint )
					ParticleManager:SetParticleControl( groundFire, 2, Vector(9999,0,0) )
					self:AddParticle( groundFire, false, false, -1, false, false )	
		end)		
		
		self:StartIntervalThink( .4 )
		--self:OnIntervalThink()	
	end
end
	
--------------------------------------------------------------------------------

function modifier_ogre_ignite_caster:SolveAction()
	
	return MODIFIERACTION_SHORTPAUSE_SURVIVE;
end
	
--------------------------------------------------------------------------------

function modifier_ogre_ignite_caster:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end
	
--------------------------------------------------------------------------------

function modifier_ogre_ignite_caster:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function modifier_ogre_ignite_caster:OnIntervalThink()
	if IsServer() then
					   
			if GameMode:GetCurrentGamePhase()<GAMEPHASE_DASH or GameMode:GetCurrentGamePhase()>GAMEPHASE_WALK then return end			
					   
			local target = DOTA_UNIT_TARGET_TEAM_ENEMY
			
			if HasTalent(self:GetCaster(),"talent_ogre_6") then
				target = DOTA_UNIT_TARGET_TEAM_BOTH
			end
					   
			local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self.ignitionPoint, nil, self:GetAbility():GetSpecialValueFor("ignite_aoe"), target, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
			if #enemies > 0 then
				for _,enemy in pairs(enemies) do
					if enemy ~= nil and ( not enemy:IsInvulnerable() ) and not enemy:HasModifier("modifier_ogre_ignite_target") then
						enemy:AddNewModifier(self:GetCaster(),self:GetAbility(),"modifier_ogre_ignite_target",{})
					end
				end
			end	
	
	end
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------