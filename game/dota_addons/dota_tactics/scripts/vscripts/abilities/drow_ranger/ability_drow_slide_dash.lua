ability_drow_slide_dash = class({})
LinkLuaModifier( "modifier_drow_slide_dash","abilities/drow_ranger/modifier_drow_slide_dash.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_might","abilities/generic/generic_modifier_might.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_dashed_this_turn","abilities/generic/generic_modifier_dashed_this_turn.lua", LUA_MODIFIER_MOTION_NONE )
--LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_drow_slide_dash:DashAction(keys)

	if keys.target_point_A~=nil then
				
			local dash_range = self:GetSpecialValueFor("dash_range")
			local dash_energy = self:GetSpecialValueFor("dash_energy")
	
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_dashed_this_turn",{turns = 1})
			
			local startPoint = keys.caster:GetAbsOrigin() + Vector(0,0,50);
			local endPoint = startPoint			
			local direction = (keys.target_point_A-startPoint)
			direction.z=0
			
			if self:GetLevel()==4 then
				self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_might",{value=self:GetSpecialValueFor("bonus_shield"),turns = self:GetSpecialValueFor("bonus_shield_duration")})
			elseif self:GetLevel()==2 then
				self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_unstoppable",{turns = 1})
					--self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_shield",{life=self:GetSpecialValueFor("bonus_shield") , turns = self:GetSpecialValueFor("bonus_shield_duration")})
			end
			
			--local animationTime = 1;
			--StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_2})
			Alert_Mana({unit=self:GetCaster(), manavalue=dash_energy , energy=true} );
	
			Timers:CreateTimer(0.1,function()
				local projectile = {
				  --EffectName = projectile_model,	
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint,--{unit=hero, attach="attach_attack1", offset=Vector(0,0,0)},
				  fDistance = direction:Length(),
				  fStartRadius = projectile_radius,
				  fEndRadius = projectile_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  --fExpireTime = 8.0,
				  vVelocity = direction:Normalized() * 1500, 
				  UnitBehavior = PROJECTILES_NOTHING,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  --bCutTrees = true,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_DESTROY,
				  GroundBehavior = PROJECTILES_DESTROY,
				  fGroundOffset = 0,
				  --nChangeMax = 1,
				  --bRecreateOnChange = true,
				  bZCheck = false,
				  bGroundLock = false,
				  --bProvidesVision = true,
				  --iVisionRadius = 350,
				  --iVisionTeamNumber = keys.caster:GetTeam(),
				  --bFlyingVision = false,
				  --fVisionTickTime = .1,
				  --fVisionLingerDuration = 1,
				  draw = false,--             draw = {alpha=1, color=Vector(200,0,0)},
				  --iPositionCP = 0,
				  --iVelocityCP = 1,
				  --ControlPoints = {[5]=Vector(100,0,0), [10]=Vector(0,0,1)},
				  --ControlPointForwards = {[4]=hero:GetForwardVector() * -1},
				  --ControlPointOrientations = {[1]={hero:GetForwardVector() * -1, hero:GetForwardVector() * -1, hero:GetForwardVector() * -1}},
				  --[[ControlPointEntityAttaches = {[0]={
					unit = hero,
					pattach = PATTACH_ABSORIGIN_FOLLOW,
					attachPoint = "attach_attack1", -- nil
					origin = Vector(0,0,0)
				  }},]]
				  --fRehitDelay = .3,
				  --fChangeDelay = 1,
				  --fRadiusStep = 10,
				  bUseFindUnitsInRadius = true,
									  bHitScan = true,

				  --[[UnitTest = function(instance, unit) return unit:HasAbility("walker_dummy_unit") == false and unit:HasAbility("dummy_unit") == false and unit:GetTeamNumber() ~= keys.caster:GetTeamNumber() end,
				  OnUnitHit = function(instance, unit) 
					
						self:OnProjectileHit(unit,instance:GetPosition())
					
				  end,]]
				  --OnTreeHit = function(self, tree) ... end,
				  --OnWallHit = function(self, gnvPos) ... end,
				  --OnGroundHit = function(self, groundPos) ... end,
				  --OnFinish = function(self, pos) ... end,
				   OnFinish = function(instance, pos) 
					
						EmitSoundOn( "Hero_DrowRanger.Silence", self:GetCaster() )
						self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_drow_slide_dash",{dash_destination_x=instance:GetPosition().x,dash_destination_y=instance:GetPosition().y})
					
				  end,
				}
				
				keys.caster:FaceTowards(keys.target_point_A)
						
				Projectiles:CreateProjectile(projectile)
			end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_drow_slide_dash:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_drow_slide_dash:disablesMovement()
	return true;
end

--------------------------------------------------------------------------------

function ability_drow_slide_dash:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_drow_slide_dash:RequiresHitScanCheck()
	return true;
end

--------------------------------------------------------------------------------

function ability_drow_slide_dash:RequiresGridSnap()
	return true;
end

--------------------------------------------------------------------------------

function ability_drow_slide_dash:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_drow_slide_dash:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_drow_slide_dash:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_drow_slide_dash:GetConeArc(coneID,coneRange)
	return self:GetCaster():GetHullRadius()
end

--------------------------------------------------------------------------------

function ability_drow_slide_dash:GetMinRange(coneID)	
	return 100
end

--------------------------------------------------------------------------------

function ability_drow_slide_dash:GetMaxRange(coneID)
	if self:GetLevel()==3 then
		return self:GetSpecialValueFor("dash_range")+self:GetSpecialValueFor("bonus_dash_range")
	end
	return self:GetSpecialValueFor("dash_range")
end

--------------------------------------------------------------------------------

function ability_drow_slide_dash:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_drow_slide_dash:GetCooldown(level)
	
	return 4
end

--------------------------------------------------------------------------------

function ability_drow_slide_dash:GetAOERadius()
	return self:GetCaster():GetHullRadius()
end

--------------------------------------------------------------------------------