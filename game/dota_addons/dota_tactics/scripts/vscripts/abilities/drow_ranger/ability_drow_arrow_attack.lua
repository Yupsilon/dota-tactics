ability_drow_arrow_attack = class({})
LinkLuaModifier( "modifier_energy_regeneration","abilities/modifier_energy_regeneration.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_drow_arrow_attack:GetIntrinsicModifierName()
	return "modifier_energy_regeneration"
end

--------------------------------------------------------------------------------

function ability_drow_arrow_attack:IsDisabledBySilence()
	return false
end


--------------------------------------------------------------------------------

function ability_drow_arrow_attack:GetDamageConversion()
	return 0
end

--------------------------------------------------------------------------------

function ability_drow_arrow_attack:GetRegenerationValue()
	return self:GetSpecialValueFor("base_energy_regen")
end

--------------------------------------------------------------------------------

function ability_drow_arrow_attack:SolveAction(keys)

	if keys.target_point_A~=nil then
				
			local projectile_range = self:GetSpecialValueFor("projectile_range")
			local projectile_radius = self:GetSpecialValueFor("projectile_radius")
	
			self.LastCastTurn = math.max(GameMode:GetCurrentTurn()+math.ceil(self:GetCooldown(self:GetLevel())),self.LastCastTurn or -1);		
			
			local startPoint = keys.caster:GetAbsOrigin() + Vector(0,0,50)*keys.caster:GetModelScale();	
			local direction = (keys.target_point_A-startPoint):Normalized()
			direction.z=0		
			startPoint=startPoint+direction*projectile_radius/2
			
			local animationTime = PLAYER_TIME_MODIFIER;
			local decalSpeed = math.max(900,projectile_range/PLAYER_TIME_MODIFIER)*2
			local projectile_model = "particles/units/heroes/hero_drow/drow_base_attack.vpcf"
			if self:GetCaster():HasModifier("modifier_drow_frost_arrows") then
				projectile_model= "particles/units/heroes/hero_drow/drow_frost_arrow.vpcf"
				
			end
						   
				keys.caster:FaceTowards(keys.target_point_A)
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_ATTACK, rate = 1/PLAYER_TIME_MODIFIER})	
						   
			local vectorZ = Vector(0,0,100)
			
			Timers:CreateTimer(animationTime*25/30, function() 

			
			if self:GetCaster():HasModifier("modifier_drow_frost_arrows") then
				EmitSoundOn( "Hero_DrowRanger.FrostArrows", self:GetCaster() )
				else
				EmitSoundOn( "Hero_DrowRanger.Attack", self:GetCaster() )
			end
			
				self.nChainParticleFXIndex = ParticleManager:CreateParticle( projectile_model, PATTACH_CUSTOMORIGIN, self:GetCaster() )		
					ParticleManager:SetParticleAlwaysSimulate( self.nChainParticleFXIndex )
					ParticleManager:SetParticleControlEnt( self.nChainParticleFXIndex, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_attack1",  startPoint+vectorZ, true )
					ParticleManager:SetParticleControl( self.nChainParticleFXIndex, 1, startPoint+vectorZ )
					ParticleManager:SetParticleControl( self.nChainParticleFXIndex, 2, Vector( decalSpeed*1.1, 9999, 0 ) )-- self.fireball_speed, self.fireball_distance, self.fireball_width
					--ParticleManager:SetParticleControl( self.nChainParticleFXIndex, 3, Vector( 1, 0, 0 ) )	--killswitch
					ParticleManager:SetParticleControl( self.nChainParticleFXIndex, 4, Vector( 1, 0, 0 ) )
					ParticleManager:SetParticleControl( self.nChainParticleFXIndex, 5, Vector( 0, 0, 0 ) )
					ParticleManager:SetParticleControlEnt( self.nChainParticleFXIndex, 7, self:GetCaster(), PATTACH_CUSTOMORIGIN, nil, self:GetCaster():GetOrigin()+vectorZ, true )	
					
	
				local projectile = {
				  --EffectName = projectile_model,	
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint,--{unit=hero, attach="attach_attack1", offset=Vector(0,0,0)},
				  fDistance = projectile_range-projectile_radius,
				  fStartRadius = projectile_radius,
				  fEndRadius = projectile_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  --fExpireTime = 8.0,
				  vVelocity = direction * decalSpeed, 
				  UnitBehavior = PROJECTILES_DESTROY,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  --bCutTrees = true,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_DESTROY,
				  GroundBehavior = PROJECTILES_DESTROY,
				  fGroundOffset = 0,
				  --nChangeMax = 1,
				  --bRecreateOnChange = true,
				  bZCheck = false,
				  bGroundLock = false,
				  --bProvidesVision = true,
				  --iVisionRadius = 350,
				  --iVisionTeamNumber = keys.caster:GetTeam(),
				  --bFlyingVision = false,
				  --fVisionTickTime = .1,
				  --fVisionLingerDuration = 1,
				  draw = false,--             draw = {alpha=1, color=Vector(200,0,0)},
				  --iPositionCP = 0,
				  --iVelocityCP = 1,
				  --ControlPoints = {[5]=Vector(100,0,0), [10]=Vector(0,0,1)},
				  --ControlPointForwards = {[4]=hero:GetForwardVector() * -1},
				  --ControlPointOrientations = {[1]={hero:GetForwardVector() * -1, hero:GetForwardVector() * -1, hero:GetForwardVector() * -1}},
				  --[[ControlPointEntityAttaches = {[0]={
					unit = hero,
					pattach = PATTACH_ABSORIGIN_FOLLOW,
					attachPoint = "attach_attack1", -- nil
					origin = Vector(0,0,0)
				  }},]]
				  --fRehitDelay = .3,
				  --fChangeDelay = 1,
				  --fRadiusStep = 10,
				  bUseFindUnitsInRadius = true,

				  UnitTest = function(instance, unit) return not unit:IsInvulnerable() and unit:GetTeamNumber() ~= keys.caster:GetTeamNumber() end,
				  OnUnitHit = function(instance, unit) 
					
						self:OnProjectileHit(unit,instance:GetPosition())
						EmitSoundOn( "Hero_DrowRanger.ProjectileImpact", unit )
					
				  end,
				  OnIntervalThink = function(instance)
						ParticleManager:SetParticleControl( self.nChainParticleFXIndex, 1, instance:GetPosition()+vectorZ )
				  end,
				  OnFinish = function(instance, pos)		  
						ParticleManager:DestroyParticle( self.nChainParticleFXIndex, false )
						ParticleManager:ReleaseParticleIndex( self.nChainParticleFXIndex )
				  end
				  --OnTreeHit = function(self, tree) ... end,
				  --OnWallHit = function(self, gnvPos) ... end,
				  --OnGroundHit = function(self, groundPos) ... end,
				  --OnFinish = function(self, pos) ... end,
				}						
				Projectiles:CreateProjectile(projectile)
			end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_drow_arrow_attack:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_drow_arrow_attack:OnProjectileHit(hTarget, vLocation)
					
				
						local damage_table = {
							victim = hTarget,
							attacker = self:GetCaster(),
							damage = self:GetSpecialValueFor("projectile_damage") or 1,
							damage_type = DAMAGE_TYPE_PHYSICAL,
							ability=self,
							cover_reduction = 1
						}
							
							if self:GetLevel()==2 then
								damage_table.damage=damage_table.damage+self:GetSpecialValueFor("bonus_damage")					
						end
					
					if IsTargetPointOverCover(vLocation,damage_table.attacker) then
						damage_table.cover_reduction=1/2
					end
						local energy = self:GetSpecialValueFor("projectile_energy")
												
							if self:GetLevel()==4 then
								Alert_CooldownRefresh(self:GetCaster(),1,-self:GetSpecialValueFor("bonus_refreshing"))												
						elseif self:GetLevel() == 3 then
							energy=energy+self:GetSpecialValueFor("bonus_energy")
						end
											
					Alert_Damage(damage_table)
					Alert_Mana({unit=self:GetCaster(), manavalue=energy, energy=true}  );
end

--------------------------------------------------------------------------------

function ability_drow_arrow_attack:getAbilityType()
	return 1;
end

--------------------------------------------------------------------------------

function ability_drow_arrow_attack:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_drow_arrow_attack:RequiresHitScanCheck()
	return true;
end

--------------------------------------------------------------------------------

function ability_drow_arrow_attack:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_drow_arrow_attack:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_drow_arrow_attack:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_drow_arrow_attack:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_drow_arrow_attack:GetMinRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_drow_arrow_attack:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_drow_arrow_attack:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------