 ability_drow_arrow_cone = class({})
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_drow_arrow_cone:SolveAction(keys)

	if keys.target_point_A~=nil then
				
			local projectile_range = self:GetSpecialValueFor("projectile_range")
			local projectile_radius = self:GetSpecialValueFor("projectile_radius")*2
			local projectile_angle = (self:GetSpecialValueFor("projectile_angle")*math.pi/180)/2
			local projectile_count = self:getConeRadius()/self:GetSpecialValueFor("projectile_angle")
	
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			
			local startPoint = keys.caster:GetAbsOrigin() + Vector(0,0,50)*keys.caster:GetModelScale();		
			local direction = (keys.target_point_A-startPoint):Normalized()
			direction.z=0
			startPoint=startPoint+direction*projectile_radius/2
			
			local animationTime = PLAYER_TIME_MODIFIER;
			local decalSpeed = math.max(900,projectile_range/PLAYER_TIME_MODIFIER)*2
			local projectile_model = "particles/units/heroes/hero_drow/drow_base_attack.vpcf"
			if self:GetCaster():HasModifier("modifier_drow_frost_arrows") then
				projectile_model= "particles/units/heroes/hero_drow/drow_frost_arrow.vpcf"
			end
			
				keys.caster:FaceTowards(keys.target_point_A)
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_ATTACK, rate = 1/PLAYER_TIME_MODIFIER})	
						   local vectorZ = Vector(0,0,100)
			
			Timers:CreateTimer(animationTime*25/30, function() 
	
			if self:GetCaster():HasModifier("modifier_drow_frost_arrows") then
				EmitSoundOn( "Hero_DrowRanger.FrostArrows", self:GetCaster() )
				else
				EmitSoundOn( "Hero_DrowRanger.Attack", self:GetCaster() )
			end
	
			local startangle = math.atan2(direction.x,direction.y)
	
				for i=0,projectile_count-1 do
			
				
				
				   
						local nChainParticleFXIndex = ParticleManager:CreateParticle( projectile_model, PATTACH_CUSTOMORIGIN, self:GetCaster() )		
						ParticleManager:SetParticleAlwaysSimulate( nChainParticleFXIndex )
						ParticleManager:SetParticleControlEnt( nChainParticleFXIndex, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_attack1",  startPoint+vectorZ, false )
						ParticleManager:SetParticleControl( nChainParticleFXIndex, 1, startPoint+vectorZ )
						ParticleManager:SetParticleControl( nChainParticleFXIndex, 2, Vector( decalSpeed, 0, 0 ) )-- self.fireball_speed, self.fireball_distance, self.fireball_width
						--ParticleManager:SetParticleControl( nChainParticleFXIndex, 3, Vector( 1, 0, 0 ) )	--killswitch
						ParticleManager:SetParticleControl( nChainParticleFXIndex, 4, Vector( 1, 0, 0 ) )
						ParticleManager:SetParticleControl( nChainParticleFXIndex, 5, Vector( 0, 0, 0 ) )
						ParticleManager:SetParticleControlEnt( nChainParticleFXIndex, 7, self:GetCaster(), PATTACH_CUSTOMORIGIN, nil, self:GetCaster():GetOrigin()+vectorZ, true )	
	
					local fireAngle = startangle-projectile_angle*(projectile_count-1)/2+projectile_angle*i
					local newdirection = Vector(math.sin(fireAngle),math.cos(fireAngle),0)
			
					local projectile = {
					  --EffectName = projectile_model,			  			  
					  vSpawnOrigin = startPoint,--{unit=hero, attach="attach_attack1", offset=Vector(0,0,0)},
					  fDistance = projectile_range-projectile_radius,
					  fStartRadius = projectile_radius,
					  fEndRadius = projectile_radius,
					  fCollisionRadius = 30,
					  Source = self:GetCaster(),
					  --fExpireTime = 8.0,
					  vVelocity = newdirection * decalSpeed, 
					  UnitBehavior = PROJECTILES_DESTROY,
					  bMultipleHits = false,
					  bIgnoreSource = true,
					  TreeBehavior = PROJECTILES_NOTHING,
					  --bCutTrees = true,
					  bTreeFullCollision = false,
					  WallBehavior = PROJECTILES_DESTROY,
					  GroundBehavior = PROJECTILES_DESTROY,
					  fGroundOffset = 0,
					  --nChangeMax = 1,
					  --bRecreateOnChange = true,
					  bZCheck = false,
					  bGroundLock = false,
					  --bProvidesVision = true,
					  --iVisionRadius = 350,
					  --iVisionTeamNumber = keys.caster:GetTeam(),
					  --bFlyingVision = false,
					  --fVisionTickTime = .1,
					  --fVisionLingerDuration = 1,
					  draw = false,--             draw = {alpha=1, color=Vector(200,0,0)},
					  --iPositionCP = 0,
					  --iVelocityCP = 1,
					  --ControlPoints = {[5]=Vector(100,0,0), [10]=Vector(0,0,1)},
					  --ControlPointForwards = {[4]=hero:GetForwardVector() * -1},
					  --ControlPointOrientations = {[1]={hero:GetForwardVector() * -1, hero:GetForwardVector() * -1, hero:GetForwardVector() * -1}},
					  --[[ControlPointEntityAttaches = {[0]={
						unit = hero,
						pattach = PATTACH_ABSORIGIN_FOLLOW,
						attachPoint = "attach_attack1", -- nil
						origin = Vector(0,0,0)
					  }},]]
					  --fRehitDelay = .3,
					  --fChangeDelay = 1,
					  --fRadiusStep = 10,
					  bUseFindUnitsInRadius = true,

					  UnitTest = function(instance, unit) return unit:IsInvulnerable() == false and unit:GetTeamNumber() ~= keys.caster:GetTeamNumber() and unit:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())==nil end,
					  OnUnitHit = function(instance, unit) 
						
							self:OnProjectileHit(unit,instance:GetPosition())
							EmitSoundOn( "Hero_DrowRanger.ProjectileImpact", unit )
						
					  end,
					  OnIntervalThink = function(instance)
							ParticleManager:SetParticleControl( nChainParticleFXIndex, 1, instance:GetPosition()+vectorZ )
					  end,
					  OnFinish = function(instance, pos)		  
								ParticleManager:DestroyParticle( nChainParticleFXIndex, false )
								ParticleManager:ReleaseParticleIndex( nChainParticleFXIndex )
					  end
					  --OnTreeHit = function(self, tree) ... end,
					  --OnWallHit = function(self, gnvPos) ... end,
					  --OnGroundHit = function(self, groundPos) ... end,
					  --OnFinish = function(self, pos) ... end,
					}
							
						
							
					Projectiles:CreateProjectile(projectile)
				end
			end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_drow_arrow_cone:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_drow_arrow_cone:OnProjectileHit(hTarget, vLocation)
					
					if hTarget:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())~=nil then
						return
					else
						hTarget:AddNewModifier(self:GetCaster(),self,"modifier_hit",{turns=1})
					end
					
					local damage_table = {
							victim = hTarget,
							attacker = self:GetCaster(),
							damage = self:GetSpecialValueFor("projectile_damage") ,
							damage_type = DAMAGE_TYPE_MAGICAL,
							ability=self,
							cover_reduction = 1
						}
						
					
					
					if self:GetLevel()==2 then
						damage_table.damage = damage_table.damage - self:GetSpecialValueFor("bonus_damage_penalty")
						if damage_table.victim:HasModifier("generic_modifier_shield") then
							damage_table.damage = damage_table.damage+self:GetSpecialValueFor("bonus_damage_correction")
						end
					elseif self:GetLevel()==3 then
						damage_table.damage = self:GetSpecialValueFor("projectile_damage_narrow") 
					end
					
					if IsTargetPointOverCover(vLocation,damage_table.attacker) then
						if self:GetLevel()==4 then
							damage_table.cover_reduction=3/4
						else
							damage_table.cover_reduction=1/2
						end
					end
				
						Alert_Damage(damage_table)
						
						local energy = self:GetSpecialValueFor("projectile_energy")
						--if self:GetLevel()==2 then
						--	 energy = energy+self:GetSpecialValueFor("bonus_energy")
						--end
						
						Alert_Mana({unit=self:GetCaster(),manavalue=energy, energy=true} );
										
end

--------------------------------------------------------------------------------

function ability_drow_arrow_cone:getAbilityType()
	return ABILITY_TYPE_CONE;
end

--------------------------------------------------------------------------------

function ability_drow_arrow_cone:getConeRadius()
	if self:GetLevel()==3 then
		return self:GetSpecialValueFor("cone_angle_narrow")
	end
	return self:GetSpecialValueFor("cone_angle")
end

--------------------------------------------------------------------------------

function ability_drow_arrow_cone:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_drow_arrow_cone:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_drow_arrow_cone:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_drow_arrow_cone:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_drow_arrow_cone:GetConeArc(coneID,coneRange)
	--if coneID ~= 0 then return self:GetSpecialValueFor("projectile_radius") end
	--return self:GetMaxRange(0)	*math.sin(self:getConeRadius()*math.pi/180)
	if coneID == 1 then return self:getConeRadius() / self:GetSpecialValueFor("projectile_angle") end
	return self:getConeRadius()
end

--------------------------------------------------------------------------------

function ability_drow_arrow_cone:GetMinRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_drow_arrow_cone:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_drow_arrow_cone:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------