modifier_drow_frost_arrows = class({})

--------------------------------------------------------------------------------

function modifier_drow_frost_arrows:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_drow_frost_arrows:OnCreated( kv )

	self:SetStackCount(kv.turns or 3)

	if IsServer() then
	
		self:GetAbility():SetActivated(false)
		self.used_turns = 0
		
		self.linked_modifiers={}
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_drow/drow_marksmanship_glow.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		self:AddParticle( nFXIndex, false, false, -1, false, false )		
	end
end

--------------------------------------------------------------------------------

function modifier_drow_frost_arrows:DeclareFunctions()
	local funcs = {
		MODIFIER_EVENT_ON_TAKEDAMAGE ,
	}	

	return funcs
end
	
--------------------------------------------------------------------------------

function modifier_drow_frost_arrows:LinkModifier(modifier)
	
	table.insert(self.linked_modifiers,modifier)
end
	
--------------------------------------------------------------------------------

function modifier_drow_frost_arrows:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		self.used_turns = self.used_turns + 1
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	
	for index,modifier in pairs(self.linked_modifiers) do
		if modifier~=nil then
			modifier:Destroy()
		end
	end
	
	return MODIFIERACTION_NOPAUSE_DESTROY;
end
	
--------------------------------------------------------------------------------

function modifier_drow_frost_arrows:OnDestroy()
	
	if not IsServer() then return end
	
		self:GetAbility():SetActivated(true)
		
		if self.used_turns == 0 then 
			self:GetAbility().LastCastTurn = GameMode:GetCurrentTurn()+self:GetAbility():GetCooldown(self:GetAbility():GetLevel())+1;			
			self:GetAbility():UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self:GetAbility(),"modifier_ability_cooldown",{})
		end
end
	
--------------------------------------------------------------------------------

function modifier_drow_frost_arrows:OnTakeDamage ( params )
		if IsServer() then
				local hAttacker = params.attacker
				local hVictim = params.unit

			if hAttacker == self:GetParent() and params.damage_type~=DAMAGE_TYPE_PURE then
									
					local duration = self:GetAbility():GetSpecialValueFor("ice_duration")
					if self:GetAbility():GetLevel()==2 then
						duration = duration + self:GetAbility():GetSpecialValueFor("bonus_weaken_duration")
					end
									
					local slowModifier = hVictim:AddNewModifier( hAttacker, self:GetAbility(), "generic_modifier_movespeed_slow", {
						turns = duration,
						move_slow = self:GetAbility():GetSpecialValueFor("ice_slow"),
						nodraw = 1
					} )			
					if slowModifier~=nil then
						local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_drow/drow_frost_arrow_debuff.vpcf", PATTACH_ABSORIGIN_FOLLOW, hVictim )
						slowModifier:AddParticle( nFXIndex, false, false, -1, false, false )
					end
					self:SetStackCount(1);
					
					--if self:GetAbility():GetLevel()==4 then
					--	hVictim:AddNewModifier( hAttacker, self:GetAbility(), "generic_modifier_weak", {value = self:GetAbility():GetSpecialValueFor("bonus_weakness"),turns = self:GetAbility():GetSpecialValueFor("bonus_weaken_duration"),applythisturn=0} )					
					--end
			end
		end
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------