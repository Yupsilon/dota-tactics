ability_drow_arrow_ice = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_drow_frost_arrows","abilities/drow_ranger/modifier_drow_frost_arrows.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_weak","abilities/generic/generic_modifier_weak.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_scarred","abilities/generic/generic_modifier_scarred.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_conversion","abilities/generic/generic_modifier_conversion.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_drow_arrow_ice:PrepAction(keys)

	local center_modifier = self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_drow_frost_arrows",{})
	
	EmitSoundOn( "Hero_DrowRanger.Marksmanship.Target", self:GetCaster() )
	--self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;			
	--self:UseResources(false,false,true)	
	--self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			
	local animationTime = 1.25;			
	StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_3})
	
	--local static_perma = self:GetSpecialValueFor("base_scar_damage")
	if self:GetLevel()==2 then
		--center_modifier:LinkModifier(self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_might", {value= self:GetSpecialValueFor("bonus_might") , turns = 10 , nodraw=1} ))
		
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_shield",{life=self:GetSpecialValueFor("bonus_shield") , turns = self:GetSpecialValueFor("bonus_weaken_duration")})
	elseif self:GetLevel()==3 then
		--static_perma=self:GetSpecialValueFor("bonus_scar_damage")
		center_modifier:LinkModifier(self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_conversion", {value= self:GetSpecialValueFor("bonus_scar_damage") , turns = 10 , nodraw=1} ))
	end
	
	
	Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("ice_energy"), energy=true} )
	return ABILITYACTION_COMPLETE_SHORT;
end

--------------------------------------------------------------------------------

function ability_drow_arrow_ice:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_drow_arrow_ice:GetPriority()
	return 1;
end

--------------------------------------------------------------------------------

function ability_drow_arrow_ice:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function ability_drow_arrow_ice:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_drow_arrow_ice:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------