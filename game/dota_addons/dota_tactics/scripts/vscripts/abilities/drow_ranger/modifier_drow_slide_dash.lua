modifier_drow_slide_dash = class({})

--------------------------------------------------------------------------------

function modifier_drow_slide_dash:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_drow_slide_dash:OnCreated( kv )

	self.dash_start = self:GetParent():GetAbsOrigin()
	self.dash_end = Vector(kv.dash_destination_x,kv.dash_destination_y,0)
	self.dash_speed = self:GetAbility():GetSpecialValueFor("dash_range") * 1/30
	self.distance_traveled = 0

	if IsServer() then
		local nFXIndex = ParticleManager:CreateParticle( "particles/econ/events/winter_major_2016/force_staff_wm_2016.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		self:AddParticle( nFXIndex, false, false, -1, false, false )		
		self.dash_speed=(self.dash_speed)/PLAYER_TIME_SIMULTANIOUS*3/2
	
		self:StartIntervalThink( 1/30 )
		self:OnIntervalThink()
	end
end

--------------------------------------------------------------------------------

function modifier_drow_slide_dash:OnIntervalThink()
	if IsServer() then

		local vectorDelta=self.dash_end-self.dash_start
		vectorDelta.z=0

		if self.distance_traveled <= vectorDelta:Length2D() then
			self:GetParent():SetAbsOrigin(self:GetParent():GetAbsOrigin() + vectorDelta:Normalized() * self.dash_speed)
			self.distance_traveled = self.distance_traveled + (vectorDelta:Normalized() * self.dash_speed):Length2D()
		else
			FindClearSpaceForUnit(self:GetParent(), self.dash_end, true)
			self:Destroy()
		end
	end
end

--------------------------------------------------------------------------------

function modifier_drow_slide_dash:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_OVERRIDE_ANIMATION,
	}

	return funcs
end

--------------------------------------------------------------------------------

function modifier_drow_slide_dash:GetOverrideAnimation( params )
	return ACT_DOTA_RUN
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------