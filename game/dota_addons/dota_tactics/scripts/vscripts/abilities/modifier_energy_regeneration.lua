modifier_energy_regeneration = class({})

--------------------------------------------------------------------------------

function modifier_energy_regeneration:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_energy_regeneration:GetAbsorptionPercentage()
	if self:GetAbility()==nil then
		return 1/5
	end	
	return (self:GetAbility():GetDamageConversion() or 20)/100
end

--------------------------------------------------------------------------------

function modifier_energy_regeneration:DeclareFunctions()
	

	local funcs = {
		MODIFIER_EVENT_ON_TAKEDAMAGE ,
		MODIFIER_PROPERTY_STATS_INTELLECT_BONUS,
	}	

	return funcs
end

--------------------------------------------------------------------------------

function modifier_energy_regeneration:GetModifierBonusStats_Intellect(params)
	return self:GetAbility():GetRegenerationValue()
end



--------------------------------------------------------------------------------

function modifier_energy_regeneration:OnTakeDamage ( params )

		if self:GetAbsorptionPercentage()==0 then
			return 
		end

		if IsServer() then
				local hAttacker = params.attacker
				local hVictim = params.unit

			if hVictim == self:GetParent() then
			
					self:GetParent():GiveMana(params.damage*self:GetAbsorptionPercentage());
					
			end
		end
end

--------------------------------------------------------------------------------

function modifier_energy_regeneration:GetRegenerationValue()
	--if self:GetAbility()==nil then
	--	return 5
	--end	
	return (self:GetParent():GetIntellect())
end

--------------------------------------------------------------------------------

function modifier_energy_regeneration:GetRegenerationValue()
	--if self:GetAbility()==nil then
	--	return 5
	--end	
	return (self:GetParent():GetIntellect())
end
	
--------------------------------------------------------------------------------

function modifier_energy_regeneration:EndOfTurnAction()
	
	if self:GetParent():IsAlive() then
		self:GetParent():GiveMana(self:GetRegenerationValue());		
	end
	return MODIFIERACTION_NOPAUSE_SURVIVE;
end
	
--------------------------------------------------------------------------------