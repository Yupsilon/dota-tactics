ability_warlock_demon_stomp = class({})
LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_warlock_demon_stomp:IsDisabledBySilence()
	return false
end

--------------------------------------------------------------------------------

function ability_warlock_demon_stomp:SolveAction(keys)
		
			local cleave_range = self:GetSpecialValueFor("stomp_range")
			local cleave_damage = self:GetSpecialValueFor("stomp_damage")
				
			local startPoint = self:GetCaster():GetAbsOrigin()
	
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;					
			local animationTime = 35/30 * PLAYER_TIME_MODIFIER;
						   	
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_ATTACK, rate = 1/PLAYER_TIME_MODIFIER})	
				--EmitSoundOn( "Hero_EarthShaker.Woosh", self:GetCaster() )
			
			Timers:CreateTimer(animationTime*1/7, function() 
			
				EmitSoundOn( "Roshan.Slam", self:GetCaster() )
			
				local nFXIndex = ParticleManager:CreateParticle( "particles/neutral_fx/roshan_slam.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )				
				ParticleManager:SetParticleControl( nFXIndex, 1, Vector(cleave_range,cleave_range,cleave_range) )
				ParticleManager:ReleaseParticleIndex( nFXIndex )	

				local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), startPoint, self:GetCaster(), cleave_range, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
				if #enemies > 0 then
					for _,enemy in pairs(enemies) do
						if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
					
							local enemy_direction = (enemy:GetAbsOrigin()-startPoint)
								local projectile = {
								EffectAttach = PATTACH_WORLDORIGIN,			  			  
								vSpawnOrigin = startPoint+Vector(0,0,30),
								fDistance = cleave_range,
								fStartRadius = 50,
								fEndRadius = 50,
								fCollisionRadius = 30,
								  Source = self:GetCaster(),
								  vVelocity = enemy_direction:Normalized() * cleave_range*10, 
								  UnitBehavior = PROJECTILES_NOTHING,
								  bMultipleHits = true,
								  bIgnoreSource = true,
								  TreeBehavior = PROJECTILES_NOTHING,
								  bTreeFullCollision = false,
								  WallBehavior = PROJECTILES_DESTROY,
								  GroundBehavior = PROJECTILES_DESTROY,
								  fGroundOffset = 0,
								  bZCheck = false,
								  bGroundLock = false,
								  draw = false,
								  bUseFindUnitsInRadius = true,
									bHitScan=true,

								  UnitTest = function(instance, unit) return unit==enemy end,
								  OnUnitHit = function(instance, unit) 
														
										local damage_table = {
											victim = enemy,
											attacker = self:GetCaster(),
											damage = cleave_damage,
											damage_type = DAMAGE_TYPE_MAGICAL,
											cover_reduction = 1,
										}
										
										if IsTargetOverCover(instance,damage_table.attacker) then
											damage_table.cover_reduction=1/2
										end

										Alert_Damage( damage_table )
										local slowModifier = enemy:AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_slow",{turns=self:GetSpecialValueFor("stomp_duration"),nodraw = 1})
										
										
										local mFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_warlock/warlock_upheaval_debuff.vpcf", PATTACH_ABSORIGIN_FOLLOW, enemy )
										slowModifier:AddParticle( mFXIndex, false, false, -1, false, false )		
										
										local hit_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_spirit_breaker/spirit_breaker_greater_bash.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())
										ParticleManager:SetParticleControl(hit_particle, 0, unit:GetAbsOrigin())
										ParticleManager:ReleaseParticleIndex(hit_particle)
								  end,
								}						
								Projectiles:CreateProjectile(projectile)						
					
						end
					end
				end
			end)
			
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_warlock_demon_stomp:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_warlock_demon_stomp:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_warlock_demon_stomp:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_warlock_demon_stomp:GetCastPoints()
	return 0;
end

--------------------------------------------------------------------------------

function ability_warlock_demon_stomp:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_warlock_demon_stomp:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_warlock_demon_stomp:GetConeArc(coneID,coneRange)	
	return 1
end

--------------------------------------------------------------------------------

function ability_warlock_demon_stomp:GetMinRange(coneID)	
	return self:GetSpecialValueFor("stomp_range")
end

--------------------------------------------------------------------------------

function ability_warlock_demon_stomp:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("stomp_range")
end

--------------------------------------------------------------------------------

function ability_warlock_demon_stomp:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------