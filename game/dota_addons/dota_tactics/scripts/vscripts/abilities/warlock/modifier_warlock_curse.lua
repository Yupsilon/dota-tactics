modifier_warlock_curse = class({})

--------------------------------------------------------------------------------

function modifier_warlock_curse:IsDebuff()
	return true
end

-----------------------------------------------------------------------------

function modifier_warlock_curse:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function modifier_warlock_curse:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_warlock_curse:OnCreated( kv )
	
	local turnsDuration = kv.turns or 1
	self:SetStackCount(turnsDuration)
	
	if IsServer() then			
		local mFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_warlock/warlock_fatal_bonds_icon.vpcf", PATTACH_OVERHEAD_FOLLOW, self:GetParent() )
		self:AddParticle( mFXIndex, false, false, -1, false, false )		
	end
end

--------------------------------------------------------------------------------

function modifier_warlock_curse:WalkMovementAction()
	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	
	local damage_table = {
		victim = self:GetParent(),
		attacker = self:GetCaster(),
		damage = self:GetAbility():GetSpecialValueFor("curse_damage"),
		damage_type = DAMAGE_TYPE_PURE,
		ability = self:GetAbility()
	}
	Alert_Damage( damage_table )
	local nFXIndex = ParticleManager:CreateParticle( "particles/econ/events/ti9/shovel_revealed_baby_roshan.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
	ParticleManager:ReleaseParticleIndex(nFXIndex)
	EmitSoundOn("n_black_dragon.Fireball.Cast",self:GetParent())
	
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
