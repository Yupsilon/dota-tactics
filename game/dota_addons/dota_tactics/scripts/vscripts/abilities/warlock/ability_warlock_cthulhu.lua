ability_warlock_cthulhu = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "modifier_turn_restricted_creature","mechanics/heroes/modifier_turn_restricted_creature.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_warlock_demon","abilities/warlock/modifier_warlock_demon.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_warlock_demonstatus","abilities/warlock/modifier_warlock_demonstatus.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_summonedunit","abilities/generic/generic_modifier_summonedunit.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_splash_resistance","abilities/generic/generic_modifier_splash_resistance.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_absolute","abilities/generic/generic_modifier_movespeed_absolute.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_truesight","abilities/generic/generic_modifier_truesight.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_warlock_cthulhu:SolveAction(keys)

	if keys.target_point_A~=nil then				
				
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			--self:UseResources(false,false,false)	
			Alert_Mana({unit=self:GetCaster(),manavalue=0-self:GetManaCost(self:GetLevel());} )
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})		
				
			local summon_duration = self:GetSpecialValueFor("summon_duration")
			local arate=.6
				
			local startPoint = keys.caster:GetAbsOrigin()
			local direction = (keys.target_point_A-startPoint):Normalized()
			direction.z=0
			local centerangle = math.atan2(direction.x,direction.y)
	
			self.LastCastTurn = math.max(GameMode:GetCurrentTurn()+math.ceil(self:GetCooldown(self:GetLevel())),self.LastCastTurn or -1);			
			local animationTime = 26/30*PLAYER_TIME_MODIFIER/arate;
						   
			keys.caster:FaceTowards(keys.target_point_A)	
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_FATAL_BONDS,rate = arate/PLAYER_TIME_MODIFIER})	
			
			EmitSoundOn( "Hero_Warlock.RainOfChaos_buildup", self:GetCaster() )
			Timers:CreateTimer(animationTime*.5, function() 
			
			EmitSoundOn( "Hero_Warlock.RainOfChaos", self:GetCaster() )
				
				self:SummonCreature(keys.target_point_A,summon_duration)
	

				local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_warlock/warlock_rain_of_chaos_fallback_mid.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster() )
				ParticleManager:SetParticleControl(nFXIndex, 0, keys.target_point_A)	
				ParticleManager:SetParticleControl(nFXIndex, 1, Vector(300,1,1))	
				ParticleManager:ReleaseParticleIndex(nFXIndex)
				
			end)
			
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_warlock_cthulhu:SummonCreature(point,duration)

	local main_modifier = self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_warlock_demon",{x=point.x,y=point.y})		   
	main_modifier.AssignedUnit:FaceTowards(main_modifier.AssignedUnit:GetAbsOrigin()+point-self:GetCaster():GetAbsOrigin())
				
	local aiModifier = self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_summonedunit",{turns = duration,destroyonend=1})
	aiModifier:AssignUnit(main_modifier.AssignedUnit)
				
	main_modifier.AssignedUnit:AddNewModifier(self:GetCaster(),self,"modifier_turn_restricted_creature",{})
	main_modifier.AssignedUnit:AddNewModifier(self:GetCaster(),self,"modifier_warlock_demonstatus",{})
	main_modifier.AssignedUnit:AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_absolute",{speed = HERO_BASE_MOVEMENTSPEED*100*5/4})
	main_modifier.AssignedUnit:AddNewModifier(self:GetCaster(),self,"generic_modifier_truesight",{ range = 200})	
	
	if self:GetLevel() == 2 then
		main_modifier.AssignedUnit:AddNewModifier(self:GetCaster(),self,"generic_modifier_splash_resistance",{value = self:GetSpecialValueFor("bonus_resistance"),turns = self:GetSpecialValueFor("summon_duration")+1})
		main_modifier.AssignedUnit:AddNewModifier(self:GetCaster(),self,"generic_modifier_unstoppable",{turns = self:GetSpecialValueFor("summon_duration")+1})
	end
	
	if self:GetLevel()>2 then
			main_modifier.AssignedUnit:GetAbilityByIndex(1):SetLevel(self:GetLevel()-1)
	else 
		main_modifier.AssignedUnit:GetAbilityByIndex(1):SetLevel(1)
	end		
	main_modifier.AssignedUnit:GetAbilityByIndex(0):SetLevel(1)
	main_modifier.AssignedUnit:GetAbilityByIndex(0):SolveAction({})
end

--------------------------------------------------------------------------------

function ability_warlock_cthulhu:GetPriority()
	return 0;
end


--------------------------------------------------------------------------------

function ability_warlock_cthulhu:getAbilityType()
	return ABILITY_TYPE_POINT_MULTIPLE;
end

--------------------------------------------------------------------------------

function ability_warlock_cthulhu:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_warlock_cthulhu:RequiresGridSnap()
	return true;
end

--------------------------------------------------------------------------------

function ability_warlock_cthulhu:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_warlock_cthulhu:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_warlock_cthulhu:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_warlock_cthulhu:GetConeArc(coneID,coneRange)
	return self:GetAOERadius()
end

--------------------------------------------------------------------------------

function ability_warlock_cthulhu:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_warlock_cthulhu:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("summon_range")
end

--------------------------------------------------------------------------------

function ability_warlock_cthulhu:GetAOERadius()	
	return self:GetSpecialValueFor("summon_aoe")
end

--------------------------------------------------------------------------------

function ability_warlock_cthulhu:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------