modifier_warlock_souldisplaced = class({})

--------------------------------------------------------------------------------

function modifier_warlock_souldisplaced:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_warlock_souldisplaced:OnCreated( kv )

	local turnsDuration = kv.turns or 2
	self:SetStackCount(turnsDuration)
	
	if IsServer() then	

		self.center = Vector(kv.point_x,kv.point_y,0)

		local vectorZ = Vector (0,0,120)
		local start = self:GetParent():GetAbsOrigin()
		local nFXIndex = ParticleManager:CreateParticle( "particles/econ/items/wraith_king/wraith_king_ti6_bracer/wraith_king_ti6_hellfireblast.vpcf", PATTACH_WORLDORIGIN, self:GetParent() )
		ParticleManager:SetParticleControl( nFXIndex, 0, start + vectorZ )
		ParticleManager:SetParticleControl( nFXIndex, 1,  self.center + Vector (0,0,start.z) + vectorZ)
		ParticleManager:SetParticleControl( nFXIndex, 2,  Vector(999,0,0))
		self:AddParticle( nFXIndex, false, false, -1, false, false )	
		
		if self:GetParent():IsIllusion() or not self:GetParent():IsRealHero() then
			self:Destroy()
		end
		
		self:StartIntervalThink( .25 )
		self:OnIntervalThink()	
	end
end
	
--------------------------------------------------------------------------------

function modifier_warlock_souldisplaced:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end
	
--------------------------------------------------------------------------------

function modifier_warlock_souldisplaced:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function modifier_warlock_souldisplaced:OnIntervalThink()
	if IsServer() then
					   
		if not GameMode:GetCurrentGamePhase()==GAMEPHASE_DASH and not GameMode:GetCurrentGamePhase()==GAMEPHASE_WALK then return end			
			
			local activated = false;
			local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self.center, nil, self:GetAbility():GetSpecialValueFor("soul_sizeradius"), DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_ALL, DOTA_UNIT_TARGET_FLAG_INVULNERABLE, 0, false )
			if #enemies > 0 then
				for _,enemy in pairs(enemies) do
					local unitname = enemy:GetUnitName()
					if enemy ~= nil and ( not enemy:IsInvulnerable() or unitname == "npc_warloc_summoned_imp") and not activated then
					
						if unitname == "npc_warloc_summoned_imp" and enemy.ability and enemy.ability:GetLevel() == 3 then
							self:CollectSoulForUnit(self:GetCaster())	
							activated = true
						else
							self:CollectSoulForUnit(enemy)						
							activated = true
						end					
					end
				end
			end	
			if activated then 
				self:Destroy()
				--[[if self:GetAbility():GetLevel() == 4 then					
					
					
					self:GetParent():AddNewModifier( self:GetCaster(), self, "generic_modifier_reveal", {turns = self:GetAbility():GetSpecialValueFor("bonus_reveal_duration") } )
				end]]
			end
		
	end
end
	
--------------------------------------------------------------------------------

function modifier_warlock_souldisplaced:CollectSoulForUnit(target)

			local soul_healing = self:GetAbility():GetSpecialValueFor("soul_healing")
			
			if target== self:GetCaster() then
				soul_healing=soul_healing/2
			end

	Alert_Heal({caster=self:GetCaster(),target=target,value=soul_healing	})
							
							if self:GetAbility():GetLevel() == 4 then 
								local damage_table = {
									victim = self:GetParent(),
									attacker = self:GetCaster(),
									damage = self:GetAbility():GetSpecialValueFor("bonus_soul_damage") or 1,
									damage_type = DAMAGE_TYPE_PURE,
									ability=self:GetAbility(),
									cover_reduction = 1
								}													
								Alert_Damage(damage_table)
								
								
								local hit_particle = ParticleManager:CreateParticle( "particles/units/heroes/hero_warlock/warlock_fatal_bonds_pulse.vpcf", PATTACH_WORLDORIGIN, self:GetParent() )
								ParticleManager:SetParticleControl( hit_particle, 0, damage_table.attacker:GetAbsOrigin() )
								ParticleManager:SetParticleControl( hit_particle, 1,  damage_table.victim:GetAbsOrigin())
								ParticleManager:ReleaseParticleIndex( hit_particle )
								
								EmitSoundOn( "Hero_Pugna.NetherWard.Attack", unit )
							else
								EmitSoundOn( "Hero_Warlock.FatalBondsDamage", unit )
							end
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------