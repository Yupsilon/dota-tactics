ability_warlock_conecurse = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_warlock_curse","abilities/warlock/modifier_warlock_curse.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_warlock_souldisplaced","abilities/warlock/modifier_warlock_souldisplaced.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_warlock_conecurse:SolveAction(keys)

	self.LastCastTurn = math.max(GameMode:GetCurrentTurn()+math.ceil(self:GetCooldown(self:GetLevel())),self.LastCastTurn or -1);	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})	
	if keys.target_point_A~=nil then self:CastInstance(keys.target_point_A) end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_warlock_conecurse:CastInstance(point)

			local cone_range = self:GetSpecialValueFor("cone_range")
			local cone_angle = self:GetSpecialValueFor("cone_angle")
				
			local startPoint = self:GetCaster():GetAbsOrigin()
			local direction = (point-startPoint):Normalized()
			direction.z=0
					
			local animationTime = PLAYER_TIME_MODIFIER;
						   
				self:GetCaster():FaceTowards(point)			
			StartAnimation(self:GetCaster(), {duration=animationTime, rate=1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_CAST_ABILITY_2})	
								
			--EmitSoundOn( "Hero_BountyHunter.PreAttack", self:GetCaster() )
			--Timers:CreateTimer(animationTime/2, function() 
			
			EmitSoundOn( "Hero_Warlock.FatalBonds", self:GetCaster() )
			
				--[[local nFXIndex = ParticleManager:CreateParticle( "particles/econ/items/sven/sven_ti7_sword/sven_ti7_sword_spell_great_cleave_gods_strength_crit.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )

				
				ParticleManager:SetParticleControl( nFXIndex, 0, startPoint )
				ParticleManager:SetParticleControlForward( nFXIndex, 0, (point-startPoint):Normalized());
				ParticleManager:SetParticleControl( nFXIndex, 2, startPoint )
				ParticleManager:SetParticleControl( nFXIndex, 3, startPoint )
				ParticleManager:SetParticleControl( nFXIndex, 4, startPoint )
				ParticleManager:SetParticleControl( nFXIndex, 5, startPoint )
				ParticleManager:ReleaseParticleIndex( nFXIndex )]]

				local enemies = FindUnitsInCone({				
					caster = self:GetCaster(),
					startPoint = startPoint,
					startAngle = math.atan2(direction.x,direction.y),
					search_range = cone_range,
					search_angle = cone_angle,
					searchteam = DOTA_UNIT_TARGET_TEAM_ENEMY,
					searchtype = DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
					searchflags = 0
				})
				
				if #enemies > 0 then
					for _,enemy in pairs(enemies) do
						if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
						
								
										local energyGain = self:GetSpecialValueFor("cone_energy")
										enemy:AddNewModifier( self:GetCaster(), self, "modifier_warlock_curse", { turns = 2} )											

											if self:GetLevel()==2 then
												Alert_CooldownRefresh(self:GetCaster(),self,-self:GetSpecialValueFor("bonus_cooldown"))		
											elseif self:GetLevel()==3 then
												enemy:AddNewModifier( self:GetCaster(), self, "generic_modifier_weak", {value= self:GetSpecialValueFor("bonus_weaken") , turns = self:GetSpecialValueFor("bonus_weaken_duration")} )
											elseif self:GetLevel()==4 then	

												local linkAbility = self:GetCaster():GetAbilityByIndex(0)
												local soul_turns = linkAbility:GetSpecialValueFor("soul_turns")
						
												if linkAbility:GetLevel() == 4 and enemy:IsHero() then
													soul_turns = soul_turns + linkAbility:GetSpecialValueFor("bonus_soul_turns")
												end
												
												local vectordelta = enemy:GetAbsOrigin() - self:GetCaster():GetAbsOrigin()
												vectordelta = enemy:GetAbsOrigin() + vectordelta:Normalized() * linkAbility:GetSpecialValueFor("souldisplace_distance")
												
												enemy:AddNewModifier(self:GetCaster(),linkAbility,"modifier_warlock_souldisplaced",{
													point_x = vectordelta.x,
													point_y = vectordelta.y,
													turns = soul_turns
												})
											end
												
										local hit_particle = ParticleManager:CreateParticle( "particles/units/heroes/hero_warlock/warlock_fatal_bonds_hit.vpcf", PATTACH_WORLDORIGIN, enemy )
										ParticleManager:SetParticleControl( hit_particle, 0, self:GetCaster():GetAbsOrigin() )
										ParticleManager:SetParticleControl( hit_particle, 1,  enemy:GetAbsOrigin())
										ParticleManager:ReleaseParticleIndex( hit_particle )								
																
										Alert_Mana({unit=self:GetCaster(),manavalue=energyGain, energy=true});
												
						end
					end
				end
			--end)
end

--------------------------------------------------------------------------------

function ability_warlock_conecurse:getAbilityType()
	return ABILITY_TYPE_CONE;
end

--------------------------------------------------------------------------------

function ability_warlock_conecurse:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_warlock_conecurse:GetPriority()
	return 1;
end

--------------------------------------------------------------------------------

function ability_warlock_conecurse:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_warlock_conecurse:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_warlock_conecurse:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_warlock_conecurse:GetConeArc(coneID,coneRange)
	if coneID == 1 then return 13 end
	return self:GetSpecialValueFor("cone_angle")*2
end

--------------------------------------------------------------------------------

function ability_warlock_conecurse:GetMinRange(coneID)	
	return self:GetSpecialValueFor("cone_range")
end

--------------------------------------------------------------------------------

function ability_warlock_conecurse:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("cone_range")
end

--------------------------------------------------------------------------------

function ability_warlock_conecurse:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------