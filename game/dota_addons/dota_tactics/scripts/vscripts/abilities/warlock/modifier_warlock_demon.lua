modifier_warlock_demon = class({})
-----------------------------------------------------------------------------

function modifier_warlock_demon:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function modifier_warlock_demon:IsPermanent()
	return true
end

--------------------------------------------------------------------------------

function modifier_warlock_demon:RemoveOnDeath()
	return false
end

--------------------------------------------------------------------------------

function modifier_warlock_demon:IsHidden()
	return not BAREBONES_DEBUG_SPEW -- show cooldown modifier in debug mode
end

--------------------------------------------------------------------------------

function modifier_warlock_demon:OnCreated( kv )

	self.AssignedUnit=nil
	self.unitStartPos = Vector (kv.x,kv.y,0)

	if IsServer() then
	
		self.AssignedUnit = ( CreateUnitByName("npc_warloc_summoned_demon", self.unitStartPos , true, self:GetCaster(), self:GetCaster(), self:GetCaster():GetTeamNumber() ))
		--self.AssignedUnit:AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_absolute",{speed = (self:GetSpecialValueFor("cast_range"))/PLAYER_TIME_MODIFIER*2/3})
		--self.AssignedUnit:FindAbilityByName("dummy_unit_caster"):SetLevel(1)
		self.AssignedUnit:SetControllableByPlayer(self:GetCaster():GetPlayerOwnerID(), true)
		self.AssignedUnit:SetOwner(self:GetCaster())
		--self.AssignedUnit:SetHullRadius(1)
		
		self.AssignedUnit.ability=self:GetAbility()
		self.AssignedUnit.caster=self:GetCaster()
		
		FindClearSpaceForUnit(self.AssignedUnit, self.unitStartPos, true)		
	end	
end

--------------------------------------------------------------------------------

function modifier_warlock_demon:SolveAction()	
	if self.AssignedUnit and not self.AssignedUnit:IsNull() and self.AssignedUnit:IsAlive() then 
		return MODIFIERACTION_SHORTPAUSE_SURVIVE;
	else 
		return MODIFIERACTION_NOPAUSE_DESTROY;
	end
end
	
--------------------------------------------------------------------------------