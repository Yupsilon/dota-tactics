modifier_warlock_summonedimp = class({})
-----------------------------------------------------------------------------

function modifier_warlock_summonedimp:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function modifier_warlock_summonedimp:IsHidden()
	return not BAREBONES_DEBUG_SPEW -- show cooldown modifier in debug mode
end

--------------------------------------------------------------------------------

function modifier_warlock_summonedimp:OnCreated( kv )

	self.AssignedUnit=nil
	self.unitStartPos = Vector (kv.x,kv.y,0)

	if IsServer() then
	
		self.AssignedUnit = ( CreateUnitByName("npc_warloc_summoned_imp", self.unitStartPos , true, self:GetCaster(), self:GetCaster(), self:GetCaster():GetTeamNumber() ))
		--self.AssignedUnit:AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_absolute",{speed = (self:GetSpecialValueFor("cast_range"))/PLAYER_TIME_MODIFIER*2/3})
		self.AssignedUnit:FindAbilityByName("dummy_unit_caster"):SetLevel(1)
		self.AssignedUnit:SetControllableByPlayer(self:GetCaster():GetPlayerOwnerID(), true);
		self.AssignedUnit:SetOwner(self:GetCaster());
		--self.AssignedUnit:SetHullRadius(1)
		self.AssignedUnit:GetAbilityByIndex(0):SetLevel(1)
		if self:GetAbility():GetLevel() == 2 then
			self.AssignedUnit:GetAbilityByIndex(0):SetLevel(2)
		end
		
		self.AssignedUnit.ability=self:GetAbility()
		self.AssignedUnit.caster=self:GetCaster()
		
		FindClearSpaceForUnit(self.AssignedUnit, self.unitStartPos, true)	
	
	end
	
end

--------------------------------------------------------------------------------

function modifier_warlock_summonedimp:Activate ( )

		
		EmitSoundOn( "n_black_dragon.Fireball.Cast", self.AssignedUnit )
			
		local spergout_aoe = self:GetAbility():GetSpecialValueFor("spergout_aoe")
		local helix_damage = self:GetAbility():GetSpecialValueFor("spergout_damage")
		
		if self:GetAbility():GetLevel() == 4 then
			helix_damage = helix_damage+self:GetAbility():GetSpecialValueFor("bonus_spergout_damage")
		end
		
		local earned_mana = 0
		
			local helix = ParticleManager:CreateParticle("particles/neutral_fx/roshan_slam.vpcf", PATTACH_ABSORIGIN_FOLLOW, self.AssignedUnit)
			ParticleManager:SetParticleControl( helix, 1, Vector(spergout_aoe,spergout_aoe,spergout_aoe) )
			ParticleManager:ReleaseParticleIndex( helix )	
			
				StartAnimation(self.AssignedUnit, {duration=PLAYER_TIME_MODIFIER, rate = 1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_CAST_ABILITY_3})
				
					local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self.AssignedUnit:GetAbsOrigin(), self:GetCaster(),spergout_aoe , DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
					
					print("imp found "..#enemies)
					if #enemies > 0 then
						for _,enemy in pairs(enemies) do
							if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
						
									local direction = enemy:GetAbsOrigin()-self.AssignedUnit:GetAbsOrigin()
									direction.z=0
									local projectile = {
									EffectAttach = PATTACH_WORLDORIGIN,			  			  
									vSpawnOrigin = self.AssignedUnit:GetAbsOrigin()+Vector(0,0,30),
									fDistance = spergout_aoe,
									fStartRadius = 50,
									fEndRadius = 50,
									fCollisionRadius = 30,
									  Source = self:GetCaster(),
									  vVelocity = direction:Normalized() * spergout_aoe*2, 
									  UnitBehavior = PROJECTILES_NOTHING,
									  bMultipleHits = true,
									  bIgnoreSource = true,
									  TreeBehavior = PROJECTILES_NOTHING,
									  bTreeFullCollision = false,
									  WallBehavior = PROJECTILES_DESTROY,
									  GroundBehavior = PROJECTILES_DESTROY,
									  fGroundOffset = 0,
									  bZCheck = false,
									  bGroundLock = false,
									  draw = false,
									  bUseFindUnitsInRadius = true,
									  bHitScan = true,

									  UnitTest = function(instance, unit) return unit==enemy end,
									  OnUnitHit = function(instance, unit) 
															
											print("projectile hit")
											local damage_table = {
												victim = enemy,
												attacker = self:GetCaster(),
												damage = helix_damage,
												damage_type = DAMAGE_TYPE_PURE,
												ability = self:GetAbility(),
												cover_reduction = 1
											}
									
											if IsTargetOverCover(instance,damage_table.attacker) then
												damage_table.cover_reduction=1/2
											end
															
											local hit_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_beastmaster/beastmaster_wildaxes_hit.vpcf", PATTACH_CUSTOMORIGIN, self.AssignedUnit)									
											ParticleManager:SetParticleControl(hit_particle, 0, unit:GetAbsOrigin())
											ParticleManager:ReleaseParticleIndex(hit_particle)
											earned_mana = earned_mana+self:GetAbility():GetSpecialValueFor("spergout_energy")

											Alert_Damage( damage_table )
										
									  end,
									}						
									Projectiles:CreateProjectile(projectile)
														
							end
						end
					end							
				
	
		Alert_Mana({unit=self:GetCaster(),manavalue=earned_mana, energy=true});	

end

--------------------------------------------------------------------------------

function modifier_warlock_summonedimp:SolveAction()	
	if self.AssignedUnit and not self.AssignedUnit:IsNull() and self.AssignedUnit:IsAlive() then 
		self:Activate()	
	else 
		return MODIFIERACTION_NOPAUSE_DESTROY;
	end
	return MODIFIERACTION_SHORTPAUSE_SURVIVE;
end
	
--------------------------------------------------------------------------------