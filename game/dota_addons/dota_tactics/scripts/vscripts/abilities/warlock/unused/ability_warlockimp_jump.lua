ability_warlockimp_jump = class({})
LinkLuaModifier( "modifier_warlockimp_jump","abilities/warlock/modifier_warlockimp_jump.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_warlockimp_jump:PrepAction(keys)

	if keys.target_point_A~=nil then
				
			local dash_range = self:GetMaxRange(0)
			local projectile_radius = self:GetCaster().ability:GetSpecialValueFor("spergout_aoe")
	
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			--self:UseResources(false,false,true)	
			--self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			--self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_dashed_this_turn",{turns = 1})				
			keys.caster:FaceTowards(keys.target_point_A)
			
			local startPoint = keys.caster:GetAbsOrigin() + Vector(0,0,50);
			local endPoint = startPoint			
			local direction = (keys.target_point_A-startPoint)
			direction.z=0
			StartAnimation(self:GetCaster(), {duration=PLAYER_TIME_SIMULTANIOUS*2, rate = 1/PLAYER_TIME_SIMULTANIOUS, activity=ACT_DOTA_CAST_ABILITY_4})
			
			local animTimer = PLAYER_TIME_MODIFIER/2
						
			Timers:CreateTimer(animTimer, function()
				local hitunit=false
				local projectile = {
				  --EffectName = projectile_model,	
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint,
				  fDistance = math.min(direction:Length2D(),dash_range),
				  fStartRadius = projectile_radius,
				  fEndRadius = projectile_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  --fExpireTime = 8.0,
				  vVelocity = direction:Normalized() * dash_range , 
				  UnitBehavior = PROJECTILES_NOTHING,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  --bCutTrees = true,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_NOTHING,
				  GroundBehavior = PROJECTILES_NOTHING,
				  fGroundOffset = 0,
				  bZCheck = false,
				  bGroundLock = false,
				  draw = false,
				  bHitScan=true,
				   OnFinish = function(instance, pos) 
					
						if not self:GetCaster():HasModifier("modifier_warlockimp_jump") then
							self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_warlockimp_jump",{dash_destination_x=instance:GetPosition().x,dash_destination_y=instance:GetPosition().y})							
						end
				  end,
				}
						
				Projectiles:CreateProjectile(projectile)
			end);
	end
	return ABILITYACTION_COMPLETE_SHORT;
end

--------------------------------------------------------------------------------

function ability_warlockimp_jump:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_warlockimp_jump:disablesMovement()
	return true;
end

--------------------------------------------------------------------------------

function ability_warlockimp_jump:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_warlockimp_jump:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_warlockimp_jump:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_warlockimp_jump:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_warlockimp_jump:GetConeArc(coneID,coneRange)
	return self:GetAOERadius()
end

--------------------------------------------------------------------------------

function ability_warlockimp_jump:GetMinRange(coneID)	

	return 0
end

--------------------------------------------------------------------------------

function ability_warlockimp_jump:GetMaxRange(coneID)	
	
	return self:GetSpecialValueFor("imp_dash_range")
end

--------------------------------------------------------------------------------

function ability_warlockimp_jump:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_warlockimp_jump:GetAOERadius()	
	return self:GetSpecialValueFor("tooltip_hull_size")
end

--------------------------------------------------------------------------------