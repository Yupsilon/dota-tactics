ability_warlock_detonate = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_reveal","abilities/generic/generic_modifier_reveal.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_splash_resistance","abilities/generic/generic_modifier_splash_resistance.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_weak","abilities/generic/generic_modifier_weak.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_warlock_detonate:PrepAction(keys)

	if keys.target_point_A~=nil then
				 
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
						
			local animationTime = 29/30*PLAYER_TIME_MODIFIER;		
				keys.caster:FaceTowards(keys.target_point_A)
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_ATTACK, rate = 1/PLAYER_TIME_MODIFIER})	
						     
			local detonateradius = self:GetAOERadius()
			local explosionradius = self:GetSpecialValueFor( "explosion_aoe" )

			local nFXIndex = ParticleManager:CreateParticle( "particles/items2_fx/veil_of_discord.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster() )
			ParticleManager:SetParticleControl(nFXIndex, 0, keys.target_point_A)	
			ParticleManager:SetParticleControl(nFXIndex, 1, Vector(detonateradius*1.25,1,1))	
			ParticleManager:ReleaseParticleIndex(nFXIndex)
			
			EmitSoundOn( "n_creep_SatyrTrickster.Cast", self:GetCaster() )

			--OLD
							 
			--[[for index,modifier in pairs(self:GetCaster():FindAllModifiersByName("modifier_warlock_summonedimp")) do		
				if modifier.AssignedUnit~= nil and modifier.AssignedUnit:IsAlive() then
					local distance = (modifier.AssignedUnit:GetAbsOrigin()-keys.target_point_A):Length2D()
					if distance<detonateradius then
					
						local allies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), keys.target_point_A, self:GetCaster(), explosionradius, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
						if #allies > 0 then
							for _,unit in pairs(allies) do
								if unit ~= nil and ( not unit:IsInvulnerable() ) then
									
									Alert_HealScars({unit = unit, value = self:GetSpecialValueFor("scar_heal")})
									if self:GetLevel() == 2 then
										--Alert_Heal({caster=self:GetCaster(),target=unit,value=self:GetSpecialValueFor("bonus_healing")	})
										unit:AddNewModifier( self:GetCaster(), self, "generic_modifier_scar_immunity", {turns = self:GetSpecialValueFor("bonus_scar_immunity_duration") } )
									end
								end
							end							
						end	

						local nFXIndex = ParticleManager:CreateParticle( "particles/econ/events/ti8/blink_dagger_ti8_start_lvl2.vpcf", PATTACH_ABSORIGIN_FOLLOW, modifier.AssignedUnit )
						ParticleManager:ReleaseParticleIndex(nFXIndex)
						
						EmitSoundOn( "Ability.SandKing_CausticFinale", modifier.AssignedUnit )
						modifier.AssignedUnit:Destroy()
					end
				end
			end]]
			
			--[[if self:GetLevel() == 2 then
			
			
			for _,hero in pairs(FindUnitsInRadius( self:GetCaster():GetTeam(), Vector(0,0,0), nil, FIND_UNITS_EVERYWHERE, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO, DOTA_UNIT_TARGET_FLAG_DEAD, 0, false )) do	
				if hero ~= nil and hero:IsRealHero() then					
						for index,modifier in pairs(hero:FindAllModifiersByName("modifier_warlock_souldisplaced")) do		
							
								local distance = (modifier.center-keys.target_point_A):Length2D()
								if distance<detonateradius then
								
									local allies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), keys.target_point_A, self:GetCaster(), explosionradius, DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
									if #allies > 0 then
										for _,unit in pairs(allies) do
											if unit ~= nil and ( not unit:IsInvulnerable() ) then
												
												Alert_HealScars({unit = unit, value = self:GetSpecialValueFor("scar_heal")})
												
											end
										end							
									end	

									local nFXIndex = ParticleManager:CreateParticle( "particles/econ/events/ti8/blink_dagger_ti8_start_lvl2.vpcf", PATTACH_CUSTOMORIGIN, modifier.AssignedUnit )
									ParticleManager:SetParticleControl(nFXIndex, 0, keys.target_point_A)	
									ParticleManager:ReleaseParticleIndex(nFXIndex)
									
									--EmitSoundOn( "Ability.SandKing_CausticFinale", modifier.AssignedUnit )
									modifier:Destroy()
								
							end
						end
					end
				end]]
			
			local mana_earned = 0
			
			local others = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), keys.target_point_A, nil, detonateradius, DOTA_UNIT_TARGET_TEAM_BOTH, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
						if #others > 0 then
							for _,unit in pairs(others) do
								if unit ~= nil and ( not unit:IsInvulnerable() ) then
								
									--purge FX
									local nFXIndex = ParticleManager:CreateParticle( "particles/generic_gameplay/generic_purge.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )
									ParticleManager:SetParticleControl(nFXIndex, 0, unit:GetAbsOrigin())	
									ParticleManager:ReleaseParticleIndex(nFXIndex)
								
									if unit:GetTeamNumber() == self:GetCaster():GetTeamNumber() then
										
										if self:GetLevel()==4 then
											unit:AddNewModifier( self:GetCaster(), self, "generic_modifier_splash_resistance", {life= self:GetSpecialValueFor("bonus_shield") , turns = self:GetSpecialValueFor("bonus_modifier_duration_short")} )
										end
										
										for index,modifier in pairs(unit:FindAllModifiers()) do	
											if modifier:GetName()=="generic_modifier_weak" or modifier:GetName()=="generic_modifier_movespeed_slow" then
												modifier:Destroy()
												mana_earned=mana_earned+1
											end
										end
									else									
										if self:GetLevel()==2 then
											unit:AddNewModifier( self:GetCaster(), self, "generic_modifier_reveal", {turns = self:GetSpecialValueFor("bonus_modifier_duration_short")} )
										elseif self:GetLevel() == 3 then
											unit:AddNewModifier( self:GetCaster(), self, "generic_modifier_weak", {value= self:GetSpecialValueFor("bonus_weaken") , turns = self:GetSpecialValueFor("bonus_modifier_duration")} )
										end
										
										for index,modifier in pairs(unit:FindAllModifiers()) do	
											if modifier:GetName()=="generic_modifier_might" or modifier:GetName()=="generic_modifier_movespeed_haste" then
												modifier:Destroy()
												mana_earned=mana_earned+1
											end
										end
										Alert_Mana({unit = self:GetCaster(),manavalue = self:GetSpecialValueFor("energy_perbuff"),energy = true});
									end									
								end
							end 
						end	
				Alert_Mana({unit = self:GetCaster(),manavalue = self:GetSpecialValueFor("energy_perbuff")*mana_earned,energy = true});
			
	end
	return ABILITYACTION_COMPLETE_SHORT;
end

--------------------------------------------------------------------------------

function ability_warlock_detonate:GetPriority()
	return 4;
end

--------------------------------------------------------------------------------

function ability_warlock_detonate:GetAOERadius()

	return self:GetSpecialValueFor( "search_aoe" )
end

--------------------------------------------------------------------------------

function ability_warlock_detonate:getAbilityType()
	return ABILITY_TYPE_POINT_MULTIPLE;
end

--------------------------------------------------------------------------------

function ability_warlock_detonate:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_warlock_detonate:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_warlock_detonate:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_warlock_detonate:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_warlock_detonate:GetConeArc(coneID,coneRange)
	return 100
end

--------------------------------------------------------------------------------

function ability_warlock_detonate:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_warlock_detonate:GetMaxRange(coneID)	
	
	return self:GetSpecialValueFor("search_range")
end

--------------------------------------------------------------------------------

function ability_warlock_detonate:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------