ability_warlock_heal = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_scar_immunity","abilities/generic/generic_modifier_scar_immunity.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_might","abilities/generic/generic_modifier_might.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_regenerate","abilities/generic/generic_modifier_regenerate.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_scarred","abilities/generic/generic_modifier_scarred.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_warlock_heal:PrepAction(keys)
			
	local target_unit = EntIndexToHScript(keys.target_unit)
	if target_unit~=nil then	

		self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
		self:UseResources(true,false,true)	
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})	
		StartAnimation(self:GetCaster(), {duration=animationTime, rate = 1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_CAST_ABILITY_2})		
	
		if self:GetCaster()~= target_unit then
	
			self:GetCaster():FaceTowards(target_unit:GetAbsOrigin())	
		end
					
			self:OnProjectileHit(target_unit)	
		EmitSoundOn( "Hero_Warlock.ShadowWordCastGood", self:GetCaster() )	
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_warlock_heal:OnProjectileHit(hTarget)

	--OLD MECHANIC
	--[[local scardamage = self:GetSpecialValueFor("scar_penalty")
	if self:GetLevel() == 2 then 
		scardamage = scardamage - self:GetSpecialValueFor("bonus_scar_heal")
	end
	
	Alert_Damage({
			victim = hTarget,
			attacker = self:GetCaster(),
			damage = 0,
			damage_type = DAMAGE_TYPE_PURE,
			ability  = self,
			scar_damage_absolute = scardamage,
			cover_reduction = 1
		})]]
		
	local healing = (self:GetSpecialValueFor("heal_perturn")/100) * (hTarget:GetMaxHealth()+hTarget:GetModifierStackCount("generic_modifier_scarred",nil))
		
	if self:GetLevel() == 2 then
		--healing = healing + self:GetSpecialValueFor("bonus_healing")
		Alert_HealScars({unit = hTarget, value = self:GetSpecialValueFor("bonus_healing")})
		self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_scar_immunity", { turns = 1 } )
	elseif self:GetLevel() == 3 then 
		healing = self:GetSpecialValueFor("bonus_healing")
	elseif self:GetLevel() == 4 then
		--hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_scar_immunity", {turns = self:GetSpecialValueFor("bonus_scar_immunity_duration") } )
		hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_might", {value= self:GetSpecialValueFor("bonus_might_strength") , turns = self:GetSpecialValueFor("bonus_might_duration") } )
	end
	
		local regen_mod = hTarget:AddNewModifier(self:GetCaster(),self,"generic_modifier_regenerate",{
				turns = self:GetSpecialValueFor("heal_duration"),
				health = healing,
				initial=healing,
				nodraw=1
			})	
						
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_warlock/warlock_shadow_word_buff.vpcf", PATTACH_ABSORIGIN_FOLLOW, hTarget )
		regen_mod:AddParticle( nFXIndex, false, false, -1, false, false )
	
		Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("burst_energy"), energy=true});	
end

--------------------------------------------------------------------------------

function ability_warlock_heal:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_warlock_heal:GetPriority()
	return 1;
end

--------------------------------------------------------------------------------

function ability_warlock_heal:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_warlock_heal:GetCastPoints()

	return 1;
end

--------------------------------------------------------------------------------

function ability_warlock_heal:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_warlock_heal:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_warlock_heal:GetConeArc(coneID,coneRange)
	return self:GetCaster():GetHullRadius()
end

--------------------------------------------------------------------------------

function ability_warlock_heal:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_warlock_heal:GetMaxRange(coneID)		
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_warlock_heal:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------