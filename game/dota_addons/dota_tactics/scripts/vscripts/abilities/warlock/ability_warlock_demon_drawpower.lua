ability_warlock_demon_drawpower = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_warlock_demon_drawpower:PrepAction(keys)

	StartAnimation(self:GetCaster(), {duration=PLAYER_TIME_MODIFIER, rate = .5/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_DIE})
	local warlock = self:GetCaster().caster
	
		self.LastCastTurn = GameMode:GetCurrentTurn()+1;
	if (not warlock:IsAlive()) or warlock:GetMana()<self:GetSpecialValueFor("mana_requirement") then
		
		EmitSoundOn( "n_creep_SatyrHellcaller.Shockwave.Damage", self:GetCaster() )
		self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;	
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})	
	else
		Alert_Mana({unit=warlock,manavalue=0-self:GetSpecialValueFor("mana_requirement")})
	end 
		
		EmitSoundOn( "n_creep_SatyrHellcaller.Shockwave", self:GetCaster() )
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_might",{
								value=self:GetSpecialValueFor("self_might"),
								turns=self:GetSpecialValueFor("modifier_duration"),
								--nodraw=1
							})		
		self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_shield", {life= self:GetSpecialValueFor("self_shield") , turns = self:GetSpecialValueFor("modifier_duration"),nodraw=1} )
		
		
		if self:GetLevel() == 2 then 
			Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=self:GetSpecialValueFor("bonus_healing")	})
			
			if self:GetCaster().caster~=nil then
				for index,modifier in pairs(self:GetCaster().caster:FindAllModifiersByName("generic_modifier_summonedunit")) do
					if modifier.AssignedUnit ~= nil and modifier.AssignedUnit==self:GetCaster() then
						modifier:SetStackCount(modifier:GetStackCount()+self:GetSpecialValueFor("modifier_duration"))
					end
				end
			end
		else
			local allies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetCaster():GetAbsOrigin(), nil, self:GetSpecialValueFor("bonus_shield_aoe"), DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO, 0, 0, false )
			
			if #allies > 0 then
				for _,unit in pairs(allies) do
					if unit ~= nil and ( not unit:IsInvulnerable() ) then
						unit:AddNewModifier( self:GetCaster(), self, "generic_modifier_shield", {life= self:GetSpecialValueFor("bonus_allied_shield") , turns = self:GetSpecialValueFor("modifier_duration")} )
					end
				end 
			end	
		end
	
	
	return MODIFIERACTION_SHORTPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function ability_warlock_demon_drawpower:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_warlock_demon_drawpower:GetPriority()
	return 1;
end

--------------------------------------------------------------------------------

function ability_warlock_demon_drawpower:GetMaxRange(coneID)	
	if self:GetLevel()==3 then return self:GetSpecialValueFor("bonus_shield_aoe") end
	return 0
end

--------------------------------------------------------------------------------

function ability_warlock_demon_drawpower:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function ability_warlock_demon_drawpower:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_warlock_demon_drawpower:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_warlock_demon_drawpower:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end


--------------------------------------------------------------------------------