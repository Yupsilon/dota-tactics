modifier_warlock_demonstatus = class({})

--------------------------------------------------------------------------------

function modifier_warlock_demonstatus:IsHidden()
	return true
end


--------------------------------------------------------------------------------

function modifier_warlock_demonstatus:OnCreated( kv )

	if IsServer() then
	
			local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_jakiro/jakiro_liquid_fire_debuff.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
			self:AddParticle( nFXIndex, false, false, -1, false, false )
		
	end

end

--------------------------------------------------------------------------------

function modifier_warlock_demonstatus:CheckState()
	local state = {
	[MODIFIER_STATE_NO_UNIT_COLLISION] = true,
	}

	return state
end

--------------------------------------------------------------------------------

--function modifier_warlock_demonstatus:GetStatusEffectName()
--	return "particles/status_fx/status_effect_grimstroke_ink_swell.vpcf"
--end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------