ability_omniknight_archangel = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "modifier_omniknight_archangel","abilities/omniknight/modifier_omniknight_archangel.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_splash_resistance","abilities/generic/generic_modifier_splash_resistance.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_omniknight_archangel:PrepAction(keys)
			
	local animationTime = 1;
	
	if self:GetCaster():HasModifier("modifier_omniknight_archangel") then
	
		self:GetCaster():RemoveModifierByNameAndCaster("modifier_omniknight_archangel",self:GetCaster())
		
	else
		self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
		--self:UseResources(false,false,false)	
		--self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})			
	
		--EmitSoundOn( "Hero_Dark_Seer.Surge", self:GetCaster() )		
								
		local angel_modifier = self:GetCaster():AddNewModifier( self:GetCaster(), self, "modifier_omniknight_archangel", {} )	
		angel_modifier:Activate();
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_omniknight_archangel:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_omniknight_archangel:IsDisabledBySilence()
	
	if self:GetCaster():HasModifier("modifier_omniknight_archangel") then
		return false
	end
	return true
end

--------------------------------------------------------------------------------

function ability_omniknight_archangel:GetPriority()
	return 2;
end

--------------------------------------------------------------------------------

function ability_omniknight_archangel:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function ability_omniknight_archangel:GetCastPoints()

	return 1;
end

--------------------------------------------------------------------------------

function ability_omniknight_archangel:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_omniknight_archangel:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_omniknight_archangel:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("aura_aoe")	
end

--------------------------------------------------------------------------------

function ability_omniknight_archangel:GetAOERadius()
	return self:GetSpecialValueFor( "aura_aoe" )
end

--------------------------------------------------------------------------------

function ability_omniknight_archangel:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------