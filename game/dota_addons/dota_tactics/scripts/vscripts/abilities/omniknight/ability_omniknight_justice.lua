ability_omniknight_justice = class({})
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_energy_regeneration","abilities/modifier_energy_regeneration.lua", LUA_MODIFIER_MOTION_NONE )
--LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_healing_reduced","abilities/generic/generic_modifier_healing_reduced.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_omniknight_justice:GetIntrinsicModifierName()
	return "modifier_energy_regeneration"
end

--------------------------------------------------------------------------------

function ability_omniknight_justice:IsDisabledBySilence()
	return false
end


--------------------------------------------------------------------------------

function ability_omniknight_justice:GetDamageConversion()
	return 0
end

--------------------------------------------------------------------------------

function ability_omniknight_justice:GetRegenerationValue()
		if self:GetCaster():HasModifier("modifier_omniknight_archangel") then return 0 end
	return self:GetSpecialValueFor("base_energy_regen")
end

--------------------------------------------------------------------------------

function ability_omniknight_justice:SolveAction(keys)

	if keys.target_point_A~=nil then
				
			local projectile_range = self:GetMaxRange(0)
			local projectile_radius = self:GetSpecialValueFor("projectile_radius")*2
			local projectile_angle = (self:GetSpecialValueFor("projectile_angle")*math.pi/180)/2
	
			self.firstHit = false
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			--self:UseResources(false,false,true)	
			--self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			
			local startPoint = keys.caster:GetAbsOrigin() + Vector(0,0,50)*keys.caster:GetModelScale();		
			local direction = (keys.target_point_A-startPoint):Normalized()
			direction.z=0
			startPoint=startPoint+direction*projectile_radius/2
			
			local cone_arc = self:GetConeArc(0,(keys.target_point_A-keys.caster:GetAbsOrigin()):Length2D())
			local projectile_count = cone_arc/self:GetSpecialValueFor("projectile_angle")
						
			local animationTime = PLAYER_TIME_MODIFIER;
			local decalSpeed = math.max(900,projectile_range/PLAYER_TIME_MODIFIER)*2
						
			keys.caster:FaceTowards(keys.target_point_A)
			StartAnimation(self:GetCaster(), {duration=animationTime, rate = 1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_CAST_ABILITY_2})
			
			Timers:CreateTimer(animationTime*3/5,function()
	
			EmitSoundOn( "Hero_Phoenix.FireSpirits.Target", self:GetCaster() )
			
	
			local startangle = math.atan2(direction.x,direction.y)
	
				for i=0,projectile_count-1 do		
					local fireAngle = startangle-projectile_angle*(projectile_count-1)/2+projectile_angle*i
					local newdirection = Vector(math.sin(fireAngle),math.cos(fireAngle),0)									
				   
						local nChainParticleFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_phoenix/phoenix_base_attack.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster() )		
						ParticleManager:SetParticleAlwaysSimulate( nChainParticleFXIndex )
						ParticleManager:SetParticleControlEnt( nChainParticleFXIndex, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_attack2",  startPoint, true )
						ParticleManager:SetParticleControl( nChainParticleFXIndex, 1, startPoint + newdirection * projectile_range)
						ParticleManager:SetParticleControl( nChainParticleFXIndex, 2, Vector( decalSpeed, 0, 0 ) )
						ParticleManager:SetParticleControl( nChainParticleFXIndex, 4, Vector( 1, 0, 0 ) )
						ParticleManager:SetParticleControl( nChainParticleFXIndex, 5, Vector( 0, 0, 0 ) )
						ParticleManager:SetParticleControlEnt( nChainParticleFXIndex, 7, self:GetCaster(), PATTACH_CUSTOMORIGIN, nil, self:GetCaster():GetOrigin(), true )	
	
			
					local projectile = {			  			  
					  vSpawnOrigin = startPoint+Vector (0,0,110),
					  fDistance = projectile_range-projectile_radius,
					  fStartRadius = projectile_radius,
					  fEndRadius = projectile_radius,
					  fCollisionRadius = 30,
					  Source = self:GetCaster(),
					  vVelocity = newdirection * decalSpeed*(2/3+math.random()*1/3), 
					  UnitBehavior = PROJECTILES_NOTHING,
					  bMultipleHits = false,
					  bIgnoreSource = true,
					  TreeBehavior = PROJECTILES_NOTHING,
					  bTreeFullCollision = false,
					  WallBehavior = PROJECTILES_DESTROY,
					  GroundBehavior = PROJECTILES_DESTROY,
					  fGroundOffset = 0,
					  bZCheck = false,
					  bGroundLock = false,
					  draw = false,
					  bUseFindUnitsInRadius = true,

					  UnitTest = function(instance, unit) return (not unit:IsInvulnerable()) and unit:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())==nil end,
					  OnUnitHit = function(instance, unit) 
						
							self:OnProjectileHit(unit,instance:GetPosition(),instance)
						
					  end,
					  --OnIntervalThink = function(instance)
							--ParticleManager:SetParticleControl( nChainParticleFXIndex, 1, instance:GetPosition() )
					  --end,
					  OnFinish = function(instance, pos)		  
								ParticleManager:DestroyParticle( nChainParticleFXIndex, false )
								ParticleManager:ReleaseParticleIndex( nChainParticleFXIndex )
					  end
					}
							
						
							Projectiles:CreateProjectile(projectile)
				end
			end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_omniknight_justice:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_omniknight_justice:OnProjectileHit(hTarget, vLocation, instance)
					
					if hTarget:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())~=nil then
						return
					else
						hTarget:AddNewModifier(self:GetCaster(),self,"modifier_hit",{turns=1})
					end
					
					if hTarget:GetTeamNumber() ~= self:GetCaster():GetTeamNumber() then
					
						local damage_table = {
							victim = hTarget,
							attacker = self:GetCaster(),
							damage = self:GetSpecialValueFor("cone_damage"),
							damage_type = DAMAGE_TYPE_MAGICAL,
							ability=self,
							cover_reduction = 1
						}
					
						if self.firstHit==false then
							self.firstHit = true
							damage_table.damage_type = DAMAGE_TYPE_PHYSICAL
						end
					
						local mana_value = self:GetSpecialValueFor("projectile_energy")
					
						if self:GetLevel()==2 and self:GetCaster():HasModifier("modifier_omniknight_archangel") then
							mana_value = mana_value+self:GetSpecialValueFor("bonus_energy")
						elseif self:GetLevel() == 3 then								
							--hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_healing_reduced", {
							--	turns = self:GetSpecialValueFor("bonus_hr_duration"),
							--	value = self:GetSpecialValueFor("bonus_hr")
							--} )	
							damage_table.damage=damage_table.damage+self:GetSpecialValueFor("bonus_damage")
						elseif self:GetLevel() == 4 then
							Alert_CooldownRefresh(self:GetCaster(),"ability_omniknight_hammer",-self:GetSpecialValueFor("bonus_hr_duration"))	
						end
												
						if IsTargetOverCover(instance,damage_table.attacker) then					
							damage_table.cover_reduction=1/2
						end
				
						Alert_Damage(damage_table)						
						Alert_Mana({unit=self:GetCaster(),manavalue=mana_value, energy=true});
					end
end

--------------------------------------------------------------------------------

function ability_omniknight_justice:getAbilityType()
	return ABILITY_TYPE_CONE;
end

--------------------------------------------------------------------------------

function ability_omniknight_justice:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_omniknight_justice:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_omniknight_justice:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_omniknight_justice:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_omniknight_justice:GetConeArc(coneID,coneRange)

	local cone_total = self:GetSpecialValueFor("projectile_cone")
	
	if coneID == 1 then return cone_total / self:GetSpecialValueFor("projectile_angle") end		
	return cone_total
end

--------------------------------------------------------------------------------

function ability_omniknight_justice:GetMinRange(coneID)	
	return self:GetMaxRange(0)	
end

--------------------------------------------------------------------------------

function ability_omniknight_justice:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_omniknight_justice:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------