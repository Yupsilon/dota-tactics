ability_omniknight_hammer = class({})
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_weak","abilities/generic/generic_modifier_weak.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_shield","abilities/generic/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_omniknight_hammer:SolveAction(keys)

	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;			
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})	
	
	if keys.target_point_A~=nil then self:CastInstance(keys.target_point_A) end	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_omniknight_hammer:CastInstance(point)

			local cleave_range = self:GetSpecialValueFor("cleave_range")
			local cleave_angle = self:GetSpecialValueFor("cleave_angle")
				
			local startPoint = self:GetCaster():GetAbsOrigin()
			local direction = (point-startPoint):Normalized()
			direction.z=0
					
			local animationTime = PLAYER_TIME_MODIFIER;
						   
				self:GetCaster():FaceTowards(point)			
			StartAnimation(self:GetCaster(), {duration=animationTime, rate=1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_ATTACK})	
								
				EmitSoundOn( "Hero_Omniknight.PreAttack", self:GetCaster() )
			Timers:CreateTimer(animationTime*3/5, function() 
			
			EmitSoundOn( "Hero_Omniknight.Attack", self:GetCaster() )
			
				
						
				if self:GetLevel() == 4 then
					self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_shield",{life= self:GetSpecialValueFor("bonus_selfshield") , turns = self:GetSpecialValueFor("bonus_selfshield_duration"),applythisturn=0})		
				end
			
				local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_mars/mars_shield_bash.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )				
				ParticleManager:SetParticleControl( nFXIndex, 0, startPoint )
				ParticleManager:SetParticleControlForward( nFXIndex, 0, (point-startPoint):Normalized());
				ParticleManager:SetParticleControl( nFXIndex, 1, Vector(1,1,1)*cleave_range )
				ParticleManager:SetParticleControl( nFXIndex, 60, Vector(248, 222, 126) )
				ParticleManager:SetParticleControl( nFXIndex, 61, Vector(1,0,0) )
				ParticleManager:ReleaseParticleIndex( nFXIndex )

				local enemies = FindUnitsInCone({				
					caster = self:GetCaster(),
					startPoint = startPoint,
					startAngle = math.atan2(direction.x,direction.y),
					search_range = cleave_range,
					search_angle = cleave_angle,
					searchteam = DOTA_UNIT_TARGET_TEAM_ENEMY,
					searchtype = DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
					searchflags = 0
				})
				
				if #enemies > 0 then
					for _,enemy in pairs(enemies) do
						if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
					
								local enemy_direction = (enemy:GetAbsOrigin()-startPoint)
								local projectile = {
								EffectAttach = PATTACH_WORLDORIGIN,			  			  
								vSpawnOrigin = startPoint+Vector(0,0,30),
								fDistance = cleave_range*2,
								fStartRadius = 50,
								fEndRadius = 50,
								fCollisionRadius = 30,
								  Source = self:GetCaster(),
								  vVelocity = enemy_direction:Normalized() * cleave_range, 
								  UnitBehavior = PROJECTILES_NOTHING,
								  bMultipleHits = true,
								  bIgnoreSource = true,
								  TreeBehavior = PROJECTILES_NOTHING,
								  bTreeFullCollision = false,
								  WallBehavior = PROJECTILES_DESTROY,
								  GroundBehavior = PROJECTILES_DESTROY,
								  fGroundOffset = 0,
								  bZCheck = false,
								  bGroundLock = false,
								  draw = false,
								  bUseFindUnitsInRadius = true,
								  bHitScan=true,
								  OnFinish = function(instance, pos) ParticleManager:ReleaseParticleIndex( nFXIndex ) end,
								  UnitTest = function(instance, unit) return unit==enemy end,
								  OnUnitHit = function(instance, unit) 
															
														
										local damage_table = {
											victim = enemy,
											attacker = self:GetCaster(),
											damage = self:GetSpecialValueFor("cleave_damage"),
											damage_type = DAMAGE_TYPE_MAGICAL,
											cover_reduction = 1
										}	
										local energyGain = self:GetSpecialValueFor("cleave_energy")
										
					
										if self:GetLevel()==2 and self:GetCaster():HasModifier("modifier_omniknight_archangel") then
											energyGain = energyGain+self:GetSpecialValueFor("bonus_energy")
										elseif self:GetLevel() == 3 then
										
												local slowModifier = enemy:AddNewModifier( self:GetCaster(), self, "generic_modifier_movespeed_slow", {
													turns = self:GetSpecialValueFor("modifier_duration"),
													move_slow = self:GetSpecialValueFor("bonus_slow"),
													nodraw = 1
												} )		
											--enemy:AddNewModifier( self:GetCaster(), self, "generic_modifier_stun", {turns = self:GetSpecialValueFor("bonus_stun_duration")} )	
											--damage_table.scar_damage_direct = self:GetSpecialValueFor("bonus_scar")
										end
										
									EmitSoundOn( "Hero_Omniknight.Attack.Impact", enemy )
										
											
											enemy:AddNewModifier( self:GetCaster(), self, "generic_modifier_weak", {value = self:GetSpecialValueFor("cleave_weaken"),turns = self:GetSpecialValueFor("modifier_duration"),applythisturn=0} )		
										
																
										if IsTargetOverCover(instance,damage_table.attacker) then
											damage_table.cover_reduction=1/2
										end

										Alert_Damage( damage_table )
										Alert_Mana({unit=self:GetCaster(),manavalue=energyGain, energy=true});
								  end,
								}						
								Projectiles:CreateProjectile(projectile)
							
							end
					
						end
				end
			end)
end

--------------------------------------------------------------------------------

function ability_omniknight_hammer:getAbilityType()
	return ABILITY_TYPE_CONE;
end

--------------------------------------------------------------------------------

function ability_omniknight_hammer:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_omniknight_hammer:GetPriority()
	return 1;
end

--------------------------------------------------------------------------------

function ability_omniknight_hammer:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_omniknight_hammer:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_omniknight_hammer:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_omniknight_hammer:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("cleave_angle")*2
end

--------------------------------------------------------------------------------

function ability_omniknight_hammer:GetMinRange(coneID)	
	return self:GetSpecialValueFor("cleave_range")
end

--------------------------------------------------------------------------------

function ability_omniknight_hammer:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("cleave_range")
end

--------------------------------------------------------------------------------

function ability_omniknight_hammer:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------