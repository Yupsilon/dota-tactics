modifier_omniknight_heal = class({})

--------------------------------------------------------------------------------

function modifier_omniknight_heal:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_omniknight_heal:OnCreated( kv )
	
	self:SetStackCount(kv.turns or 2	)	
	
	if IsServer() then
		
		local nFXIndex = ParticleManager:CreateParticle( "particles/econ/items/omniknight/omni_ti8_head/omniknight_repel_buff_ti8.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		ParticleManager:SetParticleControlEnt(nFXIndex, 0, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_origin", self:GetParent():GetAbsOrigin(), true)
		self:AddParticle( nFXIndex, false, false, -1, false, false )	
	end
end

--------------------------------------------------------------------------------

function modifier_omniknight_heal:Activate ( )

	--local animationTime = PLAYER_TIME_MODIFIER*2;
		--StartAnimation(self:GetCaster(), {duration=animationTime, rate = 1/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_CAST_ABILITY_1})
		
		local helix_aoe = self:GetAbility():GetSpecialValueFor("burst_aoe")
		local helix_damage = self:GetAbility():GetSpecialValueFor("burst_damage")
		
		--EmitSoundOn( "Hero_Omniknight.Repel", self:GetParent() )
		
		local helix = ParticleManager:CreateParticle("particles/units/heroes/hero_keeper_of_the_light/keeper_of_the_light_blinding_light_aoe.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
		ParticleManager:SetParticleControl( helix, 0, Vector(0,0,-999) )	
		ParticleManager:SetParticleControl( helix, 1, self:GetParent():GetAbsOrigin() )	
		ParticleManager:SetParticleControl(helix, 1, Vector(helix_aoe,helix_aoe,helix_aoe))		
		ParticleManager:ReleaseParticleIndex(helix)	

			
				
					local enemies = FindUnitsInRadius( self:GetParent():GetTeamNumber(), self:GetParent():GetAbsOrigin(), self:GetParent(),helix_aoe , DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
					if #enemies > 0 then
						for _,enemy in pairs(enemies) do
							if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
						
									local direction = enemy:GetAbsOrigin()-self:GetParent():GetAbsOrigin()
									direction.z=0
									local projectile = {
									EffectAttach = PATTACH_WORLDORIGIN,			  			  
									vSpawnOrigin = self:GetParent():GetAbsOrigin()+Vector(0,0,30),
									fDistance = helix_aoe*2,
									fStartRadius = 50,
									fEndRadius = 50,
									fCollisionRadius = 30,
									  Source = self:GetCaster(),
									  vVelocity = direction:Normalized() * helix_aoe*10, 
									  UnitBehavior = PROJECTILES_NOTHING,
									  bMultipleHits = true,
									  bIgnoreSource = true,
									  TreeBehavior = PROJECTILES_NOTHING,
									  bTreeFullCollision = false,
									  WallBehavior = PROJECTILES_DESTROY,
									  GroundBehavior = PROJECTILES_DESTROY,
									  fGroundOffset = 0,
									  bZCheck = false,
									  bGroundLock = false,
									  draw = false,
									  bUseFindUnitsInRadius = true,
									  bHitScan = true,

									  UnitTest = function(instance, unit) return unit==enemy end,
									  OnUnitHit = function(instance, unit) 
															
											if GameMode:GetCurrentGamePhase() == GAMEPHASE_PREP then
											
												enemy:AddNewModifier( self:GetCaster(), self:GetAbility(), "generic_modifier_weak", {value = self:GetAbility():GetSpecialValueFor("bonus_weaken"),turns = self:GetAbility():GetSpecialValueFor("bonus_weaken_duration"),applythisturn=0} )			
											else
												--[[local damage_table = {
													victim = enemy,
													attacker = self:GetCaster(),
													damage = helix_damage,
													damage_type = DAMAGE_TYPE_MAGICAL,
													ability = self:GetAbility(),
													cover_reduction = 1
												}
										
												if IsTargetOverCover(instance,damage_table.attacker) then
													damage_table.cover_reduction=1/2
												end												

												Alert_Damage( damage_table )]]
												
												
												local enemy_direction = (enemy:GetAbsOrigin()-self:GetParent():GetAbsOrigin())
													
													local kbDirection = enemy_direction:Normalized() * self:GetAbility():GetSpecialValueFor("burst_damage")
													enemy:AddNewModifier(self:GetCaster(),nil,"modifier_order_knockback",{
														direction_x = kbDirection.x,
														direction_y = kbDirection.y		
													})													
												
												Alert_Mana({unit=self:GetCaster(),manavalue=self:GetAbility():GetSpecialValueFor("hit_energy"), energy=true});
											end
									  end,
									}						
									Projectiles:CreateProjectile(projectile)
														
							end
						end
					end							
				

end
	
--------------------------------------------------------------------------------

function modifier_omniknight_heal:SolveAction()
	if IsServer() then		
		self:Activate();	
	end
	return MODIFIERACTION_SHORTPAUSE_SURVIVE;	
end

--------------------------------------------------------------------------------

function modifier_omniknight_heal:EndOfTurnAction()	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
