modifier_omniknight_archangel = class({})

--------------------------------------------------------------------------------

function modifier_omniknight_archangel:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function modifier_omniknight_archangel:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_omniknight_archangel:OnCreated( kv )
	if not IsServer() then return end
	self:SetStackCount(1)

	local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_omniknight/omniknight_guardian_angel_omni.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )
        ParticleManager:SetParticleControl(nFXIndex, 0, self:GetParent():GetAbsOrigin())
        ParticleManager:SetParticleControlEnt(nFXIndex, 5, self:GetParent(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetParent():GetAbsOrigin(), true)
        self:AddParticle(nFXIndex, false, false, -1, false, false)  
end

--------------------------------------------------------------------------------

function modifier_omniknight_archangel:Activate ( )

		local aura_aoe = self:GetAbility():GetSpecialValueFor("aura_aoe")		
		local aura_shield = self:GetAbility():GetSpecialValueFor("aura_shield")		
		local helix_center = self:GetParent():GetAbsOrigin()
		
		if self:GetAbility():GetLevel()==3 then
			aura_shield = aura_shield+math.max(0,(self:GetStackCount()-1)*self:GetAbility():GetSpecialValueFor("bonus_shield"))
		end
	
		Alert_Mana({unit=self:GetCaster(),manavalue=0-self:GetManaRequirement()} )
		StartAnimation(self:GetParent(), {duration=PLAYER_TIME_MODIFIER*2, rate = 1/PLAYER_TIME_MODIFIER,activity=ACT_DOTA_CAST_ABILITY_4})
				
		EmitSoundOn( "Hero_Omniknight.GuardianAngel", self:GetParent() )
		
		local helix = ParticleManager:CreateParticle("particles/items_fx/arcane_boots.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent())
		ParticleManager:SetParticleControl(helix, 0, helix_center)		
		ParticleManager:ReleaseParticleIndex(helix)	
				
		local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), helix_center, nil,aura_aoe , DOTA_UNIT_TARGET_TEAM_FRIENDLY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
					if #enemies > 0 then
						for _,hTarget in pairs(enemies) do
						
							if hTarget ~= nil and ( not hTarget:IsInvulnerable() )  then
							
								local other_modifier = hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_shield", {life= aura_shield , turns = self:GetAbility():GetSpecialValueFor("modifier_duration") , nodraw=1} )
								
								if self:GetAbility():GetLevel()==2 then
									hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_splash_resistance", {value = self:GetAbility():GetSpecialValueFor("bonus_resistance") ,turns = self:GetAbility():GetSpecialValueFor("modifier_duration") , nodraw=0} )
								elseif self:GetAbility():GetLevel()==4 and self:GetStackCount() == 1 then
									hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_unstoppable", {turns = self:GetAbility():GetSpecialValueFor("modifier_duration") , nodraw=1} )
								end
								
								local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_omniknight/omniknight_guardian_angel_ally.vpcf", PATTACH_ABSORIGIN_FOLLOW, hTarget )
									ParticleManager:SetParticleControlEnt(nFXIndex, 0, hTarget, PATTACH_POINT_FOLLOW, "attach_origin", hTarget:GetAbsOrigin(), true)
									ParticleManager:SetParticleControl( nFXIndex, 1, Vector(hTarget:GetHullRadius(),0,hTarget:GetHullRadius()) )		
									other_modifier:AddParticle( nFXIndex, false, false, -1, false, false )	
														
							end
						end
					end							
				

								local self_modifier = self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_unstoppable", { turns = 1 } )
								
								--local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_dark_seer/dark_seer_ion_shell.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )
									--ParticleManager:SetParticleControlEnt(nFXIndex, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_hitloc", self:GetCaster():GetAbsOrigin(), true)
									--ParticleManager:SetParticleControl( nFXIndex, 1, Vector(self:GetCaster():GetHullRadius(),0,self:GetCaster():GetHullRadius()) )		
									--self_modifier:AddParticle( nFXIndex, false, false, -1, false, false )
end
	
--------------------------------------------------------------------------------

function modifier_omniknight_archangel:GetManaRequirement()	
	
	return (self:GetStackCount())*self:GetAbility():GetManaCost(self:GetAbility():GetLevel())+self:GetCaster():GetIntellect();
end
	
--------------------------------------------------------------------------------

function modifier_omniknight_archangel:PrepAction()	
	
	if self:GetCaster():GetMana()>self:GetManaRequirement() then

		self:SetStackCount(self:GetStackCount()+1)
		self:Activate();
	
		return MODIFIERACTION_SHORTPAUSE_SURVIVE;
	else
		return MODIFIERACTION_SHORTPAUSE_DESTROY;
	end
end
	
--------------------------------------------------------------------------------

function modifier_omniknight_archangel:OnDestroy()	

	if IsServer() then
		self:GetAbility().LastCastTurn = GameMode:GetCurrentTurn()+math.ceil(self:GetAbility():GetCooldown(self:GetAbility():GetLevel()));
		self:GetAbility():UseResources(false,false,true)	
		self:GetCaster():AddNewModifier(self:GetCaster(),self:GetAbility(),"modifier_ability_cooldown",{})	
	end
end

--------------------------------------------------------------------------------
