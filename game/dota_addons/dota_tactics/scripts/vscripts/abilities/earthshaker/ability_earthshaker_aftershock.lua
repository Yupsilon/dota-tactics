 ability_earthshaker_aftershock = class({})
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_weak","abilities/generic/generic_modifier_weak.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_combobreaker","abilities/generic/generic_modifier_combobreaker.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_order_knockback","mechanics/heroes/modifier_order_knockback.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_action_knockback","mechanics/heroes/modifier_action_knockback.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_earthshaker_aftershock:SolveAction(keys)

	if keys.target_point_A~=nil then
				
			local cone_full_range = self:GetSpecialValueFor("cone_full_range")
			local projectile_radius = self:GetSpecialValueFor("projectile_radius")
			local projectile_angle = (self:GetSpecialValueFor("projectile_angle")*math.pi/180)/2
			local projectile_count = self:GetSpecialValueFor("cone_angle")/self:GetSpecialValueFor("projectile_angle")*2
	
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,false)	
			Alert_Mana({unit=self:GetCaster(),manavalue=0-self:GetManaCost(self:GetLevel());} )
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			
			local startPoint = keys.caster:GetAbsOrigin() + Vector(0,0,50)*keys.caster:GetModelScale();		
			local direction = (keys.target_point_A-startPoint):Normalized()
			direction.z=0
			startPoint=startPoint+direction*projectile_radius/2
			
			local animationTime = 2 * PLAYER_TIME_MODIFIER;
			local decalSpeed = math.max(900,cone_full_range/PLAYER_TIME_MODIFIER)*3
			local projectile_model = "particles/units/heroes/hero_earthshaker/earthshaker_echoslam.vpcf"
			
			if self:GetLevel() == 4 then
				self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_haste",{turns = self:GetSpecialValueFor("bonus_haste_duration"),applythisturn=1})	
				self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_unstoppable",{turns = self:GetSpecialValueFor("bonus_haste_duration")})		
			end
			
				keys.caster:FaceTowards(keys.target_point_A)
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_1, rate = 1/PLAYER_TIME_MODIFIER})
			
			Timers:CreateTimer(animationTime*2/7,function()
	
				EmitSoundOn( "Hero_EarthShaker.EchoSlam", self:GetCaster() )
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_4, rate = 1/PLAYER_TIME_MODIFIER})
	
				local startangle = math.atan2(direction.x,direction.y)
	
				for i=0,projectile_count-1 do
					
					local fireAngle = startangle-projectile_angle*(projectile_count-1)/2+projectile_angle*i
					local newdirection = Vector(math.sin(fireAngle),math.cos(fireAngle),0)
				
					local nChainParticleFXIndex = ParticleManager:CreateParticle( "particles/econ/items/lion/lion_ti9/lion_spell_impale_ti9.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster() )			ParticleManager:SetParticleControl( nChainParticleFXIndex, 0, startPoint )
					ParticleManager:SetParticleControl( nChainParticleFXIndex, 1, newdirection*decalSpeed )
					ParticleManager:SetParticleControl( nChainParticleFXIndex, 2, Vector( 0.5, 0, 0 ) )
			
					local projectile = {
					  --EffectName = projectile_model,			  			  
					  vSpawnOrigin = startPoint,
					  fDistance = cone_full_range,
					  fStartRadius = projectile_radius,
					  fEndRadius = projectile_radius,
					  fCollisionRadius = 30,
					  Source = self:GetCaster(),
					  vVelocity = newdirection * decalSpeed, 
					  UnitBehavior = PROJECTILES_NOTHING,
					  bMultipleHits = false,
					  bIgnoreSource = true,
					  TreeBehavior = PROJECTILES_NOTHING,
					  bTreeFullCollision = false,
					  WallBehavior = PROJECTILES_DESTROY,
					  GroundBehavior = PROJECTILES_DESTROY,
					  fGroundOffset = 0,
					  bZCheck = false,
					  bGroundLock = false,
					  draw = false,
					  bUseFindUnitsInRadius = true,
					  bHitScan=false,

					  UnitTest = function(instance, unit) return unit:IsInvulnerable() == false and unit:GetTeamNumber() ~= keys.caster:GetTeamNumber() and unit:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())==nil end,
					  OnUnitHit = function(instance, unit) 
						
							self:OnProjectileHit(unit,instance:GetPosition(),(instance:GetPosition()-startPoint):Length2D())
							--EmitSoundOn( "Hero_DrowRanger.ProjectileImpact", unit )
						
					  end,
					  OnFinish = function(instance, pos)		  
						ParticleManager:DestroyParticle( nChainParticleFXIndex, false )
						ParticleManager:ReleaseParticleIndex( nChainParticleFXIndex )
								
					  end
					}
							
						
							
					Projectiles:CreateProjectile(projectile)
				end
			end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_earthshaker_aftershock:GetPriority()
	return 5;
end

--------------------------------------------------------------------------------

function ability_earthshaker_aftershock:OnProjectileHit(hTarget, vLocation,fDistance)
					
					if hTarget:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())~=nil then
						return
					else
						hTarget:AddNewModifier(self:GetCaster(),self,"modifier_hit",{turns=1})
					end
					
					local damage_table = {
							victim = hTarget,
							attacker = self:GetCaster(),
							damage = self:GetSpecialValueFor("cleave_damage_full") ,
							damage_type = DAMAGE_TYPE_MAGICAL,
							ability=self,
							cover_reduction = 1
						}
					local knockback_distance = self:GetSpecialValueFor("knockback_distance_full") 
					
								
					if self:GetLevel() == 2 then
						damage_table.damage = damage_table.damage + self:GetSpecialValueFor("bonus_damage") 					
					end
								
										if fDistance>self:GetSpecialValueFor("cone_half_range")  then
											damage_table.damage = self:GetSpecialValueFor("cleave_damage_half")
											knockback_distance = self:GetSpecialValueFor("knockback_distance_half") 
										end
					
					if IsTargetPointOverCover(vLocation,damage_table.attacker) then
						damage_table.cover_reduction=1/2					
					end
				
						Alert_Damage(damage_table)
						
												
						local kbDirection = (hTarget:GetAbsOrigin()-self:GetCaster():GetAbsOrigin()):Normalized()*knockback_distance
												
						hTarget:AddNewModifier(self:GetCaster(),nil,"modifier_order_knockback",{
							direction_x = kbDirection.x,
							direction_y = kbDirection.y		
						})			
					if self:GetLevel() == 3 then
						hTarget:AddNewModifier( self:GetCaster(), self, "generic_modifier_weak", {turns = self:GetSpecialValueFor("bonus_weaken_duration"),applythisturn=0} )						
					end
end

--------------------------------------------------------------------------------

function ability_earthshaker_aftershock:getAbilityType()
	return ABILITY_TYPE_DOUBLE_CONE;
end

--------------------------------------------------------------------------------

function ability_earthshaker_aftershock:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_earthshaker_aftershock:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_earthshaker_aftershock:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_earthshaker_aftershock:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_earthshaker_aftershock:GetConeArc(coneID,coneRange)
	if coneID == 1 or coneID == 3 then return self:GetSpecialValueFor("cone_angle") / self:GetSpecialValueFor("projectile_angle") end
	return self:GetSpecialValueFor("cone_angle")*2
end

--------------------------------------------------------------------------------

function ability_earthshaker_aftershock:GetMinRange(coneID)	
	if coneID== 1 then return self:GetSpecialValueFor("cone_half_range") end
	return self:GetSpecialValueFor("cone_full_range")
end

--------------------------------------------------------------------------------

function ability_earthshaker_aftershock:GetMaxRange(coneID)	
	if coneID== 1 then return self:GetSpecialValueFor("cone_half_range") end
	return self:GetSpecialValueFor("cone_full_range")
end

--------------------------------------------------------------------------------

function ability_earthshaker_aftershock:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_earthshaker_aftershock:GetHasteBonus()	
	if self:GetLevel()==4 then			
		return (100+(self:GetSpecialValueFor("bonus_haste")))/100
	end
	return 1
end

--------------------------------------------------------------------------------