modifier_earthshaker_embrace = class({})

-----------------------------------------------------------------------------

function modifier_earthshaker_embrace:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function modifier_earthshaker_embrace:IsDebuff()
	return false
end

--------------------------------------------------------------------------------

function modifier_earthshaker_embrace:OnCreated( kv )

	local turnsDuration = kv.turns or 2
	self:SetStackCount(turnsDuration)	
	
	if IsServer() then
			self.turns = 1
			self.inc_damage = kv.incoming or 25
				
			--local nFXIndex = ParticleManager:CreateParticle( "particles/items2_fx/mask_of_madness.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
			--self:AddParticle( nFXIndex, false, false, -1, false, false )		
			--EmitSoundOn( "DOTA_Item.MaskOfMadness.Activate", self:GetCaster() )				
		
	end
end

--------------------------------------------------------------------------------

function modifier_earthshaker_embrace:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_INCOMING_DAMAGE_PERCENTAGE,
	}

	return funcs
end

--------------------------------------------------------------------------------

function modifier_earthshaker_embrace:GetModifierIncomingDamage_Percentage(params)
	return -math.abs(self.inc_damage)
end
	
--------------------------------------------------------------------------------

function modifier_earthshaker_embrace:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self.turns=self.turns+1
		self:DecrementStackCount()
		return MODIFIERACTION_NOPAUSE_SURVIVE;
	end
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function modifier_earthshaker_embrace:GetStatusEffectName()
	return "particles/status_fx/status_effect_earth_spirit_petrify.vpcf"
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------