ability_earthshaker_fissure = class({})
LinkLuaModifier( "modifier_hit","abilities/modifier_hit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_order_knockback","mechanics/heroes/modifier_order_knockback.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_action_knockback","mechanics/heroes/modifier_action_knockback.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_shield","mechanics/heroes/generic_modifier_shield.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_combobreaker","abilities/generic/generic_modifier_combobreaker.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_earthshaker_fissure:SolveAction(keys)

	if keys.target_point_A~=nil then
						
			local projectile_range = self:GetSpecialValueFor("projectile_range")
			local projectile_radius = self:GetSpecialValueFor("projectile_radius") /2
		
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})	
			
			local startDistance = projectile_radius/2;
			
			local startPoint = keys.caster:GetAbsOrigin() + Vector(0,0,50)*keys.caster:GetModelScale();	
			local direction = (keys.target_point_A-startPoint):Normalized()
			direction.z=0		
			startPoint=startPoint+direction*startDistance
			
			local animationTime = 2*PLAYER_TIME_MODIFIER;
			local decalSpeed = 1800
			
						   
				keys.caster:FaceTowards(keys.target_point_A)
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_1, rate = 1/PLAYER_TIME_MODIFIER})	
			
			local unit_behavior = PROJECTILES_DESTROY
			
									
			if self:GetLevel() == 2 then
				self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_shield",{life= self:GetSpecialValueFor("bonus_selfshield") , turns = self:GetSpecialValueFor("bonus_selfshield_duration"), applythisturn=0})	
			elseif self:GetLevel() == 3 then
				projectile_range = projectile_range+ self:GetSpecialValueFor("bonus_range")
			elseif self:GetLevel() == 4 then
				unit_behavior = PROJECTILES_NOTHING
			end
			EmitSoundOn( "Hero_EarthShaker.Woosh", self:GetCaster() )
			
			Timers:CreateTimer(animationTime/2, function() 

			self.GaveEnergy=false
						EmitSoundOn( "Hero_EarthShaker.Fissure", self:GetCaster()  )
			local proj_hit = false
	
				local projectile = {
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint,
				  fDistance = projectile_range-startDistance-projectile_radius,
				  fStartRadius = projectile_radius,
				  fEndRadius = projectile_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  vVelocity = direction * decalSpeed, 
				  UnitBehavior = unit_behavior,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_DESTROY,
				  GroundBehavior = PROJECTILES_DESTROY,
				  fGroundOffset = 0,
				  bZCheck = false,
				  bGroundLock = false,
				  draw = false,
				  bUseFindUnitsInRadius = true,
				  bHitScan = true,

				  UnitTest = function(instance, unit) return unit:IsInvulnerable() == false and unit:GetTeamNumber() ~= keys.caster:GetTeamNumber() end,
				  OnUnitHit = function(instance, unit) 
					
						self:OnProjectileHit(unit,instance:GetPosition(),direction)
						proj_hit= true
				  end,
				  OnFinish = function(instance, pos)		  
			
						local vDelta = Vector (0,0,0)
						
						if proj_hit then
							vDelta=direction*projectile_radius/2
						end
			
						local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_earthshaker/earthshaker_fissure.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )				
						ParticleManager:SetParticleControl( nFXIndex, 0, startPoint )
						ParticleManager:SetParticleControl( nFXIndex, 1, pos+vDelta)
						ParticleManager:SetParticleControl( nFXIndex, 2, Vector (2,0,0))
						ParticleManager:ReleaseParticleIndex( nFXIndex )	
				  end
				  --OnTreeHit = function(self, tree) ... end,
				  --OnWallHit = function(self, gnvPos) ... end,
				  --OnGroundHit = function(self, groundPos) ... end,
				  --OnFinish = function(self, pos) ... end,
				}						
				Projectiles:CreateProjectile(projectile)
			end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_earthshaker_fissure:GetPriority()
	return 5;
end

--------------------------------------------------------------------------------

function ability_earthshaker_fissure:OnProjectileHit(hTarget, vLocation,vDirection)
					
					if hTarget:FindModifierByNameAndCaster("modifier_hit",self:GetCaster())~=nil then
						return
					else
						hTarget:AddNewModifier(self:GetCaster(),self,"modifier_hit",{turns=1})
					end
				
					local damagetype = DAMAGE_TYPE_PHYSICAL
					
					if self:GetLevel() == 4 then
						damagetype = DAMAGE_TYPE_MAGICAL
					end
					
					local damage_table = {
							victim = hTarget,
							attacker = self:GetCaster(),
							damage = self:GetSpecialValueFor("projectile_damage") or 1,
							damage_type = damagetype,
							ability=self,
							cover_reduction = 1
						}
					local dealt_push = self:GetSpecialValueFor("projectile_knockback")
					
					if self:GetLevel() == 3 then
						dealt_push = dealt_push+ self:GetSpecialValueFor("bonus_knockback")
					end
					
					if IsTargetPointOverCover(vLocation,damage_table.attacker) then
						damage_table.cover_reduction=1/2
					end
				
						Alert_Damage(damage_table)
												
						local kbDirection = vDirection:Normalized()*dealt_push
												
						hTarget:AddNewModifier(self:GetCaster(),nil,"modifier_order_knockback",{
							direction_x = kbDirection.x,
							direction_y = kbDirection.y		
						})
												
						if self.GaveEnergy==false then
							Alert_Mana({unit=self:GetCaster(), manavalue=self:GetSpecialValueFor("projectile_energy"), energy=true}  );
							self.GaveEnergy=true
						end
end

--------------------------------------------------------------------------------

function ability_earthshaker_fissure:getAbilityType()
	return 1;
end

--------------------------------------------------------------------------------

function ability_earthshaker_fissure:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_earthshaker_fissure:RequiresGridSnap()
	return true;
end

--------------------------------------------------------------------------------

function ability_earthshaker_fissure:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_earthshaker_fissure:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_earthshaker_fissure:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_earthshaker_fissure:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_earthshaker_fissure:GetMinRange(coneID)	
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_earthshaker_fissure:GetMaxRange(coneID)	
	if self:GetLevel() == 3 then
		return self:GetSpecialValueFor("projectile_range") + self:GetSpecialValueFor("bonus_range")
	end
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_earthshaker_fissure:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------