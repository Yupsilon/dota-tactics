ability_earthshaker_pound = class({})
LinkLuaModifier( "modifier_energy_regeneration","abilities/modifier_energy_regeneration.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_earthshaker_pound:GetIntrinsicModifierName()
	return "modifier_energy_regeneration"
end

--------------------------------------------------------------------------------

function ability_earthshaker_pound:IsDisabledBySilence()
	return false
end


--------------------------------------------------------------------------------

function ability_earthshaker_pound:GetDamageConversion()
	local value = 0
	if self:GetLevel()==3 then
		value = self:GetSpecialValueFor("bonus_energy_absorbtion")
	end
	
	local embrace = self:GetCaster():FindModifierByNameAndCaster("modifier_earthshaker_embrace",self:GetCaster())
	if embrace~=nil then		
		if embrace:GetAbility():GetLevel() == 4 then
			value = value + embrace:GetAbility():GetSpecialValueFor("bonus_energy_absorbtion")
		end
	end
	return value
end

--------------------------------------------------------------------------------

function ability_earthshaker_pound:GetRegenerationValue()
	return self:GetSpecialValueFor("base_energy_regen")
end

--------------------------------------------------------------------------------

function ability_earthshaker_pound:SolveAction(keys)

	if keys.target_point_A~=nil then				
		
		self.LastCastTurn = math.max(GameMode:GetCurrentTurn()+math.ceil(self:GetCooldown(self:GetLevel())),self.LastCastTurn or -1);	
		self.firstHit = false
			
		local startPoint = keys.caster:GetAbsOrigin()
		local direction = (keys.target_point_A-startPoint)
		direction.z=0		
		local animationTime = 35/30*PLAYER_TIME_MODIFIER;
						
		if self:GetLevel()==4 then
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_haste",{speed=self:GetSpecialValueFor("bonus_haste"),turns = self:GetSpecialValueFor("bonus_haste_duration"),applythisturn=1})
		end
			
		if direction:Length2D() <= self:GetMinRange(0) then
		
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_2,rate = 1/PLAYER_TIME_MODIFIER})
				
			EmitSoundOn( "Hero_EarthShaker.Woosh", self:GetCaster() )
			
			Timers:CreateTimer(animationTime/2, function() 			
				EmitSoundOn( "Hero_EarthShaker.Totem", self:GetCaster() )

				local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_earthshaker/earthshaker_aftershock.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )				
				ParticleManager:SetParticleControl( nFXIndex, 0, startPoint )			
				ParticleManager:SetParticleControl( nFXIndex, 1, Vector(1,1,1) * self:GetSpecialValueFor("slam_range"))
				ParticleManager:ReleaseParticleIndex( nFXIndex )	
				
				local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), startPoint, self:GetCaster(), self:GetSpecialValueFor("slam_range"), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, FIND_CLOSEST, false )
				if #enemies > 0 then
					for _,enemy in pairs(enemies) do
						if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
							self:StrikeTarget(enemy,false)						
						end
					end
				end	
			
			end)			
		
		else
						   
			keys.caster:FaceTowards(keys.target_point_A)
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_ATTACK, translate="enchant_totem",rate = 1/PLAYER_TIME_MODIFIER})

				
			EmitSoundOn( "Hero_EarthShaker.Woosh", self:GetCaster() )
			
			Timers:CreateTimer(animationTime*4/7, function() 			
				EmitSoundOn( "Hero_EarthShaker.Totem.Attack", self:GetCaster() )
			
				
				local enemies = FindUnitsInCone({				
					caster = self:GetCaster(),
					startPoint = startPoint,
					startAngle = math.atan2(direction.x,direction.y)		,
					search_range = self:GetSpecialValueFor("cleave_range"),
					search_angle = self:GetSpecialValueFor("cleave_angle"),
					searchteam = DOTA_UNIT_TARGET_TEAM_ENEMY,
					searchtype = DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
					searchflags = 0
				})
				if #enemies > 0 then
					for _,enemy in pairs(enemies) do
						if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
							self:StrikeTarget(enemy,true)
						end
					end
				end	
			
			end)
		end
			
				
				
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_earthshaker_pound:StrikeTarget(hTarget,strong)

	local startPoint = self:GetCaster():GetAbsOrigin()
	local enemy_direction = (hTarget:GetAbsOrigin()-startPoint)
							
	local selfheal = 0
	local cleave_damage = self:GetSpecialValueFor("cleave_damage_half")
	if strong then
		cleave_damage = self:GetSpecialValueFor("cleave_damage_full")
	end
	
	if self:GetLevel()==2 then
		if strong then
			selfheal = self:GetSpecialValueFor("bonus_selfheal_full")											
		end	
		selfheal = self:GetSpecialValueFor("bonus_selfheal_half")	
	end	
										 
										
	local projectile = {
								EffectAttach = PATTACH_WORLDORIGIN,			  			  
								vSpawnOrigin = startPoint+Vector(0,0,30),
								fDistance =  enemy_direction:Length2D(),
								fStartRadius = 50,
								fEndRadius = 50,
								fCollisionRadius = 30,
								  Source = self:GetCaster(),
								  vVelocity = enemy_direction:Normalized() * enemy_direction:Length2D()*2, 
								  UnitBehavior = PROJECTILES_NOTHING,
								  bMultipleHits = true,
								  bIgnoreSource = true,
								  TreeBehavior = PROJECTILES_NOTHING,
								  bTreeFullCollision = false,
								  WallBehavior = PROJECTILES_DESTROY,
								  GroundBehavior = PROJECTILES_DESTROY,
								  fGroundOffset = 0,
								  bZCheck = false,
								  bGroundLock = false,
								  draw = false,
								  bUseFindUnitsInRadius = true,
								  bHitScan=true,
								  UnitTest = function(instance, unit) return unit==hTarget end,
								  OnUnitHit = function(instance, unit) 
															 
									local hit_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_earthshaker/earthshaker_totem_leap_impact.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())
									ParticleManager:SetParticleControl(hit_particle, 0, unit:GetAbsOrigin())
									ParticleManager:ReleaseParticleIndex(hit_particle)
														
										local damage_table = {
											victim = hTarget,
											attacker = self:GetCaster(),
											damage = cleave_damage,
											damage_type = DAMAGE_TYPE_MAGICAL,
											cover_reduction = 1,
										}	
										if self.firstHit == false then
											self.firstHit = true
											damage_table.damage_type = DAMAGE_TYPE_PHYSICAL
										end						
																
										if IsTargetOverCover(instance,damage_table.attacker) then
											damage_table.cover_reduction=1/2
										end
										
										if selfheal>0 then
											Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=selfheal})
										end

										Alert_Damage( damage_table )
										Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("cleave_energy"), energy=true} );										
				
										--ParticleManager:SetParticleControl( nFXIndex, nFXEnemies, unit:GetAbsOrigin() )
										
										--nFXEnemies = nFXEnemies+1
								  end,
								}						
								Projectiles:CreateProjectile(projectile)
end

--------------------------------------------------------------------------------

function ability_earthshaker_pound:getAbilityType()
	return ABILITY_TYPE_CONE;
end

--------------------------------------------------------------------------------

function ability_earthshaker_pound:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_earthshaker_pound:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_earthshaker_pound:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_earthshaker_pound:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_earthshaker_pound:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_earthshaker_pound:GetConeArc(coneID,coneRange)

	local angle = self:GetSpecialValueFor("cleave_angle")
	if coneRange<=self:GetMinRange(0) then
		angle = 360
	end
	--if coneID == 3 then return self:GetSpecialValueFor("cleave_angle_narrow")/20 end
	--if coneID == 2 then return self:GetSpecialValueFor("cleave_angle_narrow")*2 end
	
	
	if coneID == 1 then return angle/self:GetSpecialValueFor("cleave_angle") end
	return angle*2
end

--------------------------------------------------------------------------------

function ability_earthshaker_pound:GetMinRange(coneID)	
	return self:GetSpecialValueFor("slam_range")
end

--------------------------------------------------------------------------------

function ability_earthshaker_pound:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("cleave_range")
end

--------------------------------------------------------------------------------

function ability_earthshaker_pound:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_earthshaker_pound:GetHasteBonus()	
	if self:GetLevel()==4 then			
		return (100+(self:GetSpecialValueFor("bonus_haste")))/100
	end
	return 1
end

--------------------------------------------------------------------------------