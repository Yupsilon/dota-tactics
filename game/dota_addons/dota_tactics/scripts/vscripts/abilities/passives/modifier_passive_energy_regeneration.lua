modifier_passive_energy_regeneration = class({})

--------------------------------------------------------------------------------

function modifier_passive_energy_regeneration:IsHidden()
	return true
end

--------------------------------------------------------------------------------

--------------------------------------------------------------------------------

function modifier_passive_energy_regeneration:GetRegenerationValue()
	if self:GetAbility()==nil then
		return 5
	end	
	return (self:GetAbility():GetRegenerationValue())
end
	
--------------------------------------------------------------------------------

function modifier_passive_energy_regeneration:WalkMovementAction()
	
	if self:GetParent():IsAlive() then
		Alert_Mana({unit = self:GetParent(), manavalue = self:GetRegenerationValue(), energy = false})
	end
	return MODIFIERACTION_NOPAUSE_SURVIVE;
end
	
--------------------------------------------------------------------------------