modifier_passive_energy_absorption = class({})

--------------------------------------------------------------------------------

function modifier_passive_energy_absorption:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_passive_energy_absorption:GetAbsorptionPercentage()
	if self:GetAbility()==nil then
		return 1/5
	end	
	return (self:GetAbility():GetDamageConversion() or 20)/100
end

--------------------------------------------------------------------------------

function modifier_passive_energy_absorption:DeclareFunctions()
	

	local funcs = {
		MODIFIER_EVENT_ON_TAKEDAMAGE ,
	}	

	return funcs
end

--------------------------------------------------------------------------------

function modifier_passive_energy_absorption:OnTakeDamage ( params )

		if self:GetAbsorptionPercentage()==0 then
			return 
		end

		if IsServer() then
				local hAttacker = params.attacker
				local hVictim = params.unit

			if hVictim == self:GetParent() then
			
					Alert_Mana({unit = self:GetParent(), manavalue = params.damage*self:GetAbsorptionPercentage(), energy = false})
					
			end
		end
end

--------------------------------------------------------------------------------

function modifier_passive_energy_absorption:GetRegenerationValue()
	if self:GetAbility()==nil then
		return 5
	end	
	return (self:GetAbility():GetRegenerationValue())
end
	
--------------------------------------------------------------------------------

function modifier_passive_energy_absorption:WalkMovementAction()
	
	if self:GetParent():IsAlive() then
		Alert_Mana({unit = self:GetParent(), manavalue = self:GetRegenerationValue(), energy = false})
	end
	return MODIFIERACTION_NOPAUSE_SURVIVE;
end
	
--------------------------------------------------------------------------------