ability_pudge_rot = class({})
LinkLuaModifier( "modifier_pudge_rot_target","abilities/pudge/modifier_pudge_rot_target.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_pudge_rot_caster","abilities/pudge/modifier_pudge_rot_caster.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_might","abilities/generic/generic_modifier_might.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_pudge_rot:SolveAction(keys)

	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;			
	self:UseResources(false,false,true)	
	Alert_Mana({unit=self:GetCaster(),manavalue=0-self:GetManaCost(self:GetLevel());} )
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})

	local animationTime = PLAYER_TIME_MODIFIER;		
	StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_CAST_ABILITY_ROT, rate = 1/PLAYER_TIME_MODIFIER})	

	for abi = 0, 6, 1	do		
		Alert_CooldownRefresh(self:GetCaster(),abi,-10)			
	end
	
	local point = self:GetCaster():GetAbsOrigin()
	
		self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_pudge_rot_caster",{turns = self:GetSpecialValueFor("cloud_duration")})					
		
		if self:GetLevel()== 2 then 
			self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_unstoppable", {turns = self:GetSpecialValueFor("bonus_unstoppable_duration") } )		
		elseif self:GetLevel()== 3 then 
			self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_might", {value= self:GetSpecialValueFor("bonus_might") , turns = self:GetSpecialValueFor("bonus_might_duration") } )
		elseif self:GetLevel()== 4 then 
			Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=self:GetSpecialValueFor("bonus_self_heal")})	
			Alert_HealScars({unit = self:GetCaster(), value = self:GetSpecialValueFor("bonus_healing_total")})
		end
		
	
	
	return ABILITYACTION_COMPLETE_SHORT;
end

--------------------------------------------------------------------------------

function ability_pudge_rot:GetPriority()
	return 5;
end

--------------------------------------------------------------------------------

function ability_pudge_rot:GetAOERadius()
	return self:GetSpecialValueFor( "rot_aoe" )
end

--------------------------------------------------------------------------------

function ability_pudge_rot:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_pudge_rot:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function ability_pudge_rot:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_pudge_rot:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_pudge_rot:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--[[------------------------------------------------------------------------------

function ability_pudge_rot:GetConeArc(coneID,coneRange)
	return 100
end

--------------------------------------------------------------------------------

function ability_pudge_rot:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_pudge_rot:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("rot_aoe")
end

--------------------------------------------------------------------------------

function ability_pudge_rot:GetCastRange()	
	return self:GetSpecialValueFor("rot_aoe")
end
]]
--------------------------------------------------------------------------------