modifier_pudge_showhook = class({})

--------------------------------------------------------------------------------

function modifier_pudge_showhook:IsHidden()
	return true
end
--------------------------------------------------------------------------------

function modifier_pudge_showhook:OnCreated( kv )
	
	self.particle=nil
	self.target=nil
	
	if IsServer() then	
	
		self:StartIntervalThink( .1 )
	end
end

--------------------------------------------------------------------------------

function modifier_pudge_showhook:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_OVERRIDE_ANIMATION,
	}

	return funcs
end

--------------------------------------------------------------------------------

function modifier_pudge_showhook:GetOverrideAnimation( params )
	return ACT_DOTA_PUDGE_HOOK_POSE
end

--------------------------------------------------------------------------------

function modifier_pudge_showhook:Attach( particle, target )
	self.particle=particle
	self.target=target
end

--------------------------------------------------------------------------------

function modifier_pudge_showhook:OnDestroy( kv )

	if not IsServer() then return end
	
	local caster = self:GetCaster()
	local particle = self.particle
	
	StartAnimation(self:GetCaster(), {duration=2/3*PLAYER_TIME_MODIFIER, activity=ACT_DOTA_CHANNEL_ABILITY_1, rate = 1/PLAYER_TIME_MODIFIER})	
	EmitSoundOn( "Hero_Pudge.AttackHookRetract", caster )
	
	
	ParticleManager:SetParticleControl(particle, 2, Vector((self.target:GetAbsOrigin()-caster:GetAbsOrigin()):Length2D()/PLAYER_TIME_MODIFIER*3, 1, 1))
	ParticleManager:SetParticleControl(particle, 1, caster:GetAbsOrigin())
		
	Timers:CreateTimer(PLAYER_TIME_MODIFIER*1/2, function()	
		StopSoundOn("Hero_Pudge.AttackHookRetract", caster)
		ParticleManager:DestroyParticle(particle , false )
	end)
	
end
		
--------------------------------------------------------------------------------

function modifier_pudge_showhook:WalkMovementAction()
	
	return MODIFIERACTION_NOPAUSE_DESTROY;
end
	

--------------------------------------------------------------------------------

function modifier_pudge_showhook:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function modifier_pudge_showhook:OnIntervalThink()
	if IsServer() then					   
			
		ParticleManager:SetParticleControl( self.particle, 1, self.target:GetAbsOrigin() )
	
	end
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------