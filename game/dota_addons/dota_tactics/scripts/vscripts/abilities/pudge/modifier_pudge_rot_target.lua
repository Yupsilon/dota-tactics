modifier_pudge_rot_target = class({})

--------------------------------------------------------------------------------

function modifier_pudge_rot_target:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_pudge_rot_target:OnCreated( kv )
	
	if IsServer() then
	
		if self:GetCaster():GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
	
			Alert_Damage( {
											victim = self:GetParent(),
												attacker = self:GetCaster(),
												damage = self:GetAbility():GetSpecialValueFor("rot_damage"),
												damage_type = DAMAGE_TYPE_PURE,
												ability = self:GetAbility()
											} )	
		end
	
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_pudge/pudge_rot_recipient.vpcf", PATTACH_CUSTOMORIGIN, self:GetParent() )
		
		
		self:AddParticle( nFXIndex, false, false, -1, false, false )		
	end
end
	
--------------------------------------------------------------------------------

function modifier_pudge_rot_target:EndOfTurnAction()		
	
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------