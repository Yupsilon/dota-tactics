modifier_pudge_rot_caster = class({})

--------------------------------------------------------------------------------

function modifier_pudge_rot_caster:IsHidden()
	return true
end
--------------------------------------------------------------------------------

function modifier_pudge_rot_caster:OnCreated( kv )
	
	local turnsDuration = kv.turns or 2
	self:SetStackCount(turnsDuration)
	if IsServer() then	
	
		self.center = self:GetParent() :GetAbsOrigin()
	

		self.soundDummy = CreateUnitByName("npc_dummy_unit", self.center , true, nil, nil, DOTA_TEAM_NOTEAM)	
		self.soundDummy:FindAbilityByName("dummy_unit"):SetLevel(1)
		EmitSoundOn("Hero_Pudge.Rot", self.soundDummy)

		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_pudge/pudge_rot.vpcf", PATTACH_CUSTOMORIGIN, self:GetParent() )
		ParticleManager:SetParticleControl( nFXIndex, 0, self.center)
		ParticleManager:SetParticleControl( nFXIndex, 1, Vector(self:GetAbility():GetSpecialValueFor("rot_aoe"),1,1))
		self:AddParticle( nFXIndex, false, false, -1, false, false )	
	
		self:StartIntervalThink( .2 )
		self:OnIntervalThink()	
	end
end

--------------------------------------------------------------------------------

function modifier_pudge_rot_caster:EndOfTurnAction()
	
	if self:GetStackCount()>1 then	
		self:DecrementStackCount()
		return MODIFIERACTION_SHORTPAUSE_SURVIVE;
	end
	StopSoundOn("Hero_Pudge.Rot", self.soundDummy)
	self.soundDummy:Destroy()
	return MODIFIERACTION_SHORTPAUSE_DESTROY;
end
	

--------------------------------------------------------------------------------

function modifier_pudge_rot_caster:GetAttributes()
	return MODIFIER_ATTRIBUTE_MULTIPLE
end

--------------------------------------------------------------------------------

function modifier_pudge_rot_caster:OnIntervalThink()
	if IsServer() then
					   
			if GameMode:GetCurrentGamePhase()<GAMEPHASE_ACTION or GameMode:GetCurrentGamePhase()>GAMEPHASE_WALK then return end			
					   
			local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self.center, nil, self:GetAbility():GetSpecialValueFor("rot_aoe"), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
			if #enemies > 0 then
				for _,enemy in pairs(enemies) do
					if enemy ~= nil and ( not enemy:IsInvulnerable() ) and not enemy:HasModifier("modifier_pudge_rot_target") then
						enemy:AddNewModifier(self:GetCaster(),self:GetAbility(),"modifier_pudge_rot_target",{})
					end
				end
			end	
	
	end
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------