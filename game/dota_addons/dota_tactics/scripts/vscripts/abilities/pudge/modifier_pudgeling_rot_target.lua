modifier_pudgeling_rot_target = class({})

--------------------------------------------------------------------------------

function modifier_pudgeling_rot_target:IsHidden()
	return false
end

--------------------------------------------------------------------------------

function modifier_pudgeling_rot_target:OnCreated( kv )
	
	if IsServer() then
	
		if self:GetCaster():GetTeamNumber() ~= self:GetParent():GetTeamNumber() then
			
			local damage_table = {
											victim = self:GetParent(),
												attacker = self:GetCaster(),
												damage = self:GetAbility():GetSpecialValueFor("rot_damage"),
												damage_type = DAMAGE_TYPE_PURE,
												ability = self:GetAbility()
											}
					
											
			if self:GetAbility():GetLevel() == 2 then
				damage_table.damage =damage_table.damage/2
			end
			if self:GetAbility():GetLevel() == 4 then
				if GameMode:GetCurrentGamePhase()==GAMEPHASE_ACTION then
					self:GetParent():AddNewModifier(self:GetCaster(),self:GetAbility(),"generic_modifier_snare",{								
								turns = self:GetAbility():GetSpecialValueFor("bonus_snared_duration")	,
								--slow = self:GetAbility():GetSpecialValueFor("bonus_slow_strength")
							})
				end
			else
					self:GetParent():AddNewModifier(self:GetCaster(),self:GetAbility(),"generic_modifier_movespeed_slow",{								
								turns = self:GetAbility():GetSpecialValueFor("bonus_slow_duration")	,
								slow = self:GetAbility():GetSpecialValueFor("bonus_slow_strength"),
								applythisturn=0
							})
			end
			Alert_Damage(  damage_table)	
		end
	
		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_pudge/pudge_rot_recipient.vpcf", PATTACH_CUSTOMORIGIN, self:GetParent() )
		
		
		self:AddParticle( nFXIndex, false, false, -1, false, false )		
	end
end
	
--------------------------------------------------------------------------------

function modifier_pudgeling_rot_target:EndOfTurnAction()		
	
	return MODIFIERACTION_NOPAUSE_DESTROY;
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------