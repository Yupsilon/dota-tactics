modifier_pudgeling_rot_caster = class({})

--------------------------------------------------------------------------------

function modifier_pudgeling_rot_caster:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_pudgeling_rot_caster:OnCreated( kv )
	
	if IsServer() then	
		EmitSoundOn("Hero_Pudge.Rot", self:GetParent())

		local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_pudge/pudge_rot.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		ParticleManager:SetParticleControl( nFXIndex, 1, Vector(self:GetAbility():GetSpecialValueFor("rot_aoe"),1,1))
		self:AddParticle( nFXIndex, false, false, -1, false, false )	
	
		self:StartIntervalThink( .2 )
		self:OnIntervalThink()	
	end
end

--------------------------------------------------------------------------------

function modifier_pudgeling_rot_caster:OnDestroy( kv )
	StopSoundOn("Hero_Pudge.Rot", self:GetParent())
end

--------------------------------------------------------------------------------

function modifier_pudgeling_rot_caster:EndOfTurnAction()
	
		return MODIFIERACTION_SHORTPAUSE_SURVIVE;
end
	
--------------------------------------------------------------------------------

function modifier_pudgeling_rot_caster:OnIntervalThink()
	if IsServer() then
					   
			if GameMode:GetCurrentGamePhase()<GAMEPHASE_ACTION or GameMode:GetCurrentGamePhase()>GAMEPHASE_WALK then return end			
					   
			local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), self:GetParent():GetAbsOrigin(), nil, self:GetAbility():GetSpecialValueFor("rot_aoe"), DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
			if #enemies > 0 then
				for _,enemy in pairs(enemies) do
					if enemy ~= nil and ( not enemy:IsInvulnerable() ) and not enemy:HasModifier("modifier_pudgeling_rot_target") then
						enemy:AddNewModifier(self:GetCaster(),self:GetAbility(),"modifier_pudgeling_rot_target",{})
					end
				end
			end	
	
	end
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------