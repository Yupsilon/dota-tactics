ability_pudge_hook = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_pudge_showhook","abilities/pudge/modifier_pudge_showhook.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_scarred","abilities/generic/generic_modifier_scarred.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_order_knockback","mechanics/heroes/modifier_order_knockback.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_action_knockback","mechanics/heroes/modifier_action_knockback.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_combobreaker","abilities/generic/generic_modifier_combobreaker.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_pudge_hook:SolveAction(keys)

	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
							   

	if keys.target_point_A~=nil then
	
			local animationTime = 3 * PLAYER_TIME_MODIFIER;
			self:GetCaster():FaceTowards(keys.target_point_A)		
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_OVERRIDE_ABILITY_1, rate = 1/PLAYER_TIME_MODIFIER})
				
			local projectile_range = self:GetSpecialValueFor("projectile_range")
			local projectile_radius = self:GetSpecialValueFor("projectile_radius") 
			local decalSpeed = projectile_range/PLAYER_TIME_MODIFIER*3
			
			local startPoint = self:GetCaster():GetAbsOrigin() + Vector(0,0,50)*self:GetCaster():GetModelScale();	
			local direction = (keys.target_point_A-startPoint):Normalized()
			direction.z=0		
			startPoint=startPoint+direction*projectile_radius/2
			
			EmitSoundOn( "Hero_Pudge.AttackHookExtend", self:GetCaster() )	
			
			Timers:CreateTimer(animationTime/10, function() 
			
				local hook_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_pudge/pudge_meathook.vpcf", PATTACH_CUSTOMORIGIN, nil)
				ParticleManager:SetParticleAlwaysSimulate(hook_particle)
				ParticleManager:SetParticleControlEnt(hook_particle, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_weapon_chain_rt", self:GetCaster():GetAbsOrigin() + Vector( 0, 0, 96 ), true)
				ParticleManager:SetParticleControl(hook_particle, 2, Vector(decalSpeed, projectile_range, projectile_radius))
				ParticleManager:SetParticleControl(hook_particle, 3, Vector (3600,0,0))
				ParticleManager:SetParticleControl(hook_particle, 4, Vector( 1, 0, 0 ) )
				ParticleManager:SetParticleControl(hook_particle, 5, Vector( 0, 0, 0 ) )
										
				local bHit = false;		
				local projectile = {
				  --EffectName = projectile_model,	
				  EffectAttach = PATTACH_WORLDORIGIN,			  			  
				  vSpawnOrigin = startPoint,--{unit=hero, attach="attach_attack1", offset=Vector(0,0,0)},
				  fDistance = projectile_range-projectile_radius,
				  fStartRadius = projectile_radius,
				  fEndRadius = projectile_radius,
				  fCollisionRadius = 30,
				  Source = self:GetCaster(),
				  --fExpireTime = 8.0,
				  vVelocity = direction * decalSpeed, 
				  UnitBehavior = PROJECTILES_DESTROY,
				  bMultipleHits = false,
				  bIgnoreSource = true,
				  TreeBehavior = PROJECTILES_NOTHING,
				  bTreeFullCollision = false,
				  WallBehavior = PROJECTILES_DESTROY,
				  GroundBehavior = PROJECTILES_DESTROY,
				  fGroundOffset = 0,
				  bZCheck = false,
				  bGroundLock = false,
				  draw = false,
				  bUseFindUnitsInRadius = true,
				  bHitScan = false,

				  UnitTest = function(instance, unit) return unit:IsInvulnerable() == false and unit:GetTeamNumber() ~= self:GetCaster():GetTeamNumber() end,
				  OnUnitHit = function(instance, unit) 					
						self:OnProjectileHit(unit,instance:GetPosition())	 						
						bHit=true
						local hookattach = self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_pudge_showhook",{})
						hookattach:Attach(hook_particle,unit)											
				  end,
				 OnIntervalThink = function(instance)
						ParticleManager:SetParticleControl( hook_particle, 1, instance:GetPosition() )
				  end,
				  OnFinish = function(instance, pos)								
						StopSoundOn("Hero_Pudge.AttackHookExtend", self:GetCaster())						
						if not bHit then
							StartAnimation(self:GetCaster(), {duration=2/3*PLAYER_TIME_MODIFIER, activity=ACT_DOTA_CHANNEL_ABILITY_1, rate = 1/PLAYER_TIME_MODIFIER})	
							ParticleManager:SetParticleControl(hook_particle, 2, Vector((pos-self:GetCaster():GetAbsOrigin()):Length2D()/PLAYER_TIME_MODIFIER*3, projectile_range, projectile_radius))
							ParticleManager:SetParticleControl(hook_particle, 1, self:GetCaster():GetAbsOrigin())
							
							Timers:CreateTimer(PLAYER_TIME_MODIFIER*2/3, function()	
								ParticleManager:DestroyParticle( hook_particle, false )
							end)
						end
				  end
				}						
				Projectiles:CreateProjectile(projectile)
			end)	
	end
	
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_pudge_hook:GetPriority()
	return 4;
end

--------------------------------------------------------------------------------

function ability_pudge_hook:OnProjectileHit(hTarget, vLocation)
					
					local damage_table = {
							victim = hTarget,
							attacker = self:GetCaster(),
							damage = self:GetSpecialValueFor("projectile_damage") or 1,
							damage_type = DAMAGE_TYPE_PHYSICAL,
							ability=self,
							cover_reduction=1
						} 
						local energy = self:GetSpecialValueFor("projectile_energy")
						
						
						
						EmitSoundOn( "Hero_Pudge.AttackHookImpact", self:GetCaster() )		
							
						
						if self:GetLevel()==2 then
							damage_table.scar_damage_percent = self:GetSpecialValueFor("bonus_scar_damage") 
						elseif self:GetLevel()==4 then
							hTarget:AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_slow",{								
								turns = self:GetSpecialValueFor("bonus_slow_duration")	,
								slow = self:GetSpecialValueFor("bonus_slow_strength")
							})
						end
					
					
					if IsTargetPointOverCover(vLocation,damage_table.attacker) then
						damage_table.cover_reduction=1/2
										end
				
						Alert_Damage(damage_table)
						
						local vDirection = hTarget:GetAbsOrigin() - self:GetCaster():GetAbsOrigin()
						local kbDirection = vDirection:Normalized()*math.max(1,(vDirection:Length2D()-self:GetSpecialValueFor("base_kb_delta")))
												
						hTarget:AddNewModifier(self:GetCaster(),nil,"modifier_order_knockback",{
							direction_x = -kbDirection.x,
							direction_y = -kbDirection.y		
						})

						Alert_Mana({unit=self:GetCaster(), manavalue=energy, energy=true} );
end

--------------------------------------------------------------------------------

function ability_pudge_hook:getAbilityType()
	return ABILITY_TYPE_POINT_MULTIPLE;
end

--------------------------------------------------------------------------------

function ability_pudge_hook:RequiresHitScanCheck()
	return true;
end

--------------------------------------------------------------------------------

function ability_pudge_hook:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_pudge_hook:GetCastPoints()

	for _,modifierorder in pairs(self:GetCaster():FindAllModifiers()) do	

				if modifierorder:GetAbility()~=nil and modifierorder:GetName() == "modifier_order_cast" and modifierorder:GetAbility():GetAbilityName()=="ability_ogre_multicast" then
					return 2
				end
	end
				
	return 1;
end

--------------------------------------------------------------------------------

function ability_pudge_hook:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_pudge_hook:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_pudge_hook:GetConeArc(coneID,coneRange)
	return self:GetSpecialValueFor("projectile_radius")	/2
end

--------------------------------------------------------------------------------

function ability_pudge_hook:GetMinRange(coneID)	
	return self:GetMaxRange(coneID)	
end

--------------------------------------------------------------------------------

function ability_pudge_hook:GetMaxRange(coneID)	
	if self:GetLevel() == 3 then
		projectile_range = self:GetSpecialValueFor("projectile_range") + self:GetSpecialValueFor("bonus_range")
	end
	return self:GetSpecialValueFor("projectile_range")
end

--------------------------------------------------------------------------------

function ability_pudge_hook:GetCastRange()	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------