ability_pudge_repair = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_regenerate","abilities/generic/generic_modifier_regenerate.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_pudge_repair:PrepAction(keys)

	EmitSoundOn( "Hero_Undying.SoulRip.Cast", self:GetCaster() )
	self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;			
	self:UseResources(false,false,true)	
	self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			
	StartAnimation(self:GetCaster(), {duration=1*PLAYER_TIME_MODIFIER, rate = 1.5/PLAYER_TIME_MODIFIER, activity=ACT_DOTA_TELEPORT, translate="ftp_dendi_back"})
	
	local heal = self:GetSpecialValueFor("heal_value_lesser")
	local tresspass = self:GetSpecialValueFor("heal_tresspass")
	
	if self:GetLevel() == 2 then
			self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_unstoppable", {turns = self:GetSpecialValueFor("bonus_unstoppable_duration") } )
	elseif self:GetLevel() == 3 then
		tresspass = self:GetSpecialValueFor("bonus_tresspass")
	elseif self:GetLevel() == 4 then
		 self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_shield", {life= self:GetSpecialValueFor("bonus_shield") , turns = self:GetSpecialValueFor("bonus_shield_duration") } )
	end
	
	if self:GetCaster():GetHealth()<tresspass then
		heal = self:GetSpecialValueFor("heal_value_greater")
	end
	
	local regen_mod = self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_regenerate",{
		turns = 2,
		health = heal,
		initial=heal,
		nodraw=1
	})	
						
	local nFXIndex = ParticleManager:CreateParticle( "particles/econ/items/pudge/pudge_immortal_arm/pudge_immortal_arm_rot_recipient.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetCaster() )
	ParticleManager:SetParticleControlEnt(nFXIndex, 0, self:GetCaster(), PATTACH_POINT_FOLLOW, "attach_origin", self:GetCaster():GetAbsOrigin(), true)
	regen_mod:AddParticle( nFXIndex, false, false, -1, false, false )	
	
	Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("self_energy"), energy=true} )
	return MODIFIERACTION_SHORTPAUSE_DESTROY;
end

--------------------------------------------------------------------------------

function ability_pudge_repair:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_pudge_repair:GetPriority()
	return 1;
end

--------------------------------------------------------------------------------

function ability_pudge_repair:GetMaxRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_pudge_repair:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function ability_pudge_repair:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_pudge_repair:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_pudge_repair:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end


--------------------------------------------------------------------------------