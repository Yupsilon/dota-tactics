ability_pudge_pudgeling = class({})
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "modifier_pudgeling_rot_caster","abilities/pudge/modifier_pudgeling_rot_caster.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_pudgeling_rot_target","abilities/pudge/modifier_pudgeling_rot_target.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_summonedunit","abilities/generic/generic_modifier_summonedunit.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_absolute","abilities/generic/generic_modifier_movespeed_absolute.lua", LUA_MODIFIER_MOTION_NONE )

LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_snare","abilities/generic/generic_modifier_snare.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_combobreaker","abilities/generic/generic_modifier_combobreaker.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_pudge_pudgeling:SolveAction(keys)

	if keys.target_point_A~=nil then
				 
			EmitSoundOn( "Hero_Undying.Tombstone", self:GetCaster() )
		
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
						
			local animationTime = 60/30*PLAYER_TIME_MODIFIER/2;		
			self:GetCaster():FaceTowards(keys.target_point_A)
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_VICTORY, rate = 1/PLAYER_TIME_MODIFIER})	

			local pudgling_duration = self:GetSpecialValueFor( "pudgling_duration" )
			
			if self:GetLevel()==2 then
				pudgling_duration=pudgling_duration+self:GetSpecialValueFor( "bonus_pudgling_duration" )
			--elseif self:GetLevel() == 2 then
			--	self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_movespeed_haste", {speed= self:GetSpecialValueFor("bonus_haste") , turns = self:GetSpecialValueFor("bonus_snared_duration"), applythisturn=1} )
			end
					
			local control_modifier = self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_summonedunit",{turns = pudgling_duration})

			local startPos = self:GetCaster():GetAbsOrigin();
			control_modifier:AssignUnit( CreateUnitByName("npc_pudge_dummy_pudgeling", startPos , true, self:GetCaster(), self:GetCaster():GetOwner(), self:GetCaster():GetTeamNumber() ))
			control_modifier.AssignedUnit:AddNewModifier(self:GetCaster(),self,"modifier_pudgeling_rot_caster",{})
			control_modifier.AssignedUnit:AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_absolute",{speed = (self:GetSpecialValueFor("cast_range"))/PLAYER_TIME_MODIFIER*2/3})
			control_modifier.AssignedUnit:FindAbilityByName("dummy_unit"):SetLevel(1)
			control_modifier.AssignedUnit:SetHullRadius(1)
			FindClearSpaceForUnit(control_modifier.AssignedUnit, startPos, true)	
			
			Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("self_energy"), energy=true} )
			local point = keys.target_point_A
			
			local walk_distance = 0			
			local moveState = -1
			Timers:CreateTimer(1/30, function()
										
						if control_modifier:IsNull() then return nil end
						
							if control_modifier.AssignedUnit~=nil and not control_modifier.AssignedUnit:IsNull() then
							
								local deltaVector = control_modifier.AssignedUnit:GetAbsOrigin()-startPos
								
								if deltaVector:Length2D()>0 then
									walk_distance = walk_distance+deltaVector:Length2D()
									
									if walk_distance >= self:GetMaxRange(0) then
										moveState = 1
									end
								end							
								startPos = control_modifier.AssignedUnit:GetAbsOrigin()
							
								if moveState == 1 then
									control_modifier.AssignedUnit:Stop()
									return nil
								elseif moveState == -1 then
									moveState=0
									control_modifier.AssignedUnit:MoveToPosition(point);
								end
							else 
								return nil
							end
							
						return 1/30
					
					end);		
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_pudge_pudgeling:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_pudge_pudgeling:GetAOERadius()
	return self:GetSpecialValueFor( "rot_aoe" )
end

--------------------------------------------------------------------------------

function ability_pudge_pudgeling:getAbilityType()
	return ABILITY_TYPE_POINT;
end

--------------------------------------------------------------------------------

function ability_pudge_pudgeling:isFreeAction()
	return false --self:GetLevel()==4;
end

--------------------------------------------------------------------------------

function ability_pudge_pudgeling:RequiresGridSnap()
	return true
end

--------------------------------------------------------------------------------

function ability_pudge_pudgeling:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_pudge_pudgeling:GetCooldown(level)
	if self:GetLevel()==3 then
		return self:GetSpecialValueFor("bonus_cooldown");
	end
	return 3
end

--------------------------------------------------------------------------------

function ability_pudge_pudgeling:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_pudge_pudgeling:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_pudge_pudgeling:GetConeArc(coneID,coneRange)
	return 100
end

--------------------------------------------------------------------------------

function ability_pudge_pudgeling:GetMinRange(coneID)	
	return 0
end

--------------------------------------------------------------------------------

function ability_pudge_pudgeling:GetMaxRange(coneID)		
	return self:GetSpecialValueFor("cast_range")
end

--------------------------------------------------------------------------------

function ability_pudge_pudgeling:GetCastRange()	
	return self:GetSpecialValueFor("cast_range")
end

--------------------------------------------------------------------------------

function ability_pudge_pudgeling:GetHasteBonus()	
	if self:GetLevel()==2 then			
		return (100+(self:GetSpecialValueFor("bonus_haste")))/100
	end
	return 1
end

--------------------------------------------------------------------------------