ability_centaur_axe = class({})
LinkLuaModifier( "modifier_energy_regeneration","abilities/modifier_energy_regeneration.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_movespeed_haste","abilities/generic/generic_modifier_movespeed_haste.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_centaur_axe:GetIntrinsicModifierName()
	return "modifier_energy_regeneration"
end

--------------------------------------------------------------------------------

function ability_centaur_axe:IsDisabledBySilence()
	return false
end


--------------------------------------------------------------------------------

function ability_centaur_axe:GetDamageConversion()
	if self:GetLevel()==3 then
		return self:GetSpecialValueFor("bonus_energy_absorbtion")
	end
	return 0
end

--------------------------------------------------------------------------------

function ability_centaur_axe:GetRegenerationValue()
	return self:GetSpecialValueFor("base_energy_regen")
end

--------------------------------------------------------------------------------

function ability_centaur_axe:SolveAction(keys)

	if keys.target_point_A~=nil then				
				
				
			local cleave_range = self:GetSpecialValueFor("cleave_range")
			local cleave_angle_narrow = self:GetSpecialValueFor("cleave_angle_narrow")
			local cleave_angle_wide = self:GetSpecialValueFor("cleave_angle_wide")
			local cleave_damage = self:GetSpecialValueFor("cleave_damage_full")
			local cleave_far_damage = self:GetSpecialValueFor("cleave_damage_half")
				
			local startPoint = keys.caster:GetAbsOrigin()
			local direction = (keys.target_point_A-startPoint):Normalized()
			direction.z=0
			local centerangle = math.atan2(direction.x,direction.y)
	
			self.LastCastTurn = math.max(GameMode:GetCurrentTurn()+math.ceil(self:GetCooldown(self:GetLevel())),self.LastCastTurn or -1);					
			local animationTime = 35/30*PLAYER_TIME_MODIFIER;
						   
				keys.caster:FaceTowards(keys.target_point_A)	
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_ATTACK, translate="enchant_totem",rate = 1/PLAYER_TIME_MODIFIER})	
					
			if self:GetLevel()==4 then
				self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_haste",{speed=self:GetSpecialValueFor("bonus_haste"),turns = self:GetSpecialValueFor("bonus_haste_duration"),applythisturn=1})
			end
			
				EmitSoundOn( "Hero_EarthShaker.Woosh", self:GetCaster() )
			Timers:CreateTimer(animationTime*4/7, function() 
			
			EmitSoundOn( "Hero_EarthShaker.Totem.Attack", self:GetCaster() )
		--		local nFXIndex = ParticleManager:CreateParticle( "particles/econ/items/faceless_void/faceless_void_weapon_bfury/faceless_void_weapon_bfury_cleave.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )
	--			local nFXEnemies = 2
				
				--ParticleManager:SetParticleControl( nFXIndex, 0, startPoint )
--				ParticleManager:SetParticleControl( nFXIndex, 1, keys.target_point_A )

			local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_earthshaker/earthshaker_aftershock.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )				
			ParticleManager:SetParticleControl( nFXIndex, 0, startPoint )			
			ParticleManager:SetParticleControl( nFXIndex, 1, Vector(cleave_range,cleave_range,cleave_range) )
			ParticleManager:ReleaseParticleIndex( nFXIndex )	

				local enemies = FindUnitsInCone({				
					caster = self:GetCaster(),
					startPoint = startPoint,
					startAngle = centerangle,
					search_range = cleave_range,
					search_angle = cleave_angle_wide,
					searchteam = DOTA_UNIT_TARGET_TEAM_ENEMY,
					searchtype = DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC,
					searchflags = 0
				})
				
				
				if #enemies > 0 then
					for _,enemy in pairs(enemies) do
						if enemy ~= nil and ( not enemy:IsInvulnerable() )  then
					
							local enemy_direction = (enemy:GetAbsOrigin()-startPoint)
							enemy_direction.z=0				
							local enemy_deltaangle = math.atan2(enemy_direction.x,enemy_direction.y)
							local enemy_angle = math.abs(AngleDiff(centerangle*180/math.pi,enemy_deltaangle*180/math.pi))
							
								local projectile = {
								EffectAttach = PATTACH_WORLDORIGIN,			  			  
								vSpawnOrigin = startPoint+Vector(0,0,30),
								fDistance = cleave_range,
								fStartRadius = 50,
								fEndRadius = 50,
								fCollisionRadius = 30,
								  Source = self:GetCaster(),
								  vVelocity = enemy_direction:Normalized() * cleave_range*10, 
								  UnitBehavior = PROJECTILES_NOTHING,
								  bMultipleHits = true,
								  bIgnoreSource = true,
								  TreeBehavior = PROJECTILES_NOTHING,
								  bTreeFullCollision = false,
								  WallBehavior = PROJECTILES_DESTROY,
								  GroundBehavior = PROJECTILES_DESTROY,
								  fGroundOffset = 0,
								  bZCheck = false,
								  bGroundLock = false,
								  draw = false,
								  bUseFindUnitsInRadius = true,
								  bHitScan=true,
								  OnFinish = function(instance, pos) ParticleManager:ReleaseParticleIndex( nFXIndex ) end,
								  UnitTest = function(instance, unit) return unit==enemy end,
								  OnUnitHit = function(instance, unit) 
															
									local hit_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_spirit_breaker/spirit_breaker_greater_bash.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())
									ParticleManager:SetParticleControl(hit_particle, 0, unit:GetAbsOrigin())
									ParticleManager:ReleaseParticleIndex(hit_particle)
														
										local damage_table = {
											victim = enemy,
											attacker = self:GetCaster(),
											damage = cleave_damage,
											damage_type = DAMAGE_TYPE_MAGICAL,
											cover_reduction = 1,
										}	
										local selfheal = 0
										
										if enemy_angle > cleave_angle_narrow/2 then
											damage_table.damage=cleave_far_damage	
											
											if self:GetLevel()==2 then
												selfheal = self:GetSpecialValueFor("bonus_selfheal_half")											
											end	
										else 
											if self:GetLevel()==2 then
												selfheal = self:GetSpecialValueFor("bonus_selfheal_full")											
											end	
										end														
																
										if IsTargetOverCover(instance,damage_table.attacker) then
											damage_table.cover_reduction=1/2
										end
										
										if selfheal>0 then
											Alert_Heal({caster=self:GetCaster(),target=self:GetCaster(),value=selfheal})
										end

										Alert_Damage( damage_table )
										Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("cleave_energy"), energy=true} );										
				
										--ParticleManager:SetParticleControl( nFXIndex, nFXEnemies, unit:GetAbsOrigin() )
										
										--nFXEnemies = nFXEnemies+1
								  end,
								}						
								Projectiles:CreateProjectile(projectile)
							
							end
					end
				end
			end)
	end
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_centaur_axe:getAbilityType()
	return ABILITY_TYPE_DOUBLE_CONE;
end

--------------------------------------------------------------------------------

function ability_centaur_axe:isFreeAction()
	return false;
end

--------------------------------------------------------------------------------

function ability_centaur_axe:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_centaur_axe:GetCastPoints()
	return 1;
end

--------------------------------------------------------------------------------

function ability_centaur_axe:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_centaur_axe:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_centaur_axe:GetConeArc(coneID,coneRange)
	if coneID == 3 then return self:GetSpecialValueFor("cleave_angle_narrow")/20 end
	if coneID == 2 then return self:GetSpecialValueFor("cleave_angle_narrow")*2 end
	if coneID == 1 then return self:GetSpecialValueFor("cleave_angle_wide")/20 end
	return self:GetSpecialValueFor("cleave_angle_wide")*2
end

--------------------------------------------------------------------------------

function ability_centaur_axe:GetMinRange(coneID)	
	return self:GetSpecialValueFor("cleave_range")
end

--------------------------------------------------------------------------------

function ability_centaur_axe:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("cleave_range")
end

--------------------------------------------------------------------------------

function ability_centaur_axe:GetCastRange(coneID)	
	return self:GetMaxRange(0)
end

--------------------------------------------------------------------------------

function ability_centaur_axe:GetHasteBonus()	
	if self:GetLevel()==4 then			
		return (100+(self:GetSpecialValueFor("bonus_haste")))/100
	end
	return 1
end

--------------------------------------------------------------------------------