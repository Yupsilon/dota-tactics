ability_earthshaker_quake = class({})
--LinkLuaModifier( "generic_modifier_movespeed_slow","abilities/generic/generic_modifier_movespeed_slow.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_shield_life","mechanics/heroes/modifier_shield_life.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_ability_cooldown","abilities/modifier_ability_cooldown.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "modifier_earthshaker_embrace","abilities/earthshaker/modifier_earthshaker_embrace.lua", LUA_MODIFIER_MOTION_NONE )
LinkLuaModifier( "generic_modifier_unstoppable","abilities/generic/generic_modifier_unstoppable.lua", LUA_MODIFIER_MOTION_NONE )

--------------------------------------------------------------------------------

function ability_earthshaker_quake:PrepAction(keys)
		
			local cleave_range = self:GetCaster():GetHullRadius()--GetSpecialValueFor("quake_range")
			--local cleave_damage = self:GetSpecialValueFor("quake_damage")
				
			local startPoint = keys.caster:GetAbsOrigin()
			--local direction = (keys.target_point_A-startPoint):Normalized()
			--direction.z=0
			--local centerangle = math.atan2(direction.x,direction.y)
	
			self.LastCastTurn = GameMode:GetCurrentTurn()+self:GetCooldown(self:GetLevel())+1;
			self:UseResources(false,false,true)	
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_ability_cooldown",{})
			
			--local selfshield = self:GetSpecialValueFor("quake_selfshield")
						
			--if self:GetLevel() == 4 then
			--	self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_shield",{life= self:GetSpecialValueFor("bonus_selfshield") , turns = self:GetSpecialValueFor("quake_selfshield_duration"),applythisturn=1})		
		--	end
			
			local animationTime = 40/30 * PLAYER_TIME_MODIFIER;
			
			local dam_red = self:GetSpecialValueFor("self_reduction")
			
			if self:GetLevel() == 3 then
				self:GetCaster():AddNewModifier(self:GetCaster(),self,"generic_modifier_unstoppable",{turns = self:GetSpecialValueFor("bonus_unstoppable_duration")})	
			elseif self:GetLevel() == 4 then
				dam_red = self:GetSpecialValueFor("bonus_reduction")
			end
			
			self:GetCaster():AddNewModifier(self:GetCaster(),self,"modifier_earthshaker_embrace",{incoming= dam_red , turns = self:GetSpecialValueFor("modifier_duration")})		
						   	
							
				local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_earth_spirit/earthspirit_petrify_shockwave.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )				
				ParticleManager:SetParticleControl( nFXIndex, 0, startPoint )			
				ParticleManager:SetParticleControl( nFXIndex, 1, Vector(cleave_range,cleave_range,cleave_range) )
				ParticleManager:ReleaseParticleIndex( nFXIndex )	
							
							
			StartAnimation(self:GetCaster(), {duration=animationTime, activity=ACT_DOTA_ATTACK, rate = 1/PLAYER_TIME_MODIFIER})	
			EmitSoundOn( "Hero_EarthShaker.FissureDestroy", self:GetCaster() )
			
			Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("quake_energy"), energy=true} );
			
			--[[Timers:CreateTimer(animationTime*4/7, function() 
			
				if self:GetLevel() == 3 then
					Timers:CreateTimer(1, function() 
						local might_modifier = self:GetCaster():AddNewModifier( self:GetCaster(), self, "generic_modifier_might", {value= self:GetSpecialValueFor("bonus_might") , turns = self:GetSpecialValueFor("bonus_might_duration") , nodraw=1} )
					
					end)
				end
				
				--EmitSoundOn( "Hero_EarthShaker.Totem", self:GetCaster() )
			
				local nFXIndex = ParticleManager:CreateParticle( "particles/econ/items/earthshaker/earthshaker_totem_ti6/earthshaker_totem_ti6_leap_impact.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )				
				ParticleManager:SetParticleControl( nFXIndex, 0, startPoint )			
				ParticleManager:SetParticleControl( nFXIndex, 1, Vector(cleave_range,cleave_range,cleave_range) )
				ParticleManager:ReleaseParticleIndex( nFXIndex )	

				local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), startPoint, self:GetCaster(), cleave_range, DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
				if #enemies > 0 then
					for _,enemy in pairs(enemies) do
						if enemy ~= nil and ( not enemy:IsInvulnerable() ) then
					
							local enemy_direction = (enemy:GetAbsOrigin()-startPoint)
								local projectile = {
								EffectAttach = PATTACH_WORLDORIGIN,			  			  
								vSpawnOrigin = startPoint+Vector(0,0,30),
								fDistance = cleave_range,
								fStartRadius = 50,
								fEndRadius = 50,
								fCollisionRadius = 30,
								  Source = self:GetCaster(),
								  vVelocity = enemy_direction:Normalized() * cleave_range*10, 
								  UnitBehavior = PROJECTILES_NOTHING,
								  bMultipleHits = true,
								  bIgnoreSource = true,
								  TreeBehavior = PROJECTILES_NOTHING,
								  bTreeFullCollision = false,
								  WallBehavior = PROJECTILES_DESTROY,
								  GroundBehavior = PROJECTILES_DESTROY,
								  fGroundOffset = 0,
								  bZCheck = false,
								  bGroundLock = false,
								  draw = false,
								  bUseFindUnitsInRadius = true,
									bHitScan=true,

								  UnitTest = function(instance, unit) return unit==enemy end,
								  OnUnitHit = function(instance, unit) 
														
										local damage_table = {
											victim = enemy,
											attacker = self:GetCaster(),
											damage = cleave_damage,
											damage_type = DAMAGE_TYPE_MAGICAL,
											cover_reduction = 1,
										}
										
										if IsTargetOverCover(instance,damage_table.attacker) then
											damage_table.cover_reduction=1/2
										end

										Alert_Damage( damage_table )
										enemy:AddNewModifier(self:GetCaster(),self,"generic_modifier_movespeed_slow",{turns=self:GetSpecialValueFor("quake_slow")})
										Alert_Mana({unit=self:GetCaster(),manavalue=self:GetSpecialValueFor("quake_energy"), energy=true} );
										
										local hit_particle = ParticleManager:CreateParticle("particles/units/heroes/hero_spirit_breaker/spirit_breaker_greater_bash.vpcf", PATTACH_CUSTOMORIGIN, self:GetCaster())
										ParticleManager:SetParticleControl(hit_particle, 0, unit:GetAbsOrigin())
										ParticleManager:ReleaseParticleIndex(hit_particle)
								  end,
								}						
								Projectiles:CreateProjectile(projectile)						
					
						end
					end
				end
			end)]]
			
	return ABILITYACTION_COMPLETE;
end

--------------------------------------------------------------------------------

function ability_earthshaker_quake:getAbilityType()
	return ABILITY_TYPE_NOPOINT;
end

--------------------------------------------------------------------------------

function ability_earthshaker_quake:isFreeAction()
	return true;
end

--------------------------------------------------------------------------------

function ability_earthshaker_quake:GetCooldown(level)	
	if self:GetLevel()==2 then return self:GetSpecialValueFor("bonus_casttime") end
	return 3
end

--------------------------------------------------------------------------------

function ability_earthshaker_quake:GetPriority()
	return 3;
end

--------------------------------------------------------------------------------

function ability_earthshaker_quake:GetCastPoints()
	return 0;
end

--------------------------------------------------------------------------------

function ability_earthshaker_quake:GetLastCastTurn()
	return self.LastCastTurn or -1;
end

--------------------------------------------------------------------------------

function ability_earthshaker_quake:GetCooldownTimeRemaining()
	return self:GetLastCastTurn()-GameMode:GetCurrentTurn();
end

--------------------------------------------------------------------------------

function ability_earthshaker_quake:GetConeArc(coneID,coneRange)	
	return 1
end

--------------------------------------------------------------------------------

function ability_earthshaker_quake:GetMinRange(coneID)	
	return self:GetSpecialValueFor("quake_range")
end

--------------------------------------------------------------------------------

function ability_earthshaker_quake:GetMaxRange(coneID)	
	return self:GetSpecialValueFor("quake_range")
end

--------------------------------------------------------------------------------

function ability_earthshaker_quake:GetCastRange(coneID)	
	return 0--self:GetMaxRange(0)
end

--[[------------------------------------------------------------------------------

function ability_earthshaker_quake:GetHasteBonus()	
	if self:GetLevel()==2 then			
		return (100+(self:GetSpecialValueFor("bonus_haste")))/100
	end
	return 1
end]]

--------------------------------------------------------------------------------