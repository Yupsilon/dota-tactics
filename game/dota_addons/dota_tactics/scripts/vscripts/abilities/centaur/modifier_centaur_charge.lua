modifier_centaur_charge = class({})

--------------------------------------------------------------------------------

function modifier_centaur_charge:IsHidden()
	return true
end

--------------------------------------------------------------------------------

function modifier_centaur_charge:OnCreated( kv )

	self.dash_start = self:GetParent():GetAbsOrigin()
	self.dash_end = Vector(kv.dash_destination_x,kv.dash_destination_y,0)
	self.dash_speed = (self:GetAbility():GetCastRange()/30) 
	self.distance_traveled = 0
	self.kb_height = 0

	if IsServer() then
		--AddAnimationTranslate(self:GetParent(), "forcestaff_friendly")
		--StartAnimation(self:GetCaster(), {duration=1, activity=ACT_DOTA_OVERRIDE_ABILITY_2})
		
		--local nFXIndex = ParticleManager:CreateParticle( "particles/econ/events/ti9/shovel_dig.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		--self:AddParticle( nFXIndex, false, false, -1, false, false )		
	
		--local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_spirit_breaker/spirit_breaker_charge_dust.vpcf", PATTACH_ABSORIGIN_FOLLOW, self:GetParent() )
		--self:AddParticle( nFXIndex, false, false, -1, false, false )		
		self.dash_speed = self.dash_speed  / PLAYER_TIME_SIMULTANIOUS*3/2
		
		self:StartIntervalThink( 1/30 )
		self:OnIntervalThink()
	end
end

--------------------------------------------------------------------------------

function modifier_centaur_charge:OnIntervalThink()
	if IsServer() then

		self:Activate ( )
		local vectorDelta=self.dash_end-self.dash_start
		vectorDelta.z=0
		
			EmitSoundOn( "Hero_Earthshaker.Attack.Impact", self:GetCaster() )
			local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_earthshaker/earthshaker_totem_leap_impact.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )				
			ParticleManager:SetParticleControl( nFXIndex, 0, self:GetParent():GetAbsOrigin() )			
			ParticleManager:ReleaseParticleIndex( nFXIndex )	
			
		if self.distance_traveled < vectorDelta:Length2D() then
			self:GetParent():SetAbsOrigin(self:GetParent():GetAbsOrigin() + vectorDelta:Normalized() * self.dash_speed)
			self.distance_traveled = self.distance_traveled + (vectorDelta:Normalized() * self.dash_speed):Length2D()
			self.kb_height=math.sin(self.distance_traveled/vectorDelta:Length2D()*math.pi)*1000
			
		else
			self.kb_height=0
			local animationTime= 1/2;
			FindClearSpaceForUnit(self:GetParent(), self.dash_end, true)
			
			--local nFXIndex = ParticleManager:CreateParticle( "particles/units/heroes/hero_earthshaker/earthshaker_totem_leap_impact.vpcf", PATTACH_WORLDORIGIN, self:GetCaster() )				
			--ParticleManager:SetParticleControl( nFXIndex, 0, self.dash_end )			
			--ParticleManager:ReleaseParticleIndex( nFXIndex )	
			
			StartAnimation(self:GetParent(), { duration = animationTime,activity=ACT_DOTA_TELEPORT_END})	
			self:Destroy()
		end
	end
end

--------------------------------------------------------------------------------

function modifier_centaur_charge:Activate ( )

		if not GameMode:GetCurrentGamePhase() == GAMEPHASE_DASH then return end

		local helix_aoe = self:GetAbility():GetSpecialValueFor("butt_size")
		local helix_damage = self:GetAbility():GetSpecialValueFor("totem_damage")
		local helix_energy =  self:GetAbility():GetSpecialValueFor("totem_energy")
		local caster = self:GetParent()
		local ability = self:GetAbility()
		
		local start = caster:GetAbsOrigin()

					local enemies = FindUnitsInRadius( self:GetCaster():GetTeamNumber(), start, self:GetCaster(),helix_aoe , DOTA_UNIT_TARGET_TEAM_ENEMY, DOTA_UNIT_TARGET_HERO + DOTA_UNIT_TARGET_BASIC, 0, 0, false )
					if #enemies > 0 then
						for _,enemy in pairs(enemies) do						
							if enemy ~= nil and ( not enemy:IsInvulnerable() ) and (enemy:HasModifier("generic_modifier_dashed_this_turn")==false)  then
						
									if enemy:FindModifierByNameAndCaster("modifier_hit",caster)==nil then
										enemy:AddNewModifier(caster,ability,"modifier_hit",{turns=1})
										
										if ability:GetLevel() == 2 then
											
												enemy:AddNewModifier(caster,ability,"generic_modifier_movespeed_slow",{value=self:GetAbility():GetSpecialValueFor("bonus_slow"),turns=self:GetAbility():GetSpecialValueFor("bonus_slow_duration")})
										end
									
						
										local direction = enemy:GetAbsOrigin()-start
										direction.z=0
										
										local projectile = {		  			  
										vSpawnOrigin = start+Vector(0,0,30),
										fDistance = helix_aoe*2,
										fStartRadius = 50,
										fEndRadius = 50,
										fCollisionRadius = 30,
										Source = self:GetCaster(),
										vVelocity = direction:Normalized() * helix_aoe, 
										UnitBehavior = PROJECTILES_NOTHING,
										bMultipleHits = true,
										bIgnoreSource = true,
										TreeBehavior = PROJECTILES_NOTHING,
										bTreeFullCollision = false,
										WallBehavior = PROJECTILES_DESTROY,
										GroundBehavior = PROJECTILES_DESTROY,
										fGroundOffset = 0,
										bZCheck = false,
										bGroundLock = false,
										bUseFindUnitsInRadius = true,
										bHitScan=true,

										UnitTest = function(instance, unit) return unit==enemy end,
										OnUnitHit = function(instance, unit) 
												
											local damage_table = {
												victim = unit,
												attacker = caster,
												damage = helix_damage,
												damage_type = DAMAGE_TYPE_MAGICAL,
												ability  = ability,
												cover_reduction = 1,
											}
									
											if IsTargetOverCover(instance,damage_table.attacker) then
												damage_table.cover_reduction=1/2
											end

											Alert_Damage( damage_table )
											Alert_Mana({unit=caster,manavalue=helix_energy, energy=true} );
										
										  end,
										}						
										Projectiles:CreateProjectile(projectile)
														
								end
							end
						end
					
					end	
end

--------------------------------------------------------------------------------

function modifier_centaur_charge:DeclareFunctions()
	local funcs = {
		MODIFIER_PROPERTY_OVERRIDE_ANIMATION
	}

	return funcs
end

--------------------------------------------------------------------------------

function modifier_centaur_charge:GetVisualZDelta( params )
	return self.kb_height
end

--------------------------------------------------------------------------------

function modifier_centaur_charge:GetOverrideAnimation( params )
	return ACT_DOTA_RUN
end

--------------------------------------------------------------------------------
--------------------------------------------------------------------------------