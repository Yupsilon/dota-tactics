"DOTAAbilities"
{
  "item_containers_lua_pack"
  {
    // General
    //-------------------------------------------------------------------------------------------------------------
    "AbilityBehavior"       "DOTA_ABILITY_BEHAVIOR_NO_TARGET"
    "ID"              "1835"
    "BaseClass"         "item_lua"
    "AbilityTextureName"        "item_present"
    "ScriptFile"          "libraries/abilities/item_containers_lua_pack.lua"
    "MaxLevel"            "1"

    "ItemCost"            "0"
    "ItemPurchasable"          "0"
    "ItemKillable"             "0"
    "ItemDroppable"            "0"
    "ItemSellable"             "0"
    "ItemCanChangeContainer"   "0"
    
    // Casting
    //-------------------------------------------------------------------------------------------------------------
    "AbilityCastRange"        "0"

    // Time   
    //-------------------------------------------------------------------------------------------------------------
    "AbilityCooldown"       "0"

    // Cost
    //-------------------------------------------------------------------------------------------------------------
    "AbilityManaCost"       "0 0 0 0"
  }
 

	"item_ready_up"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"item_lua"
		"ScriptFile"					"mechanics/items/item_ready_up"
		"AbilityTextureName"			"item_neutral"

		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"0"
		"ItemPurchasable"				"0"
		"ItemQuality"					""
		"ItemDroppable"            "0"
		"ItemStackable"					"0"
		"ItemSellable"					"0"
		"ItemShareability"				"ITEM_NOT_SHAREABLE"
		"ItemPermanent"					"1"
		"ItemKillable"					"0"
		"ItemDisplayOwnership"			"1"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"ready_up_time"			"1"
			}
		}
	}
 
	"item_respec"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"item_lua"
		"ScriptFile"					"mechanics/items/item_respec"
		"AbilityTextureName"			"item_ex_machina"

		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"0"
		"ItemPurchasable"				"0"
		"ItemQuality"					""
		"ItemDroppable"            "0"
		"ItemStackable"					"0"
		"ItemSellable"					"0"
		"ItemShareability"				"ITEM_NOT_SHAREABLE"
		"ItemPermanent"					"1"
		"ItemKillable"					"0"
		"ItemDisplayOwnership"			"1"
	}
 
	"item_changehero"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"item_lua"
		"ScriptFile"					"mechanics/items/item_changehero"
		"AbilityTextureName"			"item_apex"

		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"0"
		"ItemPurchasable"				"0"
		"ItemQuality"					""
		"ItemDroppable"            "0"
		"ItemStackable"					"0"
		"ItemSellable"					"0"
		"ItemShareability"				"ITEM_NOT_SHAREABLE"
		"ItemPermanent"					"1"
		"ItemKillable"					"0"
		"ItemDisplayOwnership"			"1"
	}

	"item_walkboots"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"item_lua"
		"ScriptFile"					"mechanics/items/item_walkboots"
		"AbilityTextureName"			"item_boots"
		"AbilityCooldown"				"0"

		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_PASSIVE | DOTA_ABILITY_BEHAVIOR_AURA"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"0"
		"ItemPurchasable"				"0"
		"ItemQuality"					""
		"ItemDroppable"            "0"
		"ItemStackable"					"0"
		"ItemSellable"					"0"
		"ItemShareability"				"ITEM_NOT_SHAREABLE"
		"ItemPermanent"					"1"
		"ItemKillable"					"0"
		"ItemDisplayOwnership"			"1"
	}
	
	//---------------------------------
	//		PREP ACTIONS ITEMS
	//---------------------------------
	 
	"item_might"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"item_lua"
		"ScriptFile"					"mechanics/items/item_might"
		"AbilityTextureName"			"custom/item_might"
		"AbilityCooldown"				"0"

		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"0"
		"ItemPurchasable"				"0"
		"ItemQuality"					""
		"ItemDroppable"            "0"
		"ItemStackable"					"0"
		"ItemSellable"					"0"
		"ItemShareability"				"ITEM_NOT_SHAREABLE"
		"ItemPermanent"					"1"
		"ItemKillable"					"0"
		"ItemDisplayOwnership"			"1"
		"ItemInitialCharges"			"1"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"might_duration"			"1"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"might_power"			"25"
			}
		}
	}
 
	"item_regen"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"item_lua"
		"ScriptFile"					"mechanics/items/item_regen"
		"AbilityTextureName"			"item_flask"
		"AbilityCooldown"				"0"
		
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_UNIT_TARGET"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_FRIENDLY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"0"
		"ItemPurchasable"				"0"
		"ItemQuality"					""
		"ItemDroppable"            "0"
		"ItemStackable"					"0"
		"ItemSellable"					"0"
		"ItemShareability"				"ITEM_NOT_SHAREABLE"
		"ItemPermanent"					"1"
		"ItemKillable"					"0"
		"ItemDisplayOwnership"			"1"
		"ItemInitialCharges"			"1"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"regen_duration"			"3"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"regen_health"			"100"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"reach_range"			"750"
			}
		}
	}
 
	"item_selfshield"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"item_lua"
		"ScriptFile"					"mechanics/items/item_selfshield"
		"AbilityTextureName"			"item_hyperstone"
		"AbilityCooldown"				"0"

		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"0"
		"ItemPurchasable"				"0"
		"ItemQuality"					""
		"ItemDroppable"            "0"
		"ItemStackable"					"0"
		"ItemSellable"					"0"
		"ItemShareability"				"ITEM_NOT_SHAREABLE"
		"ItemPermanent"					"1"
		"ItemKillable"					"0"
		"ItemDisplayOwnership"			"1"
		"ItemInitialCharges"			"1"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"regen_duration"			"2"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"regen_health"			"200"
			}
		}
	}
 
	"item_energy"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"item_lua"
		"ScriptFile"					"mechanics/items/item_energy"
		"AbilityTextureName"			"item_essence_ring"
		"AbilityCooldown"				"0"

		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"0"
		"ItemPurchasable"				"0"
		"ItemQuality"					""
		"ItemDroppable"            "0"
		"ItemStackable"					"0"
		"ItemSellable"					"0"
		"ItemShareability"				"ITEM_NOT_SHAREABLE"
		"ItemPermanent"					"1"
		"ItemKillable"					"0"
		"ItemDisplayOwnership"			"1"
		"ItemInitialCharges"			"1"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"energy_duration"			"1"
			}
		}
	}
 
	"item_cleanse"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"item_lua"
		"ScriptFile"					"mechanics/items/item_cleanse"
		"AbilityTextureName"			"custom/item_cleanse"
		"AbilityCooldown"				"0"

		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"0"
		"ItemPurchasable"				"0"
		"ItemQuality"					""
		"ItemDroppable"            "0"
		"ItemStackable"					"0"
		"ItemSellable"					"0"
		"ItemShareability"				"ITEM_NOT_SHAREABLE"
		"ItemPermanent"					"1"
		"ItemKillable"					"0"
		"ItemDisplayOwnership"			"1"
		"ItemInitialCharges"			"1"
		"precache"
		{
			"particle"		"particles/items5_fx/magic_lamp.vpcf"		
		}

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"scar_healing"			"200"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"scar_duration"			"1"
			}
		}
	}
	
	//---------------------------------
	//		DASH ACTIONS ITEMS
	//---------------------------------
	 
	"item_blinky"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"item_lua"
		"ScriptFile"					"mechanics/items/item_blinky"
		"AbilityTextureName"			"custom/item_blinky"
		"AbilityCooldown"				"0"

		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"0"
		"ItemPurchasable"				"0"
		"ItemQuality"					""
		"ItemDroppable"            "0"
		"ItemStackable"					"0"
		"ItemSellable"					"0"
		"ItemShareability"				"ITEM_NOT_SHAREABLE"
		"ItemPermanent"					"1"
		"ItemKillable"					"0"
		"ItemDisplayOwnership"			"1"
		"ItemInitialCharges"			"1"
		
		"precache"
		{
			"particle"		"particles/units/heroes/hero_antimage/antimage_blink_start.vpcf"		
			"particle"		"particles/units/heroes/hero_antimage/antimage_blink_end.vpcf"		
		}

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"blinky_range"			"750"
			}
		}
	}
 
	"item_invisible"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"item_lua"
		"ScriptFile"					"mechanics/items/item_invisible"
		"AbilityTextureName"			"custom/item_invisible"
		"AbilityCooldown"				"0"

		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"0"
		"ItemPurchasable"				"0"
		"ItemQuality"					""
		"ItemDroppable"            "0"
		"ItemStackable"					"0"
		"ItemSellable"					"0"
		"ItemShareability"				"ITEM_NOT_SHAREABLE"
		"ItemPermanent"					"1"
		"ItemKillable"					"0"
		"ItemDisplayOwnership"			"1"
		"ItemInitialCharges"			"1"
		
		"precache"
		{
			"particle"	"particles/items3_fx/glimmer_cape_initial_flash.vpcf"}

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"blinky_range"			"500"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"modifier_duration"			"2"
			}
		}
	}
 
	"item_teleport"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"item_lua"
		"ScriptFile"					"mechanics/items/item_teleport"
		"AbilityTextureName"			"custom/item_teleport"
		"AbilityCooldown"				"0"
	
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_OPTIONAL_POINT | DOTA_ABILITY_BEHAVIOR_UNIT_TARGET"
		"AbilityUnitTargetTeam"     "DOTA_UNIT_TARGET_TEAM_FRIENDLY"    
		"AbilityUnitTargetType"     "DOTA_UNIT_TARGET_BASIC | DOTA_UNIT_TARGET_HERO"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"0"
		"ItemPurchasable"				"0"
		"ItemQuality"					""
		"ItemDroppable"            "0"
		"ItemStackable"					"0"
		"ItemSellable"					"0"
		"ItemShareability"				"ITEM_NOT_SHAREABLE"
		"ItemPermanent"					"1"
		"ItemKillable"					"0"
		"ItemDisplayOwnership"			"1"
		"ItemInitialCharges"			"1"
		
		"precache"
		{
			"particle"		"particles/econ/events/ti5/blink_dagger_start_ti5.vpcf"		
			"particle"		"particles/econ/events/ti5/blink_dagger_end_ti5.vpcf"
		}

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"jump_range"			"200"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"dash_range"			"2400"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"regen_health"			"150"
			}
			"04"
			{
				"var_type"				"FIELD_INTEGER"
				"regen_duration"		"1"
			}
		}
	}
	
	//---------------------------------
	//		ACTION ACTIONS ITEMS
	//---------------------------------
 
	"item_unstoppable"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"item_lua"
		"ScriptFile"					"mechanics/items/item_unstoppable"
		"AbilityTextureName"			"item_minotaur_horn"
		"AbilityCooldown"				"0"

		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"0"
		"ItemPurchasable"				"0"
		"ItemQuality"					""
		"ItemDroppable"            "0"
		"ItemStackable"					"0"
		"ItemSellable"					"0"
		"ItemShareability"				"ITEM_NOT_SHAREABLE"
		"ItemPermanent"					"1"
		"ItemKillable"					"0"
		"ItemDisplayOwnership"			"1"
		"ItemInitialCharges"			"1"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"resistance_duration"			"1"
			}
		}
	}
 
	"item_cooldown"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"item_lua"
		"ScriptFile"					"mechanics/items/item_cooldown"
		"AbilityTextureName"			"custom/item_cooldown"
		"AbilityCooldown"				"0"

		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"0"
		"ItemPurchasable"				"0"
		"ItemQuality"					""
		"ItemDroppable"            "0"
		"ItemStackable"					"0"
		"ItemSellable"					"0"
		"ItemShareability"				"ITEM_NOT_SHAREABLE"
		"ItemPermanent"					"1"
		"ItemKillable"					"0"
		"ItemDisplayOwnership"			"1"
		"ItemInitialCharges"			"1"
		
		"precache"
		{
			"particle"	"particles/items2_fx/refresher.vpcf"
		}

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"refres_duration"			"2"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"energy_fatigue"			"-100"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"energy_fatigue_duration"			"2"
			}
		}
	}
 
	"item_cooldown_lesser"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"item_lua"
		"ScriptFile"					"mechanics/items/item_cooldown_lesser"
		"AbilityTextureName"			"item_timeless_relic"
		"AbilityCooldown"				"0"

		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"0"
		"ItemPurchasable"				"0"
		"ItemQuality"					""
		"ItemDroppable"            "0"
		"ItemStackable"					"0"
		"ItemSellable"					"0"
		"ItemShareability"				"ITEM_NOT_SHAREABLE"
		"ItemPermanent"					"1"
		"ItemKillable"					"0"
		"ItemDisplayOwnership"			"1"
		"ItemInitialCharges"			"1"
		
		"precache"
		{
			"particle"	"particles/items2_fx/refresher.vpcf"
		}

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"refres_duration"			"2"
			}
		}
	}
 
	"item_miniward"
	{
		// General
		//-------------------------------------------------------------------------------------------------------------
		"BaseClass"						"item_lua"
		"ScriptFile"					"mechanics/items/item_miniward"
		"AbilityTextureName"			"custom/item_miniward"
		"AbilityCooldown"				"0"
	
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT"

		// Item Info
		//-------------------------------------------------------------------------------------------------------------
		"ItemCost"						"0"
		"ItemPurchasable"				"0"
		"ItemQuality"					""
		"ItemDroppable"            "0"
		"ItemStackable"					"0"
		"ItemSellable"					"0"
		"ItemShareability"				"ITEM_NOT_SHAREABLE"
		"ItemPermanent"					"1"
		"ItemKillable"					"0"
		"ItemDisplayOwnership"			"1"
		"ItemInitialCharges"			"1"

		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"cast_range"			"3200"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"ward_sight"			"1250"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"ward_life"			"2"
			}
		}
	}
}