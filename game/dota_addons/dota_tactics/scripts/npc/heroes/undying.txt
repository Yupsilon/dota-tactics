"DOTAAbilities"
{
	 "ability_undying_slap"
	{
		"BaseClass"             		"ability_lua"
		"AbilityTextureName"			"brewmaster_thunder_clap"
		"MaxLevel" 						"4"
		"LevelsBetweenUpgrades"		"0"
		"AbilityCooldown"				"0"
	
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_AUTOCAST"
				
		"ScriptFile"					"abilities/undying/ability_undying_slap"
		"precache"
		{
			"soundfile"	"soundevents/game_sounds_heroes/game_sounds_undying.vsndevts"
			"particle"	"particles/econ/events/ti9/ti9_monkey_projectile.vpcf"
		}
		
		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{			
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"projectile_range"		"600"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"projectile_radius"		"50"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"projectile_angle"		"20"
			}
			"04"
			{
				"var_type"				"FIELD_INTEGER"
				"projectile_cone"		"120"
			}			
			"05"
			{
				"var_type"					"FIELD_INTEGER"
				"cone_damage"				"275"
			}	
			"06"
			{
				"var_type"					"FIELD_INTEGER"
				"projectile_energy"				"120"
			}	
			"07"
			{
				"var_type"				"FIELD_INTEGER"
				"base_absorbtion"		"25"
			}
			"08"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_slow"			"50"
			}
			"09"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_slow_duration"		"1"
			}
			"10"
			{
				"var_type"				"FIELD_INTEGER"
				"scar_payment"		"50"
			}
			"11"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_haste"		"50"
			}	
			"12"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_haste_duration"		"1"
			}	
			"13"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_damage"		"25"
			}							
			"14"
			{
				"var_type"				"FIELD_INTEGER"
				"tooltip_points_spent"		"0 1 2 3"
			}
		}
	}
  
	 "ability_undying_strengthdrain"
	{
		"BaseClass"             		"ability_lua"
		"AbilityTextureName"			"undying_decay"
		"MaxLevel" 						"4"
		"LevelsBetweenUpgrades"		"0"
		"AbilityCooldown"				"5"
	
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_AOE | DOTA_ABILITY_BEHAVIOR_AUTOCAST"
				
		"ScriptFile"					"abilities/undying/ability_undying_strengthdrain"
		"precache"{
			"particle"	"particles/units/heroes/hero_undying/undying_decay.vpcf"
			"particle"	"particles/units/heroes/hero_undying/undying_decay_strength_buff.vpcf"
		}
		
		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"					"FIELD_INTEGER"
				"hunger_range"				"800"
			}			
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"hunger_aoe"		"500"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"weaken_str"		"25"
			}
			"04"
			{
				"var_type"				"FIELD_INTEGER"
				"weaken_duration"		"2"
			}
			"05"
			{
				"var_type"				"FIELD_INTEGER"
				"might_str"		"10"
			}
			"06"
			{
				"var_type"				"FIELD_INTEGER"
				"might_duration"			"3"
			}
			"07"
			{
				"var_type"				"FIELD_INTEGER"
				"burst_energy"			"150"
			}
			"08"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_modifier_duration"			"1"
			}
			"09"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_aoe"			"400"
			}
			"10"
			{
				"var_type"				"FIELD_INTEGER"
				"scar_payment"			"100"
			}
			"11"
			{
				"var_type"				"FIELD_INTEGER"
				"tooltip_points_spent"		"0 1 2 3"
			}
			"12"
			{
				"var_type"				"FIELD_INTEGER"
				"tooltip_range"		"80"
			}	
		}
	}
      
	 "ability_undying_lifesap"
	{
		"BaseClass"             		"ability_lua"
		"AbilityTextureName"			"undying_soul_rip"
		"MaxLevel" 						"4"
		"LevelsBetweenUpgrades"		"0"
		"AbilityCooldown"				"6"
		
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET | DOTA_ABILITY_BEHAVIOR_AUTOCAST"
		"AbilityUnitTargetTeam"			"DOTA_UNIT_TARGET_TEAM_FRIENDLY"
		"AbilityUnitTargetType"			"DOTA_UNIT_TARGET_HERO | DOTA_UNIT_TARGET_BASIC"
				
		"ScriptFile"					"abilities/undying/ability_undying_lifesap"
		"precache"
		{
			"particle"	"particles/units/heroes/hero_undying/undying_soul_rip_heal.vpcf"
		}
		
		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{	
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"burst_energy"		"120"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"burst_damage"		"150"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"burst_healing"		"30"
			}
			"04"
			{
				"var_type"					"FIELD_INTEGER"
				"burst_aoe"				"400"
			}			
			"05"
			{
				"var_type"				"FIELD_INTEGER"
				"projectile_radius"				"100"
			}			
			"06"
			{
				"var_type"				"FIELD_INTEGER"
				"projectile_range"				"9999"
			}	
			"07"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_healing_perma"				"300"
			}	
			"08"
			{
				"var_type"				"FIELD_INTEGER"
				"scar_payment"				"100"
			}					
			"09"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_base_healing"				"100"
			}		
			"10"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_healing"				"15"
			}	
			"11"
			{
				"var_type"					"FIELD_INTEGER"
				"base_healing"				"150"
			}	
			"12"
			{
				"var_type"				"FIELD_INTEGER"
				"tooltip_points_spent"		"0 1 2 3"
			}
		}
	}
  
	 "ability_undying_fetter"
	{
		"BaseClass"             		"ability_lua"
		"AbilityTextureName"			"necrolyte_death_pulse"
		"MaxLevel" 						"4"
		"LevelsBetweenUpgrades"		"0"
		"AbilityCooldown"				"4"	
	
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_AOEz§ | DOTA_ABILITY_BEHAVIOR_AUTOCAST"
				
		"ScriptFile"					"abilities/undying/ability_undying_fetter"
		
		"precache"
		{
			"particle"	"particles/items_fx/ethereal_blade.vpcf"
			"particle"	"particles/status_fx/status_effect_wraithking_ghosts.vpcf"
			"particle"	"particles/units/heroes/hero_warlock/warlock_upheaval_debuff.vpcf"
		}		
		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"					"FIELD_INTEGER"
				"dash_range"				"1000"
			}			
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"corpse_butt"				"100"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"self_energy"			"60"
			}
			"04"
			{
				"var_type"				"FIELD_INTEGER"
				"helix_aoe"				"375"
			}
			"05"
			{
				"var_type"				"FIELD_INTEGER"
				"helix_damage"			"250"
			}
			"06"
			{
				"var_type"				"FIELD_INTEGER"
				"helix_energy"			"100"
			}
			"07"
			{
				"var_type"				"FIELD_INTEGER"
				"slow_strength"			"50"
			}
			"08"
			{
				"var_type"				"FIELD_INTEGER"
				"slow_duration"			"2"
			}
			"09"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_dash_range"		"300"
			}
			"10"
			{
				"var_type"				"FIELD_INTEGER"
				"slow_duration_long"		"2"
			}
			"11"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_root_duration"		"1"
			}
			"12"
			{
				"var_type"				"FIELD_INTEGER"
				"tooltip_points_spent"		"0 1 2 3"
			}
			"13"
			{
				"var_type"				"FIELD_INTEGER"
				"tooltip_range"		"30"
			}
		}
	}
  
	 "ability_undying_giant"
	{
		"BaseClass"             		"ability_lua"
		"AbilityTextureName"			"undying_flesh_golem"
		"MaxLevel" 						"4"
		"LevelsBetweenUpgrades"		"0"
		"AbilityCooldown"				"0"
		"AbilityManaCost"       "1000"
	
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET | DOTA_ABILITY_BEHAVIOR_AUTOCAST"
				
		"ScriptFile"					"abilities/undying/ability_undying_giant"
		"precache"
		{
			"particle"	"particles/units/heroes/hero_undying/undying_fg_transform.vpcf"
			"particle"	"particles/units/heroes/hero_undying/undying_fg_transform_reverse.vpcf"
			"model"		"models/heroes/undying/undying_flesh_golem.vmdl"
		}
		
		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"					"FIELD_INTEGER"
				"modifier_duration"				"10"
			}	
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"percent_conversion"			"40"
			}		
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"aura_aoe"				"900"
			}	
			"04"
			{
				"var_type"				"FIELD_INTEGER"
				"shield_block"		"100"
			}
			"05"
			{
				"var_type"				"FIELD_INTEGER"
				"initial_shield"		"200"
			}
			"06"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_healing_perma"		"500"
			}
			"07"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_ability_refresh"		"10"
			}	
			"08"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_energy_boost"		"50"
			}
			"09"
			{
				"var_type"				"FIELD_INTEGER"
				"tooltip_points_spent"		"0 1 2 3"
			}
		}
	}	
}