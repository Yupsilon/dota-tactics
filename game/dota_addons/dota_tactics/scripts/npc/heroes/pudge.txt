"DOTAAbilities"
{
	"ability_pudge_disember"
	{
		"BaseClass"             		"ability_lua"
		"AbilityTextureName"			"pudge_dismember"
		"MaxLevel" 						"4"
		"LevelsBetweenUpgrades"		"0"
		"AbilityCooldown"				"0"
	
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_AUTOCAST"
				
		"ScriptFile"					"abilities/pudge/ability_pudge_disember"
		
		"precache"
		{
			"particle"	"particles/units/heroes/hero_pudge/pudge_dismember.vpcf"
		}
		
		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{			
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"cleave_range"		"450"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"cleave_radius"		"200"
			}
			"03"
			{
				"var_type"					"FIELD_INTEGER"
				"cleave_damage_side"				"200"
			}	
			"04"
			{
				"var_type"				"FIELD_INTEGER"
				"cleave_damage_main"			"300"
			}
			"05"
			{
				"var_type"				"FIELD_INTEGER"
				"base_energy_regen"		"0"
			}
			"06"
			{
				"var_type"				"FIELD_INTEGER"
				"cleave_energy"		"100"
			}
			"07"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_damage_main"		"25"
			}
			"08"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_energy_absorbtion"		"20"
			}
			"09"
			{
				"var_type"				"FIELD_INTEGER"
				"tooltip_points_spent"		"0 1 2 3"
			}
		}
	}
  
	 "ability_pudge_hook"
	{
		"BaseClass"             		"ability_lua"
		"AbilityTextureName"			"pudge_meat_hook"
		"MaxLevel" 						"4"
		"LevelsBetweenUpgrades"		"0"
		"AbilityCooldown"				"2"
	
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_AUTOCAST"
				
		"ScriptFile"					"abilities/pudge/ability_pudge_hook"
		"precache"
		{
			"particle"	"particles/units/heroes/hero_pudge/pudge_meathook.vpcf"
		
		}
		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"					"FIELD_INTEGER"
				"projectile_damage"				"260"
			}			
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"projectile_range"		"1500"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"projectile_radius"		"200"
			}
			"04"
			{
				"var_type"				"FIELD_INTEGER"
				"projectile_energy"		"100"
			}
			"05"
			{
				"var_type"					"FIELD_INTEGER"
				"base_kb_delta"				"200"
			}	
			"06"
			{
				"var_type"					"FIELD_INTEGER"
				"bonus_slow_duration"				"2"
			}			
			"07"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_slow_strength"				"50"
			}
			"08"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_scar_damage"				"100"
			}
			"09"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_range"				"400"
			}
			"10"
			{
				"var_type"				"FIELD_INTEGER"
				"tooltip_points_spent"		"0 1 2 3"
			}
		}
	}
	  
	 "ability_pudge_pudgeling"
	{
		"BaseClass"             		"ability_lua"
		"AbilityTextureName"			"pudge_eject"
		"MaxLevel" 						"4"
		"LevelsBetweenUpgrades"		"0"
		"AbilityCooldown"				"3"
	
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_POINT | DOTA_ABILITY_BEHAVIOR_AOE | DOTA_ABILITY_BEHAVIOR_AUTOCAST"
				
		"ScriptFile"					"abilities/pudge/ability_pudge_pudgeling"
		
		"precache"
		{			
			"particle"  "particles/units/heroes/hero_pudge/pudge_rot.vpcf"
			"particle"  "particles/units/heroes/hero_pudge/pudge_rot_recipient.vpcf"
			"soundfile"	"soundevents/game_sounds_heroes/game_sounds_undying.vsndevts"
		}
		
		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{		
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"self_energy"		"60"
			}
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"rot_damage"		"180"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"cast_range"		"800"
			}	
			"04"
			{
				"var_type"				"FIELD_INTEGER"
				"pudgling_duration"		"1"
			}	
			"05"
			{
				"var_type"				"FIELD_INTEGER"
				"rot_aoe"				"175"
			}				
			"06"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_pudgling_duration"		"2"
			}	
			"07"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_slow_duration"		"2"
			}
			"08"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_slow_strength"		"50"
			}
			"09"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_snared_duration"		"1"
			}
			"10"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_cooldown"		"1"
			}
			"11"
			{
				"var_type"				"FIELD_INTEGER"
				"tooltip_points_spent"		"0 1 2 3"
			}
		}
	}
  
	 "ability_pudge_repair"
	{
		"BaseClass"             		"ability_lua"
		"AbilityTextureName"			"pudge_flesh_heap"
		"MaxLevel" 						"4"
		"LevelsBetweenUpgrades"		"0"
		"AbilityCooldown"				"7"
	
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET | DOTA_ABILITY_BEHAVIOR_AUTOCAST"
				
		"ScriptFile"					"abilities/pudge/ability_pudge_repair"
		
		"precache"
		{
			"particle"	"particles/econ/items/pudge/pudge_immortal_arm/pudge_immortal_arm_rot_recipient.vpcf"
		}		
		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"					"FIELD_INTEGER"
				"heal_value_lesser"				"100"
			}			
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"heal_value_greater"				"150"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"self_energy"		"50"
			}
			"04"
			{
				"var_type"				"FIELD_INTEGER"
				"heal_tresspass"		"650"
			}
			"05"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_unstoppable_duration"		"1"
			}
			"06"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_tresspass"		"900"
			}
			"07"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_shield"		"150"
			}
			"08"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_shield_duration"		"2"
			}
			
			"09"
			{
				"var_type"				"FIELD_INTEGER"
				"tooltip_points_spent"		"0 1 2 3"
			}
		}
	}
  
	 "ability_pudge_rot"
	{
		"BaseClass"             		"ability_lua"
		"AbilityTextureName"			"pudge_rot"
		"MaxLevel" 						"4"
		"LevelsBetweenUpgrades"			"0"
		"AbilityCooldown"				"3"
		"AbilityManaCost"				"1000"
	
		"AbilityBehavior"				"DOTA_ABILITY_BEHAVIOR_NO_TARGET | DOTA_ABILITY_BEHAVIOR_AUTOCAST"
				
		"ScriptFile"					"abilities/pudge/ability_pudge_rot"
		
		"precache"
		{
			"particle"  "particles/units/heroes/hero_pudge/pudge_rot.vpcf"
			"particle"  "particles/units/heroes/hero_pudge/pudge_rot_recipient.vpcf"
		}		
		// Special
		//-------------------------------------------------------------------------------------------------------------
		"AbilitySpecial"
		{
			"01"
			{
				"var_type"				"FIELD_INTEGER"
				"cloud_duration"			"2"
			}			
			"02"
			{
				"var_type"				"FIELD_INTEGER"
				"rot_aoe"					"600"
			}
			"03"
			{
				"var_type"				"FIELD_INTEGER"
				"rot_damage"				"200"
			}
			"04"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_unstoppable_duration"	"1"
			}
			"05"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_might"				"25"
			}
			"06"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_might_duration"		"2"
			}
			"07"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_self_heal"			"100"
			}
			"08"
			{
				"var_type"				"FIELD_INTEGER"
				"bonus_healing_total"			"500"
			}
			"09"
			{
				"var_type"				"FIELD_INTEGER"
				"tooltip_points_spent"		"0 1 2 3"
			}
		}
	}	
}