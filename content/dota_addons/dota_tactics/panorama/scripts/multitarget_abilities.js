'use strict';
var VECTOR_TARGET_VERSION = [0, 3, 0]; //version data

var VectorTarget = {} // public API

VectorTarget.SetFastClickDragMode = function(flag) {
    /* Enables fast click-drag mode, where releasing the mouse button will complete the cast. */
    VectorTarget.fastClickDragMode = flag;
};

VectorTarget.IsFastClickDragMode = function() {
    /* Checks whether or not we're in fast click-drag mode */
    return VectorTarget.fastClickDragMode;
};


(function() {
    //constants
    var UPDATE_RANGE_FINDER_RATE = 1/30; // rate in seconds to update range finder control points
    var INACTIVE_CANCEL_DELAY = 0.1 // number of seconds to wait before the UI senda the cancel order event (prevents some race conditons between client/server handling)
    //state variables
    var rangeFinderParticle;
    var eventKeys = { };
    var prevEventKeys = { };
    var inactiveTimer = Game.GetGameTime(); // amount of time that no ability has been active 
    
    GameEvents.Subscribe("vector_target_order_start", function(keys) {
        eventKeys = keys;
        keys.initialPosition = [keys.initialPosition.x, keys.initialPosition.y, keys.initialPosition.z];
        Abilities.ExecuteAbility(keys.abilId, keys.unitId, false);
    });
    
    function updateRangeFinder() {
        var activeAbil = Abilities.GetLocalPlayerActiveAbility();
       
        //if(rangeFinderParticle) {
            if(eventKeys.abilId == activeAbil) {
                var pos = GameUI.GetScreenWorldPosition(GameUI.GetCursorPosition());
                if(pos != null) {
                    var start = eventKeys.initialPosition;
                    var keys = { }
                    //if (pos[0] != start[0] || pos[1] != start[1] || pos[2] != start[2]) {
                        eventKeys.currentPosition = pos;
                        //keys.midpoint = vMidPoint(start, pos);
                        //keys.maxdistancedelta = mapToControlPoints(keys, true);
                    //}
                }
            }
        //}
        if(eventKeys.abilId != null && activeAbil === -1) {
            var now = Game.GetGameTime();
            inactiveTimer = inactiveTimer || now;
            if (now - inactiveTimer >= INACTIVE_CANCEL_DELAY ) {
                cancelVectorTargetOrder()
            }
        }
        else {
            inactiveTimer = null;
        }
        $.Schedule(UPDATE_RANGE_FINDER_RATE, updateRangeFinder);
    }
    updateRangeFinder();
    
    function cancelVectorTargetOrder() {
        if(eventKeys.abilId === undefined) return;
        GameEvents.SendCustomGameEventToServer("vector_target_order_cancel", eventKeys);
        finalize();
    }
        
    function finalize() {
		eventKeys.finalized=true
        prevEventKeys = eventKeys;
        /*
        if(Abilities.GetLocalPlayerActiveAbility() == eventKeys.abilId) {
            $.Msg("re-execute");
            Abilities.ExecuteAbility(eventKeys.abilId, eventKeys.unitId, false);
        }
        */
        eventKeys = { };
    }
    
    GameEvents.Subscribe("vector_target_order_cancel", function(keys) {
        //$.Msg("canceling");
        if(keys.seqNum === eventKeys.seqNum && keys.abilId === eventKeys.abilId && keys.unitId === eventKeys.unitId) {
            finalize();
        }
    });
    
    GameEvents.Subscribe("vector_target_order_finish", function(keys) {
        //$.Msg("finished")
        if(keys.seqNum === eventKeys.seqNum && keys.abilId === eventKeys.abilId && keys.unitId === eventKeys.unitId) {
            finalize();
        }
    });

    GameEvents.Subscribe("dota_update_selected_unit", function(keys) {
        var selection = Players.GetSelectedEntities(Game.GetLocalPlayerID());
        //$.Msg("update selected unit")
        if(selection[0] !== eventKeys.unitId) {
            cancelVectorTargetOrder();
        }
    });
    
    GameEvents.Subscribe("dota_hud_error_message", function(keys) {
        if(keys.reason == 105) { // reason code for a full order queue
            GameEvents.SendCustomGameEventToServer("vector_target_queue_full", prevEventKeys);
        }
    });
    
    //fast click-drag handling
    GameUI.SetMouseCallback(function(eventName, button) {
        if (eventKeys.abilId && VectorTarget.IsFastClickDragMode() && eventName == "released" && button == 0) {
            Abilities.ExecuteAbility(eventKeys.abilId, eventKeys.unitId, true);
        }
    });
    
    //VectorTarget.SetFastClickDragMode(true);

    /* functional programming helpers */

    function zipWith(f, a, b) {
        return a.map(function(x, i) { return f(x, b[i]); });
    }

    /* vector math */

    function vMidPoint(a, b) {
        return zipWith(function(a,b) { return (a + b) / 2; }, a, b)
        //return [ (a[0] + b[0])/2, (a[1] + b[1])/2, (a[2] + b[2])/2 ];
    }

    function vLength(v) {
        return Math.sqrt(v.reduce(function(a,b) { return a + Math.pow(b, 2); }, 0));
        //return Math.sqrt(Math.pow(v[0], 2), Math.pow(v[1], 2), Math.pow(v[2], 2));
    }

    function vNormalize(v) {
        var d = vLength(v);
        return vScalarDiv(v, d);
        //return [v[0] / d, v[1] / d, v[2] / d];
    }

    function vScalarMul(v, s) {
        return v.map(function(x) { return x * s; });
        //return [v[0] * s, v[1] * s, v[2] * s];
    }

    function vScalarDiv(v, s) {
        return v.map(function(x) { return x / s; });
    }

    function vDiff(a, b) {
        return zipWith(function(a,b){return a-b;}, a, b);
    }
    
})();

$.Msg("vector_target.js loaded");